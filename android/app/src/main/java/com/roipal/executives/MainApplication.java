package com.roipal.executives;

import android.app.Application;

import com.shahenlibrary.RNVideoProcessingPackage;
import com.facebook.react.ReactApplication;
import com.mybigday.rnmediameta.RNMediaMetaPackage;
import com.imagepicker.ImagePickerPackage;
import me.hauvo.thumbnail.RNThumbnailPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import org.devio.rn.splashscreen.SplashScreenReactPackage;
import kim.taegon.rnintl.ReactNativeIntlPackage;
import br.com.dopaminamob.gpsstate.GPSStatePackage;
import com.BV.LinearGradient.LinearGradientPackage;
import com.reactnative.ivpusic.imagepicker.PickerPackage;
import io.invertase.firebase.RNFirebasePackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.airbnb.android.react.maps.MapsPackage;

import java.util.Arrays;
import java.util.List;

import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;
import com.brentvatne.react.ReactVideoPackage;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNMediaMetaPackage(),
            new ImagePickerPackage(),
            new RNThumbnailPackage(),
            new VectorIconsPackage(),
            new SplashScreenReactPackage(),
            new ReactNativeIntlPackage(),
            new GPSStatePackage(),
            new LinearGradientPackage(),
            new PickerPackage(),
            new RNFirebasePackage(),
            new RNGestureHandlerPackage(),
            new MapsPackage(),
            new RNFirebaseMessagingPackage(),
            new RNFirebaseNotificationsPackage(),
            new ReactVideoPackage(),
            new RNVideoProcessingPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
