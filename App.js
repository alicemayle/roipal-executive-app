import React, { Component } from 'react';
import { AsyncStorage, Platform, YellowBox, NativeModules } from 'react-native';
import { Root, StyleProvider } from 'native-base';
import { Provider } from 'unstated';
import Validator from 'validatorjs';
import en from 'validatorjs/src/lang/es';
import { Logger, Manager } from 'unstated-enhancers';
import { Persist } from 'unstated-enhancers'
import firebase from 'react-native-firebase';
import { createAppContainer } from 'react-navigation';
import { AppNavigator } from './src/routers/AppNavigator';
import NavigationService from './src/shared/services/NavigationService';
import NotificationEvent from './src/shared/NotificationEvent';
import Observer from './src/shared/Observer';
import InvitationAcceptanceContainer from './src/screens/invitations/containers/InvitationAcceptanceContainer';
import HomeContainer from './src/screens/home/containers/HomeContainer';
import getTheme from './native-base-theme/components';
import custom from './native-base-theme/variables/custom';
import SigninContainer from './src/screens/auth/signin/containers/SigninContainer';
import ChecklistContainer from './src/screens/checklist/containers/ChecklistContainer';
import SplashScreen from 'react-native-splash-screen'
import AuthContainer from './src/screens/auth/containers/AuthContainer';
import LogoutContainer from './src/screens/auth/containers/LogoutContainer';
import en_moment from 'moment/locale/en-SG';
import moment from 'moment';
import GPSState from 'react-native-gps-state';
import Firebase from './src/shared/support/Firebase';
import verificationContainer from './src/screens/profile/containers/VerificationContainer'
const GPSStateNative = NativeModules.GPSState
const isIOS = Platform.OS === 'ios';

const AppContainer = createAppContainer(AppNavigator);

GPSState.openAppDetails = ()=>{
  if(isIOS){
      GPSStateNative.openSettings()
  }else{
      GPSStateNative.openSettings(true)
  }
};

GPSState.openLocationSettings = ()=>{
  if(isIOS){
      GPSStateNative.openSettings()
  }else{
      GPSStateNative.openSettings(false)
  }
};

/**
 * Ignore socket io warning using websocket as transport
 */
console.ignoredYellowBox = ['Remote debugger'];

YellowBox.ignoreWarnings([
    'Unrecognized WebSocket connection option(s) `agent`, `perMessageDeflate`, `pfx`, `key`, `passphrase`, `cert`, `ca`, `ciphers`, `rejectUnauthorized`. Did you mean to put these under `headers`?'
]);

const options = {
  key: 'persist::roipal-seller-app',
  version: 1.0,
  storage: AsyncStorage,
  debounce: 250
}

Persist.start(options);
Manager.run();

Validator.setMessages('en', en);
Logger.start();
moment.locale('en', en_moment);


let renderComponent = {};

console.count = (component) => {
  if (!renderComponent.hasOwnProperty(component)) {
    renderComponent[component] = 1;
  } else {
    renderComponent[component] += 1;
  }
  console.log(`${component}: ${renderComponent[component]}`);
};

const inject = [
  new InvitationAcceptanceContainer(),
  new HomeContainer(),
  new SigninContainer(),
  new ChecklistContainer(),
  new AuthContainer(),
  new LogoutContainer(),
  new verificationContainer()
]

class App extends Component {

  async componentDidMount() {

    if (Platform.OS === 'ios') {
      SplashScreen.hide();
    }

    NotificationEvent.subscribe();

    Firebase.checkPermission();
    const channel = new firebase.notifications.Android.Channel(
      'roipalExecutiveChannelId',
      'Channel Roipal Executive',
      firebase.notifications.Android.Importance.Max
    ).setDescription('Channel para recibir notificaciones de Roipal Executive');
    firebase.notifications().android.createChannel(channel);

    // the listener returns a function you can use to unsubscribe
    this.unsubscribeFromNotificationListener = firebase.notifications().onNotification((notification) => {
      if (Platform.OS === 'android') {

        const localNotification = new firebase.notifications.Notification({
          sound: 'default',
          show_in_foreground: true,
        })
          .setNotificationId(notification.notificationId)
          .setTitle(notification.title)
          .setSubtitle(notification.subtitle)
          .setBody(notification.body)
          .setData(notification.data)
          .android.setChannelId('roipalExecutiveChannelId') // e.g. the id you chose above
          //.android.setSmallIcon('ic_stat_notification') // create this icon in Android Studio
          .android.setColor('#000000') // you can set a color here
          .android.setPriority(firebase.notifications.Android.Priority.High);

        firebase.notifications()
          .displayNotification(localNotification)
          .catch(err => console.error(err));
      } else if (Platform.OS === 'ios') {

        const localNotification = new firebase.notifications.Notification()
          .setNotificationId(notification.notificationId)
          .setTitle(notification.title)
          .setSubtitle(notification.subtitle)
          .setBody(notification.body)
          .setData(notification.data)
          .ios.setBadge(notification.ios.badge);

        firebase.notifications()
          .displayNotification(localNotification)
          .catch(err => console.error(err));
      }
    });

    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
      const notification = notificationOpen.notification;
      const { action}  = notification.data;

       Observer.notify(action, notification.data);
       Logger.dispatch('Entró al onNotificationOpened', notificationOpen.notification.data)
      firebase.notifications().removeDeliveredNotification(notificationOpen.notification.notificationId)
    });

  }

  componentWillUnmount() {
    // this is where you unsubscribe
    NotificationEvent.unsubscribe();
    this.unsubscribeFromNotificationListener();
    this.notificationOpenedListener();
  }

  render() {
    return (
      <Provider {...this.props} inject={ inject }>
        <StyleProvider style={getTheme(custom)}>
        <Root>
            <AppContainer
              ref={navigatorRef => {
                NavigationService.setTopLevelNavigator(navigatorRef);
              }}
            />
        </Root>
        </StyleProvider>
      </Provider>
    );
  }
}
export default App;