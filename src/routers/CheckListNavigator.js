import { createStackNavigator } from 'react-navigation';
import ChecklistScreen from '../screens/checklist/ChecklistScreen';
import VerificationScreen from '../screens/auth/registration/VerificationScreen';
import { ProfileNavigator } from './ProfileNavigator';
import { PaymentNavigator } from './PaymentNavigator'

import {
  StarDisc,
  Evaluation,
  EvaluationCompany
} from '../components/HOC';


const CheckListNavigator = createStackNavigator({
  ChecklistScreen: ChecklistScreen,
  VerificationScreen: VerificationScreen,
  ProfileNavigator: ProfileNavigator,
  PaymentNavigator: PaymentNavigator,
  StarDiscScreen: StarDisc,
  EvaluationScreen: Evaluation,
  EvaluationCompanyScreen: EvaluationCompany
}, {
    headerMode: 'none',
  });

export { CheckListNavigator }
