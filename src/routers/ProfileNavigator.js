import { createStackNavigator } from 'react-navigation';
import VideoProfileScreen from '../screens/profile/VideoProfileScreen';

import {
  InfoProfile,
  MapHoc
} from '../components/HOC';


const ProfileNavigator = createStackNavigator({
    InfoProfileScreen: InfoProfile,
    MapScreen: MapHoc,
    VideoProfileScreen: VideoProfileScreen,
  }, {
    headerMode: 'none',
  });

export {
  ProfileNavigator
}