import { createStackNavigator } from 'react-navigation';
import { AppDrawerNavigator } from './AppDrawerNavigator';
import { InvitationNavigator } from "./InvitationNavigator";
//import { ComunicationNavigator } from "./ComunicationNavigator";
import { MissionsNavigator } from './MissionsNavigator';
import { AccountNavigator } from './AccountNatigator'
import ChatScreen from '../screens/chat/ChatScreen';
import BlockLocationScreen from '../screens/activities/BlockLocationScreen';


export const AuthNavigator = createStackNavigator({
  AppDrawerNavigator: AppDrawerNavigator,
  MissionsNavigator: MissionsNavigator,
  AccountNavigator: AccountNavigator,
  InvitationNavigator: InvitationNavigator,
  //ComunicationNavigator: ComunicationNavigator,
  ChatScreen: ChatScreen,
  BlockLocationScreen: BlockLocationScreen
}, {
  headerMode: 'none',
});
