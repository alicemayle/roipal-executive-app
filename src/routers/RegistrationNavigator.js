import { createStackNavigator } from 'react-navigation';
import PromissoryNoteScreen from '../screens/auth/registration/PromissoryNoteScreen';

import {
    Registration
  } from '../components/HOC';

const RegistrationNavigator = createStackNavigator({
    RegistrationScreen: Registration,
    PromissoryNoteScreen: PromissoryNoteScreen,
  }, {
      headerMode: 'none',
    });

export { RegistrationNavigator }
