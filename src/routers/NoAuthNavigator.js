import { createStackNavigator } from 'react-navigation';
import SigninScreen from '../screens/auth/signin/SigninScreen';
import { RegistrationNavigator } from './RegistrationNavigator';

import {
  ResetPassword
} from '../components/HOC';

export const NoAuthNavigator = createStackNavigator({
  SigninScreen: SigninScreen,
  RegistrationNavigator: RegistrationNavigator,
  ResetPasswordScreen: ResetPassword
}, {
  headerMode: 'none',
});
