import createStackNavigator from '../components/createStackNavigator';
import ChatScreen from '../screens/chat/ChatScreen';
//import VideoCallScreen from '../screens/videocall/VideoCallScreen';
import BlankScreen from '../screens/BlankScreen';

const ComunicationNavigator = createStackNavigator({
    BlankScreenComunicationNavigator: BlankScreen,
    ChatScreen: ChatScreen,
    //VideoCallScreen: VideoCallScreen //sin funcionalidad hasta el momento
  }, {
    headerMode: 'none',
  });

export { ComunicationNavigator }
