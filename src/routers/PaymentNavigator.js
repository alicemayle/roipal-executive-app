import { createStackNavigator } from 'react-navigation';
import TermsConditionsPaymentScreen from '../screens/payment/TermsConditionsPaymentScreen';

import {
  RegistrationPayment
} from '../components/HOC';

const PaymentNavigator = createStackNavigator({
  RegistrationPaymentScreen: RegistrationPayment,
  TermsConditionsPaymentScreen: TermsConditionsPaymentScreen
}, {
    headerMode: 'none',
  });

export { PaymentNavigator }
