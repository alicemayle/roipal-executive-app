import { createStackNavigator } from 'react-navigation';
import VideoEditProfileScreen from '../screens/account/VideoEditProfileScreen';
import EditPasswordScreen from '../screens/account/EditPasswordScreen';
import InfoPaymentScreen from '../screens/account/InfoPaymentScreen';
import NotificationsOptionsScreen from '../screens/account/NotificationsOptionsScreen';
import { PaymentNavigator } from './PaymentNavigator';
import DisableAccountScreen from '../screens/account/DisableAccountScreen';
import MessageValidationScreen from '../screens/profile/MessageValidationScreen';
import WalletScreen from '../screens/account/WalletScreen';
import DetailsBalance from '../screens/account/DetailsBalance'

import {
  EditAccount,
  EditInfoProfile,
  MapScreenEditProfile,
  VerificationDocumentsHoc,
} from '../components/HOC';

export const AccountNavigator = createStackNavigator({
    EditAccountScreen: EditAccount,
    EditInfoProfileScreen: EditInfoProfile,
    MapScreenEditProfileScreen: MapScreenEditProfile,
    VideoEditProfileScreen: VideoEditProfileScreen,
    EditPasswordScreen: EditPasswordScreen,
    InfoPaymentScreen: InfoPaymentScreen,
    PaymentNavigator: PaymentNavigator,
    NotificationsOptionsScreen: NotificationsOptionsScreen,
    VerificationDocuments: VerificationDocumentsHoc,
    MessageValidationScreen: MessageValidationScreen,
    DisableAccountScreen: DisableAccountScreen,
    WalletScreen: WalletScreen,
    DetailsBalance: DetailsBalance
  }, {
      headerMode: 'none',
    });

