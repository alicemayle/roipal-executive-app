import { createStackNavigator } from 'react-navigation';
import InvitationsListScreen from '../screens/invitations/InvitationsListScreen'
import InvitationDetailScreen from '../screens/invitations/InvitationDetailScreen'
import InvitationDetailCompanyScreen from '../screens/invitations/InvitationDetailCompanyScreen';
import VideoInvitationScreen from '../screens/invitations/VideoInvitationScreen';
import MapViewScreen from '../screens/MapViewScreen';
import MessageValidationScreen from '../screens/profile/MessageValidationScreen';

import {
    CompanyAssessment,
    VerificationDocumentsHoc
  } from '../components/HOC';

const InvitationNavigator = createStackNavigator({
    InvitationsListScreen: InvitationsListScreen,
    InvitationDetailScreen: InvitationDetailScreen,
    InvitationDetailCompanyScreen: InvitationDetailCompanyScreen,
    VideoInvitationScreen: VideoInvitationScreen,
    CompanyAssessmentScreen: CompanyAssessment,
    MapViewScreen: MapViewScreen,
    VerificationDocuments: VerificationDocumentsHoc,
    MessageValidationScreen: MessageValidationScreen
  }, {
      headerMode: 'none'
    });

export { InvitationNavigator }
