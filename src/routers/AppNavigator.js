import { createSwitchNavigator } from 'react-navigation';
import AuthLoadingScreen from '../screens/auth/AuthLoadingScreen';
import { NoAuthNavigator } from './NoAuthNavigator';
import { AuthSwitchNavigator } from './AuthSwitchNavigator';
import Introduction from '../screens/auth/signin/Introduction';

export const AppNavigator = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,   
    Introduction: Introduction,
    NoAuthNavigator: NoAuthNavigator,
    AuthSwitchNavigator: AuthSwitchNavigator,
  },
  {
    initialRouteName: 'AuthLoading',
  }
);