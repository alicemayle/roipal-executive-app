import { createStackNavigator } from 'react-navigation';
import ActivityDetailsScreen from '../screens/activities/ActivityDetailsScreen';
import MapViewScreen from '../screens/MapViewScreen';

import {
    ActivityRecord,
    MissionDetails
  } from '../components/HOC';

const MissionsNavigator = createStackNavigator({
    MissionDetailsScreen: MissionDetails,
    ActivityDetailsScreen: ActivityDetailsScreen,
    ActivityRecordScreen: ActivityRecord,
    MapViewScreen: MapViewScreen
  }, {
      headerMode: 'none'
    });

export { MissionsNavigator }
