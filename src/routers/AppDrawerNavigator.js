import { createDrawerNavigator } from 'react-navigation';
import SideBar from '../components/sidebar/SideBar';
import HomeScreen from "../screens/home/HomeScreen";

export const AppDrawerNavigator = createDrawerNavigator({
  HomeScreen: HomeScreen,
}, {
  headerMode: 'none',
  contentComponent: SideBar,
  drawerPosition: "right"
});