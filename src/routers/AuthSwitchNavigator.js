import { createSwitchNavigator } from 'react-navigation';
import { CheckListNavigator } from './CheckListNavigator';
import { AuthNavigator } from './AuthNavigator';
import WelcomeScreen from '../screens/checklist/WelcomeCommunityScreen';

export const AuthSwitchNavigator = createSwitchNavigator(
  {
    AuthNavigator: AuthNavigator,
    WelcomeScreen: WelcomeScreen,
    CheckListNavigator: CheckListNavigator,
  },
  {
    initialRouteName: 'AuthNavigator',
  }
);
