import React, { Component } from 'react';
import { View, Text, Dimensions, Modal, Platform, SafeAreaView } from 'react-native';
import styles from '../../theme/globalStyles';
import StarDisc from './components/StarDisc';
import { Button, Content, Header, Icon, Body, Container, Card, CardItem } from 'native-base';
import Lang from '../../shared/languages/Lang';
import autobind from 'class-autobind';
import discAssessment from './containers/DiscContainer';
import { connect, Logger } from 'unstated-enhancers';
import ImageViewer from 'react-native-image-zoom-viewer';
import { FlatList } from 'react-native-gesture-handler';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import LinearGradient from 'react-native-linear-gradient';

const { height, width } = Dimensions.get('window');

class StarDiscScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalVisible: false,
      images: []
    };

    autobind(this)
  }

  setModalVisible() {
    const inverse = !this.state.modalVisible;
    this.setState({
      modalVisible: inverse,
    });
  }

  onPressLeftButton() {
    this.props.navigation.navigate('ChecklistScreen');
  }

  keyExtractor(index) {
    return index.toString();
  }

  nextStep() {

    const { discAssessment } = this.props.containers;

    discAssessment.resetState();

    this.props.navigation.navigate('ChecklistScreen');
  }

  renderDescription(item) {
    return (
      <View style={{ flexDirection: 'row', width: '90%' }}>
        <Icon name="arrow-dropright-circle" style={styles.iconCircle2} />
        <Text style={{ textAlign: 'justify', width: '90%' }}>
          {item.item}
          {'\n'}
        </Text>
      </View>
    );
  }

  renderDescriptionBig(item) {
    const description = item.item.split(':');
    return (
      <View style={{ flexDirection: 'row', width: '90%' }}>
        <Icon name="arrow-dropright-circle" style={[styles.iconCircle2]} />
        <Text style={{ width: '90%', textAlign: 'justify' }}>
          <Text style={{fontWeight: 'bold', color: '#394a4d'}}>{description[0]}:</Text>
          <Text>{description[1]}{'\n'}</Text>
        </Text>
      </View>
    );
  }

  render() {
    const { starDisc, description = {}, resultDisc } = this.props.containers.discAssessment.state;
    const { profile } = resultDisc.assessment.description;

    return (
      <Container style={styles.container}>
        <NavBarComponent  {...this.props.navigation} onPressLeftButton={this.onPressLeftButton}
          name={Lang.get('title.star', 'evaluation.title')} />

        <LinearGradient
          colors={['#00a7e5', '#5bc1be']}
          start={{ x: 0, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={{
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <Card style={{ borderRadius: 10, width: '95%', marginBottom: 15 }}>
            <View style={{ padding: 20 }}>
              <Text style={{ fontWeight: 'bold', fontSize: 18, textAlign: 'center', marginBottom: 10 }}>{Lang.get('assessment.completeFirstStage', 'evaluation.complete_first')}</Text>
              <Text style={{ textAlign: 'center' }}>{`${Lang.get('assessment.discProfileLabel', 'evaluation.disc_profile')}:`}</Text>
              <Text style={{ fontWeight: 'bold', fontSize: 18, textAlign: 'center' }}>{profile || 'Profile'}</Text>
            </View>
          </Card>
        </LinearGradient>

        <Content padder bounces={false}>
          {starDisc && <StarDisc style={{ width: width * 0.85, height: width * 0.85 }} image={starDisc} onModal={this.setModalVisible} />}
          <Text style={{ textAlign: 'justify', color: '#0285c9', fontWeight: 'bold', marginLeft: 8 }}> {description.big_trends} </Text>
          <Card style={{ borderRadius: 10, marginLeft: 8, marginRight: 8, marginBottom: 15 }}>
            <CardItem style={[{ borderRadius: 10 }]}>
              <FlatList
                data={description.big_trends_steps}
                keyExtractor={this.keyExtractor}
                renderItem={this.renderDescriptionBig}
              />
            </CardItem>
          </Card>

          <Text style={{ textAlign: 'justify', color: '#0285c9', paddingTop: 15, fontWeight: 'bold', marginLeft: 8 }}> {description.improvement_areas} </Text>
          <Card style={{ borderRadius: 10, marginLeft: 8, marginRight: 8, marginBottom: 15 }}>
            <CardItem style={{ borderRadius: 10 }}>
              <FlatList
                data={description.improvement_areas_steps}
                keyExtractor={this.keyExtractor}
                renderItem={this.renderDescription}
              />
            </CardItem>
          </Card>

          <Text style={{ textAlign: 'justify', color: '#0285c9', paddingTop: 15, fontWeight: 'bold', marginLeft: 8 }}> {description.strongholds} </Text>
          <Card style={{ borderRadius: 10, marginLeft: 8, marginRight: 8, marginBottom: 15 }}>
            <CardItem style={{ borderRadius: 10 }}>
              <FlatList
                data={description.strongholds_steps}
                keyExtractor={this.keyExtractor}
                renderItem={this.renderDescription}
              />
            </CardItem>
          </Card>

          <Button block onPress={this.nextStep} style={{ marginTop: 15, marginBottom: Platform.OS === 'ios' ? 15 : 0 }}>
            <Text style={{ color: 'white' }}>{Lang.get('button.next', 'generic.button.next')}</Text>
          </Button>
          <SafeAreaView />
        </Content>

        <Modal
          visible={this.state.modalVisible}
          onRequestClose={this.setModalVisible}
          transparent={false}>
          <Header style={{ backgroundColor: Platform.OS === 'android' ? 'black' : 'white' }}>
            <Button
              transparent
              onPress={this.setModalVisible}
            >
              <Icon name={'arrow-back'} style={{ color: Platform.OS === 'android' ? 'white' : 'black' }} />
            </Button>
            <Body style={{ alignItems: 'center' }}>
            </Body>
            <View style={{ width: 70, justifyContent: 'center', alignItems: 'center' }}>
            </View>
          </Header>

          <ImageViewer onModal={this.setModalVisible} backgroundColor={Platform.OS === 'android' ? 'black' : 'white'} imageUrls={[
            {
              url: starDisc
            }
          ]} />
        </Modal>

      </Container>
    );
  }
}

const containers = {
  discAssessment: discAssessment,
}

const mapStateToProps = (containers) => {
  return {
    disc: containers.discAssessment.state
  };
}

export default connect(containers, mapStateToProps)(StarDiscScreen);
