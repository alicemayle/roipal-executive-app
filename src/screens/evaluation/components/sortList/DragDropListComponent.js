import React, { Component } from 'react';
import { View, Text, Title } from 'native-base';
import SortableListView from 'react-native-sortable-listview';
import { TouchableHighlight } from 'react-native';

class RowComponent extends Component {
  render() {
    return (
      <TouchableHighlight
        underlayColor={'#eee'}
        style={{
          padding: 25,
          backgroundColor: '#F8F8F8',
          borderBottomWidth: 1,
          borderColor: '#eee',
        }}
        {...this.props.sortHandlers}
      >
        <Text>{this.props.data}</Text>
      </TouchableHighlight>
    )
  }
}

class DragDropListComponent extends Component {
  constructor(props) {
    super(props);
    const { data } = this.props;
    this.order = Object.keys(data) //Array of keys
    this._bind('sort');
  }

  _bind(...methods) {
    methods.forEach((method) => {
      this[method] = this[method].bind(this);
    });
  }

  sort(e) {
    const { onSendAnswer } = this.props;
    this.order.splice(e.to, 0, this.order.splice(e.from, 1)[0])
    onSendAnswer(this.order);
    this.forceUpdate();
  }

  render() {
    const { data, title } = this.props;
    return (
      <View style={{ flex: 1 }}>
        <Title style={{ color: 'black', marginVertical: 20 }}>{`${title}`}</Title>
        <SortableListView
          style={{ flex: 1 }}
          data={data}
          order={this.order}
          onRowMoved={this.sort}
          renderRow={row => <RowComponent data={row} />}
        />
      </View>
    )
  }
}

export default DragDropListComponent;



