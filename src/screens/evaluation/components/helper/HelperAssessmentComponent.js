import React, { Component } from 'react';
import { View, Text, Button, H3, Content, Title } from 'native-base';
import { Modal, Image } from 'react-native';
import { connect } from 'unstated-enhancers';

import lang from '../../../../shared/languages/Lang';
import language from '../../../../components/language/containers/LanguagesContainer';
import { helperEvaluation, titleHelperEvaluation } from '../../sources/HelpMessages';
import styles from '../../../../theme/globalStyles';
import logo from '../../../../images/logo_colores/logo_colores.png';
import NavBarComponent from '../../../../components/navBar/NavBarComponent';
import autobind from 'class-autobind';

class HelperAssessmentComponent extends Component {
  constructor(props) {
    super(props)

    autobind(this);

  }

  closed() { }

  render() {
    const { padding20 } = styles;

    const { onHandleSetModalVisible, modalVisible } = this.props;

    return (
      <Modal
        animationType="slide"
        visible={modalVisible}
        presentationStyle={'fullScreen'}
        onRequestClose={this.closed}
      >

        <NavBarComponent
          {...this.props} name={lang.get('title.assessment', 'evaluation.assessment')}
        />

        <Content
          bounces={false}
          padder
          contentContainerStyle={{ paddingBottom: 20, alignItems: 'center', justifyContent: 'space-between' }}>
          <View
            style={{ alignItems: 'center', justifyContent: 'center' }}>
            <View style={{ flexDirection: 'row', justifyContent: 'center', paddingTop: 10 }}>
              <Image style={styles.logo} source={logo} />
            </View>
            <Title style={{paddingTop: 25}}>{lang.get('evaluation.behavior', 'evaluation.behavior')}</Title>
            <Text style={[padding20, {textAlign: 'justify'}]}>{lang.get('evaluation.helperEvaluation', 'evaluation.helper_evaluation')}</Text>
          </View>
        </Content>
        <Button
          style={{ margin: 25 }}
          block
          onPress={onHandleSetModalVisible}>
          <Text>{lang.get('button.understood', 'evaluation.button.understood')}</Text>
        </Button>
      </Modal>
    );
  }
}

export default connect({
  language
})(HelperAssessmentComponent);