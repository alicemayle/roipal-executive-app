import React, { Component } from 'react';
import { View, Text, Card, CardItem } from 'native-base';
import { Image, TouchableHighlight } from 'react-native'

class StarDisc extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  shouldComponentUpdate() {
    return false;
  }

  render() {
    const { image, onModal } = this.props

    return (
      <View style={{ paddingTop: 8, paddingBottom: 30, justifyContent: 'center', alignItems: 'center' }}>
        <TouchableHighlight onPress={onModal}>
          <Card style={{borderRadius: 10}}>
            <CardItem style={{borderRadius: 10}}>
              <Image source={{ uri: image }} {...this.props} />
            </CardItem>
          </Card>
        </TouchableHighlight>
      </View>
    );
  }
}

export default StarDisc;
