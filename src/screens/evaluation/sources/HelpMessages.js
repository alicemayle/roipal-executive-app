const titleHelperEvaluation = 'Evalución de Comportamiento'

const helperEvaluation = `
Hola, nos interesa conocer tu estilo de Comportamiento, del cual derivan tu estilo de comunicación y el cómo pones en práctica tu experiencia y conocimientos. Nadie sale mal o bien, solo sales como eres y nos ayudas a ofrecerte misiones relacionas con tu perfil.

Te pedimos 10 minutos de concentración:

Verás en cada sección 4 enunciados, cada uno representa un comportamiento específico. Haciendo click y arrastrando cada enunciado, muévalos verticalmente y deles un orden de tal forma que queden de aquello que más lo describa, hasta arriba, a lo que menos lo describe hasta abajo.

¡Gracias y disfrute el ejercicio!`

export {
  helperEvaluation, titleHelperEvaluation
}