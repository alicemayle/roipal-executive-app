import Auth from "../../../shared/services/Auth";
import http from "../../../shared/Http";
import qs from 'qs';

class AssessmentServices {

   getDISCAssessment() {
    return http.get('api/assestments/disc?includes=questions');
}

sendDISCAssessment(data) {
  return http.post(`api/executives/${Auth.getUser().uuid}/assestments/disc`, data);
}

getStarDisc() {
  return http.get(`/api/executives/${Auth.getUser().uuid}/roipal-star`)
}

}
export default new AssessmentServices;
