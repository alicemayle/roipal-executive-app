import React, { Component } from 'react';
import { Button, Spinner, Container, Title } from 'native-base';
import { connect } from 'unstated-enhancers';
import autobind from 'class-autobind';
import lang from '../../shared/languages/Lang';
import checklist from '../../screens/checklist/containers/ChecklistContainer';
import HelperAssessmentComponent from './components/helper/HelperAssessmentComponent';
import { StyleSheet, Text, View, Dimensions, Platform } from 'react-native';
import SortableList from 'react-native-sortable-list';
import RowQuestion from './RowQuestion';
import resultEvaluation from './containers/ResultsEvaluationContainer';
import discAssessment from './containers/DiscContainer';
import answersAssessment from './containers/AnswersDiscContainer';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import toast from '../../shared/support/ShowToast';

const window = Dimensions.get('window');

class EvaluationScreen extends Component {

  constructor(props) {
    super(props);

    this.state = { modalVisible: true }

    autobind(this);
  }

  changeOrder(data) {
    const { navigation } = this.props;
    const isEditing = navigation.getParam('isEditing');

    if (!isEditing) {
      navigation.setParams({
        isEditing: true
      });
    }

    const { discAssessment } = this.props.containers;
    const { currentQuestion } = discAssessment.state;
    this.newAnswer = {
      "number": currentQuestion.number,
      "answer": data.join(''),
    }
  }

  async getDISCAssessment() {
    const { discAssessment } = this.props.containers;
    const disc = await discAssessment.getDISCAssessment();
  }

  setAnswers(data) {
    try {
      const { discAssessment } = this.props.containers;
      const { currentQuestion } = discAssessment.state;
      const newAnswer = {
        "number": currentQuestion.number,
        "answer": data.join(''),
      }
      discAssessment.setAnswer(newAnswer);
    } catch (error) {
      //TODO
    }
  }

  async setNextQuestion() {
    const { httpBusy } = this.props.containers.answersAssessment.state;
    if (httpBusy) return;

    const { discAssessment, answersAssessment } = this.props.containers;
    const { assessment, currentQuestion } = discAssessment.state;
    this.newAnswer = this.newAnswer === undefined ? {
      "number": currentQuestion.number,
      "answer": currentQuestion ? Object.keys(currentQuestion.options).join('') : undefined,
    } : this.newAnswer;

    await answersAssessment.concatAnswers(this.newAnswer);

    if (assessment.length === 0) {
      const data = {
        answers: answersAssessment.state.answersArray,
      }

      await answersAssessment.sendDISCAssessment(data);

      const { error } = answersAssessment.state;

      if (error !== undefined) {
        toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'));
      } else {
        const { checklist, resultEvaluation } = this.props.containers;

        await checklist.setStepAsCompleted('disc-assessment');

        await discAssessment.getStarDisc();

        resultEvaluation.setScore(data.score);

        //discAssessment.resetState();

        answersAssessment.resetState();

        this.props.navigation.navigate('StarDiscScreen');

        //this.props.navigation.navigate('ChecklistScreen');
      }
    } else {
      discAssessment.setNextQuestion();
      this.newAnswer = undefined;
    }
  }

  handleSetModalVisible() {
    const inverse = !this.state.modalVisible;
    this.setState({ modalVisible: inverse });
  }

  setNavigationGoBackConfirmationAlert() {
    this.props.navigation.setParams({
      alert: {
        title: lang.get('evaluation.alerTitle', 'evaluation.aler_title'),
        message: lang.get('evaluation.alertDirection', 'evaluation.alert_direction')
      }
    });
  }

  componentDidMount() {
    this.setNavigationGoBackConfirmationAlert();

    setTimeout(() => this.getDISCAssessment(), 0)
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.currentQuestion !== this.props.currentQuestion ||
      nextProps.saveDisc !== this.props.saveDisc ||
      nextProps.requestDisc !== this.props.requestDisc ||
      nextState.modalVisible !== this.state.modalVisible
  }

  render() {
    const { currentQuestion } = this.props.containers.discAssessment.state;
    const { modalVisible } = this.state;

    return (
      <Container>
        <HelperAssessmentComponent
          {...this.props.navigation}
          modalVisible={this.state.modalVisible}
          onHandleSetModalVisible={this.handleSetModalVisible}
        />
        <NavBarComponent  {...this.props.navigation}
          name={lang.get('title.assessment', 'evaluation.title')} />
        {!modalVisible && currentQuestion !== undefined &&
          <View style={styles.container}>
            {/* <LinearGradient
              colors={['#00a7e5', '#5bc1be']}
              start={{ x: 0, y: 0 }}
              end={{ x: 0, y: 1 }}
              style={{
                width: '100%',
                alignItems: 'center',
                justifyContent: 'center',
                paddingHorizontal: 25,
                marginBottom: 10
              }}
            >
               <Text
                multiline
                style={{ color: 'white', textAlign: 'justify', marginVertical: 15, fontSize: 17 }}>
                {`${currentQuestion.question}`}
              </Text>
            </LinearGradient> */}
            <Title style={{marginTop: 15}}>{lang.get('evaluation.behavior', 'evaluation.behavior')}</Title>
            <Text
                multiline
                style={{ color: '#6E6E6E', textAlign: 'center', fontSize: 15, margin: 15 }}>
                {lang.get('evaluation.instructions', 'evaluation.instructions')}
              </Text>
            <SortableList
              style={styles.list}
              contentContainerStyle={styles.contentContainer}
              data={currentQuestion.options}
              renderRow={this._renderRow}
              onChangeOrder={this.changeOrder} />
            <Button
              style={{ margin: 25 }}
              block
              onPress={this.setNextQuestion}
            >
              <Text style={{ color: 'white' }}>{lang.get('button.next', 'generic.button.next')} </Text>
              {this.props.saveDisc && <Spinner color='lightblue' />}
            </Button>
          </View>
        }
        {
          !modalVisible && currentQuestion === undefined && this.props.requestDisc && <Spinner color='lightblue' />
        }
      </Container>
    );
  }

  _renderRow = ({ data, active }) => {
    return <RowQuestion data={data} active={active}/>
  }
}

const containers = {
  checklist: checklist,
  resultEvaluation: resultEvaluation,
  discAssessment: discAssessment,
  answersAssessment: answersAssessment,
}

const mapStateToProps = (containers) => {
  return {
    requestDisc: containers.discAssessment.state.httpBusy,
    saveDisc: containers.answersAssessment.state.httpBusy,
    currentQuestion: containers.discAssessment.state.currentQuestion,
  };
}

export default connect(containers, mapStateToProps)(EvaluationScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  title: {
    fontSize: 20,
    paddingVertical: 20,
    color: '#999999',
  },

  list: {
    flex: 1,
  },

  contentContainer: {
    width: window.width,
    paddingTop: 10,

    ...Platform.select({
      ios: {
        paddingHorizontal: 30,
      },

      android: {
        paddingHorizontal: 0,
      }
    })
  },

  row: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    padding: 16,
    height: 80,
    flex: 1,
    marginTop: 7,
    marginBottom: 12,
    borderRadius: 10,


    ...Platform.select({
      ios: {
        width: window.width - 30 * 2,
        shadowColor: 'rgba(0,0,0,0.2)',
        shadowOpacity: 1,
        shadowOffset: { height: 2, width: 2 },
        shadowRadius: 2,
      },

      android: {
        width: window.width - 30 * 2,
        elevation: 0,
        marginHorizontal: 30,
      },
    })
  },

  image: {
    width: 50,
    height: 50,
    marginRight: 30,
    borderRadius: 25,
  },

  text: {
    fontSize: 24,
    color: '#222222',
  },
});
