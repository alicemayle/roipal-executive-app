import { Container } from 'unstated';
import { Logger } from 'unstated-enhancers';
const initialState = {
score: undefined
}

class ResultsEvaluationContainer extends Container {
  state = { ...initialState }

  setScore(data) {
    this.setState({
      score: data,
    })
  }
}

export default ResultsEvaluationContainer;