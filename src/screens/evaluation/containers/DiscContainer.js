import { Container } from 'unstated';
import AssessmentServices from '../services/AssessmentServices';
import { Logger } from 'unstated-enhancers';

const initialState = {
  httpBusy: false,
  error: undefined,
  data: undefined,
  assessment: undefined,
  currentQuestion: undefined,
  newAnswer: undefined,
}

class DiscContainer extends Container {
  state = { ...initialState }

  setAnswer(data) {
    this.setState({
      newAnswer: data,
    })
  }

  setNextQuestion() {
    const currentQuestion = this.state.assessment.shift();
    const remainingQuestions = [...this.state.assessment];
    const answer = Object.keys(currentQuestion.options).join('');
    this.setState({
      assessment: remainingQuestions,
      currentQuestion: currentQuestion,
      newAnswer: {
        "number": currentQuestion.id,
        "answer": answer,
      },
    })
  }

  async getDISCAssessment() {
    try {
      this.setState({
        httpBusy: true,
        error: undefined,
        __action: 'GET_DISC starts'
      });

      const response = await AssessmentServices.getDISCAssessment();

      if (response) {
        const assessment = response.data.questions;
        const currentQuestion = assessment.shift();
        const remainingQuestions = [...assessment];
        const answer = Object.keys(currentQuestion.options).join('');
  
        await this.setState({
          data: response,
          assessment: remainingQuestions,
          currentQuestion: currentQuestion,
          newAnswer: {
            "number": currentQuestion.id,
            "answer": answer,
          },
          __action: 'GET_DISC success'
        });  
      }
    } catch (error) {
      await this.setState({
        error,
        __action: 'GET_DISC error'
      });
    }
    finally{
      setTimeout( () => {
        this.setState({
          httpBusy: false,
          __action: 'GET_DISC finally'
        });
      }, 500 )
    }
  }

  async getStarDisc(uuid) {

    try {
      this.setState({
        httpBusy: true,
        __action: 'Star Disc starts'
      })

      let response = await AssessmentServices.getStarDisc();

      if (response) {
        let description = response.data.assessment.description;

        await this.setState({
          starDisc: response.data.roipal_star_url,
          description: description,
          resultDisc: response.data,
          message: response.message,
          __action: 'Star Disc success'
        })
      }
    } catch (error) {
      await this.setState({
        error: error,
        __action: 'Star Disc error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'Star Disc finally'
        });
      }, 500)
    }

  }

  resetState() {
    this.setState({
      ...initialState,
      __action: 'GET_DISC reset'
    });
  }
}

export default DiscContainer;
