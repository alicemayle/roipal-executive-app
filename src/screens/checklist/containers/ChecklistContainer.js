import { Container } from "unstated";
import { Logger } from "unstated-enhancers";
import lang from '../../../shared/languages/Lang';

const initialState = {
  data: [
    {
      id:1,
      key: 'activation',
      activity: lang.get('checklist.activation', 'checklist.activation'),
      route:'',
      completed: false
    },
    {
      id:2,
      key: 'profile',
      activity: lang.get('checklist.profile', 'checklist.profile'),
      route: 'InfoProfileScreen',
      completed: false
    },
    {
      id:3,
      key: 'disc-assessment',
      activity: lang.get('checklist.disc-assessment', 'checklist.disc_assessment'),
      route: 'EvaluationScreen',
      completed: false
    },
    {
      id:4,
      key: 'roipal-assessment',
      activity: lang.get('checklist.roipal-assessment', 'checklist.roipal_assessmen'),
      route: 'EvaluationCompanyScreen',
      completed: false
    },
  ],
}

class ChecklistContainer extends Container {
  state = { ...initialState }

  name = 'ChecklistContainer'

  persist = {
    key: 'Checklist'
  }

  setCompletionStatus(data) {
    this.setState({
      data,
      __action: 'Checklist completion status'
    })
  }

  setStepAsCompleted(key) {
    const { data } = this.state;

    const found = data.find(step => step.key === key);

    const newData = data.map(step => {
      if (step.key === key) {
        step = {
          ...step,
          completed: true
        };
      }

      return step;
    })

    this.setState({
      data: newData,
      __action: `Checklist ${found.activity} completed`
    })
  }

  setStepsTranslations() {
    const steps = this.state.data.map( item => {
      const step = {
        ...item,
        activity: lang.get(`checklist.${item.key}`),
      }
      return step
    })

    this.setState({
      data: steps,
      __action: `Checklist translations`
    })
  }
}

export default ChecklistContainer;