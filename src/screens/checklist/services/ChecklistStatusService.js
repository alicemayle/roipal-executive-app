class ChecklistStatusService {

  getCurrentStatus(updated, current) {
    const steps = current;

    const completedKeys = updated.map(item => {
      if (item.completed) {
        return item.step || item.key;
      }
    })

    const status = steps.map((step) => {
      step = { ...step };

      step.completed = completedKeys.indexOf(step.key) !== -1;

      return step;
    })

    return status;
  }

}

export default new ChecklistStatusService;