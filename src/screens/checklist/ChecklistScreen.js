import React, { Component } from 'react';
import { View, Text, Image, Platform, FlatList } from 'react-native';
import { connect, Logger } from 'unstated-enhancers';
import { Container, Button, Title } from 'native-base';
import autobind from 'class-autobind';

import checkList from './containers/ChecklistContainer';
import styles from '../../theme/globalStyles';
import lang from '../../shared/languages/Lang';
import Auth from '../../shared/services/Auth';
import Observer from '../../shared/Observer';
import language from '../../components/language/containers/LanguagesContainer';
import signin from '../auth/signin/containers/SigninContainer';
import StepItem from './components/StepItem'

class ChecklistScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
    };

    autobind(this)
  }

  componentDidMount() {
    setTimeout(() => {
      this.next();
    }, 500);
  }

  keyExtractor(item) {
    return item.id.toString();
  }

  renderItem(item) {
    return (
      <StepItem item={item} onPressItem={this.onPressStep} />

    )
  }

  logout() {
    Observer.notify('logout');
  }

  next() {
    const { checkList } = this.props.containers;
    const steps = checkList.state.data;
    const data = Auth.getData();

    const completed = data.checklist.filter(item => item.completed).map(item => item.step);

    const incompleted = steps.filter(item => {
      return completed.indexOf(item.key) === -1;
    })

    const next = incompleted.find(item => !item.completed);

    if (next) {
      this.props.navigation.navigate(next.route);

      this.setState({
        next
      });
    } else {
      this.props.navigation.navigate('WelcomeScreen');
    }
  }

  goToHome() {
    this.props.navigation.navigate('WelcomeScreen');
  }

  goVerify() {
    const { data } = this.props.containers.signin.state;
    this.props.navigation.navigate('VerificationScreen', { data: data.profile });
  }

  onPressStep(step) {
    const { checkList } = this.props.containers;
    const { data } = checkList.state;
    const incomplete = data.findIndex((item) => item.completed === false);

    if (step === incomplete) {
      this.next();
    }
  }

  renderSeparator() {
    return (
      <View style={{ height: 16.8, width: 0.8, backgroundColor: '#00a7e5', marginLeft: 11 }}></View>
    )
  }

  render() {
    const { checkList } = this.props.containers;
    const { data } = checkList.state;
    const incomplete = data.findIndex((item) => item.completed === false);
    Logger.count(this)

    return (
      <Container style={[
        styles.container,
        {
          alignItems: 'center',
          paddingTop: Platform.OS === 'android' ? 25 : 50,
          paddingHorizontal: 25
        }]}>

        <Image
          style={styles.logoChecklist}
          source={require('../../images/logo_colores/logo_colores.png')}
        />

          <Title style={{ color: '#52bebb', fontSize: 20, marginVertical: 30 }} >{lang.get('title.checklist', 'checklist.checklist')}</Title>
          <FlatList
            contentContainerStyle={{ paddingHorizontal: 20}}
            data={data}
            keyExtractor={this.keyExtractor}
            renderItem={this.renderItem}
            ItemSeparatorComponent={this.renderSeparator}
          />

        <Button
          style={{ marginBottom: 20 }}
          block
          onPress={incomplete === -1 ? this.goToHome
            : incomplete === 0 ? this.goVerify : this.next}>
          <Text style={{ color: 'white' }}> {lang.get('button.next', 'generic.button.next')} </Text>
        </Button>
        <Button
          style={{ marginBottom: 25 }}
          block
          success
          onPress={this.logout}
        >
          <Text style={{ color: 'white' }}> {lang.get('router.logout', 'checklist.logout')} </Text>
        </Button>
      </Container>
    );
  }
}

export default connect({
  checkList, language, signin
})(ChecklistScreen)
