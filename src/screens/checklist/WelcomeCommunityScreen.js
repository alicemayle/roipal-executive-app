import React, { Component } from 'react';
import { Image, Dimensions } from 'react-native';
import { Container, View, Text, Button, Content } from 'native-base';
import autobind from 'class-autobind';
import styles from '../../theme/globalStyles';
import Lang from '../../shared/languages/Lang';
import LinearGradient from 'react-native-linear-gradient';

const { width, height } = Dimensions.get('window');

class WelcomeCommunityScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    autobind(this);
  }

  goAhead() {
    this.props.navigation.navigate('HomeScreen')
  }

  render() {

    const split = Lang.get('info.welcomeRoipalInfo2', 'checklist.welcome.message_thanks').split(',');
    const split2 = split[1].split('.');

    return (
      <Container>
        <LinearGradient
          colors={['#135481', '#016eb8', '#0285c9', '#00a7e5', '#1dbed6', '#5bc1be']}
          start={{ x: 0, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={{
            width: width,
            height: height, flex: 1
          }}
        >
          <Content bounces={false}>
            <View style={{ alignItems: 'center', height: '100%', width: '100%' }}>
              <View style={{ paddingTop: 35 }}>
                <Image style={styles.congratulation} source={require('../../images/congratulation/congratulation.png')} />
                <Text style={{ marginTop: 25, textAlign: 'center', fontSize: 25, color: 'white' }}>{Lang.get('info.welcomeRoipalTitle', 'checklist.welcome.message_congratulations')}</Text>
                <View style={styles.ViewBorder} />
              </View>
              <View style={{ marginTop: 30, marginBottom: 30 }}>
                <Text style={{ textAlign: 'center', fontSize: 20, color: 'white', fontWeight: 'bold' }}>{Lang.get('info.welcomeRoipalInfo', 'checklist.welcome.message_community')}
                </Text>
                <Text style={styles.TextWelcomeTitle}>
                  <Text style={styles.TextWelcome}>{split[0] + ','}</Text>
                  <Text style={[styles.TextWelcome, { fontWeight: 'bold' }]} >{split2[0] + '.'}</Text>
                  <Text style={styles.TextWelcome} >{split2[1] + '.'}</Text>
                </Text>
              </View>
              <Button light block onPress={this.goAhead}
                style={{ marginRight: 35, marginLeft: 35 }}>
                <Text>{Lang.get('button.next', 'generic.next')}</Text>
              </Button>
              <Image style={[styles.logoWell, { marginTop: 40, marginBottom: 25 }]} source={require('../../images/logo_blanco/logo_blanco.png')} />
            </View>
          </Content>
        </LinearGradient>
      </Container>
    );
  }
}

export default WelcomeCommunityScreen;
