import React, { PureComponent } from 'react'
import { Text, Icon, View } from 'native-base';
import autobind from 'class-autobind';
import { TouchableOpacity } from 'react-native';
import { Logger } from 'unstated-enhancers';


export class StepItem extends PureComponent {

  constructor(props) {
    super(props);

    autobind(this);
  }

  handlePressItem() {
    const { item } = this.props;

    this.props.onPressItem(item.index);
  }

  render() {
    const { item } = this.props.item;

    return (
      <TouchableOpacity
        style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 7.5, width:'100%'}}
        onPress={this.handlePressItem}>
        <View
          style={{
            height: 22.5,
            width: 22.5,
            backgroundColor: item.completed ? '#3aba0e' : '#8e8e93',
            borderRadius: 22.5, alignItems: 'center',
            marginRight: 15
          }}
        >
          {item.completed && <Icon name='checkmark' style={{ color: 'white', fontSize: 22 }} />}
        </View>
        <Text style={{ fontSize: 16, width:'85%' }}>{item.activity}</Text>
      </TouchableOpacity>
    )
  }
}

export default StepItem
