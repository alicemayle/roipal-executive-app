import React, { Component } from 'react';
import { Container, Icon } from 'native-base';
import autobind from 'class-autobind';
import { connect, Logger } from 'unstated-enhancers';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { Dimensions } from 'react-native';

import styles from '../theme/globalStyles';
import NavBarComponent from '../components/navBar/NavBarComponent';
import lang from '../shared/languages/Lang';

const screen = Dimensions.get('window');
const LATITUDE_DELTA = 0.01;
const ASPECT_RATIO = screen.width / screen.height;

class MapViewScreen extends Component {
  constructor(props) {
    super(props);

    autobind(this);
  }

  render() {
    const { navigation } = this.props;
    const location = navigation.getParam('location');

    Logger.count(this)

    return (
      <Container style={styles.container}>

        <NavBarComponent {...this.props.navigation} name={lang.get('map.title', 'map.title')}>
        </NavBarComponent>

        <MapView
          provider={PROVIDER_GOOGLE}
          style={{flex:1 }}
          initialRegion={{
            latitude: location.latitude,
            longitude: location.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LATITUDE_DELTA * ASPECT_RATIO,
          }}
        >
          <Marker coordinate={{ latitude: location.latitude, longitude: location.longitude }}>
            <Icon name="pin" style={{ color: 'red', fontSize: 35 }} />
          </Marker>
        </MapView>
      </Container>
    );
  }
}

export default (MapViewScreen);
