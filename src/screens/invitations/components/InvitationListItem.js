import React, { PureComponent } from 'react'
import { Body, Text, Right, Icon, CardItem, Card } from 'native-base';
import autobind from 'class-autobind';
import moment from 'moment';

import StatusManager from '../../../shared/support/InvitationStatusManager';
import { Logger } from 'unstated-enhancers';
import lang from '../../../shared/languages/Lang';

export class InvitationListItem extends PureComponent {

  constructor(props) {
    super(props);

    autobind(this);
  }

  handlePressItem() {
    const { item } = this.props;

    this.props.onPressItem(item);
  }

  render() {
    const { item } = this.props.item;
    const statusColor = StatusManager.getStatusColor(item.status);
    const statusTag = StatusManager.getStatusTag(item.status);
    const expired = item.status === 0
      && (item.mission.status === 10
        || item.mission.status === -1
        || item.mission.accepted === item.mission.executives_requested)
      ? true
      : false;

    return (
      <Card style={{ borderRadius: 10, marginLeft: 15, marginRight: 15}}>
      <CardItem
        button={!expired}
        onPress={ this.handlePressItem }
        style={{ backgroundColor: expired ? '#f3f3f3' : 'trasparent' }}>
        <Body>
          <Text style={{ fontSize: 18 }}>{ item.mission.name }</Text>
          {
            expired && item.mission.accepted === item.mission.executives_requested &&
            <Text note>{lang.get('invitation.fullMission', 'invitations.full_mission')}</Text>
          }
          {
            expired && item.mission.status === 10 &&
            <Text note>{lang.get('invitation.missionFinish', 'invitations.mission_finish')}</Text>
          }
          {
            expired && item.mission.status === -1 &&
            <Text note>{lang.get('invitation.missionCancel', 'invitations.mission_cancel')}</Text>
          }
        </Body>
        <Right>
          <Text note>{ moment(item.started_at).format("DD/MM/YYYY") }</Text>
          <Text note style={{ color: statusColor }}>
            { expired ? lang.get('invitation.expired', 'invitations.expired') : statusTag}
          </Text>
        </Right>
      </CardItem>
      </Card>
    )
  }
}

export default InvitationListItem
