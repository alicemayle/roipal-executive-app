import React, { PureComponent } from 'react'
import { FlatList, InteractionManager } from 'react-native';
import autobind from 'class-autobind';
import {Spinner, View } from 'native-base';

import InvitationListItem from './InvitationListItem';
import EmptyList from '../../../components/emptyList/EmptyList';
import lang from '../../../shared/languages/Lang';

export class InvitationList extends PureComponent {
  onEndReachedCalledDuringMomentum = true;
  constructor(props) {
    super(props);

    autobind(this);
  }

  keyExtractor(item) {
    return item.uuid;
  }

  renderItem(item) {
    return (
      <InvitationListItem
        item={item}
        onPressItem={this.props.onPressItem}
      />
    )
  }

  handleLoadMore(info) {
    InteractionManager.runAfterInteractions(() => {
      if (!this.onEndReachedCalledDuringMomentum) {
        const { onHandleLoadMore} = this.props;

        if(onHandleLoadMore) {
          onHandleLoadMore();
        }
      }

      this.onEndReachedCalledDuringMomentum = true;
    })
  }

  handleMomentumScrollBegin() {
    this.onEndReachedCalledDuringMomentum = false;
  }

  handleOnRefresh() {
    const { onRefresh } = this.props;

    if (onRefresh) {
      onRefresh();
    }
  }

  renderFooter() {
    const { hasMoreItems, refreshing } = this.props;

    return (hasMoreItems && !refreshing ? <Spinner color="lightblue" /> : <View/>);
  }


  render() {
    const { data = [], refreshing } = this.props;

    return (
      <FlatList
        data={data}
        keyExtractor={this.keyExtractor}
        renderItem={this.renderItem}
        onEndReached={this.handleLoadMore}
        onEndThreshold={0.01}
        refreshing={ refreshing }
        initialNumToRender={ 15 }
        onMomentumScrollBegin={this.handleMomentumScrollBegin}
        onScrollBeginDrag={this.handleMomentumScrollBegin}
        onRefresh={ this.handleOnRefresh }
        ListFooterComponent={ this.renderFooter }
        ListEmptyComponent={ <EmptyList message={lang.get('intro.noInvitations', 'invitations.no_invitations')}/> }
      />
    )
  }
}

export default InvitationList
