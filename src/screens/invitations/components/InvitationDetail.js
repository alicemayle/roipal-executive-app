import React, { PureComponent } from 'react'
import { Text, View, Button, Card, CardItem, Thumbnail, Icon, Left, Body } from 'native-base';
import autobind from 'class-autobind';
import { Dimensions, SafeAreaView, FlatList, Platform } from 'react-native';
import Geocoder from 'react-native-geocoding';

import { Logger, connect } from 'unstated-enhancers';
import lang from '../../../shared/languages/Lang';
import StatusManager from '../../../shared/support/InvitationStatusManager';
import logo from '../../../images/video/video.png';
import moment from 'moment';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import AuthContainer from '../../auth/containers/AuthContainer';
const screen = Dimensions.get('window');
const LATITUDE_DELTA = 0.01;
const ASPECT_RATIO = screen.width / screen.height;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export class InvitationDetail extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      status: undefined,
      accept: undefined,
      reason: '',
      map: false,
      totalFormat: undefined
    }

    autobind(this);
  }

  async formatTotal(number) {
    const { item } = this.props;

    const coordinates = {
      latitude: item.location.latitude,
      longitude: item.location.longitude
    };
    let format = undefined;

    const json = await Geocoder.from(coordinates)
    const countryObj = json.results.find(item => item.types[0] === 'country' )

    if (countryObj.address_components[0].short_name === 'US' || countryObj.address_components[0].short_name === 'MX') {
      format = 'en-IN'
    } else if (countryObj.address_components[0].short_name === 'FR') {
      format = 'de-DE'
    } else {
      format = 'en-IN'
    }

    if (number) {
      if (Platform.OS === 'android') {
        const Intl = require('react-native-intl');
        let totalFormat = await new Intl.NumberFormat(format).format(parseFloat(number.toFixed(2)));
        this.setState({
          totalFormat: totalFormat
        })
      } else {
        this.setState({
          totalFormat: Intl.NumberFormat(format).format(number.toFixed(2))
        })
      }
    }
  }

  acceptInvitation() {
    const data = {
      status: 1
    }
    this.props.onInvitationActionRequest(data);
  }

  rejectInvitation() {
    const data = {
      status: -1
    }
    this.props.onInvitationActionRequest(data);
  }

  viewMap() {
    const { map } = this.state;

    this.setState({
      map: !map
    })
  }

  keyExtractor(index) {
    return index.toString();
  }

  renderDescription(item) {
    var activitySplit = item.item.split(':');

    return (
      <Left style={{ justifyContent: 'flex-start', alignItems:'flex-start' }}>
        <Icon style={{ color: 'rgba(2,133,201,1)', marginTop: 2}} name='checkmark-circle-outline' />
        <Body>
          <Text>
            <Text style={{ fontWeight: 'bold' }}>{activitySplit[0]}:</Text>
            <Text>{activitySplit[1]}{'\n'}</Text>
          </Text>
        </Body>
      </Left>
    );
  }

  goToMapFullScreen(data) {
    const { onMapFullScreen, item } = this.props;

    if (onMapFullScreen) {
      onMapFullScreen(item.location);
    }
  }

  componentDidMount() {
    const { item } = this.props;

    if (item.location.executive_payment_cost) {
      this.formatTotal(item.location.executive_payment_cost)
    }
  }

  render() {
    const { item, httpBusy, onNavigateVideo, verification_status } = this.props;
    const { auth } = this.props.containers;
    const { accept, reason, map, totalFormat } = this.state;
    const statusTag = StatusManager.getStatusTag(item.status);
    const { data = {} } = auth.state;
    const statusProfile = data.account_status

    Logger.count(this)

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={{ flex: 1 }}>
          {item.mission.video_url &&
            <Card style={{ flexDirection: 'row', alignItems: 'center', borderRadius: 10, marginBottom: 10 }}>
              <CardItem button onPress={onNavigateVideo} style={{ backgroundColor: 'transparent' }}>
                <Thumbnail square source={{uri: item.mission.thumbnail_video}} />
                <Text style={{ marginLeft: 10}}>{lang.get('profile.playVideo', 'invitations.details.play_video')}</Text>
              </CardItem>
            </Card>
          }

          <View style={{ marginBottom: 10 }}>
            <Text style={{ marginHorizontal: 10, color: 'rgba(2,133,201,1)' }}>{lang.get('mission.description', 'invitations.details.description')}</Text>
            <Card style={{ borderRadius: 10 }} >
              <CardItem style={{ borderRadius: 10, marginHorizontal: 10, paddingHorizontal: 10 }}>
                <Text>{item.mission.description}</Text>
              </CardItem>
            </Card>
          </View>

          <View style={{ marginBottom: 10 }}>
            <Text style={{ marginHorizontal: 10, color: 'rgba(2,133,201,1)' }}>{lang.get('mission.date', 'invitations.details.date')}</Text>
            <Card style={{ borderRadius: 10 }} >
              <CardItem style={{ borderRadius: 10, marginHorizontal: 10, paddingHorizontal: 10 }}>
                <Text>{moment(item.mission.started_at).format("dddd D MMMM YYYY")}</Text>
              </CardItem>
            </Card>
          </View>

          <View style={{ marginBottom: 10 }}>
            <Text style={{ marginHorizontal: 10, color: 'rgba(2,133,201,1)' }}>{lang.get('mission.Commercial', 'invitations.details.commercial')}</Text>
            <Card style={{ borderRadius: 10 }} >
              <CardItem style={{ borderRadius: 10, marginHorizontal: 10, paddingHorizontal: 10 }}>
                <Text>{item.mission.commercial_name || item.mission.commercial_need}</Text>
              </CardItem>
            </Card>
          </View>

          <View style={{ marginBottom: 10 }}>
            <Text style={{ marginHorizontal: 10, color: 'rgba(2,133,201,1)' }}>{lang.get('mission.type', 'invitations.details.type')}</Text>
            <Card style={{ borderRadius: 10 }} >
              <CardItem style={{ borderRadius: 10, marginHorizontal: 10, paddingHorizontal: 10 }}>
                <Text>{item.mission.type}</Text>
              </CardItem>
            </Card>
          </View>

          <View style={{ marginBottom: 10 }}>
            <Text style={{ marginHorizontal: 10, color: 'rgba(2,133,201,1)' }}>{lang.get('mission.time', 'invitations.details.time')}</Text>
            <Card style={{ borderRadius: 10 }} >
              <CardItem style={{ borderRadius: 10, marginHorizontal: 10, paddingHorizontal: 10 }}>
                <Text>{`${item.mission.time} ${item.mission.time_unit}`}</Text>
              </CardItem>
            </Card>
          </View>

          <View style={{ marginBottom: 10 }}>
            <Text style={{ marginHorizontal: 10, color: 'rgba(2,133,201,1)' }}>{lang.get('mission.location', 'invitations.details.location')}</Text>
            <Card style={{ borderRadius: 10, alignItems: 'center' }}>
              <CardItem button style={{ borderRadius: 10, marginHorizontal: 10, paddingHorizontal: 10 }} onPress={this.viewMap}>
                <Text>{item.location.address}</Text>
              </CardItem>
              {map &&
                <MapView
                  onPress={this.goToMapFullScreen}
                  scrollEnabled={false}
                  rotateEnabled={false}
                  provider={PROVIDER_GOOGLE}
                  style={[{ height: 80, width: '90%', margin: 10 }]}
                  initialRegion={{
                    latitude: item.location.latitude,
                    longitude: item.location.longitude,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LATITUDE_DELTA * ASPECT_RATIO,
                  }}
                >
                  <Marker coordinate={{ latitude: item.location.latitude, longitude: item.location.longitude }}>
                    <Icon name="pin" style={{ color: 'red', fontSize: 20 }} />
                  </Marker>
                </MapView>
              }
            </Card>
          </View>

          <View style={{ marginBottom: 10 }}>
            <Text style={{ marginHorizontal: 10, color: 'rgba(2,133,201,1)' }}>{lang.get('mission.status', 'invitations.details.status')}</Text>
            <Card style={{ borderRadius: 10 }} >
              <CardItem style={{ borderRadius: 10, marginHorizontal: 10, paddingHorizontal: 10 }}>
                <Text>{item.status === 0 && item.mission.status === 10 || item.mission.status === -1 ? lang.get('invitation.expired', 'invitations.expired') :  statusTag}
                </Text>
              </CardItem>
            </Card>
          </View>

          <View style={{ marginBottom: 10 }}>
            <Text style={{ marginHorizontal: 10, color: 'rgba(2,133,201,1)' }}>{lang.get('mission.paymentExecutive', 'invitations.details.payment_executive')}</Text>
            <Card style={{ borderRadius: 10 }} >
              <CardItem style={{ borderRadius: 10, marginHorizontal: 10, paddingHorizontal: 10 }}>
                <Text>{item.mission.currency? item.mission.currency.symbol : ''} {totalFormat} {item.mission.currency? item.mission.currency.currency : ''}</Text>
              </CardItem>
            </Card>
          </View>

          <View style={{ marginBottom: 10 }}>
            <Text style={{ marginHorizontal: 10, color: 'rgba(2,133,201,1)' }}>{lang.get('mission.activities', 'invitations.details.activities')}</Text>
            <Card style={{ borderRadius: 10 }} >
              <CardItem style={{ borderRadius: 10, marginHorizontal: 10, paddingHorizontal: 10 }}>
                <FlatList
                  data={item.mission.type_sale_activities}
                  keyExtractor={this.keyExtractor}
                  renderItem={this.renderDescription}
                />
              </CardItem>
            </Card>
          </View>

          { verification_status !== 1 &&
            <View style={{ paddingTop: 10, paddingBottom: 25 }}>
              <Text note style={{ textAlign: 'center', marginRight: 35, marginLeft: 35, fontSize: 17 }}>
                {lang.get('documents.withoutVerification', 'invitations.details.without_verification')}
              </Text>
            </View>
          }

          {item.status === 0 && item.mission.status !== 10 && item.mission.status !== -1 && statusProfile === 1 && verification_status === 1 &&
            <View style={{ paddingTop: 10, paddingBottom: 25 }}>
              <Text note style={{ textAlign: 'center', marginRight: 35, marginLeft: 35, fontSize: 17 }}>{lang.get('invitation.read', 'invitations.details.read')}</Text>
            </View>
          }

          {item.status === 0 && item.mission.status !== 10 && item.mission.status !== -1 && statusProfile === 1 && verification_status === 1 &&
            <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', marginBottom: 25 }}>
              <Button block style={{ marginHorizontal: 10, width: '45%' }} onPress={this.rejectInvitation}>
                <Text>
                  {lang.get('invitation.reject', 'generic.button.reject')}
                </Text>
              </Button>
              <Button block style={{ marginHorizontal: 10, width: '45%' }} onPress={this.acceptInvitation}>
                <Text>
                  {lang.get('invitation.accept', 'genric.button.accept')}
                </Text>
              </Button>
            </View>
          }
        </View>
      </SafeAreaView>
    )
  }
}

const containers = {
  auth: AuthContainer
}

const mapStateToProps = (containers) => {
  return {
    auth: containers.auth.state,
  }
}

export default connect(containers, mapStateToProps)(InvitationDetail);