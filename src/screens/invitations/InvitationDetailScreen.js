import React, { Component } from 'react';
import { connect } from 'unstated-enhancers';
import { Container, Content, Text, Spinner, Card, CardItem, Left, Thumbnail, Body, View, Title, Button } from 'native-base';
import autobind from 'class-autobind';
import LinearGradient from 'react-native-linear-gradient';
import Modal from 'react-native-modal';
import ListContainer from './containers/InvitationsListContainer';
import AcceptanceContainer from './containers/InvitationAcceptanceContainer';
import InvitationDetail from './components/InvitationDetail';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import newLang from '../../shared/languages/Lang';
import ListErrors from '../../components/listErrors/ListErrors';
import styles from '../../theme/globalStyles';
import avatar from '../../images/drawer/perfil.png';
import TextareaInput from '../../components/inputs/TextareaInput';
import auth from '../auth/containers/AuthContainer';
import toast from '../../shared/support/ShowToast';

class InvitationDetailScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalVisible: false,
    };

    autobind(this);
  }

  handleValueChange(field, value) {
    this.setState({
      [field]: value
    })
  }

  componentWillUnmount() {
    const { acceptance } = this.props.containers;

    acceptance.reset();
  }

  async handleInvitationActionRequest() {
    this.handleCloseModal();

    const { status, reason } = this.state;

    if (status === -1) {
      const { acceptance, list, auth } = this.props.containers;
      const { uuid } = acceptance.state.data;

      let data = {
        ...this.state
      }
      delete data.modalVisible;

      await acceptance.accept(uuid, data);

      const { error } = acceptance.state;

      if (error) {
        toast.ShowToast(error.message || newLang.get('message.tryAgain', 'generic.try_again'));

        setTimeout(acceptance.clear(), 10000);
      } else {
        const newData = list.state.data.map(item => {
          if (item.uuid === uuid) {
            item = { ...item }

            item.status = status
            item.reason = reason;
          }

          return item
        })
        await list.setDataManually(newData);
        await auth.modifyInvitationsReceived()
        this.props.navigation.goBack();
      }
    } else if (status === 1) {
      this.handleAssessment();
    }
  }

  handleConfimationActionRequest(data) {
    this.setState({
      action: data.status === 1 ? 'accept' : 'reject',
      status: data.status
    })
    this.setModalVisible(true);
  }

  invitationDetailCompany() {
    this.props.navigation.navigate('InvitationDetailCompanyScreen');
  }

  handleAssessment() {
    this.props.navigation.navigate('CompanyAssessmentScreen');
  }

  handleNavigateVideo() {
    const { acceptance } = this.props.containers;
    const { video_url } = acceptance.state.data.mission;
    this.props.navigation.navigate('VideoInvitationScreen', { url: video_url });
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  handleCloseModal() {
    this.setState({
      modalVisible: false
    })
  }

  handleMapFullScreen(location) {
    this.props.navigation.navigate('MapViewScreen', { location: location });
  }

  render() {
    const { acceptance, auth } = this.props.containers;
    const { data = {}, httpBusy, error = {} } = acceptance.state;
    const dataIsLoaded = Boolean(data && data.company !== undefined);
    const { action, reason } = this.state;
    const { profile } = auth.state;

    return (
      <Container style={styles.container}>

        <NavBarComponent
          {...this.props.navigation}
          name={newLang.get('title.invitationDetails', 'invitations.invitation_details')} />
        <LinearGradient
          colors={['#00a7e5', '#5bc1be']}
          start={{ x: 0, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={{
            justifyContent: "center",
            alignItems: "center",
            width: '100%',
          }}
        >
          <Card style={{ borderRadius: 10, width: '95%', marginBottom: 15 }}>
            <CardItem button style={{ backgroundColor: 'transparent' }} onPress={this.invitationDetailCompany}>
              <Left>
                <Thumbnail
                  large
                  source={data.company.profile.photo_bio_url ? { uri: data.company.profile.photo_bio_url } : avatar}
                  style={{borderColor: '#0285c9', borderWidth: 3}}
                />
              </Left>
              <Body style={{ justifyContent: 'center' }}>
                <Title>{data.company.name}</Title>
                <Text style={[{ marginBottom: 10 }]}>{data.company.profile.website}</Text>
              </Body>
            </CardItem>
          </Card>
        </LinearGradient>
        <View style={{ width: '100%', backgroundColor: '#f0f2f4', padding: 10 }}>
          <Title style={{ textAlign: 'justify', marginLeft: 10 }}>{data.mission.name}</Title>
        </View>

        <Content padder bounces={false}>
          {
            dataIsLoaded && (
              <InvitationDetail
                httpBusy={httpBusy}
                item={data}
                onInvitationActionRequest={this.handleConfimationActionRequest}
                onNavigateVideo={this.handleNavigateVideo}
                onMapFullScreen={this.handleMapFullScreen}
                verification_status={profile.verification_status}
              />
            )
          }
          {
            httpBusy && !dataIsLoaded && <Spinner color='lightblue' />
          }
          {error.errors && (
            <ListErrors errors={error.errors} />
          )}
        </Content>
        <Modal
          transparent={true}
          isVisible={this.state.modalVisible}
          onBackdropPress={this.handleCloseModal}
          style={{ alignItems: 'center' }}
          hideModalContentWhileAnimating={ true }
        >
          <View style={[styles.modal]}>
            <Text style={{ textAlign: 'center', fontWeight: 'bold', marginBottom: 10 }}>
              {newLang.get(`invitation.${action}Invitation`,`invitations.${action}_invitation` )}
            </Text>
            <Text style={{ textAlign: 'center' }}>
              {newLang.get(`invitation.${action}Action`, `invitations.${action}_action`)}
            </Text>

            {
              action === 'reject' && (
                <View style={{width:'100%'}}>
                <TextareaInput
                  style={{ marginTop: 20, width:'100%' }}
                  field='reason'
                  rowSpan={3}
                  bordered
                  value={reason}
                  placeholder={newLang.get('invitation.reason', 'invitations.reason')}
                  onChangeText={this.handleValueChange}
                />
                </View>)
            }
            <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', marginTop: 10 }}>
              <Button transparent
                onPress={this.handleCloseModal}>
                <Text>{newLang.get('invitation.cancel', 'generic.button.cancel')}</Text>
              </Button>
              <Button transparent
                onPress={this.handleInvitationActionRequest}>
                <Text>{newLang.get('invitation.confirm', 'generic.button.done')}</Text>
              </Button>
            </View>
          </View>
        </Modal>

      </Container>
    );
  }
}

const containers = {
  acceptance: AcceptanceContainer,
  list: ListContainer,
  auth: auth
}

export default connect(containers)(InvitationDetailScreen);
