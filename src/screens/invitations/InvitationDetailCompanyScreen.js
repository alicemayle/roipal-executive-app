import React, { Component } from 'react';
import { View, Text, Content, Container, Title, Card, CardItem, Thumbnail, Left, Body, Icon, Header, Button } from 'native-base';
import { TouchableOpacity, Modal, Platform } from 'react-native';
import styles from '../../theme/globalStyles';
import { connect, Logger } from 'unstated-enhancers';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import StringFunction from '../../shared/support/StringFunction';
import lang from '../../shared/languages/Lang';
import LinearGradient from 'react-native-linear-gradient';
import avatar from '../../images/drawer/perfil.png';
import autobind from 'class-autobind';
import ImageViewer from 'react-native-image-zoom-viewer';
import acceptance from './containers/InvitationAcceptanceContainer';
import { Dimensions } from 'react-native';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
const screen = Dimensions.get('window');
const LATITUDE_DELTA = 0.01;
const ASPECT_RATIO = screen.width / screen.height;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class InvitationDetailCompanyScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      map: false,
      modalVisible: false,
    };
    autobind(this);
  }

  viewMap() {
    const { map } = this.state;

    this.setState({
      map: !map
    })
  }

  setModalVisible() {
    const inverse = !this.state.modalVisible;
    this.setState({
      modalVisible: inverse,
    });
  }

  goToMapFullScreen(data) {
    const { company} = this.props.invitation;

    this.props.navigation.navigate('MapViewScreen', { location: company.profile.position });
  }

  handleNavigateVideo() {
    const { acceptance } = this.props.containers;
    const { video_bio_url } = acceptance.state.data.company.profile;
    this.props.navigation.navigate('VideoInvitationScreen', { url: video_bio_url });
  }

  render() {
    const { data } = this.props.containers.acceptance.state;
    const { map } = this.state;

    return (
      <Container style={styles.container}>

        <NavBarComponent
          {...this.props.navigation}
          name={lang.get('title.infoCompany', 'invitations.details.info_company')} />

        <LinearGradient
          colors={['#00a7e5', '#5bc1be']}
          start={{ x: 0, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={{
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <Card style={{ borderRadius: 10, width: '95%', marginBottom: 15 }}>
            <CardItem style={{ backgroundColor: 'transparent' }}>
              <Left>
              <TouchableOpacity onPress={this.setModalVisible}>
                <Thumbnail
                  large
                  source={data.company.profile.photo_bio_url ? { uri: data.company.profile.photo_bio_url } : avatar}
                  style={{borderColor: '#0285c9', borderWidth: 3}}
                />
                </TouchableOpacity>
              </Left>
              <Body style={{ justifyContent: 'center' }}>
                <Title>{data.company.name}</Title>
                <Text>{data.company.business_name}</Text>
              </Body>
            </CardItem>
          </Card>
        </LinearGradient>

        <Content padder bounces={false}>
        {data.company.profile.video_bio_url &&
            <Card style={{ flexDirection: 'row', alignItems: 'center', borderRadius: 10, marginBottom: 10 }}>
              <CardItem button onPress={this.handleNavigateVideo} style={{ backgroundColor: 'transparent' }}>
                <Thumbnail
                  square
                  source={{uri: data.company.profile.thumbnail_video}}
                />
                <Text style={{ marginLeft: 10}}>{lang.get('profile.playVideo', 'invitations.details.play_video')}</Text>
              </CardItem>
            </Card>
          }
          <View style={{ marginBottom: 10 }}>
            <Text style={{ marginHorizontal: 10, color: 'rgba(2,133,201,1)' }}>{lang.get('mission.description', 'invitations.details.description')}</Text>
            <Card style={{ borderRadius: 10 }} >
              <CardItem style={{ borderRadius: 10, marginHorizontal: 10, paddingHorizontal: 10 }}>
                <Text>{StringFunction.capitalize(data.company.profile.bio)}</Text>
              </CardItem>
            </Card>
          </View>

          <View style={{ marginBottom: 10 }}>
            <Text style={{ marginHorizontal: 10, color: 'rgba(2,133,201,1)' }}>{lang.get('mission.slogan', 'invitations.details.slogan')}</Text>
            <Card style={{ borderRadius: 10 }} >
              <CardItem style={{ borderRadius: 10, marginHorizontal: 10, paddingHorizontal: 10 }}>
                <Text>{StringFunction.capitalize(data.company.profile.slogan)}</Text>
              </CardItem>
            </Card>
          </View>

          <View style={{ marginBottom: 10 }}>
            <Text style={{ marginHorizontal: 10, color: 'rgba(2,133,201,1)' }}>{lang.get('mission.vertical', 'invitations.details.vertical')}</Text>
            <Card style={{ borderRadius: 10 }} >
              <CardItem style={{ borderRadius: 10, marginHorizontal: 10, paddingHorizontal: 10 }}>
                <Text>{StringFunction.capitalize(data.company.profile.vertical)}</Text>
              </CardItem>
            </Card>
          </View>

          <View style={{ marginBottom: 10 }}>
            <Text style={{ marginHorizontal: 10, color: 'rgba(2,133,201,1)' }}>{lang.get('complete.website', 'invitations.details.website')}</Text>
            <Card style={{ borderRadius: 10 }} >
              <CardItem style={{ borderRadius: 10, marginHorizontal: 10, paddingHorizontal: 10 }}>
                <Text>{StringFunction.capitalize(data.company.profile.website)}</Text>
              </CardItem>
            </Card>
          </View>

          <View style={{ marginBottom: 10 }}>
            <Text style={{ marginHorizontal: 10, color: 'rgba(2,133,201,1)' }}>{lang.get('mission.address', 'invitations.details.address')}</Text>
            <Card style={{ borderRadius: 10 }} >
              <CardItem button style={{ borderRadius: 10, marginHorizontal: 10, paddingHorizontal: 10 }} onPress={this.viewMap}>
                <Text>{StringFunction.capitalize(data.company.profile.address)}</Text>
              </CardItem>
              {map && <MapView
                onPress={this.goToMapFullScreen}
                scrollEnabled={false}
                rotateEnabled={false}
                provider={PROVIDER_GOOGLE}
                style={[{ height: 80, width: '90%', margin: 10 }]}
                initialRegion={{
                  latitude: data.company.profile.position.latitude,
                  longitude: data.company.profile.position.longitude,
                  latitudeDelta: LATITUDE_DELTA,
                  longitudeDelta: LATITUDE_DELTA * ASPECT_RATIO,
                }}
              >
                <Marker coordinate={{
                  latitude: data.company.profile.position.latitude,
                  longitude: data.company.profile.position.longitude}}>
                  <Icon name="pin" style={{ color: 'red', fontSize: 20 }} />
                </Marker>
              </MapView>
              }
            </Card>
          </View>
        </Content>
        <Modal
          visible={this.state.modalVisible}
          onRequestClose={this.setModalVisible}
          transparent={false}>
          <Header style={{ backgroundColor: Platform.OS==='android' ? 'black' : 'white' }}>
            <Button
              transparent
              onPress={this.setModalVisible}
            >
              <Icon name={'arrow-back'} style={{ color: Platform.OS==='android' ? 'white' : 'black' }} />
            </Button>
            <Body style={{ alignItems: 'center' }}>
            </Body>
            <View style={{ width: 70, justifyContent: 'center', alignItems: 'center' }}>
            </View>
          </Header>

          <ImageViewer onModal={this.setModalVisible} backgroundColor={Platform.OS==='android' ? 'black' : 'white'} imageUrls={[
            {
              url: data.company.profile.photo_bio_url
            }
          ]} />
        </Modal>
      </Container>
    );
  }
}

const containers = {
  acceptance: acceptance
}

const mapStateToProps = (containers) => {
  return {
    invitation: containers.acceptance.state.data
  }
}

export default connect(containers, mapStateToProps)(InvitationDetailCompanyScreen);
