import React, { Component } from 'react';
import { InteractionManager } from 'react-native';
import { connect } from 'unstated-enhancers';
import { Container } from 'native-base';
import InvitationList from './components/InvitationList'
import autobind from 'class-autobind';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import lang from '../../shared/languages/Lang';
import SearchBarComponent from '../../components/searchBar/SearchBarComponent';
import ListContainer from './containers/InvitationsListContainer';
import AcceptanceContainer from './containers/InvitationAcceptanceContainer';
import NoteValidationDocuments from '../../components/NoteValidationDocuments';
import AuthContainer from '../auth/containers/AuthContainer';
import VerificationContainer from '../profile/containers/VerificationContainer';
import toast from '../../shared/support/ShowToast';

class InvitationsListScreen extends Component {
  debounceId = null;
  isFiltering = false;

  constructor(props) {
    super(props);
    this.state = {
      isFetching: false,
      docsStatus: null
    }

    autobind(this);
  }

  async fetch(page = 1) {
    const { list } = this.props.containers;
    const { meta, limit, httpBusy } = list.state;

    if (!this.isFiltering && (httpBusy || meta.cursor.count === 0 || meta.cursor.count < limit && page !== 1)) {
      this.setState({
        isFetching: false
      })
      return;
    }

    const params = {
      page: page || 1,
      includes: 'company,mission',
      search: this.state.value || '',
      limit: limit,
    };

    await list.fetch(params);

    const { error } = this.props.containers.list.state;

    if (error) {
      toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))

      setTimeout(() => {
        list.clearError();
      }, 200)
    } else {
      setTimeout(() => {
        this.setState({
          isFetching: false
        })
      }, 600)
    }

    this.isFiltering = false;
  }

  handleOnRefresh() {
    this.setState({ isFetching: true }, function () { this.fetch() });
  }

  handleLoadMore() {
    if (this.debounceId) {
      clearTimeout(this.debounceId);
    }

    const { list } = this.props.containers;
    const { meta, limit } = list.state;

    this.debounceId = setTimeout(() => {
      this.fetch(meta.cursor.current + 1);

      const after = this.props.containers.list.state.meta;

      if (after.cursor.count === 0 || after.cursor.count < limit) {
        InteractionManager.runAfterInteractions(() => {
          toast.ShowToast('No hay mas que mostrar');
        })
      }
    }, 500);
  }

  async handlePressItem({ item }) {
    /**
     * TODO
     *
     * Fetch the details from here?
     */

    const { acceptance } = this.props.containers;
    const params = {
      'includes': 'company.profile,location,mission'
    }

    await acceptance.get(item.uuid, params);

    this.props.navigation.navigate('InvitationDetailScreen')
  }

  handleSearchChangeText(value) {
    this.setState({
      value
    })
  }

  handleSearchInvitations() {
    this.isFiltering = true;

    this.fetch()
  }

  onPressLeftButton() {
    this.props.navigation.navigate('HomeScreen');
  }

  clearText() {
    this.setState({
      value: '',
    })
  }

  async getDocuments() {
    const { verification } = this.props.containers;
    const { data = {}, profile = {} } = this.props.auth;
    let _verification = null

    if (data.uuid && profile.verification_status === 0 ) {
      await verification.getDocumentsStatus(data.uuid);

      const { error, documentStatus } = verification.state;

      if(error) {
        toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))
      } else {
        const docsRejected = documentStatus.filter(item => item.status === -1)

        if (docsRejected && docsRejected.length > 0) {
          _verification = -1
        } else {
          _verification = 0
        }
      }
    }

    this.setState({
      docsStatus: _verification
    })
  }

  handleUploadDocuments() {
    this.props.navigation.navigate('VerificationDocuments', { goBack: 'InvitationsListScreen' })
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.list.data !== this.props.list.data ||
      nextProps.list.httpBusy !== this.props.list.httpBusy ||
      nextState.value !== this.state.value ||
      nextState.isFetching !== this.state.isFetching ||
      nextProps.auth.profile !== this.props.auth.profile ||
      nextState.docsStatus !== this.state.docsStatus
  }

  componentDidUpdate(prevProps) {
    if (prevProps.auth.profile !== this.props.auth.profile) {
      this.getDocuments()
    }
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.fetch();
    })

    this.getDocuments()
  }

  render() {
    const { data = [], meta, limit } = this.props.list;
    const { value, isFetching, docsStatus } = this.state;
    const { verification_status } = this.props.auth.profile || {};
    let message = undefined;

    if(docsStatus === -1) {
      message = lang.get('documents.rejectedDocuments', 'invitations.rejected_documents')
    } else if(docsStatus === 0) {
      message = lang.get('documents.checkingDocuments', 'invitations.checking_documents')
    } else if (verification_status && verification_status !== 1) {
      message = lang.get('documents.uploadDocuments', 'invitations.upload_documents')
    }

    return (
      <Container>

        <NavBarComponent
          {...this.props.navigation}
          name={lang.get('title.invitations', 'invitations.invitations')}
          onPressLeftButton={this.onPressLeftButton} />

        { verification_status !== 1 && message && <NoteValidationDocuments
          message={message}
          docsStatus={docsStatus}
          onUploadDocuments={this.handleUploadDocuments}
          />
        }

        {
          data !== undefined && <SearchBarComponent
            placeholder={lang.get('search.searchInvitations', 'invitations.search_invitations')}
            onChangeText={this.handleSearchChangeText}
            onSearchRequest={this.handleSearchInvitations}
            value={value}
            clear={this.clearText}
          />
        }
        <InvitationList
          data={data || []}
          hasMoreItems={!(meta.cursor.count === 0 || meta.cursor.count < limit)}
          onPressItem={this.handlePressItem}
          onHandleLoadMore={this.handleLoadMore}
          onRefresh={this.handleOnRefresh}
          refreshing={isFetching}
        />
      </Container>
    );
  }
}

const containers = {
  acceptance: AcceptanceContainer,
  list: ListContainer,
  auth: AuthContainer,
  verification: VerificationContainer
}

const mapStateToProps = (containers) => {
  return {
    list: containers.list.state,
    auth: containers.auth.state,
  }
}

export default connect(containers, mapStateToProps)(InvitationsListScreen);
