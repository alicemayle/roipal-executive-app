import { Container } from 'unstated';

import Invitation from '../services/InvitationService';

const initialState = {
  httpBusy: false,
  message: null,
  data: [],
  error: undefined,
}

class InvitationAcceptanceContainer extends Container {

    name = 'InvitationAcceptanceContainer';

    state = {
      ...initialState
    }

  async get(id, meta = {}) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Invitation info request starts'
      })

      const response = await Invitation.get(id, meta);

      if (response) {
        await this.setState({
          data: response.data,
          httpBusy: false,
          __action: 'Invitation info request success'
        })
      }
    } catch (error) {
      await this.setState({
        error,
        __action: 'Invitation info request error'
      })
    }
  }

  async accept(id, data) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Invitation reply request starts'
      })

      const response = await Invitation.acceptance(id, data);

      if (response) {
        await this.setState({
          httpBusy: false,
          __action: 'Invitation reply request success'
        })
      }
    } catch (error) {

      if (error.errors) {
        let messages = [];

        for (const [field, errors] of Object.entries(error.errors)) {
          messages = messages.concat(errors)
        }

        error.errors = messages
      }

      await this.setState({
        error,
        httpBusy: false,
        __action: 'Invitation reply request error'
      })
    }
  }

  setData(data) {
    this.setState({
      ...this.state,
      data,
      __action: 'Invitation set state data'
    })
  }

  clear() {
    this.setState({
      ...this.state,
      message: null,
      error: null
    })
  }

  reset() {
    this.setState({
      ...initialState
    })
  }
}

export default InvitationAcceptanceContainer;