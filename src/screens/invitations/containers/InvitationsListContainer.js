import { Container } from 'unstated';

import Invitation from '../services/InvitationService';

const initialState = {
  httpBusy: false,
  message: null,
  error: null,
  data: [],
  meta: {
    cursor: {
      current: 0,
    }
  },
  limit: 25
}

class InvitationsListContainer extends Container {

  state = { ...initialState }

  async fetch(meta = {}) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Fetch starts'
      })

      const response = await Invitation.list(meta);

      if (response) {
        await this.setState({
          message: response.message,
          httpBusy: false,
          data: response.meta.cursor.current === 1 ? response.data : this.state.data.concat(response.data),
          meta: response.meta,
          __action: 'Fetch success'
        })
      }
    } catch (error) {
      await this.setState({
        error,
        __action: 'Fetch error'
      })
    } finally {
      this.setState({
        httpBusy: false,
        __action: 'Fetch finally'
      })
    }
  }

  setDataManually(data) {
    this.setState({
      ...this.state,
      data,
      __action:'Set Data Manually'
    })
  }

  clearError() {
    this.setState({
      ...this.state,
      message: null,
      error: null
    })
  }

  reset() {
    this.setState({
      ...initialState
    })
  }
}

export default InvitationsListContainer;