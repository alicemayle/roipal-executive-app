import Http from '../../../shared/Http';
import Auth from '../../../shared/services/Auth';

class InvnitationService {

  list(params = {}) {
    const data = Auth.getUser();

    return Http.get(`api/executives/${data.uuid}/invitations`, { params });
  }

  get(id, params) {
    return Http.get(`api/invitations/${id}`, { params });
  }

  acceptance(id, data) {
    return Http.put(`api/invitations/${id}`, data);
  }

}

export default new InvnitationService;