import React, { Component } from 'react';
import { Container } from 'native-base';

import NavBarComponent from '../../components/navBar/NavBarComponent';
import lang from '../../shared/languages/Lang';
import PreviewVideo from '../../components/previewVideo/PreviewVideo';

class VideoInvitationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {

    const path = this.props.navigation.getParam('url')

    return (
      <Container>
        <NavBarComponent {...this.props.navigation} name={lang.get('profile.videoTitle', 'invitations.details.video_title')}></NavBarComponent>
        <PreviewVideo path={path} />
      </Container>
    );
  }
}


export default VideoInvitationScreen;