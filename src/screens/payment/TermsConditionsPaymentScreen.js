import React, { Component } from 'react';
import { Container, Content, Text, Body, CheckBox, ListItem, Form, Button, Spinner } from 'native-base';
import { ScrollView, Dimensions } from 'react-native';
import autobind from 'class-autobind';
import PromissoryNote from './sources/PromissoryNote';
import PromissoryNote2 from './sources/PromissoryNote2';
import payment from './containers/PaymentContainer';
import { connect } from 'unstated-enhancers';
import styles from '../../theme/globalStyles';
import lang from '../../shared/languages/Lang';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import ListErrors from '../../components/listErrors/ListErrors';
import Validator from 'validatorjs';
import auth from '../auth/containers/AuthContainer';
import checklist from '../checklist/containers/ChecklistContainer';
import toast from '../../shared/support/ShowToast';

const { height } = Dimensions.get('window');

class TermsConditionsPaymentScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      promissory: undefined,
      validation: new Validator(data, this.rules)
    }

    autobind(this);
  }

  get rules() {
    return {
      promissory: 'accepted',
    }
  }

  validate() {
    const { promissory } = this.props.containers.payment.state;
    const data = {
      promissory: promissory
    }

    const validation = new Validator(data, this.rules);

    this.validation = validation;

    validation.check();

    this.setState({
      validation
    })
  }

  async invertValue() {
    const { payment } = this.props.containers;
    const { promissory } = this.props.payment;

    await payment.setPromissory(!promissory);
  }

  async savePayment() {
    const { httpBusy } = this.props.payment;
    if (httpBusy) {
      return;
    }

    this.validate()

    const validation = this.validation;

    if (validation.passes()) {

      const { payment } = this.props.containers;
      const { promissory, data } = this.props.payment;

      const newData = {
        payment: {
          ...data
        },
        terms_accepted: promissory,
      }
      await payment.registrationPayment(newData);

      const { error } = this.props.payment;

      if (error !== undefined) {
        toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))

        setTimeout(() => {
          payment.clear();
        }, 1000)

      } else {
        const { data } = this.props.payment;
        const { auth, checklist } = this.props.containers;

        await auth.updatePayment(data.payments_profiles[0])
        await auth.setProfile(data.profile)
        // await checklist.setStepAsCompleted('payment');

        const { navigation } = this.props;
        const goBack = navigation.getParam('goBack');

        this.props.navigation.navigate(goBack);
      }
    } else {
      toast.ShowToast(lang.get('policy.title', 'payment.terms.title'))
    }
  }

  componentWillUnmount() {
    const { payment } = this.props.containers;
    payment.reset();
  }

  render() {
    const { promissory, flag, error = {} } = this.props.payment;
    let terms = undefined;
    let terms2 = false;

    if (lang.get('policies.terms_and_conditions', 'policies.terms_and_conditions') === 'policies.terms_and_conditions') {
      terms = PromissoryNote
      terms2 = true
    } else {
      terms = lang.get('policies.terms_and_conditions', 'policies.terms_and_conditions')
    }

    return (
      <Container style={styles.container} >
        <NavBarComponent {...this.props.navigation} name={lang.get('title.promissoryNote', 'payment.terms.promissory_note')} />

        <Content padder bounces={false}>
          <Form>
            <ScrollView style={[styles.borderWidth1, styles.lightGray, styles.padding10, { height: height *0.61 }]}>
              <Text style={[styles.textAlignJustify, { marginHorizontal:10 }]}> {terms} </Text>
              {
                terms2 &&
                <Text style={[styles.textAlignJustify, { marginHorizontal:10 }]}> {PromissoryNote2} </Text>
              }
            </ScrollView>

            <ListItem style={{ backgroundColor: '#fff' }} onPress={this.invertValue}>
              <CheckBox
                checked={promissory}
                onPress={this.invertValue}
              />
              <Body>
                <Text>{lang.get('policy.title', 'payment.terms.title')}</Text>
              </Body>
            </ListItem>

            <Text style={[styles.margin25, styles.textAlignCenter, styles.fontSize10]}>
              {lang.get('policy.privacy', 'payment.terms.privacy')}
            </Text>

            <Button
              block
              onPress={this.savePayment}
              style={styles.margin15}
            >
              <Text> {lang.get('button.save', 'generic.button.save')} </Text>
              {flag && <Spinner color='lightblue' />}
            </Button>

          </Form>
          {error.errors && (
            <ListErrors errors={error.errors} />
          )}
        </Content>
      </Container>
    );
  }

}

const containers = {
  payment: payment,
  auth: auth,
  checklist: checklist
}

const mapStateToProps = (containers) => {
  return {
    payment: containers.payment.state,
    auth: containers.auth.state
  };
}

export default connect(containers, mapStateToProps)(TermsConditionsPaymentScreen);