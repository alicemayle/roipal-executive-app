import React, { Component } from 'react';
import { Text, Container, Button, Spinner, Form, Icon, View, Picker } from 'native-base';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import Validator from 'validatorjs';
import creditCardType from 'credit-card-type';
import { Image, Platform } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import typeCard from '../../shared/support/TypeCardManagement';
import styles from '../../theme/globalStyles';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import lang from '../../shared/languages/Lang';
import { connect, Logger } from 'unstated-enhancers';
import autobind from 'class-autobind';
import TextInput from '../../components/inputs/TextInput';
import LabelError from '../../components/inputs/LabelError';
import payment from './containers/PaymentContainer';
import logoPaypal from '../../images/paypal/paypal.png';
import flagMexico from '../../images/flag_mexico/flag_mexico.png';
import language from '../../components/language/containers/LanguagesContainer';
import toast from '../../shared/support/ShowToast';

const initialState = {
  card_number: undefined,
  owner: undefined,
  name: undefined,
  surnames: undefined,
  validation: new Validator(data, this.rules),
  cardNumber: undefined,
  cardnumbervalidate: undefined,
  bank: undefined,
  bankSelected: undefined,
  bankConfirm: false,
  clabeValidate: undefined,
  ccv: undefined
}

class RegistrationPaymentScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...initialState,
      selectedTdc: false,
      selectedTransf: false,
      selectedPaypal: false,
    }

    autobind(this);
    this.inputs = {};
  }

  async reset() {
    await this.setState({
      ...initialState,
    })
  }

  validate() {
    let data = {
      ...this.state
    };

    let validation = {}

    // if (this.state.selectedTdc) {
    //   const card = this.state.card_number;

    //   if (card !== undefined) {
    //     const re = /-/g;
    //     const cardNumber = card.toString().replace(re, '');
    //     const passNumber = parseInt(cardNumber).toString();

    //     Logger.dispatch('number', passNumber)

    //     if (passNumber.length === 15 || passNumber.length === 16) {

    //       data = {
    //         ...data,
    //         card_number: passNumber,
    //       };

    //       this.setState({
    //         cardNumber: passNumber,
    //         cardnumbervalidate: true
    //       })
    //     }
    //   }
    //   validation = new Validator(data, this.rulesTdc);
    // }

    // if (this.state.selectedTransf) {
    validation = new Validator(data, this.rulesTransf);
    // }

    // if (this.state.selectedPaypal) {
    //   validation = new Validator(data, this.rulesPaypal);
    // }

    // if (this.state.selectedTransf || this.state.selectedPaypal || this.state.selectedTdc) {
    validation.setAttributeNames({
      // name: lang.get('account.name', 'payment.name'),
      // surnames: lang.get('account.surnames', 'payment.surnames'),
      // card_number: lang.get('payment.number', 'payment.number'),
      // email: lang.get('account.email', 'payment.email'),
      owner: lang.get('account.fullName', 'payment.full_name'),
      clabe: lang.get('payment.clabe', 'payment.clabe'),
      bank: lang.get('payment.bank', 'payment.bank'),
      // exp: lang.get('payment.exp', 'payment.exp'),
      // ccv: lang.get('payment.code', 'payment.code')
    });

    this.validation = validation;

    validation.check();

    this.setState({
      validation
    })
    // }
  }

  get rulesTdc() {
    return {
      'name': 'required|string',
      'surnames': 'required|string',
      'card_number': 'required|numeric',
      'bank': 'required|string',
      'exp': 'required|string',
      'ccv': 'required|numeric'
    }
  }

  get rulesTransf() {
    return {
      'owner': 'required|string',
      'clabe': 'required|numeric',
      'bank': 'required|string'
    }
  }

  get rulesPaypal() {
    return {
      'email': 'required|string|email',
      'owner': 'required|string',
    }
  }

  validateExp() {
    const { exp } = this.state;

    const month = new Date().getMonth();
    const year = new Date().getFullYear();

    if (exp) {
      const arrayDate = exp.split('/');

      if (parseInt(arrayDate[0]) > 12 || parseInt(arrayDate[0] < 1)) {
        this.setState({
          invalidExp: true
        })
        return false;
      }

      if (parseInt(arrayDate[1]) < parseInt(year.toString().substr(2, 3))) {
        this.setState({
          invalidExp: true
        })
        return false;
      } else if (parseInt(arrayDate[1]) == parseInt(year.toString().substr(2, 3))) {
        if (parseInt(arrayDate[0]) <= (month + 1)) {
          this.setState({
            invalidExp: true
          })
          return false;
        } else {
          this.setState({
            invalidExp: false
          })
          return true;
        }
      } else {
        this.setState({
          invalidExp: false
        })
        return true;
      }
    }
  }

  handleRefSet(field, component) {
    this.inputs[field] = component;
  }

  changeText(field, value) {
    this.handleIsEditing();

    this.setState({
      [field]: value
    });
  }

  changeNumbCard(init, value) {
    const cardType = value === undefined
      || value === ''
      || creditCardType(value).length === 0 ? undefined : creditCardType(value)[0].type;

    this.handleIsEditing();

    if (value.length === 4 && this.lastValue.length !== 5) {
      value += '-';
    }

    if (value.length === 9 && this.lastValue.length !== 10) {
      value += '-';
    }

    if (value.length === 14 && this.lastValue.length !== 15) {
      value += '-';
    }

    this.lastValue = value;

    if (typeCard.isAmericanExpressCard(cardType) && value.length <= 18) {
      this.changeText(init, value)

      if (this.state.ccv) {
        if (this.state.ccv.length === 4) {
          this.setState({
            ccvValidate: true
          })
        }
      }
    }

    if (!typeCard.isAmericanExpressCard(cardType) && value.length <= 19) {
      this.changeText(init, value)

      if (this.state.ccv) {
        if (this.state.ccv.length === 4) {
          this.setState({
            ccvValidate: false
          })
        }
      }
    }
  }

  changeTextDate(init, value) {
    this.handleIsEditing();

    const { invalidExp } = this.state;
    if (invalidExp) {
      this.setState({
        invalidExp: false
      })
    }

    if (value.length > 1) {
      if (value.length === 2 && this.lastValue.length !== 3) {
        value += '/';
      }
    }

    this.lastValue = value;

    if (value.length <= 5) {
      this.changeText(init, value)
    }
  }

  changeCCV(init, value) {
    this.handleIsEditing();

    const number = this.state.cardNumber;

    if (number) {
      const cardType = number === undefined
        || number === ''
        || creditCardType(number).length === 0 ? undefined : creditCardType(number)[0].type;

      if (!typeCard.isAmericanExpressCard(cardType) ? value.length <= 3 : value.length <= 4) {
        this.changeText(init, value)
        this.setState({
          ccvValidate: true
        })
      }
    } else {
      if (value.length <= 4) {
        this.changeText(init, value)
        this.setState({
          ccvValidate: true
        })
      }
    }
  }

  changeClabe(field, value) {
    if (value.length <= 18) {
      this.changeText(field, value)
    }
  }

  submitFromKeyboard() {
    this.goToTermsConditions(true)
  }

  async goToTermsConditions(validate = false) {
    const { httpBusy } = this.props.payment;
    const { selectedPaypal, selectedTdc, selectedTransf } = this.state;

    if (httpBusy) {
      return;
    }

    // if (!selectedPaypal && !selectedTdc && !selectedTransf) {
    //   toast.ShowToast(lang.get('info.paymentRequired', 'payment.payment_required));
    //   return;
    // }

    if (validate) {
      this.validate()
    }

    const validation = this.validation;

    if (validation.passes()) {

      let data = {}

      const { navigation } = this.props;
      const goBack = navigation.getParam('goBack');
      const { payment } = this.props.containers;

      // if (selectedTdc) {
      //   const cardNumber = this.state.cardNumber;
      //   const cardType = cardNumber === undefined
      //     || cardNumber === ''
      //     || creditCardType(cardNumber).length === 0 ? undefined : creditCardType(cardNumber)[0].type;

      //   if ((!typeCard.isAmericanExpressCard(cardType) && cardNumber.length === 16)
      //     || (typeCard.isAmericanExpressCard(cardType) && cardNumber.length === 15)) {
      //     this.setState({
      //       cardnumbervalidate: true
      //     })

      //     if ((!typeCard.isAmericanExpressCard(cardType) && this.state.ccv.length !== 3)
      //       || (typeCard.isAmericanExpressCard(cardType) && this.state.ccv.length !== 4)) {
      //       this.setState({
      //         ccvValidate: false
      //       })
      //       return;
      //     }

      //     const validExp = await this.validateExp();

      //     if (validExp) {
      //       data = {
      //         type_payment: 'card',
      //         owner: this.state.name.concat(' ', this.state.surnames),
      //         bank: this.state.bank,
      //         card_number: cardNumber,
      //         type_card: cardType,
      //         exp: this.state.exp,
      //         ccv: this.state.ccv
      //       }
      //     } else {
      //       return;
      //     }

      //   } else {
      //     this.setState({
      //       cardnumbervalidate: false
      //     })
      //     return;
      //   }
      // }

      // if (selectedTransf) {
      if ((this.state.clabe.length === 18)) {
        this.setState({
          clabeValidate: true
        })

        data = {
          type_payment: 'account',
          owner: this.state.owner,
          bank: this.state.bank,
          clabe: this.state.clabe,
          account_type: this.state.account_type
        }
      } else {
        this.setState({
          clabeValidate: false
        })
        return;
      }
      // }

      // if (selectedPaypal) {
      //   data = {
      //     type_payment: 'paypal',
      //     owner: this.state.owner,
      //     email: this.state.email
      //   }
      // }
      await payment.setData(data);
      this.props.navigation.navigate('TermsConditionsPaymentScreen', { goBack: goBack ? goBack : 'ChecklistScreen' });

    } else {
      toast.ShowToast(lang.get('message.dataRequired', 'generic.data_required'))
    }
  }

  async selectItem(item) {
    await this.reset();

    if (item === 'tdc') {
      await this.setState({
        selectedTdc: !this.state.selectedTdc,
      })
      if (this.state.selectedTdc) {
        this.setState({
          selectedTransf: false,
          selectedPaypal: false
        })

        const { banks } = this.props.payment;

        this.onSelectBank(banks[0].value)
      }
    }
    if (item === 'transfer') {
      await this.setState({
        selectedTransf: !this.state.selectedTransf,
      })
      if (this.state.selectedTransf) {
        this.setState({
          selectedTdc: false,
          selectedPaypal: false
        })

        const { banks } = this.props.payment;

        this.onSelectBank(banks[0].value)
      }
    }
    if (item === 'paypal') {
      await this.setState({
        selectedPaypal: !this.state.selectedPaypal,
      })
      if (this.state.selectedPaypal) {
        this.setState({
          selectedTransf: false,
          selectedTdc: false
        })
      }
    }
  }

  onSelectBank(bank) {
    if (bank) {
      const account_type = bank === 'Santander' ? 'SANTAN' : 'EXTRNA'

      this.setState({
        bank: bank,
        account_type: account_type
      })
    }
  }

  async onConfirmBank() {
    this.setState({
      bankSelected: true
    })

    const { bank } = this.state;

    if (bank) {
      await this.setState({
        bankConfirm: bank
      })
    }

    setTimeout(() => {
      this.setState({
        bankSelected: false
      })
    }, 200)
  }

  setPreviousBank() {
    setTimeout(() => {
      const { bankConfirm, bankSelected } = this.state;

      if (!bankSelected && bankConfirm)
        this.onSelectBank(bankConfirm)
    }, 100)
  }

  _renderItemBank(item, index) {
    return (
      <Picker.Item key={index} label={item.label} value={item.value} />
    )
  }

  async getBanks() {
    const { payment } = this.props.containers;

    const country = 'banks_MX'

    await payment.getBanks(country);

    const { error } = this.props.payment;

    if (error) {
      toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))
      setTimeout(() => {
        payment.clear();
      }, 200)
    } else {
      if (Platform.OS === 'android') {
        setTimeout(() => {
          if (!this.state.bank) {
            const { banks = [] } = this.props.payment;

            this.onSelectBank(banks[0].value)
          }
        }, 200)
      }
    }
  }

  clearUser() {
    this.setState({
      owner: undefined
    })
  }

  clearClabe() {
    this.setState({
      clabe: undefined
    })
  }

  handleIsEditing() {
    const { navigation } = this.props;

    const isEditing = navigation.getParam('isEditing');

    if (!isEditing) {
      navigation.setParams({
        isEditing: true
      });
    }
  }

  setNavigationGoBackConfirmationAlert() {
    this.props.navigation.setParams({
      alert: {
        title: lang.get('info.titleAlert', 'generic.title_alert'),
        message: lang.get('info.messageAlert', 'generic.message_alert')
      }
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.bank !== prevState.bank) {
      this.validate()
    }
  }

  componentDidMount() {
    this.setNavigationGoBackConfirmationAlert();
    this.getBanks()
  }

  render() {
    const httpBusy = false;
    const { card_number, validation, selectedTdc, selectedTransf, selectedPaypal, bank, invalidExp, account_type } = this.state;
    const cardType = card_number === undefined || card_number === '' || creditCardType(card_number).length === 0 ? undefined : creditCardType(card_number)[0].type;
    const cardCode = card_number === undefined || card_number === '' || creditCardType(card_number).length === 0 ? lang.get('payment.code', 'payment.code') : creditCardType(card_number)[0].code.name;
    const { banks } = this.props.payment;
    let countryIdiom = undefined;
    const { selected } = this.props.language;

    if (selected === 'es') {
      countryIdiom = 'México'
    }
    if (selected === 'fr') {
      countryIdiom = 'Mexique'
    }
    if (selected === 'en') {
      countryIdiom = 'Mexico'
    }

    Logger.count(this);

    return (
      <Container style={styles.container}>

        <NavBarComponent  {...this.props.navigation}
          name={lang.get('title.payment', 'payment.payment')} />

        <KeyboardAwareScrollView style={{ padding: 15 }} bounces={false}>
          {/* <Text style={{ textAlign: 'center', marginVertical: 20, color: 'gray' }}>
              {lang.get('payment.messagePayment', 'payment.message_payment)}
            </Text> */}
          {/* <TouchableOpacity
              style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray', flexDirection: 'row', paddingVertical: 10, alignItems: 'center', justifyContent: 'space-between', paddingRight: 20 }}
              onPress={() => this.selectItem('tdc')}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Icon name='card' style={{ color: '#5bc1be' }} />
                <Text style={{ marginLeft: 15 }}>{lang.get('payment.tdc', 'payment.tdc)}</Text>
              </View>
              <CheckBox checked={selectedTdc} onPress={() => this.selectItem('tdc')} />
            </TouchableOpacity> */}
          {/* {selectedTdc && <Form style={{ backgroundColor: '#f3f3f3' }}>
              <View style={{ flexDirection: 'row', padding: 10, marginTop: 10, alignItems: 'center' }}>
                <Image source={flagMexico} style={{ resizeMode: 'contain' }} />
                <Text style={{ marginLeft: 10 }}>México</Text>
              </View>
              {Platform.OS === 'android' && banks &&
                <View style={{ width: '100%', borderColor: 'lightgray', borderWidth: 1, marginVertical: 10 }}>
                  <Picker
                    mode="dropdown"
                    style={{ width: '100%' }}
                    onValueChange={this.onSelectBank}
                    selectedValue={bank}
                  >
                    {banks.map(this._renderItemBank)}
                  </Picker>
                </View>
              }
              {Platform.OS === 'ios' && banks &&
                <RNPickerSelect
                  placeholder={{
                    label: lang.get('payment.bank', 'payment.bank'),
                    value: undefined,
                  }}
                  items={banks}
                  value={bank}
                  onValueChange={this.onSelectBank}
                  onDonePress={this.onConfirmBank}
                  onClose={this.setPreviousBank}
                  onDownArrow={this.setPreviousBank}
                  style={{
                    ...styles.Picker,
                    inputIOS: {
                      fontSize: 16,
                      padding: 10,
                      borderWidth: 1,
                      borderColor: 'lightgray',
                      color: 'black',
                      marginVertical: 10
                    },
                    modalViewBottom: {
                      backgroundColor: 'white'
                    },
                    modalViewMiddle: {
                      backgroundColor: 'white',
                      borderColor: 'gray',
                      borderWidth: 0.2
                    },
                    iconContainer: {
                      top: 10,
                      right: 12
                    },
                  }}
                  doneText={lang.get('modal.done', 'generic.button.done')}
                  Icon={() => {
                    return <Icon name="arrow-dropdown" style={{ color: 'gray', fontSize: 24, marginTop: 10 }} />;
                  }}
                />
              }
              {this.state.validation.errors.errors.bank && !bank &&
                <LabelError message={this.state.validation.errors.errors.bank} />
              }
              <TextInput
                autoCapitalize={'words'}
                autoFocus={true}
                field="name"
                next="surnames"
                onRefSet={this.handleRefSet}
                refs={this.inputs}
                errors={validation.errors}
                returnKeyType={"next"}
                value={this.state.name}
                placeholder={lang.get('account.name', 'payment.name')}
                onBlur={this.validate}
                onChangeText={this.changeText}
              />
              <TextInput
                autoCapitalize={'words'}
                field="surnames"
                next="card_number"
                onRefSet={this.handleRefSet}
                refs={this.inputs}
                errors={validation.errors}
                returnKeyType={"next"}
                value={this.state.surnames}
                placeholder={lang.get('account.surnames', 'payment.surnames')}
                onBlur={this.validate}
                onChangeText={this.changeText}
              />
              <TextInput
                field="card_number"
                onRefSet={this.handleRefSet}
                refs={this.inputs}
                errors={validation.errors}
                placeholder={lang.get('payment.number', 'payment.number')}
                value={this.state.card_number}
                onBlur={this.validate}
                onChangeText={this.changeNumbCard}
                keyboardType="numeric"
                iconRight={
                  <Icon
                    type="FontAwesome"
                    name={typeCard.isMasterCard(cardType) ? 'cc-mastercard'
                      : typeCard.isDiscoverCard(cardType) ? 'cc-discover'
                        : typeCard.isVisaCard(cardType) ? 'cc-visa'
                          : typeCard.isAmericanExpressCard(cardType) ? 'cc-amex'
                            : 'credit-card-alt'} />}
              />
              {this.state.cardnumbervalidate === false &&
                <LabelError message={lang.get('message.cardNumber')} />
              }
              <TextInput
                field="exp"
                next="ccv"
                onRefSet={this.handleRefSet}
                refs={this.inputs}
                errors={validation.errors}
                returnKeyType={"next"}
                value={this.state.exp}
                placeholder={lang.get('payment.exp', 'payment.exp')}
                keyboardType="numeric"
                autoCorrect={false}
                onBlur={this.validate}
                onChangeText={this.changeTextDate}
              />
              {invalidExp &&
                <LabelError message={lang.get('info.invalidExp', 'payment.invalid_exp)} />
              }
              <TextInput
                field="ccv"
                onRefSet={this.handleRefSet}
                refs={this.inputs}
                errors={validation.errors}
                placeholder={cardCode}
                value={this.state.ccv}
                keyboardType="numeric"
                onBlur={this.validate}
                onChangeText={this.changeCCV}
              />
              {!this.state.ccvValidate && this.state.ccv &&
                <LabelError message={lang.get('payment.invalidCCV', 'payment.invalid_ccv)} />
              }
            </Form>
            } */}
          {/* <TouchableOpacity
              style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray', flexDirection: 'row', paddingVertical: 10, alignItems: 'center', justifyContent: 'space-between', paddingRight: 20 }}
              onPress={() => this.selectItem('transfer')}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Icon name='account-balance' type='MaterialIcons' style={{ color: '#5bc1be' }} />
                <Text style={{ marginLeft: 10 }}>{lang.get('payment.transfer', 'payment.payment')}</Text>
              </View>
              <CheckBox checked={selectedTransf} onPress={() => this.selectItem('transfer')} />
            </TouchableOpacity> */}
          {/* <Title style={{ textAlign: 'center', color: '#52bebb', fontSize: 25, marginBottom: 25, paddingTop: 25 }}>
                {lang.get('payment.transfer', 'payment.payment')}
              </Title> */}
          <Form>
            <View style={{ flexDirection: 'row', padding: 10, marginTop: 10, alignItems: 'center' }}>
              <Image source={flagMexico} style={{ resizeMode: 'contain' }} />
              <Text style={{ marginLeft: 10 }}>{countryIdiom}</Text>
            </View>
            {Platform.OS === 'android' && banks &&
              <View style={{ width: '100%', borderColor: 'lightgray', borderWidth: 1, marginVertical: 10 }}>
                <Picker
                  mode="dropdown"
                  style={{ width: '100%' }}
                  onValueChange={this.onSelectBank}
                  selectedValue={bank}
                >
                  {banks.map(this._renderItemBank)}
                </Picker>
              </View>
            }
            {Platform.OS === 'ios' && banks &&
              <RNPickerSelect
                placeholder={{
                  label: lang.get('payment.bank', 'payment.bank'),
                  value: undefined,
                }}
                items={banks}
                value={bank}
                onValueChange={this.onSelectBank}
                onDonePress={this.onConfirmBank}
                onClose={this.setPreviousBank}
                onDownArrow={this.setPreviousBank}
                style={{
                  ...styles.Picker,
                  inputIOS: {
                    fontSize: 16,
                    padding: 10,
                    borderWidth: 1,
                    borderColor: 'lightgray',
                    color: 'black',
                    marginVertical: 10
                  },
                  modalViewBottom: {
                    backgroundColor: 'white'
                  },
                  modalViewMiddle: {
                    backgroundColor: 'white',
                    borderColor: 'gray',
                    borderWidth: 0.2
                  },
                  iconContainer: {
                    top: 10,
                    right: 12
                  },
                }}
                doneText={lang.get('modal.done', 'generic.button.done')}
                Icon={() => {
                  return <Icon name="arrow-dropdown" style={{ color: 'gray', fontSize: 24, marginTop: 10 }} />;
                }}
              />
            }
            {this.state.validation.errors.errors.bank && !bank &&
              <LabelError message={this.state.validation.errors.errors.bank} />
            }
            <TextInput
              autoCapitalize={'words'}
              field="owner"
              next="clabe"
              onRefSet={this.handleRefSet}
              refs={this.inputs}
              returnKeyType={"next"}
              errors={validation.errors}
              value={this.state.owner}
              placeholder={lang.get('account.fullName', 'payment.full_name')}
              onBlur={this.validate}
              clearButtonMode={'while-editing'}
              onChangeText={this.changeText}
              iconRight={Platform.OS === 'android' && this.state.owner !== undefined
                ? <Icon name='close-circle' onPress={this.clearUser} style={{ color: 'lightgray' }} /> : undefined}
            />
            <TextInput
              field="clabe"
              keyboardType="numeric"
              onRefSet={this.handleRefSet}
              refs={this.inputs}
              errors={validation.errors}
              value={this.state.clabe}
              placeholder={lang.get('payment.clabe', 'payment.clabe')}
              onBlur={this.validate}
              clearButtonMode={'while-editing'}
              onChangeText={this.changeClabe}
              iconRight={Platform.OS === 'android' && this.state.clabe !== undefined
                ? <Icon name='close-circle' onPress={this.clearClabe} style={{ color: 'lightgray' }} /> : undefined}
            />
            {this.state.clabeValidate === false &&
              <LabelError message={lang.get('message.clabe', 'payment.message_clabe')} />
            }
          </Form>
          {/* <TouchableOpacity
              style={{ borderBottomWidth: 1, borderBottomColor: 'lightgray', flexDirection: 'row', paddingVertical: 10, alignItems: 'center', justifyContent: 'space-between', paddingRight: 20 }}
              onPress={() => this.selectItem('paypal')}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image source={logoPaypal} style={{ resizeMode: 'contain' }} />
                <Text style={{ marginLeft: 15 }}>{lang.get('payment.paypal', 'payment.paypal')}</Text>
              </View>
              <CheckBox checked={selectedPaypal} onPress={() => this.selectItem('paypal')} />
            </TouchableOpacity>
            {selectedPaypal && <Form style={{ backgroundColor: '#f3f3f3' }}>
              <TextInput
                autoCapitalize={'words'}
                autoFocus={true}
                field="owner"
                next="email"
                onRefSet={this.handleRefSet}
                refs={this.inputs}
                errors={validation.errors}
                returnKeyType={"next"}
                value={this.state.owner}
                placeholder={lang.get('account.fullName', 'payment.full_name')}
                onBlur={this.validate}
                onChangeText={this.changeText}
              />
              <TextInput
                field="email"
                onRefSet={this.handleRefSet}
                refs={this.inputs}
                errors={validation.errors}
                autoCapitalize="none"
                keyboardType="email-address"
                value={this.state.email}
                placeholder={lang.get('account.email', 'payment.email')}
                autoCorrect={false}
                onBlur={this.validate}
                onChangeText={this.changeText}
              />
            </Form>
            } */}
          <Button
            style={styles.margin25}
            block
            onPress={this.goToTermsConditions}
          >
            <Text> {lang.get('button.save', 'generic.button.save')} </Text>
            {httpBusy && <Spinner color='lightblue' />}
          </Button>

        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const containers = {
  payment: payment,
  language: language
}

const mapStateToProps = (containers) => {
  return {
    payment: containers.payment.state,
    language: containers.language.state
  };
}

export default connect(containers, mapStateToProps)(RegistrationPaymentScreen);