import { Container } from 'unstated';
import ProfileServices from '../../profile/services/ProfileServices';
import Auth from '../../../shared/services/Auth';
import {Logger} from 'unstated-enhancers';

const initialState = {
  data: undefined,
  httpBusy: false,
  error: undefined,
  promissory: undefined,
  flag: undefined
}

class PaymentContainer extends Container {
  state = { ...initialState };

  async getBanks(country) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get Banks starts'
      })

      const response = await ProfileServices.getBanks(country);

      if (response) {
        let catalogBanks = [];

        await response.data.map(item => {
          const data = {
            label: item,
            value: item,
          }
          catalogBanks.push(data);
        })

        await this.setState({
          banks: catalogBanks,
          __action: 'Get Banks success'
        })
      }
    } catch (error) {

      if(error.errors) {
        let messages = [];

        for (const [field, errors] of Object.entries(error.errors)) {
          messages = messages.concat(errors)
        }

        error.errors = messages
      }
      await this.setState({
        error: error,
        __action: 'Get Banks error'
      })
    }
    finally{
      setTimeout( () => {
        this.setState({
          httpBusy: false,
          __action: 'Get Banks finally'
        })
      }, 500) 
    } 
  }

  async registrationPayment(data) {
    try {
      this.setState({
        httpBusy: true,
        flag: true,
        __action: 'Payment request starts'
      })

      const response = await ProfileServices.updateProfile(data);

      if (response) {
        await this.setState({
          data: response.data,
          __action: 'Payment request success'
        })
      }
    } catch (error) {

      if(error.errors) {
        let messages = [];

        for (const [field, errors] of Object.entries(error.errors)) {
          messages = messages.concat(errors)
        }

        error.errors = messages
      }
      await this.setState({
        error: error,
        __action: 'Payment request error'
      })
    }
    finally{
      setTimeout( () => {
        this.setState({
          httpBusy: false,
          flag: false,
          __action: 'Payment finally'
        })
      }, 500 ) 
    } 
  }

  setPromissory(value) {
    this.setState({
        ...this.state,
        promissory: value
    })
  }

  setData(data) {
    this.setState({
        ...this.state,
        data: data,
        __action: 'Payment set data'
    })
  }

  clear() {
    this.setState({
      error: undefined,
      __action: 'Payment clear errors'
    })
  }

  reset() {
    this.setState({
      ...initialState,
      __action: 'Payment reset'
    })
  }
}

export default PaymentContainer;