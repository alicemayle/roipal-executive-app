import React, { Component } from 'react';
import { Container, Content, Text, Button, Spinner } from 'native-base';
import { connect } from 'unstated-enhancers';
import { SafeAreaView } from 'react-native';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import EditAccount from './containers/EditAccountContainer';
import Lang from '../../shared/languages/Lang';
import styles from '../../theme/globalStyles';
import autobind from 'class-autobind';
import DisableAccountComponent from './components/DisableAccountComponent';
import home from '../home/containers/HomeContainer';
import Observer from '../../shared/Observer';
import ModalDisableAccount from './components/ModalConfirmationDisableAccount';
import logout from '../auth/containers/LogoutContainer';
import signin from '../auth/signin/containers/SigninContainer';
import language from '../../components/language/containers/LanguagesContainer';
import authContainer from '../auth/containers/AuthContainer';
import toast from '../../shared/support/ShowToast';

class DisableAccountScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      optionSelected: 'disable',
      modalDisable: false
    }

    autobind(this)
  }

  async disableAccount() {
    const { optionSelected } = this.state;

    if (optionSelected === 'disable') {
      const { home, auth, editAccount } = this.props.containers;
      const { data } = home.state;

      const statusValue = {
        account_status: -1
      }

      await editAccount.statusAccount(statusValue)

      const { error, updateProfileStatus } = editAccount.state

      if (error) {
        toast.ShowToast(error.message || Lang.get('message.tryAgain', 'generic.try_again'))
      } else {
        const newData = data.map(item => {
          _item = { ...item }
          if (item.status === 0) {
            _item.status = -1;
          }
          return _item
        })
        await home.setData(newData);
        await auth.updateStateManually(updateProfileStatus);
        this.props.navigation.goBack();
      }
    } else {
      Observer.notify('delete_account')
    }
  }

  handleDisableOption(value) {
    this.setState({
      optionSelected: value
    })
  }

  handleModalDisable() {
    const inverse = !this.state.modalDisable;
    this.setState({ modalDisable: inverse });
  }

  render() {
    const { httpBusy } = this.props.edit;
    const { modalDisable, optionSelected } = this.state;

    return (
      <Container style={styles.container}>

        <NavBarComponent
          {...this.props.navigation}
          name={Lang.get('account.disable', 'account.disable.disable')} />

        <SafeAreaView style={{ flex: 1 }}>
          <Content bounces={false} style={{ margin: 15 }}>

            <DisableAccountComponent
              onDisableOption={this.handleDisableOption}
            />

            <Button
              style={styles.margin25}
              block
              onPress={this.handleModalDisable}>
              <Text> {Lang.get('button.save', 'generic.button.save')} </Text>
              {httpBusy && <Spinner color='white' />}
            </Button>

          </Content>
        </SafeAreaView>

        <ModalDisableAccount
          modalDisable={modalDisable}
          onCloseModalDisable={this.handleModalDisable}
          onDisableAccount={this.disableAccount}
          optionSelected={optionSelected}
        />
      </Container>
    )
  }
}

const containers = {
  editAccount: EditAccount,
  home: home,
  logout: logout,
  signin: signin,
  language: language,
  auth: authContainer
}

const mapStateToProps = (containers) => {
  return {
    edit: containers.editAccount.state,
    home: containers.home.state
  }
}

export default connect(containers, mapStateToProps)(DisableAccountScreen);