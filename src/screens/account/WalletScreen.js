import React, { Component } from 'react';
import { Container, View, Text, Card, CardItem, Body, Button, Icon, Title, Content } from 'native-base';
import autobind from 'class-autobind';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import { connect } from 'unstated-enhancers';
import AuthContainer from '../auth/containers/AuthContainer';
import lang from '../../shared/languages/Lang';
import LinearGradient from 'react-native-linear-gradient';
import styles from '../../theme/globalStyles';
import PaymentContainer from './containers/PaymentContainer';
import { Platform } from 'react-native';

class WalletScreen extends Component {
  constructor(props) {
    super(props);
    autobind(this);
    this.state = {
      payment: null,
      visible: false
    }
  }

  async getPayment() {
    const { uuid } = this.props.auth.data;
    const { payment } = this.props.containers;
    const { registered_payment } = this.props.auth.profile;

    this.setState({
      registered_payment: registered_payment
    })

    if (uuid) {
      await payment.getPayment(uuid)

      const { data } = this.props.containers.payment.state

      this.formatTotal(data !== undefined ? data.total : 0)
    }
  }

  async formatTotal(number) {
    if (number) {
      if (Platform.OS === 'android') {
        const Intl = require('react-native-intl');
        let totalFormat = await new Intl.NumberFormat('en-IN').format(parseFloat(number.toFixed(2)));
        this.setState({
          totalFormat: totalFormat
        })
      } else {
        this.setState({
          totalFormat: Intl.NumberFormat('en-IN').format(number.toFixed(2))
        })
      }
    }
  }

  goToPayment() {
    this.props.navigation.navigate('RegistrationPaymentScreen', { goBack: 'WalletScreen' })
  }

  goToDetails() {
    this.props.navigation.navigate('DetailsBalance')
  }

  async componentDidMount() {
    await this.getPayment()
  }

  render() {
    const { registered_payment, totalFormat } = this.state
    const { payment } = this.props.auth;
    const { data } = this.props.containers.payment.state

    return (
      <Container>
        <NavBarComponent {...this.props.navigation} name={lang.get('payment.wallet', 'account.payment.wallet')} />
        <LinearGradient
          colors={['#00a7e5', '#5bc1be']}
          start={{ x: 0, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={{
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <Card style={{ width: '75%', marginBottom: 15, borderRadius: 10 }}>
            <CardItem header style={{ borderRadius: 10 }}>
              <Text>{lang.get('payment.balance', 'account.payment.balance')}</Text>
            </CardItem>
            <CardItem style={{ borderRadius: 10 }}>
              <Body style={{ alignItems: 'center' }}>
                {
                  totalFormat &&
                  <Text style={{ fontWeight: 'bold', fontSize: 25 }}>
                    {'$ '}{totalFormat}{' MXN'}
                  </Text>
                }
              </Body>
            </CardItem>
          </Card>
        </LinearGradient>
        <Content>
          <View>
          </View>
          {
            registered_payment === 0 &&
            <View>
              <Icon name="warning" style={[styles.IconWarning, { textAlign: 'center', marginTop: 25 }]} />
              <Text style={{ textAlign: 'center', paddingTop: 30, color: '#6E6E6E' }}>{lang.get('payment.wallet_info', 'account.payment.wallet_info')}</Text>
              <Button
                style={{ marginTop: 30, marginHorizontal: 20 }}
                block
                onPress={this.goToPayment}
              >
                <Text>{lang.get('payment.register_account', 'account.payment.register_account')}</Text>
              </Button>
            </View>
          }
          {
            data &&
            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10 }}>
              <Button
                transparent
                light
                block
                onPress={this.goToDetails}
              >
                <Text style={{ textDecorationLine: 'underline', color: '#2b87ef' }}>{lang.get('payment.details_balance', 'account.payment.details_balance')}</Text>
              </Button>
            </View>
          }
          {
            registered_payment === 1 &&
            <View>
              <Title style={{ textAlign: 'center', color: '#52bebb', fontSize: 25, marginBottom: 17, paddingTop: 30 }}>
                {lang.get('payment.transfer', 'account.payment.transfer')} </Title>
              <View style={{ flexDirection: 'row', paddingHorizontal: 25, marginTop: 20 }}>
                <Icon name='account-balance' type='MaterialIcons' style={{ color: '#5bc1be', fontSize: 60 }} />
                <View style={{ marginHorizontal: 20 }}>
                  <Text style={{ fontWeight: 'bold', marginBottom: 5 }}>{payment.bank}</Text>
                  <Text>{payment.owner}</Text>
                  <Text>*****{payment.clabe}</Text>
                </View>
              </View>
              <Text style={{ textAlign: 'center', paddingTop: 30, color: '#6E6E6E', marginHorizontal: 20 }}>{lang.get('payment.info', 'account.payment.info')}</Text>
              <Button
                style={{ marginTop: 30, marginHorizontal: 20 }}
                block
                onPress={this.goToPayment}
              >
                <Text>{lang.get('payment.update', 'account.payment.update')}</Text>
              </Button>
            </View>
          }
        </Content>
      </Container>
    )
  }
}

const containers = {
  auth: AuthContainer,
  payment: PaymentContainer
}

const mapStateToProps = (containers) => {
  return {
    auth: containers.auth.state,
    payment: containers.payment.state
  };
}

export default connect(containers, mapStateToProps)(WalletScreen);