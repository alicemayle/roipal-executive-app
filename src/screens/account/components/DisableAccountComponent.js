import React, { PureComponent } from 'react';
import { View, Text, Icon, ListItem, CheckBox, Body } from 'native-base';
import autobind from 'class-autobind';
import Lang from '../../../shared/languages/Lang';

class DisableAccountComponent extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      disable: true,
      erase: false
    };
    autobind(this);
  }

  selectOption() {
    const disableValue = this.state.disable;
    const eraseValue = this.state.erase;

    this.setState({
      disable: !disableValue,
      erase: !eraseValue
    }, () => this.changeDisableOption())
  }

  changeDisableOption() {
    const { onDisableOption } = this.props;
    const { disable } = this.state;
    const value = disable ? 'disable' : 'delete'

    onDisableOption(value);
  }

  render() {
    const { disable, erase } = this.state;

    return (
      <View>
        {/* <ListItem
          onPress={() => this.selectOption()}>
          <CheckBox checked={disable}
            onPress={() => this.selectOption()} />
          <Body>
            <Text>{Lang.get('account.disable.option', 'account.disable.option)}</Text>
          </Body>
        </ListItem> */}
        {
          disable &&
          <View
            style={{ padding: 15, backgroundColor: 'rgba(142, 142, 147, 0.12)', alignItems: 'center' }}>
            <Icon
              name='warning'
              type='MaterialIcons'
              style={{ color: '#5bc1be', fontSize: 60 }}
            />
            <Text style={{ textAlign: 'justify', marginTop: 10 }}>
              <Text>{Lang.get('account.disable.message_one', 'account.disable.message_one')}{'\n'}{'\n'}</Text>
              <Text>{Lang.get('account.disable.message_two', 'account.disable.message_two')}{'\n'}{'\n'}</Text>
              <Text>{Lang.get('account.disable.message_three', 'account.disable.message_three')}</Text>
            </Text>
          </View>
        }

        {/* <ListItem
          onPress={() => this.selectOption()}>
          <CheckBox checked={erase}
            onPress={() => this.selectOption()} />
          <Body>
            <Text>{Lang.get('account.erase', 'account.disable.erase)}</Text>
          </Body>
        </ListItem> */}
        {
          erase &&
          <View
            style={{ padding: 15, backgroundColor: 'rgba(142, 142, 147, 0.12)', alignItems: 'center' }}>
            <Icon
              name='delete-forever'
              type='MaterialIcons'
              style={{ color: '#5bc1be', fontSize: 60 }}
            />
            <Text style={{ textAlign: 'justify', marginTop: 10 }}>
              {Lang.get('account.erase.message', 'account.erase.message')}
            </Text>
          </View>
        }
      </View>
    );
  }
}

export default DisableAccountComponent;
