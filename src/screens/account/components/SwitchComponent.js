import React, { PureComponent } from 'react';
import { Content, View, Text } from 'native-base';
import { Switch, Platform } from 'react-native';
import autobind from 'class-autobind';
import styles from '../../../theme/globalStyles';
import { Logger } from 'unstated-enhancers';

class SwitchComponent extends PureComponent {
  constructor(props) {
    super(props);
    autobind(this);
  }

  onchange() {
    const { onchange, name, name2 } = this.props;

    if (onchange) {
      onchange(name, name2);
    }
  }

  onchange2() {
    const { onchange2, name2, name } = this.props;

    if (onchange2) {
      onchange2(name2, name);
    }
  }

  render() {
    const { id, text, active, active2, disable } = this.props;

    return (
      <View style={{ flexDirection: 'row' }}>
        <View style={styles.RenderNotifiText}>
          <Text style={{ paddingLeft: '5%' }}>{text}</Text>
        </View>
        <View style={styles.RenderNotifi}>
          <Switch onValueChange={this.onchange} value={active} disabled={disable} />
        </View>
        <View style={styles.RenderNotifi}>
          <Switch onValueChange={this.onchange2} value={active2} disabled={disable} />
        </View>
      </View>
    )
  }
}

export default SwitchComponent;
