import React, { Component } from 'react';
import { View, Text, Left, Right, Body, Icon } from 'native-base';
import { TouchableOpacity } from 'react-native';
import autobind from 'class-autobind';


class EditOptionComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };

    autobind(this);
  }

  handleEdit() {
    const { onHandleEdit } = this.props;

    if (onHandleEdit) {
      onHandleEdit();
    }
  }

  handleNavigateToEdit() {
    const { onHandleNavigateToEdit } = this.props;

    if (onHandleNavigateToEdit) {
      onHandleNavigateToEdit();
    }
  }

  render() {
    const {
      height = 50,
      bgColor = "#fff",
      borderColor = "lightgrey",
      borderWidth = 1,
      textColor = "black",
      iconColor = "#52bebb",
      optionText = "Perfil personal",
      icon = "account-circle",
      wid
    } = this.props;

    return (
      <TouchableOpacity onPress={this.handleNavigateToEdit}>
        <View style={{
          height: height,
          backgroundColor: bgColor,
          width: "100%",
          borderColor: borderColor,
          borderTopWidth: wid,
          borderBottomWidth: borderWidth,
          flexDirection: "row",
          justifyContent: "space-around",
          alignItems: "center",
          paddingRight: 15
        }}>

          <View style={{
            flex: 1,
            flexDirection: "row",
            justifyContent: "flex-start",
            alignItems: "center"
          }}
          >
            <Icon style={{ color: iconColor, marginRight: 10 }} name={icon} type={'MaterialIcons'} />
            <Text style={{ color: textColor }}> {optionText} </Text>
          </View>
          <Icon style={{ color: 'gray', fontSize:20 }} name="arrow-dropright" />
        </View>
      </TouchableOpacity>
    );
  }
}

export default EditOptionComponent;
