import React, { PureComponent } from 'react';
import { Text, View, Button } from 'native-base';
import { Logger } from 'unstated-enhancers';
import autobind from 'class-autobind';

import lang from '../../../shared/languages/Lang';
import styles from '../../../theme/globalStyles';
import Modal from 'react-native-modal';
import { ScrollView } from 'react-native-gesture-handler';

class ModalDisableAccount extends PureComponent {
  constructor(props) {
    super(props);

    autobind(this);
  }

  async disable() {
    const { onCloseModalDisable, onDisableAccount } = this.props;

    await onCloseModalDisable();
    onDisableAccount();
  }

  render() {
    const { onCloseModalDisable, modalDisable, optionSelected } = this.props
    Logger.count(this);

    return (
      <Modal
        transparent={true}
        isVisible={modalDisable}
        onBackdropPress={onCloseModalDisable}
        hideModalContentWhileAnimating={true}
        presentationStyle={'overFullScreen'}
        useNativeDriver={true}
        style={[{ flex: 1, alignItems: 'center', margin: 0 }]}
      >
        <View style={styles.modal}>
          <Text style={{ textAlign: 'center', fontWeight: 'bold', marginBottom: 20 }}>
            {lang.get('info.titleAlert', ' generic.title_alert')}
          </Text>
          <ScrollView contentContainerStyle={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center'
          }}>
            <Text style={{ textAlign: 'justify' }}>
              { optionSelected === 'disable'
                ? lang.get('account.disable.confirmation', 'account.disable.disable_confirmation')
                : lang.get('account.erase.confirmation', 'account.disable.erase_confirmation')}
              {'\n'}
            </Text>

            <View
              style={{flexDirection: 'row', marginTop: 25, alignItems:'center'}}>
              <Button
                style={{marginRight: 15}}
                onPress={onCloseModalDisable}>
                <Text>{lang.get('profile.buttonBack', 'generic.button.back')}</Text>
              </Button>
              <Button
                success
                onPress={this.disable}>
                <Text>{lang.get('mission.confirm', 'generic.button.done')}</Text>
              </Button>

            </View>

          </ScrollView>
        </View>
      </Modal>
    )
  }
}

export default (ModalDisableAccount);