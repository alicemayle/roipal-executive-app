import React, { PureComponent } from 'react';
import { View, Text, Thumbnail, Icon, Card, CardItem } from 'native-base';
import autobind from 'class-autobind';
import avatar from '../../../images/picture/default-img.png';
import { TouchableOpacity } from 'react-native';
import {Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window');

class AccountPresentationCardComponent extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
    autobind(this);
  }

  handleImageSelectionOption() {
    const { onHandleImageSelectionOption, onHandleImageSelectIOS } = this.props;

    if (onHandleImageSelectionOption && Platform.OS === 'android') {
      onHandleImageSelectionOption();
    }

    if (onHandleImageSelectIOS && Platform.OS === 'ios') {
      onHandleImageSelectIOS();
    }
  }

  render() {
    const { name, photo, email, score } = this.props;

    return (
      <Card style={{ borderRadius: 10, width: '90%', marginBottom: 15 }}>
        <CardItem style={{ borderRadius: 10, justifyContent: 'center' }}>
          <TouchableOpacity onPress={this.handleImageSelectionOption}
            style={{width: 90, alignItems: 'center'}}>
              <Thumbnail large source={photo ? { uri: photo } : avatar} style={{
                borderColor: photo ? '#0285c9' : undefined,
                borderWidth: photo ? 3 : 0
              }} />
          </TouchableOpacity>
          <View style={{ justifyContent: 'center', alignItems: 'center', paddingHorizontal: 10, width: (width - (width*0.1) - 110)}}>
            <Text style={{ textAlign: 'center', marginTop: 5, color: "#000", fontSize: 20, fontWeight: 'bold' }}>{name}</Text>
            <Text style={{ textAlign: 'center', color: "#000", fontSize: 17 }}>{email}</Text>
            <View style={{ flexDirection: 'row' }}>
              <Text style={{ textAlign: 'center', color: "#000", textDecorationLine: 'underline' }}>{score}</Text>
              <Icon name='star' type='MaterialIcons' style={{ color: "#ffdf00", fontSize: 20 }} />
            </View>
          </View>
        </CardItem>
      </Card>
    );
  }
}

export default AccountPresentationCardComponent;
