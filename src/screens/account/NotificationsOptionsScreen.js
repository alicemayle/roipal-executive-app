import React, { Component } from 'react';
import { Content, Container, View, Text } from 'native-base';
import { Switch, Platform, FlatList } from 'react-native';
import autobind from 'class-autobind';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import LinearGradient from 'react-native-linear-gradient';
import Lang from '../../shared/languages/Lang';
import styles from '../../theme/globalStyles';
import permission from '../../shared/support/Permissions';
import { Logger, connect } from 'unstated-enhancers';
import SwitchComponent from './components/SwitchComponent';
import notificactions from './containers/NotificationsContainer';


class NotificationsOptionsScreen extends Component {
  constructor(props) {
    super(props);
    autobind(this);
    this.state = {
      permiso: false,
      disable: false,
      new_invitation: false,
      mission_canceled: false,
      executive_rejected: false,
      mission_initiated: false,
      new_invitation_email: false,
      mission_canceled_email: false,
      executive_rejected_email: false,
      mission_initiated_email: false,
      notification: [
        {
          text: 'Nueva invitacion',
          ident: '1',
          name: 'new_invitation',
          name2: 'new_invitation_email'
        },
        {
          text: 'Mision cancelada',
          ident: '2',
          name: 'mission_canceled',
          name2: 'mission_canceled_email'
        },
        {
          text: 'Ejecutivo rechazado',
          ident: '3',
          name: 'executive_rejected',
          name2: 'executive_rejected_email'
        },
        {
          text: 'Mision iniciada',
          ident: '4',
          name: 'mission_initiated',
          name2: 'mission_initiated_email'
        },
      ]
    }
  }

  async ChangeStatus(field, field2) {

    const { notificactions } = this.props.containers;

    const changeEmail = [field]+'_email';

    await this.setState({
      [field]: !this.state[field]
    })

    const data = {
      [field]: this.state[field]
    }

    if(this.state[field] === false && this.state[field2] === false){
      Logger.dispatch('this.state[field] === false && this.state[field2] === false', this.state[field] === false && this.state[field2] === false)
      await this.setState({
        [field2]: true
      })
    }

    if(this.state[field] === false && this.state[field2] === true){
      Logger.dispatch('this.state[field] === false && this.state[field2] === true', this.state[field] === false && this.state[field2] === true)
      await this.setState({
        [changeEmail]: true
      })
    }

    if (data) {
      notificactions.sendNotification(data)
    } else {
      Logger.dispatch('hubo un error y aqui deberia ir un toasr')
    }

  }

  async ChangeStatus2(field, field2) {

    const { notificactions } = this.props.containers;

    const changeMovil = [field].toString().replace('_email', '');

    await this.setState({
      [field]: !this.state[field]
    })

    if(this.state[field] === false && this.state[field2] === false){
      Logger.dispatch('this.state[field] === false && this.state[field2] === false', this.state[field] === false && this.state[field2] === false)
      await this.setState({
        [field2]: true
      })
    }

    if(this.state[field] === false && this.state[field2] === true){
      Logger.dispatch('this.state[field] === false && this.state[field2] === true', this.state[field] === false && this.state[field2] === true)
      await this.setState({
        [changeMovil]: true
      })
    }

    const data = {
      [field]: this.state[field]
    }

    if (data) {
      notificactions.sendNotification(data)
    } else {
      Logger.dispatch('hubo un error y aqui deberia ir un toasr')
    }

  }

  renderItem(item, index) {
    return (
      <SwitchComponent
        key={index}
        text={item.text}
        id={item.ident}
        onchange={this.ChangeStatus}
        onchange2={this.ChangeStatus2}
        disable={this.state.disable}
        name={item.name}
        name2={item.name2}
        active={this.state[item.name]}
        active2={this.state[item.name2]}
      />
    )
  }

  keyExtractor(item) {
    return item.ident;
  }

  changeGeneral() {
    const change = this.state.general;
    const disable = this.state.disable;

    this.setState({
      general: !change,
      disable: !disable
    })
  }

  activate(permiso){
    Logger.dispatch('entrando al metodo activate', permiso)
    this.setState({
      new_invitation: permiso,
      mission_canceled: permiso,
      executive_rejected: permiso,
      mission_initiated: permiso,
    })
  }

  async  componentDidMount(){
    await this.setState({
       permiso: true
     })
     this.activate(this.state.permiso);
  }

  render() {
    const { notification, general } = this.state;
    Logger.dispatch('state in render', this.state);
    Logger.count(this)

    return (
      <Container>
        <NavBarComponent {...this.props.navigation} name={Lang.get('notification.title')} />
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.NotifiText1} />
            <View style={styles.NotifiText}>
              <Text style={{ textAlign: 'center' }}>{Lang.get('notification.mobile')}</Text>
            </View>
            <View style={styles.NotifiText}>
              <Text style={{ textAlign: 'center' }}>{Lang.get('notification.email')}</Text>
            </View>
          </View>
          {notification.map(this.renderItem)}
      </Container>
    )
  }
}

const containers = {
  notificactions: notificactions,
}

const mapStateToProps = (containers) => {
  return {
    notificactions: containers.notificactions.state,
  };
}

export default connect(containers, mapStateToProps)(NotificationsOptionsScreen);