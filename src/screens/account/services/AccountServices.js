import http from "../../../shared/Http";
import Auth from "../../../shared/services/Auth";

class AccountServices {

  getProfile(params) {
    const uuid = Auth.getUser().uuid;

    return http.get(`/api/executives/${uuid}`, { params });
  }

  updatePassword(data) {
    const uuid = Auth.getUser().uuid;

    return http.put(`/api/executives/${uuid}`, data);
  }

  editAccount(data){
    const uuid = Auth.getUser().uuid;

    return http.put(`/api/user/${uuid}/account-status`, data);
  }

  getPayment(){
    return http.get(`/api/payment/executive/missions`);
  }
}
export default new AccountServices;
