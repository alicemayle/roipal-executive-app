import React, { Component } from 'react';
import { Container, View, Text, Content, Body, Right, Icon, Button } from 'native-base';
import autobind from 'class-autobind';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import { connect } from 'unstated-enhancers';
import PaymentContainer from './containers/PaymentContainer';
import { FlatList } from 'react-native';
import newLang from '../../shared/languages/Lang';

class DetailsBalance extends Component {
  constructor(props) {
    super(props);
    autobind(this);
    this.state = {
    }
  }

  close() {
    this.props.navigation.navigate('WalletScreen')
  }

  keyExtractor(item) {
    return item.uuid;
  }

  renderItem(item) {
    const total = item.item.symbol + ' ' + item.item.total + ' ' + item.item.currency

    return (
      <View style={{ flexDirection: 'row', marginBottom: 15 }}>
        <Icon name='ribbon' style={{ color: '#2b87ef', marginTop: 10, fontSize: 30 }} />
        <Body style={{ alignItems: 'flex-start', marginLeft: 15 }}>
          <Text note>{'01-06-2020'}</Text>
          <Text style={{ fontSize: 13 }}>{item.item.mission_name}</Text>
        </Body>
        <Right style={{ alignItems: 'flex-end' }}>
          <Text style={{ fontSize: 13 }}>{total}</Text>
        </Right>
      </View>
    )
  }

  render() {

    const { data } = this.props.containers.payment.state

    return (
      <Container>
        <NavBarComponent {...this.props.navigation} name={newLang.get('payment.details', 'account.payment.details')} />
        <Content style={{ padding: 20 }}>
          <FlatList
            data={data.missions}
            keyExtractor={this.keyExtractor}
            renderItem={this.renderItem}
          />
          <Button onPress={this.close} success block style={{ alignSelf: 'center', marginBottom: 35, marginTop: 15, width: '95%' }}>
            <Text>{newLang.get('button.cancel', 'generic.button.cancel')}</Text>
          </Button>
        </Content>
      </Container>
    )
  }
}

const containers = {
  payment: PaymentContainer
}

const mapStateToProps = (containers) => {
  return {
    payment: containers.payment.state
  };
}

export default connect(containers, mapStateToProps)(DetailsBalance);