import React, { Component } from 'react';
import { Container, Text, Button, View, Spinner, Thumbnail } from 'native-base';
import lang from '../../shared/languages/Lang';
import autobind from 'class-autobind';
import Validator from 'validatorjs';
import { TouchableOpacity, Platform } from 'react-native';
import ImageCropPicker from 'react-native-image-crop-picker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import { ProcessingManager } from 'react-native-video-processing';
import RNThumbnail from 'react-native-thumbnail';
import ImagePicker from 'react-native-image-picker';
import styles from '../../theme/globalStyles';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import { connect } from 'unstated-enhancers';
import infoProfile from './containers/EditAccountContainer';
import language from '../../components/language/containers/LanguagesContainer';
import checklist from '../checklist/containers/ChecklistContainer';
import observer from '../../shared/Observer';
import TextareaInput from '../../components/inputs/TextareaInput';
import TextInput from '../../components/inputs/TextInput';
import ListErrors from '../../components/listErrors/ListErrors';
import ModalSelected from '../../components/modal/ModalSelected';
import avatar from '../../images/avatar/person_executive.png';
import auth from '../auth/containers/AuthContainer';
import ThumbnailLink from '../../components/thumbnailLink/ThumbnailLink';
import MediaMetaData from '../../shared/processVideo/MediaMetaData';
import PrepareVideo from '../../shared/processVideo/PrepareVideo';
import toast from '../../shared/support/ShowToast';

const options = {
  width: 720,
  height: 1280,
  bitrateMultiplier: 3,
  saveToCameraRoll: true, // default is false, iOS only
  saveWithCurrentDate: true, // default is false, iOS only
  minimumBitrate: 300000,
};

class EditInfoProfileScreen extends Component {
  optionSelected = null;

  constructor(props) {
    super(props);

    this.state = {
      validation: new Validator(data, this.rules),
      load: undefined
    }

    autobind(this);
    this.inputs = {};
  }

  setModalVisible() {
    const inverse = !this.state.modalVisible;
    this.setState({ modalVisible: inverse });
  }

  validate() {
    const { infoProfile } = this.props.containers;
    const { initialRegion = {} } = infoProfile.state;
    const data = {
      ...this.state,
      newArea: initialRegion.address
    };
    const validation = new Validator(data, this.rules);

    validation.setAttributeNames({
      bio: lang.get('profile.description', 'edit.profile.description'),
      newArea: lang.get('complete.address', 'edit.profile.address'),
      phone: lang.get('complete.phone', 'edit.profile.phone')
    });

    this.validation = validation;

    validation.check();

    this.setState({
      validation
    })
  }

  get rules() {
    return {
      'name': 'required|string',
      'profile': {
        'bio': 'required|string',
        'phone': 'required|numeric', //TODO: falta validacion de maximo y minimo de caracteres, genera error,
      },
      'newArea': 'required|string',
    }
  }

  handleRefSet(field, component) {
    this.inputs[field] = component;
  }

  async updateProfile(validate = false) {
    const { httpBusy } = this.props.containers.infoProfile.state;
    if (httpBusy) return;

    if (validate) this.validate()

    const validation = this.validation;

    if (validation.passes()) {
      const { checklist, infoProfile, auth } = this.props.containers;
      const { newArea, initialRegion = {} } = infoProfile.state;
      const { bio, phone } = this.state.profile;
      const { name, thumbnail, mimeThumbnail } = this.state;

      const dataProfile = {
        bio: bio,
        address: newArea,
        name: name,
        position: {
          latitude: initialRegion.latitude,
          longitude: initialRegion.longitude
        },
        phone: phone
      }
      let formData = null;
      let formDataThumbnail = null;
      let config = null;

      if (this.state.video_bio_url || this.state.photo) {
        config = {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        }

        formData = new FormData();
      }

      if (this.state.video_bio_url) {
        formDataThumbnail = new FormData();

        this.setState({
          progress: true
        })

        const compressVideo = await ProcessingManager.compress(this.state.video_bio_url.path, options)

        this.setState({
          progress: false
        })

        formData.append('video', {
          name: this.state.video_bio_url.mime,
          type: this.state.video_bio_url.mime,
          uri: compressVideo.source
        });

        if(thumbnail) {
          formDataThumbnail.append('image', {
            name: mimeThumbnail,
            type: mimeThumbnail,
            uri: thumbnail
          });
        }
      }

      if (this.state.photo) {
        formData.append('image', {
          name: this.state.photo.mime,
          type: this.state.photo.mime,
          uri: this.state.photo.path
        });
      }
      await infoProfile.updateProfile(dataProfile, formData, config, formDataThumbnail);

      const { error } = infoProfile.state;

      if (error !== undefined) {
        toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))
        setTimeout(() => {
          infoProfile.clearError();
        }, 3000)

      } else {
        const { navigation } = this.props;
        const { auth, infoProfile } = this.props.containers;
        const { data } = this.props.containers.infoProfile.state;

        await auth.updateStateManually(data);

        const newData = {
          ...data,
          payment: {
            ...data.payments_profiles[0]
          }
        }
        await infoProfile.setInfoProfile(newData)

        await navigation.setParams({
            isEditing: false
          });
        await auth.setProfile(data.profile)
        this.props.navigation.goBack();
      }
    } else {
      toast.ShowToast(lang.get('message.dataRequired', 'generic.data_required'));

      const fields = Object.keys(this.rules);
      const values = {}

      for (const field of fields) {
        values[field] = this.state[field] || ''
      }

      this.setState({
        ...values
      })
    }
  }

  pickAddress() {
    this.props.navigation.navigate('MapScreenEditProfileScreen')
  }

  submitFromKeyboard() {
    this.updateProfile(true)
  }

  handleValueChange(field, value) {
    this.handleIsEditing();

    this.setState({
      [field]: value
    });
  }

  handleValueChangeProfile(field, value) {
    this.handleIsEditing();

    this.setState({
      profile: {
        ...this.state.profile,
        [field]: value
      }
    });
  }

  resetState() {
    this.setState({
      bio: undefined,
      newArea: undefined,
      phone: undefined,
      initialRegion: undefined,
      photo: undefined,
      video_bio_url: undefined,
      modalVisible: false,
      modalvideo: false,
    })
  }

  navigateVideo() {
    const { profile } = this.state;
    const { video_bio_url } = profile;
    const video_bio_url_state = this.state.video_bio_url;

    const url = video_bio_url_state ? video_bio_url_state.path : video_bio_url;

    this.props.navigation.navigate('VideoEditProfileScreen', { url: url });
  }

  handleSelectedOption(type) {
    if (!type) {
      return
    }

    const picker = type === 'gallery' ? 'openPicker' : 'openCamera';

    if (this.optionSelected === 'photo') {
      if (Platform.OS === 'android') {
        this.setState({
          modalVisible: false
        })
      }

      ImageCropPicker[picker]({
        width: 500,
        height: 500,
        cropping: true,
        cropperCircleOverlay: true,
        compressImageMaxWidth: 1000,
        compressImageMaxHeight: 1000,
        compressImageQuality: 1,
        compressVideoPreset: 'MediumQuality',
        includeExif: true,
      }).then(image => {

        this.setState({
          [this.optionSelected]: image,
          modalVisible: false,
        }, _ => this.optionSelected = null)

        this.handleIsEditing();
      }).catch(_ => { });

    } else {
      if (Platform.OS === 'android') {
        this.setState({
          modalVisible: false
        })
      }

      ImageCropPicker[picker]({
        mediaType: 'video'
      }).then(data => {

        RNThumbnail.get(data.path).then((result) => {

        this.setState({
          modalVisible: false,
          [this.optionSelected]: data,
          thumbnail: result.path
        }, _ => this.optionSelected = null)
      })
        this.handleIsEditing();
      }).catch(_ => { });
    }
  }

  handleVideoSelectionOption() {
    this.optionSelected = 'video_bio_url';

    this.setModalVisible();
  }

  handleImageSelectionOption() {
    this.optionSelected = 'photo';

    this.setModalVisible();
  }

  handleCloseModal() {
    this.setState({
      modalVisible: false
    })
  }

  handleIsEditing() {
    const { navigation } = this.props;

    const isEditing = navigation.getParam('isEditing');

    if (!isEditing) {
      navigation.setParams({
        isEditing: true
      });
    }
  }

  setNavigationGoBackConfirmationAlert() {
    this.props.navigation.setParams({
      alert: {
        title: lang.get('info.titleAlert', 'generic.title_alert'),
        message: lang.get('info.messageAlert', 'generic.message_alert')
      }
    });
  }

  componentWillMount() {
    const { infoProfile } = this.props.containers;

    this.setState({
      ...infoProfile.state.data,
      profile: {
        ...infoProfile.state.profile
      }
    })
    this.validate();

    observer.subscribe("seller/resultUpdateProfile", (data) => {
      const { checklist } = this.props.containers;

      if (data.result === 'Success') {
        checklist.setStepAsCompleted('profile');
        this.props.navigation.navigate('ChecklistScreen');
      } else {
        toast.ShowToast(data.message)
      }
    });
  }

  componentWillUnmount() {
    observer.unSubscribe("seller/resultUpdateProfile", (data) => {
      const { checklist } = this.props.containers;

      if (data.result === 'Success') {
        checklist.setStepAsCompleted('profile');
        this.props.navigation.navigate('ChecklistScreen');
      } else {
        toast.ShowToast(data.message)
      }
    });
  }

  componentDidMount() {

    this.setNavigationGoBackConfirmationAlert();
  }

  componentDidUpdate(prevProps) {
    if (this.props.infoProfile.initialRegion !== prevProps.infoProfile.initialRegion) {
      this.validate()
    }
  }

  selectVideoTapped() {
    const options = {
      title: lang.get('profile.videoPicker', 'edit.profile.image_picker'),
      takePhotoButtonTitle: lang.get('profile.takeVideo', 'edit.profile.take_image'),
      chooseFromLibraryButtonTitle: lang.get('profile.chooseLibrary', 'edit.profile.choose_library'),
      cancelButtonTitle: lang.get('profile.cancelButtonTitle', 'generic.button.cancel'),
      mediaType: 'video',
      videoQuality: 'medium',
      durationLimit: 60,
    };

    ImagePicker.showImagePicker(options, async (response) => {

      if (response.didCancel) {
        return
      } else if (response.error) {
        toast.ShowToast(response.error)
      } else if (response.customButton) {
        return
      } else {

        this.setState({
          load: true,
      })

      const metadata = await MediaMetaData.getMediaMeta(response.path);

          if (metadata.duration <= 60000) {

            const videoLoaded = await PrepareVideo.prepareVideo(response);

            this.setState({
              modalVisible: false,
              video_bio_url: {...videoLoaded.video},
              load: false,
              thumbnail: videoLoaded.thumbnail.path,
              mimeThumbnail: videoLoaded.thumbnail.mime
            })

            this.handleIsEditing();
          } else {
            toast.ShowToast(lang.get('profile.exceedsTimeVideo', 'edit.profile.exceeds_Time'))
            return
          }
      }
    });
  }

  render() {

    const { infoProfile } = this.props.containers;
    const { httpBusy, error = {}, initialRegion = {} } = infoProfile.state;
    const { validation, modalVisible, modalvideo, profile, name, photo, thumbnail, progress, load } = this.state;
    const { bio, phone, photo_bio_url, video_bio_url, thumbnail_video } = profile
    const video_bio_url_state = this.state.video_bio_url ? this.state.video_bio_url : video_bio_url;

    return (
      <Container style={styles.container}>
        <NavBarComponent {...this.props.navigation} name={lang.get('title.profile', 'edit.profile.profile')}>
        </NavBarComponent>
        <View
          style={{
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: 10
          }}
        >
          <View style={{ flexDirection: 'row' }}>
            <View style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              paddingVertical: 10,
            }} >
              <TouchableOpacity onPress={this.handleImageSelectionOption}>
                <View style={{ alignItems: 'flex-end', flexDirection: 'row' }}>
                  <Thumbnail source={photo ? { uri: photo.path } : photo_bio_url ? { uri: photo_bio_url } : avatar} style={{
                    height: 100,
                    width: 100,
                    borderRadius: 50,
                  }} />
                </View>
              </TouchableOpacity>
              <Text style={{ textAlign: 'center', marginTop: 10 }}>{lang.get('profile.logo', 'edit.profile.logo')} </Text>
            </View>
            <ThumbnailLink
              thumbnail={thumbnail ? thumbnail : thumbnail_video}
              videoLabel={lang.get('profile.video', 'edit.profile.video')}
              videoLoaded={lang.get('profile.playVideo', 'edit.profile.play_video')}
              isVideoLoaded={!!!!video_bio_url_state}
              onVideoSelectionOption={this.selectVideoTapped}
              onNavigateVideo={this.navigateVideo}
              progress={progress}
              load={load}
            />
          </View>

        </View>

        <KeyboardAwareScrollView style={{ padding: 10, }}>
          <View>
            <TextInput style={{ }}
              field="name"
              next="bio"
              value={name}
              refs={this.inputs}
              returnKeyType={"next"}
              placeholder={lang.get('account.fullName', 'edit.profile.full_name')}
              errors={validation.errors}
              onChangeText={this.handleValueChange}
              onRefSet={this.handleRefSet}
              onBlur={this.validate}
            />

            <TextareaInput
              field='bio'
              value={bio}
              rowSpan={5}
              bordered
              style={{ width: '100%', marginTop: 25, }}
              placeholder={lang.get('profile.placeholderDescription', 'edit.profile.placeholder_description')}
              errors={validation.errors}
              onChangeText={this.handleValueChangeProfile}
              onRefSet={this.handleRefSet}
              onBlur={this.validate}
            />


            <TextInput style={{ marginTop: 10 }}
              field="phone"
              next="newArea"
              value={phone.toString()}
              refs={this.inputs}
              returnKeyType={"next"}
              placeholder={lang.get('complete.phone', 'edit.profile.phone')}
              keyboardType="phone-pad"
              errors={validation.errors}
              onChangeText={this.handleValueChangeProfile}
              onRefSet={this.handleRefSet}
              onBlur={this.validate}
            />

            <TextInput
              field="newArea"
              refs={this.inputs}
              style={styles.flexDirectionRow_JustifyBetween}
              placeholder={lang.get('complete.address', 'edit.profile.address')}
              value={initialRegion.address}
              errors={validation.errors}
              onFocus={this.pickAddress}
              onRefSet={this.handleRefSet}
              onBlur={this.validate}
            />

          </View>

          <View>

            <Button
              style={[styles.margin25, { backgroundColor: load ? 'lightgray' : httpBusy ? 'lightgray' : progress ? 'lightgray' : '#0285c9', borderRadius: 4 }]}
              block
              onPress={this.updateProfile}
              disabled={load ? true : httpBusy ? true : progress ? true : false }
            >
              <Text> {lang.get('button.update', 'generic.button.update')} </Text>
              {httpBusy && <Spinner color='white' />}
            </Button>

          </View>

          {error.errors && (
            <ListErrors errors={error.errors} />
          )}
        </KeyboardAwareScrollView>

        <ModalSelected
          modalVisible={modalVisible}
          onSelectedOption={this.handleSelectedOption}
          onHandleCloseModal={this.handleCloseModal}
        />

      </Container>
    );
  }
}

const containers = {
  language: language,
  infoProfile: infoProfile,
  checklist: checklist,
  auth: auth
}

const mapStateToProps = (containers) => {
  return {
    infoProfile: containers.infoProfile.state,
  };
}

export default connect(containers, mapStateToProps)(EditInfoProfileScreen);
