import React, { Component } from 'react';
import { Container, } from 'native-base';

import NavBarComponent from '../../components/navBar/NavBarComponent';
import lang from '../../shared/languages/Lang';
import PreviewVideo from '../../components/previewVideo/PreviewVideo';

class VideoEditProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { navigation } = this.props;
    const url = navigation.getParam('url');

    return (
      <Container>
        <NavBarComponent {...this.props.navigation} name={lang.get('profile.videoTitle', 'account.profile.video_title')}></NavBarComponent>
        <PreviewVideo path={url} />
      </Container>
    );
  }
}

export default VideoEditProfileScreen;