import { Container } from 'unstated';
import { Logger } from 'unstated-enhancers';
import autobind from 'class-autobind';
import notifiService from '../../profile/services/ProfileServices';

const initialState = {
  httpBusy: false,
  error: undefined,
  data: undefined,
  success: false
};

class NotificationsContainer extends Container {

  state = { ...initialState }


  async sendNotification(data) {
    try {
      this.setState({
        httpBusy: true,
        error: undefined,
        __action: 'Notifications starts'
      });

      // const response = await notifiService.updateProfile(data);
      const response = await notifiService.updatePassword(data);

      if (response) {
        await this.setState({
          data: response.data,
          success: true,
          __action: 'Notifications success'
        });
      }

    } catch (error) {
      await this.setState({
        error: error,
        __action: 'Notifications error'
      });
    }
    finally {
      this.setState({
        httpBusy: false,
        __action: 'Notifications finally'
      });
    }
  }
}

export default NotificationsContainer;