import { Container } from 'unstated';
import autobind from 'class-autobind';
import payment from '../services/AccountServices'

const initialState = {
  httpBusy: false,
  data: undefined,
  error: undefined,
}

class PaymentContainer extends Container {

  constructor(data = {}) {
    super(data);

    this.state = {
      ...initialState
    }

    autobind(this);
  }

 async getPayment(uuid){
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get payment starts'
      })

      const response = await payment.getPayment();

      const newData = response.data.find(item => item.uuid === uuid)

      if(response) {
        await this.setState({
          data: newData,
          __action: 'Get payment setdata'
        })
      }

    } catch (error) {
      await this.setState({
        error: error,
        __action: 'Notifications error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'Get payment finally'
        });
      }, 500)
    }
  }

  clearError() {
    this.setState({
      ...this.state,
      error: undefined,
      clear: false,
      __action: ' Get payment clear error'
    })
  }

  reset() {
    this.setState({
      ...initialState,
      __action: 'Get payment reset'
    })
  }
}

export default PaymentContainer;