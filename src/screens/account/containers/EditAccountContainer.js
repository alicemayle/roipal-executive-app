import { Container } from 'unstated';
import AccountServices from '../services/AccountServices';
import ProfileServices from '../../profile/services/ProfileServices';
import autobind from 'class-autobind';
import { Logger } from 'unstated-enhancers';

const initialState = {
  httpBusy: false,
  data: undefined,
  error: undefined,
  success: undefined,
  message: undefined,
  limitations: undefined,
  updateProfileStatus: undefined,
  clear: false
}

class EditAccountContainer extends Container {

  constructor(data = {}) {
    super(data);

    this.state = {
      ...initialState
    }

    autobind(this);
  }

  async getProfile(params) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get Profile starts'
      })

      const response = await AccountServices.getProfile(params);

      if (response) {
        await this.setState({
          message: response.message,
          data: {
            ...this.state.data,
            ...response.data
          },
          initialRegion: {
            address: response.data.profile.address,
            ...response.data.profile.position

          },
          __action: 'Get Profile success'
        })
      } else {
        await this.setState({
          message: response.message,
          data: {
            ...this.state.data,
            ...response.data
          },
          __action: 'Get Profile success'
        })
      }

    } catch (error) {
      await this.setState({
        error: error,
        __action: 'Get Profile error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'Get Profile finally'
        });
      }, 700)
    }
  }

  saveProfile(data) {
    Logger.dispatch("------- saveProfile -------", data )
    this.setState({
      data: {
        ...this.state.data,
        ...data.data
      },
      initialRegion: {
        address: data.profile.address,
        ...data.profile.position
      },
      profile: {
        ...data.profile
      },
      payment: {
        ...data.payment
      },
      __action: 'Save Profile Profile success'
    })
  }

  pickPlace(data, address) {
    this.setState({
      ...this.state,
      initialRegion: {
        ...this.state.initialRegion,
        longitude: data.longitude,
        latitude: data.latitude,
      },
      newArea: address,
      clear: false,
      __action: 'Info profile pick place'
    })
  }

  setDataState(field, data) {
    this.setState({
      ...this.state,
      initialRegion: {
        ...this.state.initialRegion,
        [field]: data,
      },
      __action: 'Info profile set data state'
    })
  }

  async updateProfile(data, formData, config, formDataThumbnail) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Update Profile starts'
      })

      let urls = null;
      let urlThumbnail = null;

      if (formData) {
        urls = await ProfileServices.sendFile(formData, config);
      }
      if (formDataThumbnail) {
        urlThumbnail = await ProfileServices.sendFile(formDataThumbnail, config);
      }

      let media = {};
      let dataRequest = data;

      if (urls) {
        if (urls.video) {
          media = {
            ...media,
            video_bio_url: urls.video
          }
        }

        if (urls.image) {
          media = {
            ...media,
            photo_bio_url: urls.image
          }
        }

        if(urlThumbnail && urlThumbnail.image) {
          media = {
            ...media,
            thumbnail_video: urlThumbnail.image
          }
        }

        if (media !== {}) {
          dataRequest = {
            ...data,
            ...media
          }
        }
      }

      const response = await ProfileServices.updateProfile(dataRequest);

      if (response) {
        await this.setState({
          message: response.message,
          data: response.data,
          initialRegion: {
            address: response.data.profile.address,
            ...response.data.profile.position
          },
          __action: 'Update Profile setdata'
        })
      }
    } catch (error) {
      if (error.errors) {
        let messages = [];

        for (const [field, errors] of Object.entries(error.errors)) {
          messages = messages.concat(errors)
        }

        error.errors = messages
      }

      await this.setState({
        error: error,
        __action: 'Update Profile error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'Update Profile finally'
        });
      }, 500)
    }
  }

  setInfoProfile(data) {
    this.setState({
      data: {
        ...this.state.data,
        ...data
      },
      initialRegion: {
        address: data.profile.address,
        ...data.profile.position
      },
      profile: {
        ...data.profile
      },
      payment: {
        ...data.payment
      },
      __action: 'Set InfoProfile success'
    })
  }

  async listLimitations() {

    try {
      this.setState({
        httpBusy: true,
        __action: 'get list limitations profile starts'
      })

      const response = await ProfileServices.limitations();
      if (response) {
        const params = {
          includes: 'limitations,profile,payments_profiles'
        }

        await this.getProfile(params);

        const allLimitations = response.data;

        const profileLimitations = this.state.data.limitations;

        const limitationsUpdated = allLimitations.map(item => {

          const selected = profileLimitations.find(limit => limit.uuid === item.uuid) === undefined ? false : true;
          item = {
            ...item,
            selected
          }
          return item
        })

        await this.setState({
          limitations: limitationsUpdated,
          message: response.message,
          __action: 'get list limitations profile setdata'
        })
      }
    } catch (error) {

      await this.setState({
        error: error,
        __action: 'get list limitations  profile error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'get list limitations profile finally'
        });
      }, 500)
    }
  }

  getSelectedLimitations() {
    const selectedLimitations = this.state.limitations.filter(item => item.selected === true);
    const selectedNameLimitations = selectedLimitations.map(item => item.uuid)
    return selectedNameLimitations;
  }

  selectLimitation(item) {
    let nolimitation = false;
    const newArray = this.state.limitations.map((element) => {
      if (item.flag === 1) {
        nolimitation = true;
      }
      if (element.uuid === item.uuid) {
        element.selected = !element.selected
      }
      return element
    })

    if (nolimitation) {

      const newArrayLimitations = this.state.limitations.map((element) => {
        if (element.flag === 0) {
          element.selected = false
        }
        return element
      })
      this.setState({
        limitations: newArrayLimitations,
      })
    } else {

      const newArrayLimitations = this.state.limitations.map((element) => {
        if (element.flag === 1) {
          element.selected = false
        }
        return element
      })
      this.setState({
        limitations: newArrayLimitations,
      })
    }
  }

  async updateLanguage(data) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Update language starts'
      })

      const response = await ProfileServices.updateLanguage(data);

      if(response) {
        await this.setState({
          message: response.message,
          data: response.data,
          __action: 'Update language setdata'
        })
      }

    } catch (error) {
      if (error.errors) {
        let messages = [];

        for (const [field, errors] of Object.entries(error.errors)) {
          messages = messages.concat(errors)
        }

        error.errors = messages
      }

      await this.setState({
        error: error,
        httpBusy: false,
        __action: 'Update language error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'Update language finally'
        });
      }, 500)
    }
  }

  async statusAccount(data) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Update disable/delete account starts'
      })

      const response = await AccountServices.editAccount(data);

      Logger.dispatch('response', response)

      if(response) {
        await this.setState({
          message: response.message,
          updateProfileStatus: response.data,
          __action: 'Update disable/delete account setdata'
        })
      }

    } catch (error) {
      if (error.errors) {
        let messages = [];

        for (const [field, errors] of Object.entries(error.errors)) {
          messages = messages.concat(errors)
        }

        error.errors = messages
      }

      await this.setState({
        error: error,
        httpBusy: false,
        __action: 'Update disable/delete account error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'Update disable/delete account finally'
        });
      }, 500)
    }
  }

  setClearText(field, data) {
    this.setState({
      ...this.state,
      [field]: data,
      clear: true,
      __action: 'Edit Account map set clear text'
    })
  }

  clearError() {
    this.setState({
      ...this.state,
      error: undefined,
      clear: false,
      __action: ' Get Profile clear error'
    })
  }

  reset() {
    this.setState({
      ...initialState,
      __action: 'Edit Account reset'
    })
  }
}

export default EditAccountContainer;