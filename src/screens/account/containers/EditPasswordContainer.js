import { Container } from 'unstated';
import Password from '../services/AccountServices'
import { Logger } from 'unstated-enhancers';

const initialState = {
  httpBusy: false,
  error: undefined,
  data: undefined,
  success: false
};

class EditPasswordContainer extends Container {

  state = { ...initialState }

  async password(data) {
    try {
      this.setState({
        httpBusy: true,
        error: undefined,
        __action: 'Password starts'
      });

      const response = await Password.updatePassword(data);

      if (response) {
        await this.setState({
          data: response.data,
          success: true,
          __action: 'Password success'
        });
      }
    } catch (error) {
      await this.setState({
        error: error,
        __action: 'Password error'
      });
    } finally {
      this.setState({
        httpBusy: false,
        __action: 'Password finally'
      });
    }
  }

  clear() {
    this.setState({
      httpBusy: false,
      error: undefined,
      __action: 'Signin ends'
    });
  }

  clearError() {
    this.setState({
      error: undefined,
      __action: ' Get Profile clear error'
    })
  }

  reset() {
    this.setState({
      ...initialState,
      __action: 'Signin reset'
    })
  }
}

export default EditPasswordContainer;