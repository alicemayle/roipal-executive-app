import React, { Component } from 'react';
import { Container, Content, Text, View, Icon } from 'native-base';
import { connect, Logger } from 'unstated-enhancers';
import { Platform, Dimensions, SafeAreaView, NativeModules, TouchableOpacity, Alert } from 'react-native';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import EditAccount from './containers/EditAccountContainer';
import Lang from '../../shared/languages/Lang';
import AccountPresentationCardComponent from './components/AccountPresentationCardComponent';
import EditOptionComponent from './components/EditOptionComponent';
import styles from '../../theme/globalStyles';
import autobind from 'class-autobind';
import ImageCropPicker from 'react-native-image-crop-picker';
import ModalSelected from '../../components/modal/ModalSelected';
import LinearGradient from 'react-native-linear-gradient';
import authContainer from '../auth/containers/AuthContainer';
import LanguagesComponent from '../../components/language/LanguagesComponent';
import Validator from 'validatorjs';
import fr from 'validatorjs/src/lang/fr';
import es from 'validatorjs/src/lang/es';
import en from 'validatorjs/src/lang/en';
import Moment from '../../shared/services/Moment';
import language from '../../components/language/containers/LanguagesContainer';
import RNPickerSelect from 'react-native-picker-select';
import ImagePicker from 'react-native-image-picker';
import MimeFile from '../../shared/processVideo/MimeFile';
import homeContainer from '../../screens/home/containers/HomeContainer';
import toast from '../../shared/support/ShowToast';

const { width } = Dimensions.get('window');
class EditAccountScreen extends Component {
  optionSelected = null;

  constructor(props) {
    super(props);

    this.state = {
      modalVisible: false,
      value: undefined,
      valueLanguage: undefined,
      imm: undefined,
      missionStarted: false,
    }

    autobind(this)
  }

  handleGoToInfoProfile() {
    this.props.navigation.navigate('EditInfoProfileScreen')
  }

  handleGoToInfoDocuments() {
    this.props.navigation.navigate('VerificationDocuments', { goBack: 'EditAccountScreen' })
  }

  handleGoToPassword() {
    this.props.navigation.navigate('EditPasswordScreen')
  }

  goToNotifications() {
    this.props.navigation.navigate('NotificationsOptionsScreen')
  }

  handleGoToPayment() {
    const { registered_payment } = this.props.auth.profile;

    if (registered_payment === 0) {
      this.props.navigation.navigate('RegistrationPaymentScreen')
    } else {
      this.props.navigation.navigate('InfoPaymentScreen')
    }
  }

  handleGoToWallet() {
    this.props.navigation.navigate('WalletScreen')
  }

  componentDidUpdate(prevProps) {
    if (prevProps.auth.profile !== this.props.auth.profile) {
      this.getProfile()
    }
  }

  getLanguageCode() {
    let systemLanguage = 'en';
    if (Platform.OS === 'android') {
      systemLanguage = NativeModules.I18nManager.localeIdentifier;
    } else {
      systemLanguage = NativeModules.SettingsManager.settings.AppleLocale;
    }
    const languageCode = systemLanguage.substring(0, 2);
    return languageCode;
  }

  saveProfile() {
    const { auth, editAccount } = this.props.containers;
    const data = auth.state;

    editAccount.saveProfile(data);
  }

  async getProfile() {
    const profiless = this.props.containers.auth.state.profile;
    const imagen = profiless.photo_bio_url;

    this.setState({
      imm: imagen
    })
  }

  handleImageSelectionOption() {
    this.optionSelected = 'photo';

    this.setModalVisible();
  }

  setModalVisible() {
    const inverse = !this.state.modalVisible;
    this.setState({ modalVisible: inverse }, () => Logger.dispatch('inverse', this.state.modalVisible));
  }

  handleSelectedOption(type) {
    if (!type) {
      return
    }

    const picker = type === 'gallery' ? 'openPicker' : 'openCamera';

    if (this.optionSelected === 'photo') {
      if (Platform.OS === 'android') {
        this.setState({
          modalVisible: false
        })
      }

      ImageCropPicker[picker]({
        width: 500,
        height: 500,
        cropping: true,
        cropperCircleOverlay: true,
        compressImageMaxWidth: 1000,
        compressImageMaxHeight: 1000,
        compressImageQuality: 1,
        compressVideoPreset: 'MediumQuality',
        includeExif: true,
      }).then(image => {

        this.setState({
          [this.optionSelected]: image,
          modalVisible: false,
        }, _ => this.optionSelected = null)

        this.updateProfile();
      }).catch(_ => { });

    } else {
      if (Platform.OS === 'android') {
        this.setState({
          modalVisible: false
        })
      }

      ImageCropPicker[picker]({
        mediaType: 'video'
      }).then(data => {
        this.setState({
          modalVisible: false,
          [this.optionSelected]: data
        }, _ => this.optionSelected = null)

        this.updateProfile();
      }).catch(_ => { });
    }
  }

  handleCloseModal() {
    this.setState({
      modalVisible: false
    })
  }

  async selectImageTapped() {
    const options = {
      title: Lang.get('profile.imagePicker', 'account.image_picker'),
      takePhotoButtonTitle: Lang.get('profile.takeImage', 'account.take_image'),
      chooseFromLibraryButtonTitle: Lang.get('profile.chooseLibrary', 'account.choose_library'),
      cancelButtonTitle: Lang.get('profile.cancelButtonTitle', 'generic.button.cancel'),
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, async (response) => {
      if (response.didCancel) {
        return
      } else if (response.error) {
        toast.ShowToast(response.error)
      } else if (response.customButton) {
        return
      } else {

        const extImage = response.uri.split('.').pop();
        const mimeType = await MimeFile.getMime(extImage);

        const _photo = {
          mime: mimeType,
          path: response.uri
        }

        this.setState({
          photo: _photo,
        })

        this.updateProfile();
      }
    })
  }

  async updateProfile() {
    if (this.state.photo) {
      const { editAccount } = this.props.containers;

      let formData = null;
      let config = null;

      if (this.state.video_bio_url || this.state.photo) {
        config = {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        }

        formData = new FormData();
      }
      formData.append('image', {
        name: this.state.photo.mime,
        type: this.state.photo.mime,
        uri: this.state.photo.path
      });

      await editAccount.updateProfile(null, formData, config);

      const { error, data } = editAccount.state;

      if (error !== undefined) {
        toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))
        setTimeout(() => {
          editAccount.clearError();
        }, 3000)
      } else {
        const { auth } = this.props.containers;

        auth.setProfile(data.profile)
      }
    }
  }

  async handleValueChangeLanguage(idiom) {
    let value = idiom

    this.setState({
      selectedLanguage: true
    })

    const { language, editAccount } = this.props.containers;

    if (Platform.OS === 'android') {
      this.setLanguage(value)
    }

    if (!value) {
      value = this.state.valueLanguage
    }

    setTimeout(() => {

      let resorce = en;

      if (value === 'es') {
        resorce = es;
      }

      if (value === 'fr') {
        resorce = fr;
      }

      Validator.setMessages(value, resorce);

      Validator.useLang(value);

      Lang.setLocale(value);

      language.updateState('selected', value);

      Moment.setLanguaje(value);

      setTimeout(() => {
        this.setState({
          selectedLanguage: false
        })
      }, 200)

    }, 10)

    const { data = {} } = editAccount.state;
    const { email } = data;

    const dataLanguage = {
      email: email,
      lang: value
    }

    await editAccount.updateLanguage(dataLanguage);
  }

  setLanguage(value) {
    if (value) {
      this.setState({
        valueLanguage: value
      })
    } else {
      const { selected } = this.props.containers.language.state;

      this.setState({
        valueLanguage: selected
      })
    }
  }

  setPreviousLanguage() {
    setTimeout(() => {
      const { selected } = this.props.containers.language.state;

      if (!this.state.selectedLanguage) {
        this.setState({
          valueLanguage: selected
        })
      }
    }, 100)
  }

  goDisableAccount() {
    const { missionStarted } = this.state;

    if (missionStarted) {
      Alert.alert(
        Lang.get('account.dont_disable.title', 'account.dont_disable.title'),
        Lang.get('account.dont_disable.message', 'account.dont_disable.message')
      )
    } else {
      this.props.navigation.navigate('DisableAccountScreen')
    }
  }

  async enableAccount() {
    const { editAccount, auth } = this.props.containers;
    const { error } = editAccount.state

    const statusValue = {
      account_status: 1
    }

    await editAccount.statusAccount(statusValue)

    if (error) {
      toast.ShowToast(error.message || Lang.get('message.tryAgain', 'generic.try_again'))
    } else {
      const { updateProfileStatus } = editAccount.state
      await auth.updateStateManually(updateProfileStatus);
      toast.ShowToast(Lang.get('account.infoAccount', 'account.info_account'))
    }
  }

  getMissionsStarted() {
    const { data } = this.props.home;

    const started = data.filter(mission => mission.status === 1)
    const missionStarted = started.length > 0 ? true : false

    this.setState({
      missionStarted: missionStarted
    })
  }

  componentDidMount() {
    this.getProfile();
    this.saveProfile();
    this.setLanguage();
    this.getMissionsStarted();
  }

  render() {
    const { editAccount, auth } = this.props.containers;
    const { data = {} } = editAccount.state;
    const { name, email, profile = {} } = data;
    const { score = 0 } = profile;
    const { modalVisible, photo, valueLanguage } = this.state;
    const imagen = this.state.imm;
    const image = photo ? photo.path : imagen;
    const statusProfile = auth.state.data.account_status
    const items = [
      {
        label: Lang.get('language.english', 'generic.languaje.english'),
        value: 'en',
      },
      {
        label: Lang.get('language.spanish', 'generic.languaje.spanish'),
        value: 'es',
      },
      {
        label: Lang.get('language.french', 'generic.languaje.french'),
        value: 'fr',
      },
    ]
    const { selected } = this.props.containers.language.state;

    return (
      <Container style={styles.container}>

        <NavBarComponent
          {...this.props.navigation}
          name={Lang.get('title.update', 'account.update')} />

        <LinearGradient
          colors={['#00a7e5', '#5bc1be']}
          start={{ x: 0, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={{
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <AccountPresentationCardComponent
            email={email}
            name={name}
            photo={image}
            score={score}
            onHandleImageSelectionOption={this.handleImageSelectionOption}
            onHandleCloseModal={this.handleCloseModal}
            onHandleImageSelectIOS={this.selectImageTapped}
          />
        </LinearGradient>

        <SafeAreaView style={{ flex: 1 }}>
          <Content bounces={false} style={{ margin: 15 }}>

            <View style={{ paddingBottom: 20 }}>
              <Text style={styles.editText} >{Lang.get('account.Myaccount', 'account.my_account')}</Text>
              <EditOptionComponent optionText={Lang.get('account.optionPersonalProfile', 'account.option.personal_profile')} onHandleNavigateToEdit={this.handleGoToInfoProfile} icon={'person'} wid={1} />
              <EditOptionComponent optionText={Lang.get('documents.title', 'account.documents')} onHandleNavigateToEdit={this.handleGoToInfoDocuments} icon={'folder-shared'} />
              <EditOptionComponent optionText={Lang.get('account.optionPassword', 'account.option.password')} onHandleNavigateToEdit={this.handleGoToPassword} icon={'lock'} />
            </View>

            {/* TO DO: Descomentar cuando esten listos los endpoints de notificaciones
          <Text style={styles.editText} >{Lang.get('account.Notifications', 'account.notifications')}</Text>

          <View style={{  }}>
              <EditOptionComponent optionText={Lang.get('account.Notifications', 'account.notifications')} onHandleNavigateToEdit={this.goToNotifications} icon={'notifications-active'} wid={1}/>
          </View> */}

            {/* <View style={{ paddingBottom: 20, paddingTop: 15 }}>
              <Text style={styles.editText} >{Lang.get('account.card', 'account.card')}</Text>
              <EditOptionComponent optionText={Lang.get('account.optionPayment', 'account.option.payment')} onHandleNavigateToEdit={this.handleGoToPayment} icon={'credit-card'} wid={1} />
            </View> */}

            <View style={{ paddingBottom: 20, paddingTop: 15 }}>
              <Text style={styles.editText} >{Lang.get('payment.wallet', 'account.payment.wallet')}</Text>
              <EditOptionComponent optionText={Lang.get('payment.wallet', 'account.payment.wallet')} onHandleNavigateToEdit={this.handleGoToWallet} icon={'account-balance-wallet'} wid={1} />
            </View>

            <View>
              <Text style={styles.editText} >{Lang.get('account.Language', 'account.languaje')}</Text>
              <View style={{ flexDirection: 'row', justifyContent: 'center', borderColor: 'lightgrey', borderBottomWidth: 1, borderTopWidth: 1 }}>
                {Platform.OS === 'android' &&
                  <LanguagesComponent selected={selected} onValueChange={this.handleValueChangeLanguage} wid={width - 65} />
                }
                {
                  Platform.OS === 'ios' &&
                  <View style={{ flexDirection: 'row', width: '100%' }}>
                    <Icon name='language' type='FontAwesome' style={{ color: '#52bebb', fontSize: 22, marginRight: 20, paddingTop: 12 }} />

                    <RNPickerSelect
                      placeholder={{
                        label: Lang.get('modal.language', 'account.chose_language'),
                        value: undefined,
                      }}
                      items={items}
                      value={valueLanguage}
                      onValueChange={this.setLanguage}
                      onDonePress={this.handleValueChangeLanguage}
                      onClose={this.setPreviousLanguage}
                      onDownArrow={this.setPreviousLanguage}
                      style={{
                        inputIOS: {
                          color: 'black',
                          paddingVertical: 15,
                          width: width * 0.8,
                          fontSize: 16
                        },
                        modalViewBottom: {
                          backgroundColor: 'white'
                        },
                        modalViewMiddle: {
                          backgroundColor: 'white',
                          borderColor: 'gray',
                          borderWidth: 0.2
                        },
                        iconContainer: {
                          top: 15,
                          right: 10,
                        },
                      }}
                      Icon={() => {
                        return <Icon name="arrow-dropdown" style={{ color: 'gray', fontSize: 20 }} />;
                      }}
                      doneText={Lang.get('modal.done', 'generic.button.done')}
                    />
                  </View>
                }
              </View>

              {
                statusProfile !== -1 &&
                <TouchableOpacity
                  style={{ width: '100%', height: 30, marginVertical: 30, justifyContent: 'center' }}
                  onPress={this.goDisableAccount}>
                  <Text style={{ width: '100%', color: '#0285c9' }}>
                    {Lang.get('account.disable', 'edit.disable')}
                  </Text>
                </TouchableOpacity>
              }
              {
                statusProfile === -1 &&
                <TouchableOpacity
                  style={{ width: '100%', height: 30, marginVertical: 30, justifyContent: 'center' }}
                  onPress={this.enableAccount}>
                  <Text style={{ width: '100%', color: '#0285c9' }}>
                    {Lang.get('account.enable', 'account.enable')}
                  </Text>
                </TouchableOpacity>
              }
            </View>
          </Content>
        </SafeAreaView>
        <ModalSelected
          modalVisible={modalVisible}
          onSelectedOption={this.handleSelectedOption}
          onHandleCloseModal={this.handleCloseModal}
        />
      </Container>
    )
  }
}

const containers = {
  editAccount: EditAccount,
  auth: authContainer,
  language: language,
  home: homeContainer
}

const mapStateToProps = (containers) => {
  return {
    edit: containers.editAccount.state,
    auth: containers.auth.state,
    language: containers.language.state,
    home: containers.home.state
  }
}

export default connect(containers, mapStateToProps)(EditAccountScreen);