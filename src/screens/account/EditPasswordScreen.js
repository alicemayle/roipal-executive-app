import React, { Component } from 'react';
import { Text, Container, Button, Content, Spinner, Icon } from 'native-base';
import { View, Image } from 'react-native';
import Validator from 'validatorjs';
import styles from '../../theme/globalStyles';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import lang from '../../shared/languages/Lang';
import { connect } from 'unstated-enhancers';
import EditPasswordContainer from './containers/EditPasswordContainer'
import autobind from 'class-autobind';
import TextInput from '../../components/inputs/TextInput';
import LabelError from '../../components/inputs/LabelError';
import toast from '../../shared/support/ShowToast';

class EditPasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: undefined,
      newPassword: undefined,
      confirmPassword: undefined,
      showPassword: true,
      showNewPassword: true,
      showConfirmPassword: true,
      iconPasswordAndroid: 'md-eye-off',
      iconPasswordIos: 'ios-eye-off',
      iconNewPasswordAndroid: 'md-eye-off',
      iconNewPasswordIos: 'ios-eye-off',
      iconConfirmPasswordAndroid: 'md-eye-off',
      iconConfirmPasswordIos: 'ios-eye-off',
      validation: new Validator(data, this.rules),
      flagerror: undefined,
    }
    this.inputs = {};
    autobind(this);
  }

  validate() {

    const validation = new Validator(this.state, this.rules);

    validation.setAttributeNames({
      password: lang.get('account.currentPass', 'account.password.current_pass'),
      newPassword: lang.get('account.newPass', 'account.password.new_pass'),
      confirmPassword: lang.get('account.confirmPass', 'account.password.confirm_pass')
    });

    if(lang.getLocale() === 'es') {
      let messages = Validator.getMessages('es');
      messages.same = 'El campo :attribute y :same deben coincidir.';
      Validator.setMessages('es', messages);
    }


    this.validation = validation;

    validation.check();

    this.setState({
      validation
    })
  }

  get rules() {
    return {
      'password': 'required|string|min:8',
      'newPassword': 'required|string|min:8',
      'confirmPassword': 'same:newPassword',
    }
  }

  changeIconPassword() {
    const { showPassword } = this.state;

    const showPass = !showPassword;
    let iconShowPassAndroid = !showPass ? "ios-eye" : "ios-eye-off";
    let iconShowPassIos = !showPass ? "md-eye" : "md-eye-off";

    this.setState({
      showPassword: showPass,
      iconPasswordAndroid: iconShowPassAndroid,
      iconPasswordIos: iconShowPassIos
    });
  }

  changeIconNewPassword() {
    const { showNewPassword } = this.state;

    const showPass = !showNewPassword;
    let iconShowPassAndroid = !showPass ? "ios-eye" : "ios-eye-off";
    let iconShowPassIos = !showPass ? "md-eye" : "md-eye-off";

    this.setState({
      showNewPassword: showPass,
      iconNewPasswordAndroid: iconShowPassAndroid,
      iconNewPasswordIos: iconShowPassIos
    });
  }

  changeIconConfirmPassword() {
    const { showConfirmPassword } = this.state;

    const showPass = !showConfirmPassword;
    let iconShowPassAndroid = !showPass ? "ios-eye" : "ios-eye-off";
    let iconShowPassIos = !showPass ? "md-eye" : "md-eye-off";

    this.setState({
      showConfirmPassword: showPass,
      iconConfirmPasswordAndroid: iconShowPassAndroid,
      iconConfirmPasswordIos: iconShowPassIos
    });
  }

  setFieldValue(field, value) {
    this.setState({
      [field]: value,
      flagerror: false
    })
  }

  async update(validate = false) {

    const NewPass = this.state.newPassword;

    const regex = /(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d){8,}.+$)/;
    const valor1 = regex.test(NewPass);

    if(valor1){
      if (validate) this.validate();

      const validation = this.validation;

      if (validation.passes()) {
        const { EditPasswordContainer } = this.props.containers;
        const { password, newPassword, confirmPassword } = this.state;

          const credentials = {
            password: password,
            password_new: newPassword,
          }

          await EditPasswordContainer.password(credentials);

          const { error } = EditPasswordContainer.state;

          if (error !== undefined) {
            toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))
            setTimeout(() => {
              EditPasswordContainer.clearError();
            }, 3000)

          } else {
            this.props.navigation.navigate('EditAccountScreen');
          }
      } else {
        toast.ShowToast(lang.get('message.dataRequired', 'generic.data_required'));
        const fields = Object.keys(this.rules);
        const values = {}

        for (const field of fields) {
          values[field] = this.state[field] || ''
        }

        this.setState({
          ...values
        })
      }
    }else{
      this.setState({
        flagerror: true
      })
    }
  }

  handleRefSet(field, component) {
    this.inputs[field] = component;
  }

  render() {
    const { password, newPassword, confirmPassword, validation, flagerror } = this.state;
    const { showPassword, showNewPassword, showConfirmPassword } = this.state;
    const { iconPasswordAndroid, iconPasswordIos, iconNewPasswordAndroid, iconNewPasswordIos, iconConfirmPasswordAndroid, iconConfirmPasswordIos } = this.state;
    const { httpBusy } = this.props.containers.EditPasswordContainer.state;

    return (
      <Container style={styles.container}>

        <NavBarComponent  {...this.props.navigation}
          name={'Password'} />
        <View style={{ flexDirection: 'row', justifyContent: 'center', paddingTop: 35 }}>
          <Image style={styles.logoLimita} source={require('../../images/logo_colores/logo_colores.png')} />
        </View>

        <View style={{ paddingTop: 25, paddingBottom: 25 }}>
          <Text style={{ textAlign: 'center', marginRight: 35, marginLeft: 35, fontSize: 17, fontWeight: 'bold', color: '#00a7e5' }}>{lang.get('account.changePassword', 'account.password.change_password')}</Text>
        </View>

        <Content padder bounces={false}>
          <TextInput
            field="password"
            next="newPassword"
            onRefSet={this.handleRefSet}
            refs={this.inputs}
            returnKeyType={"next"}
            onBlur={this.validate}
            placeholder={lang.get('account.password', 'account.password.password')}
            secureTextEntry={showPassword}
            value={password}
            iconRight={<Icon onPress={this.changeIconPassword} />}
            onChangeText={this.setFieldValue}
            clearButtonMode={'while-editing'}
            errors={validation.errors}
            iconRight={<Icon ios={iconPasswordIos} android={iconPasswordAndroid} onPress={this.changeIconPassword} />}
          />
          <View style={{ marginTop: 30 }}>

            <TextInput
              field="newPassword"
              next="confirmPassword"
              onRefSet={this.handleRefSet}
              refs={this.inputs}
              returnKeyType={"next"}
              onBlur={this.validate}
              placeholder={lang.get('account.newPassword', 'account.password.new_password')}
              secureTextEntry={showNewPassword}
              value={newPassword}
              iconRight={<Icon onPress={this.changeIconNewPassword} />}
              onChangeText={this.setFieldValue}
              clearButtonMode={'while-editing'}
              errors={validation.errors}
              iconRight={<Icon ios={iconNewPasswordIos} android={iconNewPasswordAndroid} onPress={this.changeIconNewPassword} />}
            />
              {flagerror &&
                <LabelError message={lang.get('password.messageAlert', 'generic.message_alert')} />
              }

            <TextInput
              field="confirmPassword"
              onRefSet={this.handleRefSet}
              refs={this.inputs}
              returnKeyType={"go"}
              onBlur={this.validate}
              placeholder={lang.get('account.confirmPassword', 'account.password.confirm_password')}
              secureTextEntry={showConfirmPassword}
              value={confirmPassword}
              iconRight={<Icon onPress={this.changeIconConfirmPassword} />}
              onChangeText={this.setFieldValue}
              errors={validation.errors}
              clearButtonMode={'while-editing'}
              iconRight={<Icon ios={iconConfirmPasswordIos} android={iconConfirmPasswordAndroid} onPress={this.changeIconConfirmPassword}/>}
              onSubmitEditing={this.update}
            />

          </View>
          <Button
            style={styles.margin25}
            block
            onPress={this.update}
          >
            <Text>{lang.get('button.update', 'generic.button.update')}</Text>
            {httpBusy && <Spinner color='lightblue' />}
          </Button>
        </Content>
      </Container>
    );
  }
}

const containers = {
  EditPasswordContainer: EditPasswordContainer,
}

const mapStateToProps = (containers) => {
  return {
    data: containers.EditPasswordContainer.state,
  };
}

export default connect(containers, mapStateToProps)(EditPasswordScreen);