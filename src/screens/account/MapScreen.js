import React, { PureComponent } from 'react';
import { Container } from 'native-base';
import autobind from 'class-autobind';
import Geocoder from 'react-native-geocoding';
import { connect, Logger } from 'unstated-enhancers';
import styles from '../../theme/globalStyles';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import MapComponent from '../../components/map/MapComponent';
import lang from '../../shared/languages/Lang';
import infoProfile from './containers/EditAccountContainer';
import toast from '../../shared/support/ShowToast';

class MapScreen extends PureComponent {
  constructor(props) {
    super(props);

    autobind(this);
  }

  handleIsEditing() {
    const { navigation } = this.props;

    const isEditing = navigation.getParam('isEditing');

    if (!isEditing) {
      navigation.setParams({
        isEditing: true
      });
    }
  }

  initialAddress(data, address) {
    const { infoProfile } = this.props.containers;

    infoProfile.pickPlace(data, address);
  }

  async pickPlace(values, details = null) {
    const { infoProfile } = this.props.containers;

    const coordinates = {
      latitude: details.geometry.location.lat,
      longitude: details.geometry.location.lng
    };
    this.coordinates = coordinates;

    try {
      const json = await Geocoder.from(coordinates)
      const address = json.results[0].formatted_address;
      const data = {
        error: false,
        ...coordinates,
      }

      infoProfile.pickPlace(data, address);

    } catch (error) {
      toast.ShowToast(error.message || lang.get('message.tryAgain'))
    }
  }

  async hideMapComponent() {
    const { newArea } = this.props.infoProfile;

    if (newArea) {
      const { infoProfile } = this.props.containers;
      infoProfile.setDataState('address', newArea)
    }

    const { navigation } = this.props;

      await navigation.setParams({
        isEditing: false
      });

    this.props.navigation.goBack()
  }

  changePlace(address) {
      const { infoProfile } = this.props.containers;
      const { clear } = infoProfile.state;

      if(!clear) {
        const data = {
          error: false,
          latitude: address.latitude,
          longitude: address.longitude,
        }

        infoProfile.pickPlace(data, address.address);
      }
  }

  async onChangeTextUndefined(data) {
    const { infoProfile } = this.props.containers;

    await infoProfile.setClearText('newArea', data)
    this.handleIsEditing()
  }

  setNavigationGoBackConfirmationAlert() {
    this.props.navigation.setParams({
      alert: {
        title: lang.get('info.titleAlert', 'generic.title_alert'),
        message: lang.get('info.messageAlert', 'generic.message_alert')
      }
    });
  }

  componentDidMount() {
    this.setNavigationGoBackConfirmationAlert()
  }

  componentWillUnmount() {
    const { infoProfile } = this.props.containers;

    infoProfile.clearError()
  }

  render() {
    const { initialRegion = {}, newArea } = this.props.infoProfile;
    Logger.count(this)

    return (
      <Container style={styles.container}>

        <NavBarComponent {...this.props.navigation} name={lang.get('map.title', 'map.title')}>
        </NavBarComponent>

        <MapComponent
          address={newArea}
          onPickAddress={this.hideMapComponent}
          initialRegion={{
            latitude: initialRegion.latitude ? initialRegion.latitude : 0,
            longitude: initialRegion.longitude ? initialRegion.longitude : 0,
            address: initialRegion.address ? initialRegion.address : undefined
          }}
          setAddress={this.initialAddress}
          pickPlace={this.pickPlace}
          onChangePlace={this.changePlace}
          onChangeTextUndefined={this.onChangeTextUndefined}
        />
      </Container>
    );
  }
}

const containers = {
  infoProfile: infoProfile,
}

const mapStateToProps = (containers) => {
  return {
    infoProfile: containers.infoProfile.state,
  };
}

export default connect(containers, mapStateToProps)(MapScreen);
