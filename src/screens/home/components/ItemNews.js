import React, { Component } from 'react';
import { ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';


class ItemNews extends Component {
  constructor(props) {
    super(props);

  }

  render() {
    const uri = "https://facebook.github.io/react-native/docs/assets/favicon.png";
    return (
      <ListItem thumbnail>
        <Left>
          <Thumbnail source={{ uri: uri }} />
        </Left>
        <Body>
          <Text>Venta pormocional en Galerías</Text>
          <Text note numberOfLines={1}>Galerías Insuergentes</Text>
        </Body>
        <Right>
          <Button transparent onPress={this.props.navigate}>
            <Text>Ver</Text>
          </Button>
        </Right>
      </ListItem>
    );
  }
}

export default ItemNews;
