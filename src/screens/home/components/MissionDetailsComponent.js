import React, { PureComponent } from 'react';
import { Content, Text, Title, Card, Button, Icon, View, Left, Right, Spinner, CardItem } from 'native-base';
import autobind from 'class-autobind';
import { RefreshControl, Dimensions } from 'react-native';
import { Logger } from 'unstated-enhancers';

import styles from '../../../theme/globalStyles';
import newLang from '../../../shared/languages/Lang';
import StatusManager from '../../../shared/support/InvitationStatusManager';

const { width } = Dimensions.get('window');

class MissionDetailsComponent extends PureComponent {
  constructor(props) {
    super(props);
    autobind(this);
  }

  handleFinish() {
    const val = 10;
    const { onChangeStatus } = this.props;
    const data = {
      status: 10
    }

    if (onChangeStatus) {
      onChangeStatus(data, val);
    }
  }

  handleCancel() {
    const val = -1;
    const { onChangeStatus } = this.props;
    const data = {
      status: -1
    }

    if (onChangeStatus) {
      onChangeStatus(data, val);
    }
  }

  render() {
    const { mission = {}, onhttpBusyCancel } = this.props;
    const statusColor = StatusManager.getStatusColorMission(mission.status);
    const statusTag = StatusManager.getStatusMission(mission.status);

    return (
      <Card style={{ borderRadius: 10, width: '95%', marginBottom: 15 }}>
        <CardItem style={{ backgroundColor: 'trasparent', flexDirection: 'column' }}>
          <View>
            <Title>{mission.name}</Title>
            <Text style={{ textAlign: 'center', marginTop: 5 }}>{mission.type}</Text>
            <Text note style={{ color: statusColor, textAlign: 'center' }}>{statusTag}</Text>
          </View>
          <View style={styles.barTwoButtonsList}>
            {/* <Button bordered info small onPress={this.handleFinish} style={{ marginHorizontal: 5 }}
              disabled={mission.status !== 1 ? true : false}
            >
              <Text>{newLang.get('button.finishMission', 'generic.finish)}</Text>
              {onhttpBusyFinaly && <Spinner color='lightblue' />}
            </Button> */}
            <Button bordered danger small onPress={this.handleCancel} style={{ marginHorizontal: 5 }}
              disabled={mission.status === -1 || mission.status === 10  || mission.status === -10 ? true : false}
            >
              <Text>{newLang.get('button.cancelMission', 'generic.cancel')}</Text>
              {onhttpBusyCancel && <Spinner color='lightblue' />}
            </Button>
          </View>
        </CardItem>
      </Card>
    )
  }
}

export default MissionDetailsComponent;