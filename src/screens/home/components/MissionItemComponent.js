import React, { PureComponent } from 'react';
import { ListItem, Left, Body, Text, Right, CheckBox, CardItem, Card  } from 'native-base';
import autobind from 'class-autobind';
import { Logger } from 'unstated-enhancers';
import moment from 'moment';

import StatusManager from '../../../shared/support/InvitationStatusManager';

class MissionItemComponent extends PureComponent {
  constructor(props) {
    super(props);
    autobind(this)
  }

  handlePressItem() {
    const { item } = this.props;

    this.props.onPressItem(item);
  }

  render() {
    const { item } = this.props.item;
    const statusColor = StatusManager.getStatusColorMission(item.status);
    const statusTag = StatusManager.getStatusMission(item.status);

    Logger.count(this)

    return (
      <Card style={{ borderRadius: 10, marginLeft: 15, marginRight: 15}}>
      <CardItem onPress={ this.handlePressItem } style={{ backgroundColor: 'trasparent' }} button>
        <Body>
          <Text style={{ fontSize: 18 }}>{ item.name }</Text>
        </Body>
        <Right>
          <Text note>{ moment(item.started_at).format("DD/MM/YYYY") }</Text>
          <Text note style={{color: statusColor}}>{ statusTag }</Text>
        </Right>
      </CardItem>
      </Card>
    );
  }
}

export default MissionItemComponent
