import React, { PureComponent } from 'react';
import { Text, FlatList, InteractionManager } from 'react-native';
import autobind from 'class-autobind';
import { Logger } from 'unstated-enhancers';
import { Content, View, Spinner, Toast } from 'native-base';

import MissionItemComponent from './MissionItemComponent';
import EmptyList from '../../../components/emptyList/EmptyList';
import lang from '../../../shared/languages/Lang';

class MissionListComponent extends PureComponent {

  onEndReachedCalledDuringMomentum = true;

  constructor(props) {
    super(props);

    autobind(this)
  }

  keyExtractor(item) {
    return item.uuid;
  }

  renderItem(item) {
    return (
      <MissionItemComponent
          item={item}
          onPressItem={this.props.onPressItem}
        />
    )
  }

  handleLoadMore(info) {
    InteractionManager.runAfterInteractions(() => {
      if (!this.onEndReachedCalledDuringMomentum) {
        const { onHandleLoadMore} = this.props;

        if(onHandleLoadMore) {
          onHandleLoadMore();
        }
      }

      this.onEndReachedCalledDuringMomentum = true;
    })
  }

  handleMomentumScrollBegin() {
    this.onEndReachedCalledDuringMomentum = false;
  }

  handleOnRefresh() {
    const { onRefresh } = this.props;

    if (onRefresh) {
      onRefresh();
    }
  }

  renderFooter() {
    const { hasMoreItems, refreshing } = this.props;

    return (hasMoreItems && !refreshing ? <Spinner color="lightblue" /> : <View/>);
  }

  render() {
    const { data, refreshing } = this.props;

    Logger.count(this)

    return (
      <FlatList
        data={data}
        keyExtractor={this.keyExtractor}
        renderItem={this.renderItem}
        onEndReached={this.handleLoadMore}
        onEndThreshold={0}
        refreshing={ refreshing }
        initialNumToRender={ 15 }
        ListEmptyComponent={ <EmptyList welcome={lang.get('intro.welcome', 'mission.welcome')} message={lang.get('intro.question', 'mission.question')}/> }
        ListFooterComponent={ this.renderFooter }
        onMomentumScrollBegin={this.handleMomentumScrollBegin}
        onScrollBeginDrag={this.handleMomentumScrollBegin}
        onRefresh={ this.handleOnRefresh }
      />
    );
  }
}

export default MissionListComponent
