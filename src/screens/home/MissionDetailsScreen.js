import React, { Component } from 'react';
import { Container, Icon, Fab, View, Content, Title } from 'native-base';
import { connect } from 'unstated-enhancers';
import autobind from 'class-autobind';
import { Alert, Platform, SafeAreaView } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import styles from '../../theme/globalStyles';
import lang from '../../shared/languages/Lang';
import MissionDetailsComponent from './components/MissionDetailsComponent';
import ActivityList from '../activities/components/ActivityList';
import home from '../home/containers/HomeContainer';
import activities from '../activities/containers/ActivityListContainer';
import activitySelected from '../activities/containers/ActivityHistoryContainer';
import GPSState from 'react-native-gps-state'
import toast from '../../shared/support/ShowToast';

class MissionDetailsScreen extends Component {
  constructor(props) {
    super(props);
    autobind(this);

    this.state = {
      active: false,
      isFetching: false,
    };
  }

  async changeStatusMission(data, val) {
    const { home } = this.props.containers;
    const { missionSelected } = home.state;
    const listMision = home.state.data;

    await home.changeStatusMission(missionSelected.uuid, data, val);

    const { error } = home.state;

    if (error) {
      toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))
      home.clearError();
    } else {
      const newData = listMision.map(item => {
        if (item.uuid === missionSelected.uuid) {
          item = { ...item }
          item.status = data.status;
        }

        return item
      })
      await home.setData(newData);

      this.props.navigation.pop();
    }
  }

  handleConfirmationChangeStatus(data, val) {
    let action = '';
    let question = '';

    if (data.status === -1) {
      action = lang.get('mission.cancel', 'mission.cancel');
      question = lang.get('mission.cancelQuestion', 'mission.cancel_question');
    } else {
      action = lang.get('mission.finishMission', 'mission.finish_mission');
      question = lang.get('mission.finishAction', 'mission.finish_action')
    }
    Alert.alert(
      action,
      question,
      [
        {
          text: lang.get('mission.skip', 'mission.skip'),
        },
        {
          text: lang.get('mission.confirm', 'mission.confirm'),
          onPress: () => this.changeStatusMission(data, val)
        },
      ],
      {
        cancelable: false
      }
    )
  }

  async getActivities() {
    const { activities } = this.props.containers;
    const { missionSelected } = this.props.containers.home.state;

    await activities.getActivities(missionSelected.uuid);

    const { error } = activities.state;

    if (error) {
      toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic_try_again'))
      activities.clearError();
    } else {
      setTimeout(() => {
        this.setState({
          isFetching: false
        })
      }, 600)
    }
  }

  async handlePressActivityItem(data) {
    const status = await GPSState.getStatus();

    const AUTHORIZED = GPSState.AUTHORIZED;

    if (status === AUTHORIZED || status === GPSState.AUTHORIZED_WHENINUSE || status === GPSState.AUTHORIZED_ALWAYS) {
      const { activitySelected } = this.props.containers;

      await activitySelected.setData('activitySelected', data);

      this.props.navigation.navigate('ActivityDetailsScreen');
    } else {
      this.navigateToBlockLocationScreen();
    }
  }

  navigateToBlockLocationScreen() {
    this.props.navigation.navigate('BlockLocationScreen');
  }

  handleNewActivity() {
    this.props.navigation.navigate('NewActivityScreen');
  }

  navigate() {
    this.props.navigation.navigate('ChatScreen');
  }

  navigateVideoCall() {
    this.props.navigation.navigate('VideoCallScreen');
  }

  setMissionSelected() {
    const { missionSelected } = this.props.containers.home.state;
    const { activities } = this.props.containers;

    activities.setData('mission', missionSelected.uuid);
  }

  onRefresh() {
    this.setState({ isFetching: true }, function () { this.getActivities() });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.active !== this.state.active ||
      nextState.isFetching !== this.state.isFetching ||
      nextProps.home.httpBusy !== this.props.home.httpBusy ||
      nextProps.home.httpBusyFinaly !== this.props.home.httpBusyFinaly ||
      nextProps.home.httpBusyCancel !== this.props.home.httpBusyCancel ||
      nextProps.home.missionSelected !== this.props.home.missionSelected ||
      nextProps.activities.httpBusy !== this.props.activities.httpBusy ||
      nextProps.activities.data !== this.props.activities.data
  }

  componentDidMount() {

    this.setMissionSelected();

    this.getActivities();
  }

  componentWillUnmount() {
    const { activities } = this.props.containers;

    activities.reset();
  }

  render() {
    const { home } = this.props.containers;
    const { missionSelected } = home.state;
    const { data = [], httpBusy } = this.props.containers.activities.state;
    const httpBusyHome = home.state.httpBusy;
    const httpBusyFinaly = home.state.httpBusyFinaly;
    const httpBusyCancel = home.state.httpBusyCancel;

    return (
      <Container style={[styles.container]}>
        <NavBarComponent {...this.props.navigation} name={lang.get('title.missionDetails', 'mission.mission_details')} />
        <LinearGradient
          colors={['#00a7e5', '#5bc1be']}
          start={{ x: 0, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={{
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <MissionDetailsComponent
            mission={missionSelected}
            onChangeStatus={this.handleConfirmationChangeStatus}
            onhttpBusyHome={httpBusyHome}
            onhttpBusyFinaly={httpBusyFinaly}
            onhttpBusyCancel={httpBusyCancel}

          />
        </LinearGradient>
        <View style={{ width: '100%', padding: 10, backgroundColor: '#f0f2f4' }}>
          <Title style={{ textAlign: 'justify', marginLeft: 10 }}>{lang.get('activity.activities', 'mission.activities')}</Title>
        </View>

        <Content padder bounces={false}>
          <ActivityList
            activities={data}
            onPressActivityItem={this.handlePressActivityItem}
            onNewActivity={this.handleNewActivity}
            mission={missionSelected}
            refreshing={this.state.isFetching}
            onRefresh={this.onRefresh}
          />
        </Content>

        { missionSelected.status !== -10 &&
          <Fab
            active={this.state.active}
            direction="up"
            containerStyle={{}}
            style={{ backgroundColor: '#52bebb', marginBottom: Platform.OS === 'ios' ? 10 : 0 }}
            position="bottomRight"
            onPress={this.navigate}>
            <Icon name="chatboxes" />
            {/* <Button style={{ backgroundColor: '#34A34F' }} onPress={this.navigate}>
                  <Icon name="chatbubbles" />
                </Button>

                <Button style={{ backgroundColor: '#DD5144' }} onPress={this.navigateVideoCall}>
                  <Icon name="videocam" />
                </Button> */}
          </Fab>
        }

        <SafeAreaView />
      </Container>
    )
  }
}

const containers = {
  home: home,
  activities: activities,
  activitySelected: activitySelected
}

const mapStateToProps = (containers) => {
  return {
    home: containers.home.state,
    activities: containers.activities.state,
    activitySelected: containers.activitySelected.state,
  };
}

export default connect(containers, mapStateToProps)(MissionDetailsScreen);
