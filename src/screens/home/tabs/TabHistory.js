import React, { Component } from 'react';
import { List } from 'native-base';
import ItemNews from '../components/ItemHistory';

class TabHistory extends Component {
  constructor(props) {
    super(props);


  }

  render() {
    return (
      <List>{new Array(15).fill(null).map((_, i) => <ItemNews key={i} {...this.props} ></ItemNews>)}</List>
    );
  }
}

export default TabHistory;
