import React, { Component } from "react";
import { Animated, InteractionManager, AppState, Alert } from 'react-native';
import { Container } from "native-base";
import { connect, Logger } from "unstated-enhancers";
import home from './containers/HomeContainer';
import styles from "../../theme/globalStyles";
import NavBarComponent from "../../components/navBar/NavBarComponent";
import autobind from "class-autobind";
import MissionListComponent from "./components/MissionListComponent";
import lang from '../../shared/languages/Lang';
import SearchBarComponent from '../../components/searchBar/SearchBarComponent';
import authContainer from '../auth/containers/AuthContainer';
import GPSState from 'react-native-gps-state'
import NewInvitationNotice from "../../components/NewInvitationNotice";
import languageContainer from '../../components/language/containers/LanguagesContainer';
import invitationsContainer from '../invitations/containers/InvitationsListContainer';
import toast from "../../shared/support/ShowToast";

class HomeScreen extends Component {
  scroll = new Animated.Value(0);
  headerY;
  debounceId = null;
  isFiltering = false;

  constructor(props) {
    super(props);

    this.state = {
      isFetching: false,
      appState: AppState.currentState,
      invitations_expired: 0,
      invitationBanner: false
    }

    autobind(this);
  }

  openDrawer() {
    this.props.navigation.openDrawer();
  }

  handleOnRefresh() {
    this.setState({ isFetching: true }, () => this.getMissions());
  }

  handleLoadMore() {
    if (this.debounceId) {
      clearTimeout(this.debounceId);
    }

    const { home } = this.props.containers;
    const { meta, limit } = home.state;

    this.debounceId = setTimeout(() => {
      this.getMissions(meta.cursor.current + 1);

      const after = this.props.containers.home.state.meta;

      if (after.cursor.count === 0 || after.cursor.count < limit) {
        InteractionManager.runAfterInteractions(() => {
          toast.ShowToast('No hay mas que mostrar');
        })
      }
    }, 500);
  }

  async getMissions(page = 1) {
    const { home } = this.props.containers;
    const { meta, limit, httpBusy } = home.state;

    if (!this.isFiltering && (httpBusy || meta.cursor.count === 0 || meta.cursor.count < limit && page !== 1)) {
      Logger.dispatch('BLOQUED', { meta, state: this.state })
      this.setState({
        isFetching: false
      })
      return;
    }

    params = {
      search: this.state.value || '',
      limit: limit,
      page: page,
      includes: 'company.profile,room_chat'
    }

    await home.getMissions(params);

    const { error } = this.props.containers.home.state;

    if (error) {
      toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))
      setTimeout(() => {
        home.clearError();
      }, 200)
    } else {
      setTimeout(() => {
        this.setState({
          isFetching: false
        })
      }, 600)
    }
    this.isFiltering = false;
  }

  async handlePressItem(data) {

    const status = await GPSState.getStatus();

    if (status === GPSState.AUTHORIZED || status === GPSState.AUTHORIZED_WHENINUSE || status === GPSState.AUTHORIZED_ALWAYS) {
      const { home } = this.props.containers;

      await home.setMissionSelected(data.item);

      this.props.navigation.navigate('MissionDetailsScreen');
    } else {
      this.navigateToBlockLocationScreen();
    }
  }

  navigateToBlockLocationScreen() {
    this.props.navigation.navigate('BlockLocationScreen');
  }

  handleSearchChangeText(value) {
    this.setState({
      value
    })
  }

  handleSearchMissions() {
    this.isFiltering = true;

    this.getMissions()
  }

  componentWillMount() {
    GPSState.addListener((status)=>{
      switch(status){

        case GPSState.RESTRICTED:
          this.navigateToBlockLocationScreen();
        break;

        case GPSState.AUTHORIZED:
            this.pop();
        break;

        case GPSState.AUTHORIZED_ALWAYS:
            this.pop();
        break;

        case GPSState.AUTHORIZED_WHENINUSE:
            this.pop();
        break;
      }
    })
  }

  navigateToBlockLocationScreen() {
    this.props.navigation.navigate('BlockLocationScreen');
  }

  pop() {
    this.props.navigation.pop();
  }

  goToInvitations() {
    const { registered_payment } = this.props.auth.profile || {};

    if ( registered_payment === 0 ) {
      Alert.alert(
        lang.get('payment.titleAlert', 'mission.home.alert_payment'),
        lang.get('payment.messageAlert', 'mission.home.message_alert'),
        [
          {
            text: lang.get('activity.cancel', 'mission.home.cancel'),
          },
          {
            text: lang.get('activity.confirm', 'mission.home.confirm'),
            onPress: () => {
              this.props.navigation.navigate('RegistrationPaymentScreen')
            }
          },
        ],
        {
          cancelable: false
        }
      )
      return;
    } else {
        this.props.navigation.navigate('InvitationsListScreen');
    }
  }

  clearText() {
    this.setState({
      value: '',
    })
  }

  async getInvitationsExpired() {
    const { invitations } = this.props.containers;
    const { invitations_received } = this.props.auth.profile || {};

    const params = {
      includes: 'mission',
    };

    await invitations.fetch(params);

    const { error, data } = this.props.invitations;

    if (error) {
      toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))

      setTimeout(() => {
        invitations.clearError();
      }, 200)
    } else {
      let expired = 0;

      data.map((item) => {
        if(item.status === 0) {
          if(item.mission.status === 10
            || item.mission.status === -1
            || item.mission.accepted === item.mission.executives_requested) {
              expired = expired + 1
            }
        }
      })

      this.setState({
        invitations_expired: expired,
        invitationBanner: invitations_received - expired > 0 ? true : false
      })
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.home.data !== this.props.home.data ||
      nextProps.home.httpBusy !== this.props.home.httpBusy ||
      nextState.value !== this.state.value ||
      nextState.isFetching !== this.state.isFetching ||
      nextProps.auth.profile !== this.props.auth.profile ||
      nextProps.languageContainer.selected !== this.props.languageContainer.selected ||
      nextState.invitations_expired !== this.state.invitations_expired
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.getMissions();
      this.getInvitationsExpired()
    })
  }

  componentWillUnmount() {
    GPSState.removeListener()
  }

  render() {
    const { data = [], meta, limit } = this.props.home;
    const { value, isFetching, invitations_expired, invitationBanner } = this.state;
    const { invitations_received } = this.props.auth.profile || {};
    const { languageContainer } = this.props.containers;
    const { selected } = languageContainer.state;

    Logger.count(this)

    return (
      <Container style={styles.container}>
        <NavBarComponent
          {...this.props.navigation}
          menu={true}
          name={lang.get('title.missions', 'mission.home.missions')}
          hasTabs={true}
          openDrawer={this.openDrawer}
          invitations={invitationBanner ? invitations_received - invitations_expired : undefined}
        />
        {
        invitationBanner &&
        <NewInvitationNotice
          style={{
            height: 30,
            backgroundColor: '#FF8000',
            alignItems:'center',
            justifyContent:'center'
        }}
        onPress={this.goToInvitations}
        countInvitations={invitations_received - invitations_expired}
        message={lang.get('mission.newInvitationNotice', 'mission.home.new_invitation')}
        selected = {selected}
        />}
        {
          data !== undefined &&
          <SearchBarComponent
            value={value}
            placeholder={lang.get('search.searchMissions', 'mission.home.search_missions')}
            onChangeText={this.handleSearchChangeText}
            onSearchRequest={this.handleSearchMissions}
            clear={this.clearText}
          />
        }
          <MissionListComponent
            selected = {selected}
            data={data || []}
            hasMoreItems={!(meta.cursor.count === 0 || meta.cursor.count < limit)}
            onPressItem={this.handlePressItem}
            onHandleLoadMore={this.handleLoadMore}
            onRefresh={this.handleOnRefresh}
            refreshing={isFetching}
          />
      </Container>
    );
  }
}

const containers = {
  home: home,
  auth: authContainer,
  languageContainer: languageContainer,
  invitations: invitationsContainer
}

const mapStateToProps = (containers) => {
  return {
    home: containers.home.state,
    auth: containers.auth.state,
    languageContainer: containers.languageContainer.state,
    invitations: containers.invitations.state
  }
}

export default connect(containers, mapStateToProps)(HomeScreen);