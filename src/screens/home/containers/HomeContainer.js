import { Container } from 'unstated';
import MissionsServices from '../services/MissionsServices';
import { Logger } from 'unstated-enhancers';

const initialState = {
  error: undefined,
  message: undefined,
  data: undefined,
  meta: {
    cursor: {
      current: 0,
    }
  },
  limit: 25,
  httpBusy: false,
  missionSelected: undefined,
  httpBusyFinaly: false,
  httpBusyCancel: false
}

class HomeContainer extends Container {

  name = 'HomeContainer'

  constructor(data = {}) {
    super(data);
    this.state = {
      ...initialState
    }
  }

  async getMissions(params = {}) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get Mission starts'
      })

      const response = await MissionsServices.list(params);

      if (response) {
        await this.setState({
          data: response.meta.cursor.current === 1 ? response.data : this.state.data.concat(response.data),
          meta: response.meta,
          message: response.message,
          httpBusy: false,
          __action: 'Get Mission success'
        })
      }
    } catch (error) {
      await this.setState({
        error: error,
        httpBusy: false,
        __action: 'Get Mission error'
      })
    }
  }

  async changeStatusMission(mission, data, val) {

    if (val === 10) {
      this.setState({
        httpBusyFinaly: true,
      })
    } else {
      this.setState({
        httpBusyCancel: true,
      })
    }

    try {
      this.setState({
        httpBusy: true,
        __action: 'Cancel mission starts'
      })

      const response = await MissionsServices.cancelMission(mission, data)

      if (response) {
        await this.setState({
          message: response.message,
          httpBusy: false,
          httpBusyCancel: false,
          httpBusyFinaly: false,
          __action: 'Cancel mission success'
        })
      }
    } catch (error) {
      this.setState({
        httpBusy: false,
        error: error,
        __action: 'Cancel mission error'
      })
    }
  }

  async updateStatusMissionList(mission) {
    const { data } = this.state;

    const newData = data.map(item => {
      if (item.uuid === mission.uuid) {
        item = {
          ...item
        }
        item.status = mission.status;
      }
      return item;
    })

    await this.setState({
      ...this.state,
      data: newData,
      __action: 'update status mission'
    })
  }

  setMissionSelected(data) {
    this.setState({
      missionSelected: data,
      __action: 'Home set mission selected'
    })
  }

  setData(data) {
    this.setState({
      ...this.state,
      data,
      __action: 'Home set state data'
    })
  }

  clearError() {
    this.setState({
      error: undefined,
    })
  }

  reset() {
    this.setState({
      ...initialState,
    })
  }

}

export default HomeContainer;