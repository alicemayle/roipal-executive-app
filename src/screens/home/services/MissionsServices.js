import Http from '../../../shared/Http';
import Auth from '../../../shared/services/Auth';

class MissionsServices {

  list(params = {}) {
    const data = Auth.getUser();

    return Http.get(`api/executives/${data.uuid}/missions`, { params });
  }

  cancelMission(mission, data) {
    const user = Auth.getUser();
    return Http.put(`api/executives/${user.uuid}/missions/${mission}/status`, data);
  } 

}

export default new MissionsServices;