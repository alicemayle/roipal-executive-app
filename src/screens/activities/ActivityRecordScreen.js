import React, { Component } from 'react';
import { Container, Content, Toast } from 'native-base';
import { connect } from 'unstated-enhancers';
import autobind from 'class-autobind';
import Validator from 'validatorjs';
import ImagePicker from 'react-native-image-crop-picker';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import styles from '../../theme/globalStyles';
import lang from '../../shared/languages/Lang';
import activities from './containers/ActivityListContainer';
import ActivityRecord from './components/ActivityRecord';
import record from './containers/ActivityRecordContainer';
import activityHistory from './containers/ActivityHistoryContainer';
import ListErrors from '../../components/listErrors/ListErrors';
import Permissions from '../../shared/support/Permissions';
import Geocoder from "react-native-geocoding";
import home from '../home/containers/HomeContainer';
import toast from '../../shared/support/ShowToast';
Geocoder.init("AIzaSyAAjZGbgXrMZDS3T9YqdgDO-NnQC-eDZLA");

class ActivityRecordScreen extends Component {
  constructor(props) {
    super(props);
    autobind(this);

    this.state = {
      modalVisible: false,
      image: undefined,
      validation: new Validator(data, this.rules),
      busyPosition: false,
      busy: false,
      address: undefined
    }
  }

  validate() {
    const data = { ...this.state };
    const validation = new Validator(data, this.rules);

    validation.setAttributeNames({
      comment: lang.get('activity.comments', 'activity.record.comments')
    });


    this.validation = validation;
    validation.check();

    this.setState({
      validation
    })
  }

  get rules() {
    return {
      'comment': 'required|string',
    }
  }

  handleValueChange(field, value) {
    const { navigation } = this.props;
    const isEditing = navigation.getParam('isEditing');

    if (!isEditing) {
      navigation.setParams({
        isEditing: true
      });
    }

    this.setState({
      [field]: value
    })
  }

  setNavigationGoBackConfirmationAlert() {
    this.props.navigation.setParams({
      alert: {
        title: lang.get('activity.alertTitle', 'generic.title_alert'),
        message: lang.get('activity.alertRecordMessage', 'activity.record.record_message')
      }
    });
  }

  async setCurrentPosition() {
    try {
      this.setState({
        ...this.state,
        busyPosition: true
      })
      await Permissions.location();

      const position = await new Promise((resolve, reject) => {
        navigator.geolocation.getCurrentPosition(resolve, reject, { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 });
      });

      const json = await Geocoder.from(position.coords.latitude, position.coords.longitude);
      const addressComponent = json.results[0].formatted_address;

      this.setState({
        ...this.state,
        address: addressComponent,
        position: {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        },
        busyPosition: false
      })

    } catch (error) {
      this.setState({
        busyPosition: false,
        busy: false
      })
      Toast.show({
        text: error.message || "No se puede determinar su ubicación actual",
        textStyle: { color: "white", fontSize: 12 },
        duration: 3000,
        position: 'top'
      });
    }
  }

  async saveActivityRecord() {
    const { busy } = this.state;

    if (!busy) {
      await this.setState({
        busy: true
      })

      this.handleActivityRecord()
    } else {
      return;
    }
  }

  async handleActivityRecord() {
    const { httpBusy } = this.props.containers.record.state;

    if (httpBusy) return;

    await this.setCurrentPosition();

    if(!this.state.busyPosition) {
    const { record, activities, activityHistory } = this.props.containers;
    const { mission, data } = activities.state;
    const { activity, type } = record.state;


    if (type === 'comment') {
      this.validate();
      if (!this.state.validation.passes()) {
        this.setState({
          busy: false
        })
        return
      }
    }

    const activityRecord = {
      ...this.state,
      type: type,
    }

    delete activityRecord.validation;
    delete activityRecord.image;
    delete activityRecord.modalVisible;
    delete activityRecord.busy;
    delete activityRecord.busyPosition

    let formData = null;
    let config = null;

    if (this.state.image) {
      config = {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }

      formData = new FormData();
    }

    if (this.state.image) {
      formData.append('image', {
        name: this.state.image.mime,
        type: this.state.image.mime,
        uri: this.state.image.path
      });
    }

    await record.activityRecord(mission, activity.uuid, activityRecord, formData, config);

    const { error, response = {} } = record.state;

    if (error !== undefined) {
      toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))

      setTimeout(() => {
        record.clearError();
      }, 10000)

    } else {
      const newComments = [response, ...activityHistory.state.data];

      await activityHistory.setData('data', newComments);

      if (type !== 'comment') {
        let nextItem = undefined;
        const newData = data.map((item, index) => {
          if (item.uuid === activity.uuid) {
            item = { ...item }
            item.status = type === 'start' ? 1 : 10
            item.next = type === 'start' ? true : false
            nextItem = type === 'start' ? index : index + 1
          }
          if (index === nextItem) {
            item = { ...item }
            item.next = true
          }
          return item
        })
        await activities.setData('data', newData);

        const newActivity = { ...activity };

        newActivity.status = type === 'start' ? 1 : 10;
        newActivity.next = type === 'start' ? true : false;
        activityHistory.setData('activitySelected', newActivity);

        const updatedData = activities.state.data;
        const finish = updatedData.filter(item => item.status === 10);

        if (finish.length === updatedData.length) {
          const val = 10;
          const data = {
            status: 10
          }
          this.changeStatusMission(data, val)
        }
      }

      this.setState({
        busy: false
      })
      this.props.navigation.navigate('ActivityDetailsScreen');
    }
  }
  }

  async changeStatusMission(data, val) {
    const { home } = this.props.containers;
    const { missionSelected } = home.state;
    const listMision = home.state.data;

    await home.changeStatusMission(missionSelected.uuid, data, val);

    const { error } = home.state;

    if (error) {
      toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))
      home.clearError();
    } else {
      const newData = listMision.map(item => {
        if (item.uuid === missionSelected.uuid) {
          item = { ...item }
          item.status = data.status;
        }

        return item
      })
      await home.setData(newData);

      const changeMissionSelected = {
        ...missionSelected,
        status: data.status
      }

      home.setMissionSelected(changeMissionSelected)
    }
  }

  handleSelectedCamera() {
      ImagePicker.openCamera({
        width: 500,
        height: 500,
        cropping: true,
        cropperCircleOverlay: true,
        compressImageMaxWidth: 1000,
        compressImageMaxHeight: 1000,
        compressImageQuality: 1,
        compressVideoPreset: 'MediumQuality',
        includeExif: true,
      }).then(image => {
        this.setState({
          image: image,
        })
      });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.images !== this.state.images ||
      nextState.position !== this.state.position ||
      nextProps.record.httpBusy !== this.props.record.httpBusy ||
      nextState.validation !== this.state.validation ||
      nextProps.record.error !== this.props.record.error ||
      nextState.image != this.state.image ||
      nextState.busy != this.state.busy
  }

  componentWillUnmount() {
    const { record } = this.props.containers;
    record.reset();
  }

  componentDidMount() {
    this.setNavigationGoBackConfirmationAlert();
  }

  render() {
    const { httpBusy, error = {} } = this.props.record;
    const { validation, image, busy } = this.state;
    const { navigation } = this.props;
    const title = navigation.getParam('title');

    return (
      <Container style={[styles.container]}>
        <NavBarComponent {...this.props.navigation} name={title} />

        <Content padder bounces={false}>
          <ActivityRecord
            httpBusy={httpBusy}
            busyRecord={busy}
            busyPosition={this.state.busyPosition}
            data={this.state}
            onValueChange={this.handleValueChange}
            onActivityRecord={this.saveActivityRecord}
            onValidate={this.validate}
            errors={validation.errors}
            photo={image}
            onSelectedCamera={this.handleSelectedCamera}
          />
          {error.errors &&
            <ListErrors errors={error.errors} />
          }

        </Content>
      </Container>
    )
  }
}

const containers = {
  activities: activities,
  record: record,
  activityHistory: activityHistory,
  home: home
}

const mapStateToProps = (containers) => {
  return {
    record: containers.record.state
  };
}

export default connect(containers, mapStateToProps)(ActivityRecordScreen);