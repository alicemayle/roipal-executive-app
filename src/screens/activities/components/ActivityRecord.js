import React, { PureComponent } from 'react';
import { Text, View, Icon, Button, Form, Spinner, Thumbnail } from 'native-base';
import autobind from 'class-autobind';
import { Logger } from 'unstated-enhancers';
import { TouchableOpacity } from 'react-native';

import styles from '../../../theme/globalStyles';
import TextareaInput from '../../../components/inputs/TextareaInput';
import lang from '../../../shared/languages/Lang';
import LabelError from '../../../components/inputs/LabelError';
import avatar from '../../../images/picture/default-img.png';


class ActivityRecord extends PureComponent {
  constructor(props) {
    super(props);
    autobind(this);
  }

  changeText(field, value) {
    this.props.onValueChange(field, value);
  }

  activityRecord() {
    this.props.onActivityRecord();
  }

  validate() {
    this.props.onValidate();
  }

  render() {
    const { httpBusy, data, errors = {}, photo, busyPosition, busyRecord } = this.props;

    return (
      <Form>
        <Text note style={{textAlign: 'center', padding: 20}}>{lang.get('activity.evidence', 'activity.record.evidence')}</Text>
        <View style={{ flexDirection: 'row' }}>
          <View style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10,
          }} >
            <TouchableOpacity onPress={this.props.onSelectedCamera}>
              <View style={{ alignItems: 'flex-end', flexDirection: 'row' }}>
                <Thumbnail source={photo ? { uri: photo.path } : avatar} style={{
                  height: 100,
                  width: 100,
                  borderRadius: 50,
                }} />
                {/* <View style={{
                  flexDirection: 'row',
                  position: 'absolute',
                  right: 0,
                  marginLeft: 30,
                  backgroundColor: '#52bebb',
                  height: 35,
                  width: 35,
                  borderRadius: 17.5,
                }}>
                  <View style={{
                    flex: 1,
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                    <Icon name='camera-alt' type='MaterialIcons' style={{ color: '#FFF', fontSize: 23, }} onPress={this.props.onSelectedCamera} />
                  </View>
                </View> */}
              </View>
            </TouchableOpacity>
          </View>

          <View style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical: 10,
            paddingRight: 15
          }} >
            <Text note>{lang.get('activity.image', 'activity.record.image')}</Text>
          </View>
        </View>

        <TextareaInput
          field="comment"
          rowSpan={5}
          bordered
          value={data.comments}
          placeholder={lang.get('activity.comments', 'activity.record.comments')}
          onChangeText={this.changeText}
          onBlur={this.validate}
        />
        {errors.errors.comment && <LabelError message={errors.errors.comment[0]} />}
        <Button
          style={styles.margin25}
          block
          disabled={httpBusy}
          onPress={this.activityRecord}>
          <Text> {lang.get('activity.save', 'generic.button.save')} </Text>
          {
            (httpBusy || busyPosition || busyRecord) && <Spinner color="lightblue" />
          }
        </Button>
      </Form>
    )
  }
}

export default ActivityRecord;