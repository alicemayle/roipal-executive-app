import React, { PureComponent } from 'react';
import { FlatList } from 'react-native';
import autobind from 'class-autobind';
import { Logger } from 'unstated-enhancers';
import { View } from 'native-base';

import styles from '../../../theme/globalStyles';
import lang from '../../../shared/languages/Lang';
import ActivityHistoryItem from './ActivityHistoryItem';
import EmptyList from '../../../components/emptyList/EmptyList';

class ActivityHistoryList extends PureComponent {

  constructor(props) {
    super(props);

    autobind(this)
  }

  keyExtractor(item) {
    return item.uuid;
  }

  renderHistory(item) {
    return (
      <ActivityHistoryItem key={item.uuid} item={item} onZoomImage={this.props.onZoomImage} onMapFullScreen={this.props.onMapFullScreen} />
    )
  }

  render() {
    const { data = [], onRefresh, refreshing } = this.props;

    return (
      <View>
        <FlatList
          data={data}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderHistory}
          onRefresh={onRefresh}
          refreshing={refreshing}
          ListEmptyComponent={
            <EmptyList
              message={lang.get('list.noElements', 'generic.no_elements')}>
            </EmptyList>
          }
        />
      </View>
    );
  }
}

export default ActivityHistoryList
