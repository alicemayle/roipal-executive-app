import React, { PureComponent } from 'react';
import { FlatList } from 'react-native';
import autobind from 'class-autobind';
import { Logger } from 'unstated-enhancers';
import ActivityItem from '../components/ActivityItem';
import { View } from 'native-base';

import styles from '../../../theme/globalStyles';
import lang from '../../../shared/languages/Lang';
import EmptyList from '../../../components/emptyList/EmptyList';


class ActivityList extends PureComponent {

  constructor(props) {
    super(props);

    autobind(this)
  }

  keyExtractor(item) {
    return item.uuid;
  }

  renderItem(item) {
    return (
      <ActivityItem key={item.uuid} item={item} onPressItem={this.props.onPressActivityItem} data />
    )
  }

  render() {
    const { activities = [], onRefresh, refreshing } = this.props;

    return (
      <View>
        <FlatList
          data={activities}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
          onRefresh={onRefresh}
          refreshing={refreshing}
          ListEmptyComponent={
            <EmptyList
              message={lang.get('list.noElements', 'generic.no_elements')}>
            </EmptyList>
          }
        />
      </View>
    );
  }
}

export default ActivityList
