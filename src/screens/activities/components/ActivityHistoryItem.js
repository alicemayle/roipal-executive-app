import React, { PureComponent } from 'react';
import { Text, Card, CardItem, Body, Right, View, Left, Thumbnail, Title, Icon } from 'native-base';
import autobind from 'class-autobind';
import { Logger } from 'unstated-enhancers';
import Geocoder from "react-native-geocoding";
import moment from 'moment';
import ViewMoreText from 'react-native-view-more-text';
import { TouchableOpacity, Platform } from 'react-native';

Geocoder.init("AIzaSyAAjZGbgXrMZDS3T9YqdgDO-NnQC-eDZLA");

import styles from '../../../theme/globalStyles';
import lang from '../../../shared/languages/Lang';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { Dimensions } from 'react-native';
const screen = Dimensions.get('window');
const LATITUDE_DELTA = 0.01;
const ASPECT_RATIO = screen.width / screen.height;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;


class ActivityHistoryItem extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      map: false
    }
    autobind(this);
  }

  ViewMore(onPress) {
    return (
      <Text onPress={onPress} style={{ color: '#52bebb', fontSize: 11, textAlign: 'right' }}>{lang.get('activity.viewmore', 'activity.activity.view_more')}</Text>
    )
  }

  ViewLess(onPress) {
    return (
      <Text onPress={onPress} style={{ color: '#52bebb', fontSize: 11, textAlign: 'right' }}>{lang.get('activity.viewless', 'activity.activity.view_less')}</Text>
    )
  }

  zoomImage() {
    const { item } = this.props.item;

    if (item.images) {
      this.props.onZoomImage(item.images);
    }
  }

  viewMap() {
    const { map } = this.state;

    this.setState({
      map: !map
    })
  }

  goToMapFullScreen(data) {
    const { onMapFullScreen, item } = this.props;

    if (onMapFullScreen) {
      onMapFullScreen(item.item.position);
    }
  }

  render() {
    const { item } = this.props.item;
    const { map } = this.state;

    return (
      <Card style={{ borderRadius: 10, alignItems: 'center' }}>
        <CardItem style={{ backgroundColor: 'trasparent', alignItems: 'flex-start' }}>
          {
            item.images &&
            <TouchableOpacity style={{ marginRight: 10 }} onPress={this.zoomImage}>
              <Thumbnail small source={{ uri: item.images }} onPress={this.zoomImage} />
            </TouchableOpacity>
          }
          {
            !item.images &&
            <View style={[styles.thumnails]}>
              <Text style={{ color: 'white', textAlign: 'center', fontSize: 22, paddingTop: Platform.OS === 'ios' ? '12%' : '6%', paddingLeft: Platform.OS === 'ios' ? '5%' : '2%' }}>
                {item.type === 'start' ? lang.get('activity.shortStart', 'activity.activity.short_start')
                  : item.type === 'finish' ? lang.get('activity.shortFinish', 'activity.activity.short_finish')
                    : lang.get('activity.shortComments', 'activity.activity.short_comments')}
              </Text>
            </View>
          }
          <Body style={{ flexDirection: 'column', alignItems: 'flex-start' }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', width: '100%', flex: 1 }}>
              <Text style={{ fontWeight: 'bold', flex: 0.7 }}>
                {item.type === 'start' ? lang.get('activity.activityStart', 'activity.activity.activity_start')
                  : item.type === 'finish' ? lang.get('activity.activityFinish', 'activity.activity.activity_finish')
                    : lang.get('activity.comments', 'activity.activity.comments')}
              </Text>

              <Text note style={{ textAlign: 'right', flex: 0.3 }}>
                {moment(item.created_at).format("DD/MM/YYYY")}{'\n'}{moment(item.created_at).format("HH:mm")}
              </Text>
            </View>
            <TouchableOpacity
              style={{ flexDirection: 'row', width: '100%', alignItems: 'flex-start' }}
              onPress={this.viewMap}>
              <Icon name="pin" style={{ fontSize: 17, color: 'rgba(91,193,190,0.7)', marginRight: 5, marginTop:2 }}></Icon>
              <Text note>{item.address}</Text>
            </TouchableOpacity>
            <ViewMoreText
              numberOfLines={2}
              renderViewMore={this.ViewMore}
              renderViewLess={this.ViewLess}
              textStyle={{ textAlign: 'justify' }}
            >
              {item.comment && <Text>{item.comment}</Text>}
            </ViewMoreText>
          </Body>
        </CardItem>
        {map && <MapView
          onPress={this.goToMapFullScreen}
          scrollEnabled={false}
          rotateEnabled={false}
          provider={PROVIDER_GOOGLE}
          style={[{ height: 80, width: '90%', margin: 10 }]}
          initialRegion={{
            ...item.position,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LATITUDE_DELTA * ASPECT_RATIO,
          }}
        >
          <Marker coordinate={item.position}>
            <Icon name="pin" style={{ color: 'red', fontSize: 20 }} />
          </Marker>
        </MapView>
        }
      </Card>
    )
  }
}

export default ActivityHistoryItem;