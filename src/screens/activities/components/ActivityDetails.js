import React, { PureComponent } from 'react';
import { Text, Title, Card, View } from 'native-base';
import autobind from 'class-autobind';
import { Logger } from 'unstated-enhancers';

class ActivityDetails extends PureComponent {
  constructor(props) {
    super(props);
    autobind(this);
  }

  render() {
    const { activity } = this.props;

    return (
      <Card style={{ borderRadius: 10, width: '95%', marginBottom: 15 }}>
        <View style={{ padding: 10 }}>
          <Title>{activity.title}</Title>
        </View>
      </Card>

    )
  }
}

export default ActivityDetails;