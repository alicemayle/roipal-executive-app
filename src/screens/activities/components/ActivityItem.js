import React, { PureComponent } from 'react';
import { Body, Text, Card, CardItem, View } from 'native-base';
import autobind from 'class-autobind';
import { Logger } from 'unstated-enhancers';

import StatusManager from '../../../shared/support/InvitationStatusManager';
import ViewMoreText from 'react-native-view-more-text';
import lang from '../../../shared/languages/Lang';

class ActivityItem extends PureComponent {
  constructor(props) {
    super(props);
    autobind(this)
  }

  onPressItem() {
    const { item } = this.props.item;

    this.props.onPressItem(item);
  }

  ViewMore(onPress) {
    return (
      <Text onPress={onPress} style={{ color: '#52bebb', fontSize: 11, textAlign: 'right' }}>{lang.get('activity.viewmore', 'activity.activity.view_more')}</Text>
    )
  }

  ViewLess(onPress) {
    return (
      <Text onPress={onPress} style={{ color: '#52bebb', fontSize: 11, textAlign: 'right' }}>{lang.get('activity.viewless', 'activity.activity.view_less')}</Text>
    )
  }

  render() {
    const { item } = this.props.item;
    const statusColor = StatusManager.getStatusColorMission(item.status);
    const statusTag = StatusManager.getStatusActivity(item.status);

    return (
      <Card style={{ borderRadius: 10 }} >
        <CardItem button style={{ backgroundColor: 'trasparent' }} onPress={this.onPressItem}>
          <Body>
            <View style={{flex:1, flexDirection: 'row', justifyContent: 'space-between', width: '100%', marginBottom: 5 }}>
              <View style={{flex:0.7}}>
                <Text style={{ fontWeight: 'bold' }}>{item.title}</Text>
              </View>
              <View style={{flex:0.3, flexDirection:'row', justifyContent:'flex-end'}}>
                <Text note style={{ color: statusColor }}>{statusTag}</Text>
              </View>
            </View>
            <ViewMoreText
              numberOfLines={2}
              renderViewMore={this.ViewMore}
              renderViewLess={this.ViewLess}
              textStyle={{ textAlign: 'justify' }}
            >
              <Text style={{ paddingTop: 10 }}>{item.description}</Text>
            </ViewMoreText>
          </Body>
        </CardItem>

      </Card>
    );
  }
}

export default ActivityItem
