import http from '../../../shared/Http';
import Auth from '../../../shared/services/Auth';
import { Logger } from 'unstated-enhancers';

class ActivityServices {

    getActivities(missionUuid, params) {
        const data = Auth.getUser();

        return http.get(`/api/executives/${data.uuid}/missions/${missionUuid}/activities`, { params });
    }

    saveActivity(missionUuid, data) {
        const user = Auth.getUser();

        return http.post(`/api/executives/${user.uuid}/missions/${missionUuid}/activities`, data);
    }

    activityRecord(missionUuid, activityUuid, data) {
        const user = Auth.getUser();

        return http.post(`api/executives/${user.uuid}/missions/${missionUuid}/activities/${activityUuid}/events`, data);
    }

    getActivityHistory(missionUuid, activityUuid, params) {
        const user = Auth.getUser();

        return http.get(`api/executives/${user.uuid}/missions/${missionUuid}/activities/${activityUuid}/events`, { params });
    }

    sendFile(data, config) {
        return http.post(`/api/files`, data, config);
      }
}
export default new ActivityServices;