import React, { Component } from 'react';
import { Container, View, Text, Body, Spinner, Button } from 'native-base';
import { Image, Dimensions } from 'react-native';
import autobind from 'class-autobind';
import lang from '../../shared/languages/Lang';
import styles from '../../theme/globalStyles';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import GPSState from 'react-native-gps-state';

const { width } = Dimensions.get('window');

class BlockLocationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      httpBusy: false
    }
    autobind(this);
  }

  onPressLeftButton() {
    this.props.navigation.navigate('ChecklistScreen');
  }

  settings() {
    GPSState.openLocationSettings()
  }

  render() {
    const { httpBusy } = this.state;

    return (
      <Container style={styles.container}>
        <NavBarComponent  {...this.props.navigation}
          name={lang.get('mission.disabledGPS', 'activity.block.disabled_gps')} />

        <Body style={{ padding: 10, flex: 1, justifyContent:'space-around'}}>
          <View style={{ flexDirection: 'row', justifyContent: 'center'}}>
            <Image style={{ resizeMode: 'contain', width: width * 0.7, height: 150}} source={require('../../images/location/location.png')} />
          </View>

          <View>
            <Text style={[styles.marginHorizontal25, styles.textAlignJustify]}>
              {lang.get('mission.requiredLocation', 'activity.block.required_location')}
            </Text>

            <Text style={[styles.marginHorizontal25, styles.margin15, styles.textAlignJustify]} onPress={this.verify}>
              {lang.get('mission.introSettings', 'activity.block.intro_settings')}
            </Text>
          </View>

          <View>
            <Button bordered onPress={this.settings}>
              <Text style={{textAlign: 'center' }}>{lang.get('mission.settings', 'activity.block.settings')}</Text>
            </Button>
          </View>
        </Body>

        {
          httpBusy && <Spinner color="lightblue" />
        }

      </Container>
    )
  }
}

export default BlockLocationScreen;