import { Container } from 'unstated';
import ActivityServices from '../services/ActivityServices';
import { Logger } from 'unstated-enhancers';

const initialState = {
  httpBusy: false,
  data: undefined,
  error: undefined,
  success: undefined,
  message: '',
}

class ActivityRecordContainer extends Container {

  state = {
    ...initialState
  }


  async activityRecord(mission, activity, data, formData, config) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Activity Record starts'
      })

      let urls = null;

      if(formData) {
        urls = await ActivityServices.sendFile(formData, config);
      }

      let media = {};
      let dataRequest = data;

      if(urls) {
        if(urls.image) {
          media = {
            ...media,
            images: urls.image
          }
        }

        if(media !== {}) {
          dataRequest = {
            ...data,
            ...media
          }
        }
      }

      const response = await ActivityServices.activityRecord(mission, activity, dataRequest)

      if(response) {
        await this.setState({
          message: response.message,
          response: response.data,
          __action: 'Activity Record success'
        })
      }
    } catch (error) {
      if (error.errors) {
        let messages = [];

        for (const [field, errors] of Object.entries(error.errors)) {
          messages = messages.concat(errors)
        }

        error.errors = messages
      }
      this.setState({
        error: error,
        __action: 'Activity Record error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'Activity Record finally'
        });
      }, 700)
    }
  }

  setData(field, value) {
    this.setState({
      ...this.state,
      [field]: value,
      __action: 'Activity Record set state'
    })
  }

  setActivitySelected(type, activity) {
    this.setState({
      ...this.state,
      type: type,
      activity: activity,
      __action: 'Activity Record set activity selected'
    })
  }

  clearError() {
    this.setState({
      error: undefined,
      __action: 'Activity Record clear error'
    })
  }

  reset() {
    this.setState({
      ...initialState,
      __action: 'Activity Record reset'
    })
  }

}
export default ActivityRecordContainer