import { Container } from 'unstated';
import ActivityServices from '../services/ActivityServices';
import { Logger } from 'unstated-enhancers';

const initialState = {
  httpBusy: false,
  data: undefined,
  error: undefined,
  success: undefined,
  message: '',
}

class ActivityListContainer extends Container {

  state = {
    ...initialState
  }

  async getActivities(mission) {
    params = {
      limit: 15,
    }

    try {
      this.setState({
        httpBusy: true,
        __action: 'Get Activitys starts'
      })

      const response = await ActivityServices.getActivities(mission, params);

      if(response) {
        let nextFound = false
        const data = response.data.map((item, index) => {
          item = {
            ...item,
            next: !nextFound && item.status !== 10 ? true : false
          }
          if (!nextFound && item.status !== 10) {
            nextFound = true
          }
          return item;
        })

        await this.setState({
          data: data,
          message: response.message,
          __action: 'Get Activitys success'
        })
      }
    } catch (error) {
      await this.setState({
        error: error,
        __action: 'Get Activitys error'
      });
    }
    finally{
      setTimeout( () => {
        this.setState({
          httpBusy: false,
          __action: 'Get Activitys finally'
        });
      }, 500 ) 
    } 
  }

  setData(field, data) {
    this.setState({
      ...this.state,
      [field]: data,
      __action: 'Activity list set state'
    })
  }


  clearError() {
    this.setState({
      error: undefined,
      __action: 'Activity list clear error'
    })
  }

  reset() {
    this.setState({
      ...initialState,
      __action: 'Activity list reset'
    })
  }

}

export default ActivityListContainer