import { Container } from 'unstated';
import ActivityServices from '../services/ActivityServices';
import { Logger } from 'unstated-enhancers';

const initialState = {
  httpBusy: false,
  data: undefined,
  error: undefined,
  success: undefined,
  message: '',
}

class ActivityHistoryContainer extends Container {

  state = {
    ...initialState
  }

  async getActivityHistory(mission) {
    params = {
      limit: 15,
    }

    const activity = this.state.activitySelected.uuid;

    try {
      this.setState({
        httpBusy: true,
        __action: 'Get Activity History starts'
      })

      const response = await ActivityServices.getActivityHistory(mission, activity, params);

      if (response) {
        await this.setState({
          data: response.data,
          message: response.message,
          __action: 'Get Activity History success'
        })
      }
    } catch (error) {
      await this.setState({
        error: error,
        __action: 'Get Activity History error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'Get Activity History finally'
        });
      }, 500)
    }
  }

  setData(field, data) {
    this.setState({
      ...this.state,
      [field]: data,
      __action: 'Activity History set state'
    })
  }

  clearError() {
    this.setState({
      error: undefined,
      __action: 'Activity History clear error'
    })
  }

  reset() {
    this.setState({
      ...initialState,
      __action: 'Activity History reset'
    })
  }

}

export default ActivityHistoryContainer