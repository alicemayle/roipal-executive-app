import { Container } from 'unstated';
import ActivityServices from '../services/ActivityServices';
import { Logger } from 'unstated-enhancers';

const initialState = {
  httpBusy: false,
  data: undefined,
  error: undefined,
  success: undefined,
  message: '',
}

class NewActivityContainer extends Container {

  state = {
    ...initialState
  }


  async saveActivity(mission, data) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Save Activity starts'
      })

      const response = await ActivityServices.saveActivity(mission, data);

      if (response) {
        await this.setState({
          message: response.message,
          data: response.data,
          __action: 'Save Activity success'
        })
      }
    } catch (error) {
      if (error.errors) {
        let messages = [];

        for (const [field, errors] of Object.entries(error.errors)) {
          messages = messages.concat(errors)
        }

        error.errors = messages
      }

      await this.setState({
        error: error,
        __action: 'Save Activity error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'Save Activity finally'
        });
      }, 700)
    }
  }

  clearError() {
    this.setState({
      error: undefined,
      __action: ' New Activity clear error'
    })
  }

  reset() {
    this.setState({
      ...initialState,
      __action: 'New Activity reset'
    })
  }

}

export default NewActivityContainer