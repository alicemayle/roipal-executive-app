import React, { Component } from 'react';
import { Container, Content, Button, Text, View, Title, Icon, Body } from 'native-base';
import { connect } from 'unstated-enhancers';
import autobind from 'class-autobind';
import { Alert, Modal, Platform } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import ImageViewer from 'react-native-image-zoom-viewer';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import styles from '../../theme/globalStyles';
import lang from '../../shared/languages/Lang';
import activity from './containers/ActivityHistoryContainer';
import activities from './containers/ActivityListContainer';
import record from './containers/ActivityRecordContainer';
import ActivityDetails from './components/ActivityDetails';
import home from '../home/containers/HomeContainer';
import ActivityHistoryList from './components/ActivityHistoryList';
import GPSState from 'react-native-gps-state';
import toast from '../../shared/support/ShowToast';

class ActivityDetailsScreen extends Component {
  constructor(props) {
    super(props);
    autobind(this);

    this.state = {
      isFetching: false,
      modalVisible: false,
      image: undefined
    };
  }

  setModalVisible() {
    const inverse = !this.state.modalVisible;
    this.setState({
      modalVisible: inverse,
    });
  }

  setModalAceptLocation() {
    this.setState({
      permitLocation: false,
    });
  }

  setModalDeniedtLocation() {
    this.setState({
      permitLocation: true,
    });
  }

  async handleZoomImage(image) {
    if (image) {
      await this.setState({
        image: image
      })
    }

    this.setModalVisible()
  }

  async getActivityHistory() {
    const { activity, activities } = this.props.containers;
    const { mission } = activities.state;

    await activity.getActivityHistory(mission);

    const { error } = activity.state;

    if (error !== undefined) {
      toast.showToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))

      activity.clearError();
    } else {
      setTimeout(() => {
        this.setState({
          isFetching: false
        })
      }, 600)
    }
  }

  onRefresh() {
    this.setState({ isFetching: true }, function () { this.getActivityHistory() });
  }

  handleConfirmationAction(type) {
    let action = '';
    let question = '';

    if (type === 'start') {
      action = lang.get('activity.titleStart', 'activity.activity.title_start');
      question = lang.get('activity.startQuestion', 'activity.activity.start_question')
    } else if (type === 'finish') {
      action = lang.get('activity.titleFinish', 'activity.activity.title_finish');
      question = lang.get('activity.finishQuestion', 'activity.activity.finish_question')
    }

    if (type !== 'comment') {
      Alert.alert(
        action,
        question,
        [
          {
            text: lang.get('activity.cancel', 'activity.activity.cancel'),
          },
          {
            text: lang.get('activity.confirm', 'activity.activity.confirm'),
            onPress: () => this.handleUpdateActivity(type)
          },
        ],
        {
          cancelable: false
        }
      )
    } else {
      this.handleUpdateActivity(type)
    }
  }

  handleUpdateActivity(type) {
    const { record } = this.props.containers;
    const { activitySelected } = this.props.containers.activity.state;
    const title = type === 'start' ? lang.get('activity.titleStart', 'activity.activity.title_start')
      : type === 'finish' ? lang.get('activity.titleFinish', 'activity.activity.title_finish')
        : lang.get('activity.comments', 'activity.activity.comments');

    record.setActivitySelected(type, activitySelected);
    this.props.navigation.navigate('ActivityRecordScreen', { title: title });
  }

  componentDidMount() {
    this.getActivityHistory()
  }

  componentWillUnmount() {
    const { activity } = this.props.containers;

    activity.reset();
  }

  async addComment() {

    const status = await GPSState.getStatus();

    const AUTHORIZED = GPSState.AUTHORIZED;

    if (status === AUTHORIZED || status === GPSState.AUTHORIZED_WHENINUSE || status === GPSState.AUTHORIZED_ALWAYS) {

      const type = 'comment';

      this.handleConfirmationAction(type);

    } else {
      this.navigateToBlockLocationScreen();
    }

  }

  navigateToBlockLocationScreen() {
    this.props.navigation.navigate('BlockLocationScreen');
  }

  async updateActivity() {
    const status = await GPSState.getStatus();

    const AUTHORIZED = GPSState.AUTHORIZED;

    if (status === AUTHORIZED || status === GPSState.AUTHORIZED_WHENINUSE || status === GPSState.AUTHORIZED_ALWAYS) {

      const { activitySelected } = this.props.activity;
      const type = activitySelected.status === 0 ? 'start' : 'finish';

      this.handleConfirmationAction(type);

    } else {
      this.navigateToBlockLocationScreen();
    }
  }

  handleMapFullScreen(location) {
    this.props.navigation.navigate('MapViewScreen', { location: location });
  }

  render() {
    const { activitySelected, data, httpBusy } = this.props.activity;
    const { missionSelected } = this.props.home;

    return (
      <Container style={[styles.container]}>
        <NavBarComponent {...this.props.navigation} name={lang.get('title.activityDetails', 'activity.activity.activity_details')} />
        <LinearGradient
          colors={['#00a7e5', '#5bc1be']}
          start={{ x: 0, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={{
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <ActivityDetails activity={activitySelected} />
        </LinearGradient>
        <View style={{ width: '100%', backgroundColor: '#f0f2f4', padding: 10 }}>
          <Title style={{ textAlign: 'justify', marginLeft: 10 }}>{lang.get('activity.record', 'activity.activity.record')}</Title>
        </View>

        <View style={{padding: 10, justifyContent:'space-between', flex:1, marginBottom:10}}>
          <Content bounces={false}>
          <ActivityHistoryList
            data={data}
            onZoomImage={this.handleZoomImage}
            onMapFullScreen={this.handleMapFullScreen}
            onRefresh={this.onRefresh}
            refreshing={this.state.isFetching}
          />
          </Content>

          {activitySelected.status === 1 && missionSelected.status === 1 && activitySelected.next &&
            <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'space-between', marginTop: 10 }}>
              <Button block style={{ width: '45%' }} onPress={this.addComment}>
                <Text>
                  {lang.get('activity.addComment', 'activity.activity.add_comment')}
                </Text>
              </Button>
              <Button block style={{ width: '45%' }} onPress={this.updateActivity}>
                <Text>
                  {activitySelected.status === 0 ? lang.get('activity.start', 'activity.activity.start') : lang.get('activity.finish', 'activity.activity.finish')}
                </Text>
              </Button>
            </View>
          }

          {
            activitySelected.status === 0 && missionSelected.status === 1 && activitySelected.next &&
            (<Button block style={{ marginTop: 10, marginHorizontal: 10 }} onPress={this.updateActivity}>
              <Text>
                {lang.get('activity.start', 'activity.activity.start')}
              </Text>
            </Button>)
          }
        </View>

        <Modal
          visible={this.state.modalVisible}
          onRequestClose={this.setModalVisible}
          transparent={false}>
          <View style={{ backgroundColor: Platform.OS === 'android' ? 'black' : 'white', paddingTop: Platform.OS === 'ios' ? 30 : 0 }}>
            <Button
              transparent
              onPress={this.setModalVisible}
            >
              <Icon name={'arrow-back'} style={{ color: Platform.OS === 'android' ? 'white' : 'black' }} />
            </Button>
            <Body style={{ alignItems: 'center' }}>
            </Body>
            <View style={{ width: 70, justifyContent: 'center', alignItems: 'center' }}>
            </View>
          </View>

          <ImageViewer
            onModal={this.setModalVisible}
            backgroundColor={Platform.OS === 'android' ? 'black' : 'white'}
            imageUrls={[
              {
                url: this.state.image
              }
            ]} />
        </Modal>
      </Container>
    )
  }
}

const containers = {
  activity: activity,
  activities: activities,
  record: record,
  home: home
}

const mapStateToProps = (containers) => {
  return {
    activity: containers.activity.state,
    home: containers.home.state
  };
}

export default connect(containers, mapStateToProps)(ActivityDetailsScreen);