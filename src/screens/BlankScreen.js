import React, { Component } from 'react'
import { View, BackHandler, Alert } from 'react-native'
import autobind from 'class-autobind';
import NavigationService from '../shared/services/NavigationService';

(window || global).noop = () => { }
export class BlankScreen extends Component {
  willFocusSubscription = null;
  willBlurSubscription = null;

  constructor(props) {
    super(props);

    autobind(this);
  }

  routeNeedsEditingCheck(state) {
    return state
      && state.params
      && state.params.isEditing
  }

  getAlertConfig(state) {
    const config = state.params.alert || {};
  
    return {
      title: config.title,
      message: config.message || 'All changes will be lost, continue anyway?'
    }
  }

  showConfirmationAlert(state) {
    const alert = this.getAlertConfig(state);
  
    Alert.alert(
      alert.title,
      alert.message,
      [
        {
          text: 'Cancel',
          onPress: noop,
          style: 'cancel',
        },
        {
          text: 'OK', onPress: cancel = () => {
            state.params.isEditing = false;
  
            NavigationService.goBack();
          }
        },
      ],
      {
        cancelable: false
      },
    );
  }

  // onBack() {
  //   this.props.navigation.goBack();
  //   return true;
  // }

  onBack() {
    const Navigator = this.props.navigation;
    const { state } = Navigator;

    if (this.routeNeedsEditingCheck(state)) {
      this.showConfirmationAlert(state)
      return true
    } else {
      return false
    }
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.onBack)

    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
       // this.props.navigation.pop();
       BackHandler.addEventListener("hardwareBackPress", this.onBack)
      }
    );

    this.willBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      () => {
        BackHandler.removeEventListener("hardwareBackPress", this.onBack)
      }
    );
  }

  componentWillMount() {
    // Remove the listener when you are done
    // if (this.willFocusSubscription) {
    //   this.willFocusSubscription.remove();
    // }

    // BackHandler.removeEventListener("hardwareBackPress", this.onBack)

    if (this.willFocusSubscription) {
      this.willFocusSubscription.remove();
    }

    if (this.willBlurSubscription) {
      this.willBlurSubscription.remove();
    }

    BackHandler.removeEventListener("hardwareBackPress", this.onBack)
  }

  render() {
    return (
      <View></View>
    )
  }
}

export default BlankScreen
