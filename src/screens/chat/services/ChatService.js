import http from "../../../shared/Http";

class ChatService {

    getConversation(params) {
        return http.get(`api/chat`, { params });
    }

    getRoom(params) {
        return http.get(`api/chat/room`, { params });
    }
}

export default new ChatService;
