import React, { Component } from 'react';
import { Header, Button, Icon, Body, Title, Text } from 'native-base';
import autobind from 'class-autobind';
import LinearGradient from 'react-native-linear-gradient';

class NavBarChat extends Component {
  constructor(props) {
    super(props);
    autobind(this);
  }

  onPressLeftButton() {
    const { onPressLeftButton } = this.props;

    if (onPressLeftButton) {
      onPressLeftButton()
    } else {
      this.props.goBack();
    }
  }

  render() {
    const { menu = false, name, listing } = this.props;

    return (
      <LinearGradient
        colors={['#135481', '#016eb8', '#0285c9', '#00a7e5']}
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
        style={{
          width: '100%',
        }}
      >
        <Header style={{ flexDirection: 'row', backgroundColor: 'transparent' }}>

          <Button
            transparent
            onPress={this.onPressLeftButton}
          >
            <Icon name={menu === null ? null : menu === true ? null : 'arrow-back'} />
          </Button>
          <Body style={{ alignItems: 'flex-start' }}>
            <Title style={{ color: 'white' }}>{name}</Title>
            <Title style={{color: 'white', fontSize: 10, fontWeight: 'bold'}}>{listing}</Title>
          </Body>
        </Header>
      </LinearGradient>
    );
  }
}

export default NavBarChat;