import { Container } from 'unstated';
import { GiftedChat } from 'react-native-gifted-chat';
import { Logger } from 'unstated-enhancers';
import ChatService from '../services/ChatService';
import moment from 'moment';
import avatar from '../../../images/drawer/perfil_black.png'

const initialState = {
  menchat: [],
  message: undefined,
  error: undefined,
  data: undefined,
  httpBusy: false,
  room: undefined
}

class ChatContainer extends Container {
  constructor(props) {
    super(props);
    this.state = { ...initialState }
  };

  async getuuid(data, logoCompany) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'get list id_room starts'
      })
      const response = await ChatService.getConversation(data);
      if (response) {
        let messageHistory = [];

        await response.data.map(item => {
          const message = {
            message_uuid: item.message_uuid,
            room_uuid: item.room_uuid,
            createdAt: moment(item.created_at).toDate(),
            text: item.message,
            user: {
              _id: item.app === 'business' ? 2 : item.app === 'executive' ? 1 : undefined,
              avatar: (item.app === 'business' && logoCompany) ? logoCompany : avatar
            },
            sent: true,
            received: true,
            _id: item.id
          }
          messageHistory.push(message);
        })
  
        await this.setState({
          menchat: messageHistory,
          message: response.message,
          __action: 'get list id_room setdata'
        })
      }
    } catch (error) {

      await this.setState({
        error: error,
        __action: 'get list id_room error'
      });
    } finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'get list id_room finally'
        });
      }, 500)
    }
  }

  async getRoom(data) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'get list id_room starts'
      })
      const response = await ChatService.getRoom(data);

      if (response) {
        await this.setState({
          ...this.state,
          room: response.data.uuid,
          message: response.message,
          __action: 'get list id_room setdata'
        })
      }
    } catch (error) {

      await this.setState({
        error: error,
        __action: 'get list id_room error'
      });
    } finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'get list id_room finally'
        });
      }, 500)
    }
  }

  updateState(key, value) {
    const state = { ...this.state };
    state[key] = value;
    this.setState(state);
  }

  setMessageChat(messages = []) {
    this.setState(previousState => ({
      menchat: GiftedChat.append(previousState.menchat, messages),
      __action: 'Set message chat'
    }))
  }

  updateMessage(message_uuid) {
    const newMessages = this.state.menchat.map((item) => {
      if (message_uuid === item.message_uuid) {
        item = {
          ...item,
          received: true
        }
      }
      return item
    })

    this.setState({
      ...this.state,
      menchat: newMessages
    })
  }

  receivedMessages() {
    const messages = this.state.menchat.map((item) => {
      item = {
        ...item,
        received: true
      }
      return item
    })

    this.setState({
      ...this.state,
      menchat: messages
    })
  }

  clearError() {
    this.setState({
      ...this.state,
      error: undefined,
      message: undefined,
      httpBusy: false,
      __action: 'Clear error chat'
    })
  }

  reset() {
    this.setState({
      ...initialState,
      __action: 'Chat reset'
    });
  }
}

export default ChatContainer;

