import React, { Component } from 'react';
import { connect } from 'unstated-enhancers';
import { GiftedChat, Bubble, Send } from 'react-native-gifted-chat';
import Signing from '../auth/signin/containers/SigninContainer';
import HomeContainer from '../home/containers/HomeContainer';

import language from '../../components/language/containers/LanguagesContainer';
import lang from '../../shared/languages/Lang';
import chat from './containers/ChatContainer';
import { View, Text } from 'native-base';
import { Logger } from 'unstated-enhancers';
import autobind from 'class-autobind';
import Socket from '../../shared/Socket';
import { SafeAreaView } from 'react-native'
import authContainer from '../../screens/auth/containers/AuthContainer';
import NavBarChat from './components/NavBarChat';
import avatar from '../../images/drawer/perfil_black.png';


var UUID = require('uuid-js');

class ChatScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      messages: [],
      roomuuid: undefined,
      listing: lang.get('chat.disconnected', 'chat.disconnected'),
      connected: false,
      photo_avatar: undefined
    };

    autobind(this);
  }

  subscribe() {

    Socket.on('chat::message-received', this.onRecived)

    Socket.on('disconnect', this.handleDisconnect)

    Socket.on('chat::listen-typing', this.listing)

    Socket.on('chat::user-connect-listen', this.isConec)

    Socket.on('chat::user-disconnect-listen', this.isDisconec)

    Socket.on('chat::message-save', this.messageSave)
  }

  handleDisconnect() {
    Logger.dispatch('disconnect')
  }

  listing(data) {
    if (data && data.data !== '') {
      this.setState({
        listing: lang.get('chat.writing', 'chat.writing'),
        data: data
      })
    }

    setTimeout(() => {
      if (this.state.data === data) {
        this.setState({
          listing: this.state.connected ? lang.get('chat.connected', 'chat.connected') : lang.get('chat.disconnected', 'chat.disconnected'),
        });
      }
    }, 5000)
  }

  isConec(data) {
    const { chat } = this.props.containers;

    if (!this.state.connected) {
      const { room_chat } = this.props.HomeContainer.missionSelected;
      const uuidExecutiveMision = room_chat.room_uuid;
      const iduser = this.props.Signing.data.profile.uuid;

      const conec = {
        room: uuidExecutiveMision,
        id: iduser
      }

      Socket.isconnect(conec)
    }

    if (data) {
      this.setState({
        listing: lang.get('chat.connected', 'chat.connected'),
        connected: true
      })
    }

    chat.receivedMessages()
  }

  isDisconec() {
    this.setState({
      ...this.state,
      listing: lang.get('chat.disconnected', 'chat.disconnected'),
      connected: false
    })
  }

  messageSave(data) {
    if (data && this.state.connected) {
      const { chat } = this.props.containers;

      chat.updateMessage(data)
    }
  }

  onRecived(data) {
    const logoCompany = this.state.photo_avatar;
    const logo = logoCompany === null ? avatar : logoCompany;
    const chatCon = this.props.containers.chat;

    const dataMessage = {
      message_uuid: data.message_uuid,
      _id: data.message_uuid,
      text: data.message,
      createdAt: new Date(),
      user: {
        _id: 2,
        avatar: logo,
      },
    }

    chatCon.setMessageChat([dataMessage]);
  }

  renderBubble = props => {
    return (
      <Bubble
        {...props}
        wrapperStyle={{
          left: {
          },
          right: {
            backgroundColor: '#5bc1be'
          },
        }}
      />
    );
  };

  async getRoomUuid() {
    const chatContainer = this.props.containers.chat;
    const { room_chat } = this.props.HomeContainer.missionSelected;

    const logoCompany = undefined;

    const dataGet = {
      room_uuid: room_chat.room_uuid,
    }

    await chatContainer.getuuid(dataGet, logoCompany);

    const { menchat } = chatContainer.state;

    if (menchat.length > 0) {
      const getroom = menchat[0].room_uuid;
      this.setState({
        ...this.state,
        roomuuid: getroom,
      })
    }
    this.roomJoin();
  }

  async roomJoin() {
    const { room_chat } = this.props.HomeContainer.missionSelected;
    const uuidExecutiveMision = room_chat.room_uuid;
    const { data } = this.props.Signing;
    const iduser = data.profile.uuid;


    const conec = {
      room: uuidExecutiveMision,
      id: iduser
    }

    await Socket.join(uuidExecutiveMision);
    Socket.isconnect(conec)
  }

  async onSend(messages = []) {
    const { data } = this.props.Signing;
    const { missionSelected } = this.props.HomeContainer;

    const iduser = data.profile.uuid;
    const chatContainer = this.props.containers.chat;
    const idmission = missionSelected.uuid;
    const { room_chat } = this.props.HomeContainer.missionSelected;
    const uuidExecutiveMision = room_chat.room_uuid;

    for (const index in messages) {
      if (messages.hasOwnProperty(index)) {
        const message = messages[index];
        const message_uuid = UUID.create().toString()

        const data = {
          message_uuid: message_uuid,
          executive_uuid: iduser, //id de ejecutivo
          mission_uuid: idmission, //id de mision
          room_uuid: uuidExecutiveMision, //id de cuarto
          message: message.text,
          app: 'executive'
        }

        const newMessage = [{
          ...messages[0],
          message_uuid: message_uuid,
          sent: true
        }]

        this.sentChat(data);
        chatContainer.setMessageChat(newMessage);
      }
    }
  }

  sentChat(data) {
    return this.emit('chat::message-sent', { ...data });
  }

  emit(event, data) {
    Socket.client.emit(event, data);
  }

  async exitRoom() {
    const { room_chat } = this.props.HomeContainer.missionSelected;
    const uuidExecutiveMision = room_chat.room_uuid;

    await Socket.isdisconnet(uuidExecutiveMision)

    setTimeout(() => {
      Socket.leave(uuidExecutiveMision);
    }, 500)

    //this.socket.close();
  }

  change(value) {
    const { room_chat } = this.props.HomeContainer.missionSelected;
    const uuidExecutiveMision = room_chat.room_uuid;
    const { data } = this.props.Signing;
    const iduser = data.profile.uuid;


    const change = {
      room: uuidExecutiveMision,
      value: value,
      id: iduser
    }
    Socket.writing(change)
  }

  getAvatar() {
    const { company } = this.props.HomeContainer.missionSelected;

    const foto = company.profile.photo_bio_url

    this.setState({
      photo_avatar: foto
    })

  }

  renderSend(props) {
    return (
      <Send
        {...props}
      >
        <View style={{ paddingBottom: 17, marginRight: 15 }}>
          <Text style={{ color: '#0285c9', fontWeight: 'bold' }}>{lang.get('chat.send', 'chat.send')}</Text>
        </View>
      </Send>
    );
  }

  async componentDidMount() {

   await this.getAvatar();

    Socket.connect();

    this.subscribe();

    this.getRoomUuid();

  }

  componentWillUnmount() {
    const { chat } = this.props.containers;
    this.exitRoom();
    chat.reset();
  }

  render() {
    const { menchat } = this.props.containers.chat.state;

    const { company } = this.props.HomeContainer.missionSelected;
    const nameBusiness = company.business_name;

    return (
      <View style={{ flex: 1 }}>
        <NavBarChat {...this.props.navigation} name={nameBusiness} listing={this.state.listing} />
        {/* <View style={{ backgroundColor: '#eff4f4', alignItems: 'center', height: 30 }}>
          <Text style={{ paddingTop: 4 }}>{this.state.listing}</Text>
        </View> */}
        <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }} >
          <GiftedChat
            renderBubble={this.renderBubble}
            placeholder={lang.get('message.chat', 'chat.message')}
            messages={menchat}
            onSend={this.onSend}
            onInputTextChanged={this.change}
            renderSend={this.renderSend}
            user={{
              _id: 1,
            }}
          />
        </SafeAreaView>
      </View>
    );
  }
}

const containers = {
  chat: chat,
  language: language,
  Signing: Signing,
  HomeContainer: HomeContainer,
  authContainer: authContainer
};

const mapStateToProps = (containers) => {
  return {
    chat: containers.chat.state,
    language: containers.language.state,
    Signing: containers.Signing.state,
    HomeContainer: containers.HomeContainer.state,
    authContainer: containers.authContainer.state,
  }
}

export default connect(containers, mapStateToProps)(ChatScreen)



