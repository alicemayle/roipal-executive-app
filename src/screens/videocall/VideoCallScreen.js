import React, { Component } from 'react';
import { Text, Container, Button, Title, Icon } from 'native-base';
import { View, Image } from 'react-native';
import { connect } from 'unstated-enhancers';

import styles from '../../theme/globalStyles';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import videoCall from './container/VideoCallContainer'


class VideoCallScreen extends Component {
  constructor(props) {
    super(props);
    this._bind('navigate');
  }

  _bind(...methods) {
    methods.forEach((method) => {
      this[method] = this[method].bind(this);
    });
  }

  navigate() {
    this.props.navigation.navigate('SigninScreen');
  }

  render() {
    const uri = "http://cdn.onlinewebfonts.com/svg/img_568657.png";

    return (
      <Container style={styles.container}>

        <NavBarComponent
        {...this.props.navigation}
         name={lang.get('title.videoCall')} />

        <View style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'space-between',
          margin: 10,
          alignItems: 'center'
        }}>
          <View style={{ alignItems: 'center' }} >
            <Text style={[styles.margin25, styles.textAlignCenter]}>LLAMANDO...</Text>
          </View>
          <View >
            <Image source={{ uri: uri }} style={{ paddingLeft: 15, width: 200, height: 200, resizeMode: 'contain' }} />
            <Title style={{ color: 'blue', }}>HUGO FLORES BRAVO</Title>
          </View>
          <View style={{ alignItems: 'center' }} >
            <Button
              style={{
                borderRadius: 28,
                height: 56,
                elevation: 4,
                shadowColor: '#000',
                justifyContent: 'center',
                alignItems: 'center',
                shadowOpacity: 0.3,
                shadowRadius: 1,
                shadowColor: 'black',
                backgroundColor: '#DD5144',
                shadowOffset: {
                  height: 5, width: 2
                },
                marginBottom: 20
              }}
            >
              <Icon name="videocam" />
            </Button>
          </View>
        </View>
      </Container>
    );
  }
}
export default connect({
  videoCall
})(VideoCallScreen);
