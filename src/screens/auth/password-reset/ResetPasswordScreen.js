import React, { Component } from 'react';
import Validator from 'validatorjs';
import { Text, Container, Form, Button, Spinner } from 'native-base';
import { View, Image } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import { connect } from 'unstated-enhancers';
import autobind from 'class-autobind';
import lang from '../../../shared/languages/Lang'
import NavBarComponent from '../../../components/navBar/NavBarComponent';
import styles from '../../../theme/globalStyles';
import resetPassword from './containers/ResetPaswordContainer';
import TextInput from '../../../components/inputs/TextInput'
import toast from '../../../shared/support/ShowToast';

class ResetPasswordScreen extends Component {
  constructor(props) {
    super(props);
    autobind(this);
    this.state = {
      email: undefined,
      validation: new Validator(data, this.rules)
    }
  }

  validate() {
    const data = { ...this.state };
    const validation = new Validator(data, this.rules);

    this.validation = validation;

    validation.check();

    this.setState({
      validation
    })
  }

  get rules() {
    return {
      email: 'required|string|email',
    }
  }

  setFieldValue(field, value) {
    this.handleIsEditing();

    this.setState({
      [field]: value
    })
  }

  submitFromKeyboard() {
    this.resetPassword(true)
  }

  async resetPassword(validate = false) {
    const { httpBusy } = this.props.reset;

    if (httpBusy) {
      return;
    }

    if (validate) this.validate();

    const validation = this.validation;

    if (validation.passes()) {
      const { resetPassword } = this.props.containers;

      const data = {
        ...this.state
      }

      await resetPassword.attempResetPassword(data);

      const { error } = resetPassword.state;

      if (error) {
        toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))

        setTimeout(() => {
          resetPassword.clearError();
        }, 10000)
      } else {
        this.props.navigation.pop();
      }
    } else {
      toast.ShowToast('Faltan datos requeridos')

      const fields = Object.keys(this.rules);
      const values = {}

      for (const field of fields) {
        values[field] = this.state[field] || ''
      }

      this.setState({
        ...values
      })
    }
  }

  setNavigationGoBackConfirmationAlert() {
    this.props.navigation.setParams({
      alert: {
        title: lang.get('info.titleAlert', 'generic.title_alert'),
        message: lang.get('info.messageAlert', 'generic.message_alert')
      }
    });
  }

  handleIsEditing() {
    const { navigation } = this.props;

    const isEditing = navigation.getParam('isEditing');

    if (!isEditing) {
      navigation.setParams({
        isEditing: true
      });
    }
  }

  getParamsEmail() {
    const { navigation } = this.props;
    const email = navigation.getParam('email');

    this.setState({
      email: email
    })
  }

  componentDidMount() {
    this.setNavigationGoBackConfirmationAlert();
    this.getParamsEmail()
  }

  render() {
    const { email, validation } = this.state;
    const { httpBusy } = this.props.reset;

    return (
      <Container style={[styles.container]}>

        <NavBarComponent
          {...this.props.navigation}
          name={lang.get('title.resetPassword', 'signin.reset_password')} />

        <KeyboardAwareScrollView contentContainerStyle={{ paddingTop: 50, flex: 1 }} bounces={false}>
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <Image style={styles.logo} source={require('../../../images/logo_colores/logo_colores.png')} />
          </View>
          <Form style={{ paddingHorizontal: 25, paddingTop: 35 }}>
            <TextInput
              field="email"
              autoCapitalize="none"
              keyboardType="email-address"
              placeholder={lang.get('account.email', 'signin.email')}
              errors={validation.errors}
              returnKeyType={"send"}
              autoFocus={true}
              autoCorrect={false}
              value={email}
              onBlur={this.validate}
              onChangeText={this.setFieldValue}
              onSubmitEditing={this.submitFromKeyboard}
            />
            <Text note style={{textAlign: 'justify', margin: 20}}>{lang.get('policy.resetPassword', 'signin.info_password')}</Text>
            <Button
              //style={styles.margin25}
              block
              onPress={this.resetPassword}
            >
              <Text> {lang.get('button.recoverPassword', 'signin.button.recover_password')} </Text>
              {httpBusy && <Spinner color='lightblue' />}
            </Button>
          </Form>
        </KeyboardAwareScrollView>
      </Container>
    );
  }
}

const containers = {
  resetPassword: resetPassword
}

const mapStateToProps = (containers) => {
  return {
    reset: containers.resetPassword.state,
  };
}

export default connect(containers, mapStateToProps)(ResetPasswordScreen);