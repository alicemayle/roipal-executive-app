import { Container } from 'unstated';
import ResetPasswordService from '../services/ResetPasswordServices';
import { Logger } from 'unstated-enhancers';


const initialState = {
  email: undefined,
  httpBusy: false,
  message: undefined
}

class ResetPasswordContainer extends Container {
  state = { ...initialState }

  async attempResetPassword(data) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Reset Password request starts'
      })

      const response = await ResetPasswordService.attempResetPassword(data);

      if (response) {
        await this.setState({
          data: response,
          __action: 'Reset Password request success'
        })
      }
    } catch (error) {
      if (error.errors) {
        let messages = [];

        for (const [field, errors] of Object.entries(error.errors)) {
          messages = messages.concat(errors)
        }

        error.errors = messages
      }

      await this.setState({
        error: error,
        __action: 'Reset Password request error'
      })
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'Reset Password finally'
        })
      }, 500)
    }
  }

  setData(field, data) {
    this.setState({
      ...this.state,
      [field]: data,
      __action: 'reset password container set state'
    })
  }

  clearError() {
    this.setState({
      httpBusy: false,
      error: undefined,
      __action: 'reset password container ends'
    });
  }

  reset() {
    this.setState({
      ...initialState,
      __action: 'reset password container reset'
    })
  }

}

export default ResetPasswordContainer;