import http from '../../../../shared/Http';
import auth from '../../../../shared/services/Auth';
import { Logger } from 'unstated-enhancers';

class ResetPasswordService {

  attempResetPassword(data) {

    return http.post('/api/password/recovery', data);
    }
}
export default new ResetPasswordService;