import { Container } from 'unstated';
import auth from '../../../../shared/services/Auth';
import { Logger } from 'unstated-enhancers';

const initialState = {
  httpBusy: false,
  error: undefined,
  data: undefined,
  flag: true
};

class SigninContainer extends Container {
  name = 'SigninContainer'

  persist = {
    key: 'Signin'
  }
  state = { ...initialState }

  changeIconShowPassword(showPassword, iconShowPasswordAndroid, iconShowPasswordIos) {
    this.setState({ showPassword, iconShowPasswordAndroid, iconShowPasswordIos });
  }

  async setData(data) {
    await this.setState({
      data: data,
      __action: 'Signin setData'
    })

    await auth.setData(data);
  }

  async updateProfile(profile) {
    await this.setState({
      data: {
        ...this.state.data,
        profile
      },
      __action: 'Signin setProfile'
    })
  }

  async signin(data) {
    try {
      this.setState({
        httpBusy: true,
        error: undefined,
        __action: 'Signin starts'
      });

      const params = {
        includes: 'checklist'
      }

      const response = await auth.attempt(data, params);

      if (response) {
        auth.setData(response.data);

        await this.setState({
          data: response.data,
          __action: 'Signin success'
        });
      }
    } catch (error) {
      if(error.errors) {
        let messages = [];

        for (const [field, errors] of Object.entries(error.errors)) {
          messages = messages.concat(errors)
        }

        error.errors = messages
      }

      await this.setState({
        error,
        __action: 'Signin error'
      });
    } finally {
      this.setState({
        httpBusy: false,
        __action: 'Signin finally'
      });
    }
  }

  setFlag(data) {
    this.setState({
      ...this.state,
      flag: data,
      __action: 'set flag introduction'
    })
  }
  
  clear() {
    this.setState({
      httpBusy: false,
      error: undefined,
      __action: 'Signin ends'
    });
  }

  reset() {
    this.setState({
      ...initialState,
      __action: 'Signin reset'
    })
  }
}

export default SigninContainer;