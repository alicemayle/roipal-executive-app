import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Container, Text, View, Button, Icon, Content } from 'native-base';
import { Dimensions, Platform } from 'react-native';
import styles from '../styleAuth';
import logo from '../../../images/logo_blanco/logo_blanco.png';
import push from '../../../images/push_notifications/push.png';
import ubi from '../../../images/location/location.png';
import photo from '../../../images/multimedia/photo.png';
import camera from '../../../images/camera/camera.png';
import LinearGradient from 'react-native-linear-gradient';
import Permissions from '../../../shared/support/Permissions';
import lang from '../../../shared/languages/Lang';
import signin from '../signin/containers/SigninContainer'
import { connect } from 'unstated-enhancers';
import ViewPager from '../../../components/ViewPager';
import language from '../../../components/language/containers/LanguagesContainer'
import DescriptionItem from './components/DescriptionItem';

const { width, height } = Dimensions.get('window');

class Introduction extends Component {
  constructor(props) {
    super(props);
    autobind(this);

    this.state = {
      currentPage: 0,
      intro: [],
      permissionLoc: undefined,
      permissionCamera: undefined,
      permissionPhoto: undefined,
      permissionNotification: undefined,
    }
  }

  setItemsIntro() {
    const intro = [
      {
        id: 1,
        text2: lang.get('introduction.screen1', 'introduction.introduction'),
        image: logo,
        icon2: 'checkbox-blank-circle',
        icon3: 'checkbox-blank-circle-outline',
      },
      {
        id: 2,
        text: lang.get('introduction.screen2', 'introduction.notifications'),
        image: push,
        icon2: 'checkbox-blank-circle',
        icon3: 'checkbox-blank-circle-outline',
        plat: Platform.OS === 'ios' ? 'ios' : 'android'
      },
      {
        id: 3,
        text: lang.get('introduction.screen3', 'introduction.location'),
        image: ubi,
        icon2: 'checkbox-blank-circle',
        icon3: 'checkbox-blank-circle-outline',
      },
      {
        id: 4,
        text: lang.get('introduction.screen4', 'introduction.picture'),
        image: camera,
        icon2: 'checkbox-blank-circle',
        icon3: 'checkbox-blank-circle-outline',
      },
      {
        id: 5,
        text: lang.get('introduction.screen5', 'introduction.galery'),
        image: photo,
        icon2: 'checkbox-blank-circle',
        icon3: 'checkbox-blank-circle-outline',
      }
    ]

    this.setState({
      intro: intro,
      item: intro[0]
    })
  }

  async permissions() {
    this.permissionLoc = await Permissions.isAllowed('location');
    this.permissionNotification = Platform.OS === 'ios' ? await Permissions.isAllowed('notification') : undefined;
    this.permissionCamera = await Permissions.isAllowed('camera');
    this.permissionPhoto = await Permissions.isAllowed('photo');

    this.setState({
      permissionLoc: this.permissionLoc,
      permissionCamera: this.permissionCamera,
      permissionNotification: this.permissionNotification,
      permissionPhoto: this.permissionPhoto,
    })
  }

  async permitsNotifi() {
    const response = await Permissions.notification()
    this.setState({
      permissionNotification: response,
    })
  }

  async permitsLoc() {
    const response = await Permissions.location();
    this.setState({
      permissionLoc: response,
    })
  }

  async permitsCamera() {
    const response = await Permissions.camera();
    this.setState({
      permissionCamera: response,
    })
  }

  async permitsPhoto() {
    const response = await Permissions.photo();
    this.setState({
      permissionPhoto: response,
    })
  }

  goToSigninScreen() {
    const { signin } = this.props.containers;

    signin.setFlag(false);

    this.props.navigation.navigate('SigninScreen');
  }

  renderItem(record) {
    return (
      <DescriptionItem
        id={record.id}
        image={record.image}
        text={record.text}
        text2={record.text2}
        permissionNotification={this.state.permissionNotification}
        permissionLoc={this.state.permissionLoc}
        permissionCamera={this.state.permissionCamera}
        permissionPhoto={this.state.permissionPhoto}
        notification={this.permitsNotifi}
        location={this.permitsLoc}
        camera={this.permitsCamera}
        photo={this.permitsPhoto}
        buttonText={lang.get('introduction.permit', 'introduction.button.permit')}
      />
    )
  }

  handleOnPageChange(pag) {
    const page = pag < this.state.intro.length ? pag : (this.state.intro.length - 1)

    const item = this.state.intro[page]

    this.setState({
      currentPage: page,
      item,
    })
  }

  handleOnClickNextPage() {
    this.setState({
      currentPage: this.state.currentPage + 1
    })
  }

  componentDidMount() {
    this.setItemsIntro()
  }

  componentDidUpdate(prevProps) {
    if (prevProps.language.selected !== this.props.language.selected) {
      this.setItemsIntro()
    }
  }

  async componentWillMount() {
    await this.permissions();
  }

  render() {
    const { item, intro } = this.state;

    return (
      <Container style={[styles.container]}>
        <LinearGradient
          colors={['#135481', '#016eb8', '#0285c9', '#00a7e5', '#1dbed6', '#5bc1be']}
          start={{ x: 0, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={{
            width: width,
            height: height, flex: 1
          }}
        >
          {
            intro && item &&
            <View style={{ flex: 1, paddingVertical: 30, justifyContent: 'space-between', flexDirection: 'column' }}>
              <ViewPager initialPage={0} page={this.state.currentPage} onChangePage={this.handleOnPageChange}>
                {
                  [
                    <Content bounces={false} key={0}>{this.renderItem(intro[0])}</Content>,
                    <Content bounces={false} key={1}>{this.renderItem(intro[1])}</Content>,
                    <Content bounces={false} key={2}>{this.renderItem(intro[2])}</Content>,
                    <Content bounces={false} key={3}>{this.renderItem(intro[3])}</Content>,
                    <Content bounces={false} key={4}>{this.renderItem(intro[4])}</Content>,
                  ]
                }
              </ViewPager>
              <View style={{ alignItems: 'center' }}>
                <View style={{ alignItems: 'center', flexDirection: 'row', marginVertical: 10 }}>
                  <Icon name={item.id === 1 ? item.icon2 : item.icon3} type='MaterialCommunityIcons' style={[styles.iconCircle]} />
                  <Icon name={item.id === 2 ? item.icon2 : item.icon3} type='MaterialCommunityIcons' style={[styles.iconCircle]} />
                  <Icon name={item.id === 3 ? item.icon2 : item.icon3} type='MaterialCommunityIcons' style={[styles.iconCircle]} />
                  <Icon name={item.id === 4 ? item.icon2 : item.icon3} type='MaterialCommunityIcons' style={[styles.iconCircle]} />
                  <Icon name={item.id === 5 ? item.icon2 : item.icon3} type='MaterialCommunityIcons' style={[styles.iconCircle]} />
                </View>
                <View>
                  <Button
                    onPress={item.id !== 5 ? this.handleOnClickNextPage : this.goToSigninScreen}
                    style={{ backgroundColor: 'white', width: width - (width * 0.7) }}>
                    <Text style={{ color: 'black', width: '100%', textAlign: 'center' }}>
                      {item.id !== 5 ? lang.get('introduction.next', 'generic.button.next') : lang.get('introduction.finish', 'generic.button.finish')}
                    </Text>
                  </Button>
                </View>
              </View>
            </View>
          }
        </LinearGradient>
      </Container>
    );
  }
}

const containers = {
  signin: signin,
  language: language
};

const mapStateToProps = (containers) => {
  return {
    signin: containers.signin.state,
    language: containers.language.state
  }
}

export default connect(containers, mapStateToProps)(Introduction)