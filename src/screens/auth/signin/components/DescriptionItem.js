import React, { Component } from 'react';
import autobind from 'class-autobind';
import { Text, View, Button } from 'native-base';
import { Image, Platform } from 'react-native';
import style from '../../styleAuth'

class DescriptionItem extends Component {
  constructor(props) {
    super(props);
    autobind(this);
  }

  render() {

    const { image, text, text2, id, permissionLoc, permissionCamera, permissionPhoto,
      permission, notification, location, camera, photo, buttonText } = this.props

    return (
      <View style={style.description}>
        <View>
          <Image source={image} style={id === 1 ? style.image : style.image1} />
        </View>
        <View style={{ alignItems: 'center' }}>
          <Text style={id === 1 ? style.text : style.text1}>{text}</Text>
          <Text style={style.text2}>{text2}</Text>
        </View>
        <View style={style.contentButton}>
          {
            Platform.OS === 'ios' && id === 2 && permission === 'undetermined' &&
            (<Button onPress={notification} style={style.buttonPermission}>
              <Text style={style.buttonText}>{buttonText}</Text>
            </Button>)
          }
          {
            id === 3 && permissionLoc === 'undetermined' &&
            <Button onPress={location} style={style.buttonPermission}>
              <Text style={style.buttonText}>{buttonText}</Text>
            </Button>
          }
          {
            id === 4 && permissionCamera === 'undetermined' &&
            <Button onPress={camera} style={style.buttonPermission}>
              <Text style={style.buttonText}>{buttonText}</Text>
            </Button>
          }
          {
            id === 5 && permissionPhoto === 'undetermined' &&
            <Button onPress={photo} style={style.buttonPermission}>
              <Text style={style.buttonText}>{buttonText}</Text>
            </Button>
          }
        </View>
      </View>
    )
  }
}

export default DescriptionItem