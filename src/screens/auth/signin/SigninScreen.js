import React, { Component } from 'react';
import Validator from 'validatorjs';
import { Text, Container, Button, Icon, Content, Form, Spinner } from 'native-base';
import { View, Image, Platform, NativeModules, Keyboard } from 'react-native';
import autobind from 'class-autobind';
import fr from 'validatorjs/src/lang/fr';
import es from 'validatorjs/src/lang/es';
import en from 'validatorjs/src/lang/en';
import RNPickerSelect from 'react-native-picker-select';
import lang from '../../../shared/languages/Lang';
import styles from '../../../theme/globalStyles';
import LanguagesComponent from '../../../components/language/LanguagesComponent';
import signin from '../../auth/signin/containers/SigninContainer';
import language from '../../../components/language/containers/LanguagesContainer';
import { connect } from 'unstated-enhancers';
import checklist from '../../checklist/containers/ChecklistContainer';
import ConfigManagment from '../../../shared/support/ConfigManagment';
import TextInput from '../../../components/inputs/TextInput'
import ChecklistStatusService from '../../checklist/services/ChecklistStatusService';
import ListErrors from '../../../components/listErrors/ListErrors';
import Moment from '../../../shared/services/Moment';
import Permissions from '../../../shared/support/Permissions';
import { Dimensions } from 'react-native';
import Firebase from '../../../shared/support/Firebase';
import toast from '../../../shared/support/ShowToast';

const { width } = Dimensions.get('window');

class SigninScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: undefined,
      password: undefined,
      showPassword: true,
      iconShowPasswordAndroid: 'md-eye-off',
      iconShowPasswordIos: 'ios-eye-off',
      validation: new Validator(data, this.rules),
      valueLanguage: undefined
    }
    autobind(this);
    this.inputs = {};
  }

  validate() {
    const data = { ...this.state };
    const validation = new Validator(data, this.rules);

    validation.setAttributeNames({
      username: lang.get('account.email', 'signin.email'),
      password: lang.get('account.password', 'signin.password')
    });

    this.validation = validation;

    validation.check();

    this.setState({
      validation
    })
  }

  get rules() {
    return {
      username: 'required|string|email',
      password: 'required|string|min:8'
    }
  }

  handleValueChange(field, value) {
    this.setState({
      [field]: value
    });
  }

  handleRefSet(field, component) {
    this.inputs[field] = component;
  }

  setFieldValue(field, value) {
    this.setState({
      [field]: value
    })
  }

  changeIconShowPassword() {
    const { showPassword } = this.state;

    const showPass = !showPassword;
    let iconShowPassAndroid = !showPass ? "ios-eye" : "ios-eye-off";
    let iconShowPassIos = !showPass ? "md-eye" : "md-eye-off";

    this.setState({
      showPassword: showPass,
      iconShowPasswordAndroid: iconShowPassAndroid,
      iconShowPasswordIos: iconShowPassIos
    });
  }

  navigateToPassReset() {
    const { username } = this.state;

    this.props.navigation.navigate('ResetPasswordScreen', {email: username });
  }

  async signin(validate = false) {
    const { httpBusy } = this.props.containers.signin.state;
    const { language } = this.props.containers;

    if (httpBusy) return;

    if (validate) this.validate();

    const validation = this.validation;

    if (validation.passes()) {
      const { signin } = this.props.containers;

      const credentials = {
        grant_type: ConfigManagment.getGrantType(),
        client_id: ConfigManagment.getClientID(),
        client_secret: ConfigManagment.getClientSecret(),
        scope: ConfigManagment.getScope(),
        username: this.state.username,
        password: this.state.password,
      }

      await signin.signin(credentials);

      const { error } = signin.state;

      if (error) {
        toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'));

        setTimeout(() => {
          signin.clear();
        }, 10000)

      } else {
        this.setState({
          ...this.state,
          error: undefined
        })

        const { checklist } = this.props.containers;
        const { data } = signin.state;

        const status = ChecklistStatusService.getCurrentStatus(data.checklist, checklist.state.data);

        await checklist.setCompletionStatus(status);

        this.goAhead();
      }

    } else {
      toast.ShowToast(lang.get('message.dataRequired', 'generic.data_required'));

      const fields = Object.keys(this.rules);
      const values = {}

      for (const field of fields) {
        values[field] = this.state[field] || ''
      }

      this.setState({
        ...values
      })
    }
  }

  register() {
    this.props.navigation.navigate('RegistrationScreen');
  }

  goAhead() {
    this.props.navigation.navigate('AuthLoading')
  }

  submitFromKeyboard() {
    this.signin(true)
  }

  handleValueChangeLanguage(idiom) {
    let value = idiom
    this.setState( {
      selectedLanguage: true
    })

    const { language } = this.props.containers;

    if (Platform.OS === 'android') {
      this.setLanguage(value)
    }

    if (!value) {
      value = this.state.valueLanguage;
    }

    setTimeout(() => {

      let resorce = en;

      if (value === 'es') {
        resorce = es;
      }

      if (value === 'fr') {
        resorce = fr;
      }

      Validator.setMessages(value, resorce);
      Validator.useLang(value);

      lang.setLocale(value);

      this.validate();

      language.updateState('selected', value);

      Moment.setLanguaje(value);

      setTimeout( () => {
        this.setState({
          selectedLanguage: false
        })
      }, 200)

    }, 10)
  }

  setPreviousLanguage() {
    setTimeout(() => {
      const { selected } = this.props.containers.language.state;

      if (!this.state.selectedLanguage) {
        this.setState({
          valueLanguage: selected
        })
      }
    }, 100)
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.username !== this.state.username ||
      nextState.password !== this.state.password ||
      nextState.showPassword !== this.state.showPassword ||
      nextState.iconShowPasswordAndroid !== this.state.iconShowPasswordAndroid ||
      nextState.iconShowPasswordIos !== this.state.iconShowPasswordIos ||
      nextState.validation !== this.state.validation ||
      nextProps.httpBusy !== this.props.httpBusy ||
      nextProps.language.selected !== this.props.language.selected ||
      nextState.valueLanguage !== this.state.valueLanguage
  }

  getLanguageCode() {
    let systemLanguage = 'en';
    if (Platform.OS === 'android') {
      systemLanguage = NativeModules.I18nManager.localeIdentifier;
    } else {
      systemLanguage = NativeModules.SettingsManager.settings.AppleLocale;
    }
    const languageCode = systemLanguage.substring(0, 2);
    return languageCode;
  }

  setLanguage(value) {

    if (value) {
      this.setState({
        valueLanguage: value
      })
    } else {
      const { selected } = this.props.containers.language.state;

      this.setState({
        valueLanguage: selected
      })
    }
  }

  clearUser() {
    this.setState({
      username: undefined,
    })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.language.selected !== this.props.language.selected) {
      this.setLanguage()
    }
  }

  async componentDidMount() {

    const { language } = this.props.containers;
    const { selected } = language.state;

    const idiom = !!!selected ? this.getLanguageCode() : selected;

    let resorce = en;

    if (idiom === 'es') {
      resorce = es;
    }

    if (idiom === 'fr') {
      resorce = fr;
    }

    Validator.setMessages(idiom, resorce);
    Validator.useLang(idiom);

    lang.setLocale(idiom);

    language.setData(idiom);

    this.setLanguage(idiom)

    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide,
    );

    const permissionLocation = await Permissions.isAllowed('location');
    const permissionCamera = await Permissions.isAllowed('camera');
    const permissionPhoto = await Permissions.isAllowed('photo');

    if(Platform.OS === 'ios') {
      const permissionNotification = await Permissions.isAllowed('notification');

      if (permissionNotification === 'undetermined')
        await Permissions.notification()
    }

    if (permissionLocation === 'undetermined')
      await Permissions.location()

    if (permissionCamera === 'undetermined')
      await Permissions.camera()

    if (permissionPhoto === 'undetermined')
      await Permissions.photo()

    if(Platform.OS === 'ios') {
      Firebase.checkPermission()
    }
  }

  _keyboardDidHide() {
  }

  componentWillUnmount() {
    this.keyboardDidHideListener.remove();
  }

  render() {
    const { username, password, showPassword, validation, valueLanguage } = this.state;
    const { iconShowPasswordAndroid, iconShowPasswordIos } = this.state;
    const { signin, language } = this.props.containers;
    const { selected } = language.state;
    const { httpBusy } = signin.state;
    const { error = {} } = this.props.containers.signin.state;
    const items = [
      {
        label: lang.get('language.english', 'generic.languaje.english'),
        value: 'en',
      },
      {
        label: lang.get('language.spanish', 'generic.languaje.spanish'),
        value: 'es',
      },
      {
        label: lang.get('language.french', 'generic.languaje.french'),
        value: 'fr',
      },
    ];

    return (
      <Container style={[styles.container]}>
        <Content bounces={false} padder style={{flex: 1, paddingHorizontal: 15}}>
          <View style={{ flexDirection: 'row', justifyContent: 'center', paddingTop: 35 }}>
            <Image style={styles.logo} source={require('../../../images/logo_colores/logo_colores.png')} />
          </View>

          <Form style={{ paddingTop: 30 }}>
            <TextInput
              field="username"
              next="password"
              onRefSet={this.handleRefSet}
              refs={this.inputs}
              errors={validation.errors}
              returnKeyType={"next"}
              autoFocus={true}
              placeholder={lang.get('account.email', 'signin.email')}
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType="email-address"
              value={username}
              onBlur={this.validate}
              onChangeText={this.setFieldValue}
              clearButtonMode={'while-editing'}
              iconRight={Platform.OS === 'android' ? <Icon name='close-circle' onPress={this.clearUser} /> : undefined}
            />

            <TextInput
              field="password"
              onRefSet={this.handleRefSet}
              refs={this.inputs}
              errors={validation.errors}
              returnKeyType={"send"}
              placeholder={lang.get('account.password', 'signin.password')}
              secureTextEntry={showPassword}
              value={password}
              iconRight={<Icon onPress={this.changeIconShowPassword} />}
              onChangeText={this.setFieldValue}
              onSubmitEditing={this.submitFromKeyboard}
              clearButtonMode={'while-editing'}
              iconRight={<Icon ios={iconShowPasswordIos} android={iconShowPasswordAndroid} onPress={this.changeIconShowPassword} />}
            />

            <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 15 }}>
              {Platform.OS === 'android' &&
                <LanguagesComponent selected={selected} onValueChange={this.handleValueChangeLanguage} />
              }
              {
                Platform.OS === 'ios' &&
                <RNPickerSelect
                  placeholder={{
                    label: lang.get('modal.language', 'generic.languaje.languaje'),
                    value: undefined,
                  }}
                  items={items}
                  value={valueLanguage}
                  onValueChange={this.setLanguage}
                  onDonePress={this.handleValueChangeLanguage}
                  onClose={this.setPreviousLanguage}
                  onDownArrow={this.setPreviousLanguage}
                  style={{
                    inputIOS: {
                      color: 'black',
                      width: width*0.35,
                      paddingHorizontal: 30,
                      height: 25
                    },
                    modalViewBottom: {
                      backgroundColor: 'white'
                    },
                    modalViewMiddle: {
                      backgroundColor: 'white',
                      borderColor: 'gray',
                      borderWidth: 0.2
                    },
                    iconContainer: {
                      left: 0,
                    },
                  }}
                  Icon={() => {
                    return <Icon name='language' type='FontAwesome' style={{ color: '#2b87ef', fontSize: 22, marginRight: 5 }} />;
                  }}
                  doneText={lang.get('modal.done', 'generic.button.done')}
                />
              }
            </View>
          </Form>

          <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10 }}>
              <Button
                transparent
                light
                onPress={this.navigateToPassReset}
              >
                <Text style={{ textDecorationLine: 'underline', color: '#2b87ef' }}> {lang.get('button.forgetPassword', 'signin.button.forget_password')} </Text>
              </Button>
            </View>

          <Button
              style={styles.margin25}
              block
              onPress={this.signin}
            >
              <Text> {lang.get('button.buttonSignin', 'signin.button.signin')} </Text>
              {httpBusy && <Spinner color='lightblue' />}
            </Button>
            <Button
              style={styles.margin25}
              block
              success
              onPress={this.register}
            >
              <Text> {lang.get('button.buttonRegister', 'signin.button.register')} </Text>
            </Button>
          {error.errors && (
            <ListErrors errors={error.errors} />
          )}
        </Content>
      </Container>
    )
  }
}

const containers = {
  language: language,
  signin: signin,
  checklist: checklist,
}

const mapStateToProps = (containers) => {
  return {
    httpBusy: containers.signin.state,
    language: containers.language.state,
  };
}

export default connect(containers, mapStateToProps)(SigninScreen);