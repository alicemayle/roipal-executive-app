import React, { Component } from 'react';
import { Spinner, View } from 'native-base';
import { Alert, AppState, NativeModules, Platform } from "react-native";
import styles from '../../theme/globalStyles';
import Auth from '../../shared/services/Auth';
import { connect, Logger, Persist, Manager } from 'unstated-enhancers';
import checklist from '../checklist/containers/ChecklistContainer';
import signin from './signin/containers/SigninContainer';
import fr from 'validatorjs/src/lang/fr';
import es from 'validatorjs/src/lang/es';
import en from 'validatorjs/src/lang/en';
import Validator from 'validatorjs';
import Lang from '../../shared/languages/Lang';
import authContainer from './containers/AuthContainer';
import Observer from '../../shared/Observer';
import autobind from 'class-autobind';
import logout from '../auth/containers/LogoutContainer';
import firebase from 'react-native-firebase';
import Moment from '../../shared/services/Moment';
import AuthLoadingServices from './services/AuthLoadingServices';
import ChecklistStatusService from '../checklist/services/ChecklistStatusService';
import languagesContainer from '../../components/language/containers/LanguagesContainer';
import { api } from '../../shared/languages/Lang';
import toast from '../../shared/support/ShowToast';

class AuthLoadingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      appState: AppState.currentState
    }

    autobind(this);
    this.invalidToken()
    this.deleteAccount()
  }

  invalidToken() {
    Observer.subscribe('invalid_token', this.expireSession)
  }

  deleteAccount() {
    Observer.subscribe('delete_account', this.handleCloseSession)
  }

  async expireSession() {
    const { signin } = this.props.containers;

    const flag = signin.state.flag;

    Persist.clear();
    Manager.reset();

    await signin.setFlag(flag)

    await Observer.unSubscribe('invalid_token', this.expireSession)

    setTimeout(() => {
      toast.ShowToast(Lang.get('info.expireSession'))
      this.props.navigation.navigate('SigninScreen')
    }, 2000)
  }

  async sendToken() {

    const { authContainer, signin } = this.props.containers;

    const token = await Manager.get('AuthContainer').state.fcm_token;

    const data = {
      fcm_token: token
    }

    await authContainer.sendToken(data);

    const { error } = this.props.containers.authContainer.state;

    if (!error) {
      const infoProfile = this.props.containers.authContainer.state.data;
      signin.updateProfile(infoProfile);
    }
  }

  subscribeLogoutListener() {
    Observer.subscribe('logout', this.closeSession)
  }

  closeSession() {
    Alert.alert(
      Lang.get('sidebar.goOut'),
      Lang.get('sidebar.signOff'),
      [
        {
          text: Lang.get('sidebar.cancel'),
        },
        {
          text: Lang.get('sidebar.signOff1'),
          onPress: () => this.handleCloseSession()
        },
      ],
      {
        cancelable: false
      }
    )
  }

  async handleCloseSession() {
    const { logout } = this.props.containers;

    await logout.logout();

    const { error } = this.props.containers.logout.state;

    if (error !== undefined) {
      toast.ShowToast(error.message || Lang.get('info.synchronizationError'))

      setTimeout(() => {
        logout.clearError();
      }, 200)
    } else {
      setTimeout(this.close, 100)
    }
  }

  async close() {
    const { authContainer, signin, languagesContainer } = this.props.containers;
    const { selected } = languagesContainer.state;

    const flag = signin.state.flag;
    const idiom = selected;

    Persist.clear();

    Manager.reset();

    const { fcm_token } = authContainer.state;

    if (!fcm_token) {
      const fcmToken = await firebase.messaging().getToken();

      if (fcmToken) {
        authContainer.setFcmToken(fcmToken);
      }
    }

    await signin.setFlag(flag)

    await languagesContainer.setData(idiom);
    Lang.setLocale(idiom);

    Observer.unSubscribe('logout', this.closeSession)
    Observer.unSubscribe('delete_account', this.handleCloseSession)

    this.props.navigation.navigate('SigninScreen')
  }

  getLanguageCode() {
    let systemLanguage = 'en';
    if (Platform.OS === 'android') {
      systemLanguage = NativeModules.I18nManager.localeIdentifier;
    } else {
      systemLanguage = NativeModules.SettingsManager.settings.AppleLocale;
    }
    const languageCode = systemLanguage.substring(0, 2);
    return languageCode;
  }


  async _bootstrapAsync() {
    try {
      const { signin, authContainer, languagesContainer } = this.props.containers;
      const { data, flag } = signin.state;
      const { fcm_token } = authContainer.state;
      const { selected } = languagesContainer.state;

      const language =  !!!selected ? this.getLanguageCode() : selected;
      let resorce = en;

      Logger.dispatch('language', language);

      if (language === 'es') {
        resorce = es;
      }

      if (language === 'fr') {
        resorce = fr;
      }

      Validator.setMessages(language, resorce);
      Validator.useLang(language);

      const _lang = Lang.getLocale();

      Lang.setLocale(language);
      api.setLocale(language);

      Moment.setLanguaje(_lang);

      languagesContainer.getTranslationsVersion();

      const { translations } = languagesContainer.state;

      if (!translations) {
        languagesContainer.getTranslations();
      }

      if (flag) {
        this.props.navigation.navigate('Introduction');
      }
       else {
        if (data) {

          if (data.profile.fcm_token !== fcm_token) {
            await this.sendToken();
          }

          this.subscribeLogoutListener();

          await Auth.setData(data);
          await authContainer.getProfile(data);

          const { checklist } = this.props.containers;
          const steps = checklist.state.data;

          const completed = data.checklist.filter(item => item.completed).map(item => item.step);

          const status = steps.map(item => {
            item.completed = item.completed || completed.indexOf(item.key) !== -1;

            return item;
          })

          const incompleted = status.filter(item => {
            return !item.completed;
          })

          checklist.setCompletionStatus(status);

          if (incompleted.length > 0) {
            const activation = incompleted.find(step => step.key === 'activation');

            if (!!activation && (activation.key === 'activation' && activation.completed === false)) {

            const requestCheckList =  await AuthLoadingServices.getCheckList(data.profile.uuid);

            const stepActivation = requestCheckList.data.checklist.find(step => step.step === 'activation');

            if (!!stepActivation && (stepActivation.step === 'activation' && stepActivation.completed === true)) {

              const mixCheckList = await ChecklistStatusService.getCurrentStatus(requestCheckList.data.checklist, steps);

              await checklist.setCompletionStatus(mixCheckList);

              await checklist.setStepsTranslations()

              setTimeout(() => {
                this.props.navigation.navigate('ChecklistScreen');
              }, 100)
            }

            this.props.navigation.navigate('VerificationScreen');

            } else {
              await checklist.setStepsTranslations()

              setTimeout(() => {
                this.props.navigation.navigate('ChecklistScreen');
              }, 100)

            }
          } else {

            this.props.navigation.navigate('HomeScreen');
          }

        } else {
          this.props.navigation.navigate('SigninScreen');
        }
      }

    } catch (error) {

    }
  }

  handleAppStateChange = (nextAppState) => {

    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {

      if (signin.state) {
        const { data } = signin.state;

        if (data) {
          AuthLoadingServices.getCheckList(data.profile.uuid);
        }
      }
    }
    this.setState({ appState: nextAppState });
  }

  componentDidMount() {
    this._bootstrapAsync();
    AppState.addEventListener('change', this.handleAppStateChange)
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  render() {
    return (
      <View style={styles.splashScreen}>
        <Spinner large color='lightblue' />
      </View>
    );
  }
}

export default connect({
  checklist, signin, authContainer, logout, languagesContainer
})(AuthLoadingScreen);