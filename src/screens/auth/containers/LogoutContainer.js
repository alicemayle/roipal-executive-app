import { Container } from "unstated";
import AuthLoadingServices from "../services/AuthLoadingServices";

const initialState = {
  error: undefined,
  httpBusy: undefined,
  data: undefined,
  message: undefined,
}

class LogoutContainer extends Container {
  name = 'LogoutContainer'

  persist = {
    key: 'LogoutContainer'
  }

  state = { ...initialState }

  async logout() {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Logout starts'
      })

      const response = await AuthLoadingServices.logout();

      if(response) {
        await this.setState({
          data: response,
          message: response.message,
          __action: 'Logout success'
        })
      }
    } catch (error) {
      await this.setState({
        error: error,
        __action: 'Logout error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'Logout finally'
        });
      }, 500)
    }
  }

  clearError() {
    this.setState({
      ...this.state,
      error: undefined,
      message: undefined,
      __action: 'Clear error'
    })
  }

  reset() {
    this.setState({
      ...initialState,
      __action: 'Notification Reset'
    });
  }
}

export default LogoutContainer