import { Container } from "unstated";
import AuthLoadingServices from "../services/AuthLoadingServices";
import { Logger } from "unstated-enhancers";

const initialState = {
  error: undefined,
  httpBusy: undefined,
  data: undefined,
  message: undefined,
  fcm_token: undefined,
  profile: undefined
}

class AuthContainer extends Container {
  name = 'AuthContainer'

  persist = {
    key: 'AuthContainer'
  }

  state = { ...initialState }

  setData(data){
    this.setState({
      ...this.state,
      data
    })
  }

  async setFcmToken(fcm_token){
      await this.setState({
        fcm_token,
        __action: 'Token was Changed'
      })
  }

  clearError() {
    this.setState({
      ...this.state,
      error: undefined,
      message: undefined,
      __action: 'Clear error'
    })
  }

  reset() {
    this.setState({
      ...initialState,
      __action: 'Notification Reset'
    });
  }

  async sendToken(token) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Send Token starts'
      })

      const response = await AuthLoadingServices.sendToken(token);

      if (response) {
        await this.setState({
          data: response.data,
          __action: 'Send Token success'
        })
      }
    } catch (error) {
      await this.setState({
        error: error,
        __action: 'Send Token error'
      })
    } finally {
        this.setState({
          httpBusy: false,
          __action: 'SendToken finally'
        });
    }
  }

  async getProfile(data) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get Profile executive starts'
      })

      const response = await AuthLoadingServices.getProfile(data.profile.uuid);

      if (response) {
        let payment = undefined
        if(response.data.payments_profiles.length > 0 ) {
          payment = response.data.payments_profiles[0]
        }

        await this.setState({
          profile: response.data.profile,
          payment: payment,
          meta: response.meta,
          message: response.message,
          httpBusy: false,
          __action: 'Get Profile executive success'
        })
      }
    } catch (error) {
      await this.setState({
        error: error,
        httpBusy: false,
        __action: 'Get Profile executive error'
      })
    }
  }

  updateStateManually(data){
    this.setState({
      ...this.state,
      ...data,
      data: { ...data }
    })

  }

  setProfile(profile) {
    this.setState({
      ...this.state,
      profile: profile,
      __action: 'set profile registration'
    })
  }

  updatePayment(payment) {
    this.setState({
      ...this.state,
      payment: payment
    })
  }

  modifyInvitationsReceived() {
    this.setState({
      ...this.state,
      profile: {
        ...this.state.profile,
        invitations_received: this.state.profile.invitations_received - 1
      }
    })
  }
}

export default AuthContainer