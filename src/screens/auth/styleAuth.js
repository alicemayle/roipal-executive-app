import { StyleSheet, } from 'react-native';
import { Dimensions, Platform } from 'react-native';
const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  description: {
    paddingTop: Platform.OS === 'ios' ? 25 : 0,
    paddingHorizontal: 25,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  image: {
    resizeMode: 'contain',
    width: width * 0.7,
    height: 100,
    marginBottom: 30
  },
  image1: {
    resizeMode: 'contain',
    width: width * 0.7,
    height: 150,
    marginBottom: 30
  },
  text: {
    color: 'white',
    fontSize: 30,
    textAlign: 'justify',
    marginBottom: 20
  },
  text1: {
    color: 'white',
    fontSize: 20,
    textAlign: 'justify',
    marginBottom: 0
  },
  text2: {
    color: 'white',
    fontSize: 20,
    textAlign: 'justify'
  },
  contentButton: {
    alignItems: 'center',
    marginBottom: 20,
    marginTop: 30
  },
  buttonPermission: {
    backgroundColor: 'white',
    width: width - (width * 0.7)
  },
  buttonText: {
    color: 'black',
    textAlign: 'center',
    width: '100%'
  },
  container: {
    flex: 1,
  },
  iconCircle: {
    fontSize: 12,
    color: 'white',
    marginHorizontal: 2,
  },
});

export default styles;