import React, { Component } from 'react';
import { Container, Content } from 'native-base';

import InfoProfile from './components/InfoProfileComponent';
import NavBarComponent from '../../../components/navBar/NavBarComponent';
import { connect, Logger } from 'unstated-enhancers';
import language from '../../../components/language/containers/LanguagesContainer';
import register from '../../auth/registration/containers/RegistrationContainer';
import signin from '../../auth/signin/containers/SigninContainer';
import checklist from '../../checklist/containers/ChecklistContainer';
import newLang from '../../../shared/languages/Lang';
import autobind from 'class-autobind';
import ChecklistStatusService from '../../checklist/services/ChecklistStatusService';

class RegistrationScreen extends Component {
  constructor(props) {
    super(props);

    autobind(this)
  }

  async handleAccoutnActivated(data) {
    const { checklist } = this.props.containers;
    const { profile } = data;

    const updatedCheckList = profile.checklist;

    const currentCheckList = checklist.state.data;

    const mixCheckList = await ChecklistStatusService.getCurrentStatus(updatedCheckList, currentCheckList);

    await checklist.setCompletionStatus(mixCheckList);

    this.props.navigation.navigate('AuthLoading');
  }

  setNavigationGoBackConfirmationAlert() {
    this.props.navigation.setParams({
      alert: {
        title: newLang.get('profile.alertTitle', 'generic.alert_title'),
        message: newLang.get('profile.alertMessage', 'generic.alert_message')
      }
    });
  }

  handleEditing() {
    const { navigation } = this.props;
    const isEditing = navigation.getParam('isEditing');

    if (!isEditing) {
      navigation.setParams({
        isEditing: true
      });
    }
  }

  componentDidMount() {
    this.setNavigationGoBackConfirmationAlert()
  }

  render() {
    const { register, signin, checklist, language } = this.props.containers;

    return (
      <Container>
        <NavBarComponent
          {...this.props.navigation}
          name={newLang.get('title.registration', 'signin.register.title')} />

        <InfoProfile
          {...this.props}
          registrationContainer={register}
          signin={signin}
          checklist={checklist}
          language={language}
          onHandleAccoutnActivated={this.handleAccoutnActivated}
          onEditing={this.handleEditing}
        />
      </Container>
    )
  }
}

const containers = {
  language: language,
  register: register,
  signin: signin,
  checklist: checklist
}

const mapStateToProps = (containers) => {
  return {
    showAccountDataComponent: containers.register.state,
  };
}

export default connect(containers, mapStateToProps)(RegistrationScreen);