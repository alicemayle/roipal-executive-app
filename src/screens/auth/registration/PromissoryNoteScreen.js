import React, { Component } from 'react';
import { Container, Text, Form } from 'native-base';
import { ScrollView, Dimensions, View, Image } from 'react-native';

import PromissoryNote from './sources/PromissoryNote';
import PromissoryNote2 from './sources/PromissoryNote2';
import styles from '../../../theme/globalStyles';
import lang from '../../../shared/languages/Lang';
import NavBarComponent from '../../../components/navBar/NavBarComponent';
import { Logger } from 'unstated-enhancers';
import autobind from 'class-autobind';

const { height, width } = Dimensions.get('window');

class PromissoryNoteScreen extends Component {
  constructor(props) {
    super(props);
    autobind(this);
  }

  render() {
    return (
      <Container style={styles.container} >

        <NavBarComponent
          {...this.props.navigation}
          name={lang.get('title.promissoryNote', 'signin.register.promissory_note')} />

        <View padder>
           <View style={{ flexDirection: 'row', justifyContent: 'center', paddingTop: 15 }}>
            <Image style={styles.logo} source={require('../../../images/logo_colores/logo_colores.png')} />
          </View>
          <Form style={{paddingTop: 15}}>
            <ScrollView style={[styles.borderWidth1, styles.lightGray, { height: height * 0.71, backgroundColor: '#0285c9', marginLeft: 15, marginRight: 15, marginBottom: 40 }]}>
              <Text style={[styles.textAlignJustify, { color: 'white', marginHorizontal:10 }]}> {PromissoryNote} </Text>
              <Text style={[styles.textAlignJustify, { color: 'white', marginHorizontal:10 }]}> {PromissoryNote2} </Text>
            </ScrollView>
          </Form>
        </View>
      </Container>
    );
  }
}
export default PromissoryNoteScreen;