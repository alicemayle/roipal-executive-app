import { Container } from 'unstated';
import RegisterServices from '../services/RegisterServices';
import auth from '../../../../shared/services/Auth';

const initialState = {
  showAccountDataComponent: true,
  showDisabilytyComponent: false,
  accountData: {
    name: undefined,
    email: undefined,
    password: undefined
  },
  disability: {
    visual: false,
    mobility: false,
  },
  signPromissoryNote: false,
  httpBusy: false,
  response: undefined,
  error: undefined,
  data: undefined,
  validation: undefined
};

class RegistrationContainer extends Container {
  state = { ...initialState }

  updateState(key, value) {
    const state = { ...this.state };
    state[key] = value;
    this.setState(state);
  }

  updateStateNested(parent, key, value) {
    const state = { ...this.state };
    state[parent][key] = value;
    this.setState(state);
  }

  showAccountDataComponentAndHideShowDisabilytyComponent() {
    this.setState({
      showAccountDataComponent: false,
      showDisabilytyComponent: true
    });
  }
  changeIconShowPassword(showPassword, iconShowPasswordAndroid, iconShowPasswordIos) {
    this.setState({ showPassword, iconShowPasswordAndroid, iconShowPasswordIos });
  }

  setUserData(name, email, password) {
    this.setState({
      accountData: {
        name: name,
        email: email,
        password: password
      }
    })
  }

  async register(data) {

    try {
      this.setState({
        httpBusy: true,
        __action: 'Register start'
      })

      const response = await RegisterServices.register(data);

      if(response) {
        auth.setData(response.data);

        await this.setState({
          data: response.data,
          __action: 'Register success'
        })
      }
    } catch (error) {

      if (error.errors) {
        let messages = [];

        for (const [field, errors] of Object.entries(error.errors)) {
          messages = messages.concat(errors)
        }

        error.errors = messages
      }

      await this.setState({
        error,
        __action: 'Register error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'Register finally'
        });
      }, 500)
    }
  }

  async sendEmail() {
    try {
      await this.setState({
        httpBusy: true,
        __action: 'REQUEST Send Email'
      })

      const response = await RegisterServices.sendEmail();

      if(response) {
        this.setState({
          validation: response.data,
          response: response.message,
          __action: 'SUCCESS Send Email'
        })
      }
    } catch (error) {

      if(error.errors) {
        let messages = [];

        for (const [field, errors] of Object.entries(error.errors)) {
          messages = messages.concat(errors)
        }

        error.errors = messages
      }

      this.setState({
        error: error,
        __action: 'ERROR Send Email'
      })
    }
    finally{
      setTimeout( () => {
        this.setState({
          httpBusy: false,
          __action: 'Finished Send Email'
        })
      }, 500 )
    }
  }

  clear() {
    this.setState({
      error: undefined,
      __action: 'Register ends'
    });
  }

  resetState() {
    this.setState({
      ...initialState,
      __action: 'Register reset'
    });
  }
}

export default RegistrationContainer;