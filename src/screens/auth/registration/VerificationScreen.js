import React, { Component } from 'react';
import { Container, View, Text, Body, Spinner } from 'native-base';
import { Image } from 'react-native';
import autobind from 'class-autobind';
import { connect } from 'unstated-enhancers';
import lang from '../../../shared/languages/Lang';
import styles from '../../../theme/globalStyles';
import registrationContainer from '../registration/containers/RegistrationContainer'
import NavBarComponent from '../../../components/navBar/NavBarComponent';
import toast from '../../../shared/support/ShowToast';

class VerificationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      httpBusy: false
    }
    autobind(this);
  }

  onPressLeftButton() {
    this.props.navigation.navigate('ChecklistScreen');
  }

  async verify() {
    await this.setState({
      httpBusy: true
    })

    const { registrationContainer } = this.props.containers;
    const { httpBusy } = registrationContainer.state;

    if (httpBusy) return;

    await registrationContainer.sendEmail();

    await this.setState({
      httpBusy: false
    })

    const { error } = registrationContainer.state;

    if (error !== undefined) {
      toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))

      setTimeout(() => {
        registrationContainer.clear();
      }, 10000)
    } else {
      toast.ShowToast(lang.get('account.sendEmail', 'signin.send_email'))
      registrationContainer.resetState();
    }
  }

  render() {
    const { httpBusy } = this.state;

    return (
      <Container style={styles.container}>
        <NavBarComponent  {...this.props.navigation} onPressLeftButton={this.onPressLeftButton}
          name={lang.get('title.verification', 'signin.verification')} />

        <Body style={{ padding: 10, flex: 1 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'center', marginVertical: 15 }}>
            <Image style={styles.logo} source={require('../../../images/logo_colores/logo_colores.png')} />
          </View>

          <Text style={[styles.margin25, styles.marginHorizontal25, styles.textAlignJustify]}>
            {lang.get('policy.verify', 'signin.verify')}
          </Text>
          <Text style={[styles.margin25, styles.marginHorizontal25, styles.textAlignJustify]} onPress={this.verify}>
            {lang.get('resend.resend', 'signin.resend')}
            <Text style={[styles.margin25, styles.textAlignJustify, styles.blue]} onPress={this.verify}>
              {lang.get('policy.here', 'signin.here')}.
            </Text>
          </Text>
        </Body>

        {
          httpBusy && <Spinner color="lightblue" />
        }

      </Container>
    )
  }
}

const containers = {
  registrationContainer: registrationContainer,
}

const mapStateToProps = (containers) => {
  return {
    registrationContainer: containers.registrationContainer.state,
  };
}

export default connect(containers, mapStateToProps)(VerificationScreen);