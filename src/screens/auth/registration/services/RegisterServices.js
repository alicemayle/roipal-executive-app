import http from "../../../../shared/Http";
import qs from 'qs';

class RegisterServices {

  register(data) {
    return http.post('api/executives/', data);
  }

  sendEmail() {
    return http.post('/api/users/email-verification/');
  }
}
export default new RegisterServices;