import React, { PureComponent } from 'react';
import { Image } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import { Form, Button, Text, Icon, View, Content, ListItem, CheckBox, Body, Spinner } from 'native-base';
import Validator from 'validatorjs';
import lang from '../../../../shared/languages/Lang';
import styles from '../../../../theme/globalStyles';
import autobind from 'class-autobind';
import TextInput from '../../../../components/inputs/TextInput';
import ListErrors from '../../../../components/listErrors/ListErrors'
import Socket from '../../../../shared/Socket';
import LabelError from '../../../../components/inputs/LabelError';
import toast from '../../../../shared/support/ShowToast';

class InfoProfileComponent extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      name: undefined,
      email: undefined,
      password: undefined,
      iconShowPasswordAndroid: 'md-eye-off',
      iconShowPasswordIos: 'ios-eye-off',
      showPassword: true,
      validation: new Validator(data, this.rules),
      flagerror: undefined
    }

    autobind(this);

    this.inputs = {}
  }

  validate() {
    const data = { ...this.state };
    const validation = new Validator(data, this.rules);

    validation.setAttributeNames({
      email: lang.get('account.email', 'signin.register.email'),
      password: lang.get('account.password', 'signin.register.password'),
      name: lang.get('account.name', 'signin.register.name')
    });

    validation.check();

    this.setState({
      validation
    })
  }

  handleValueChange(field, value) {
    this.props.onEditing()

    this.setState({
      [field]: value,
      flagerror: false
    });
  }

  handleRefSet(field, component) {
    this.inputs[field] = component;
  }

  setFieldValue(field, value) {
    this.setState({
      [field]: value
    })
  }

  get rules() {
    return {
      'email': 'required|string|email',
      'password': 'required|string|min:8',
      'name': 'required|string',
      'terms_accepted': ['required', 'accepted']
    }
  }

  async registerAccount() {
    const { registrationContainer, signin, checklist, language } = this.props;
    const { selected } = language.state;
    const pass = this.state.password;

    const regex = /(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d){8,}.+$)/;
    const valor = regex.test(pass);

    const { httpBusy } = registrationContainer.state;
    if (httpBusy) return;

    if (!this.state.terms_accepted) {
      toast.ShowToast(lang.get('message.promissory', 'signin.register.promissory'))

      return;
    }

    if (valor) {

      if (this.state.validation.passes()) {
        const dataRegister = {
          ...this.state,
          language: selected,
          validation: undefined
        }

        await registrationContainer.register(dataRegister);

        const { error } = registrationContainer.state;

        if (error !== undefined) {
          toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))

          setTimeout(() => {
            registrationContainer.clear();
          }, 10000)
        } else {
          const { data } = registrationContainer.state;

          await signin.setData(data);

          //Socket.connectSocket(this.handleAccountActivated);
          Socket.connect();
          Socket.on('account::activate', this.handleAccountActivated)

          this.props.navigation.navigate('AuthLoading');

          registrationContainer.resetState();
        }
      } else {
        toast.ShowToast(lang.get('message.dataRequired', 'generic.data_required'));

        const fields = Object.keys(this.rules);
        const values = {}

        for (const field of fields) {
          values[field] = this.state[field] || ''
        }

        this.setState({
          ...values
        })
      }
    } else {
      this.setState({
        flagerror: true
      })
    }
  }

  async handleAccountActivated(data) {

    const { onHandleAccoutnActivated } = this.props;

    if (onHandleAccoutnActivated) {
      onHandleAccoutnActivated(data);
    }
  }

  submitFromKeyboard() {
    this.registerAccount(true)
  }

  changeIconShowPassword() {
    const { showPassword } = this.state;

    const showPass = !showPassword;
    let iconShowPassAndroid = !showPass ? "ios-eye" : "ios-eye-off";
    let iconShowPassIos = !showPass ? "md-eye" : "md-eye-off";

    this.setState({
      showPassword: showPass,
      iconShowPasswordAndroid: iconShowPassAndroid,
      iconShowPasswordIos: iconShowPassIos
    });
  }

  handleTermsCheck() {
    this.setState({
      terms_accepted: !this.state.terms_accepted
    }, () => {
      this.validate();
    })
  }

  promissory() {
    this.props.navigation.navigate('PromissoryNoteScreen');
  }

  render() {
    const { registrationContainer } = this.props;
    const { showAccountDataComponent, error = {}, httpBusy } = registrationContainer.state;
    const { iconShowPasswordIos, iconShowPasswordAndroid, showPassword, validation, flagerror } = this.state;

    return (
      showAccountDataComponent &&
      <KeyboardAwareScrollView bounces={false}>
        <Content bounces={false} padder contentContainerStyle={{ flex: 1, flexDirection: 'column', justifyContent: 'space-around', alignItems: 'center', paddingTop: 40 }}>
            <Image style={[styles.logo]} source={require('../../../../images/logo_colores/logo_colores.png')} />
          {/* <H1>{lang.get('account.dataAccountTitle')}</H1> */}
          <View style={{ width: '100%', paddingTop: 20 }}>
            <Form>
              <TextInput
                autoCapitalize={'words'}
                autoFocus={true}
                field="name"
                next="email"
                onRefSet={this.handleRefSet}
                refs={this.inputs}
                errors={validation.errors}
                returnKeyType={"next"}
                value={this.state.name}
                placeholder={lang.get('account.fullName', 'signin.register.full_name')}
                onBlur={this.validate}
                onChangeText={this.handleValueChange}
              />
              <TextInput
                field="email"
                next="password"
                onRefSet={this.handleRefSet}
                refs={this.inputs}
                errors={validation.errors}
                autoCapitalize="none"
                keyboardType="email-address"
                returnKeyType={"next"}
                value={this.state.email}
                placeholder={lang.get('account.email', 'signin.register.email')}
                autoCorrect={false}
                onBlur={this.validate}
                onChangeText={this.handleValueChange}
              />
              <TextInput
                field="password"
                value={this.state.password}
                onRefSet={this.handleRefSet}
                refs={this.inputs}
                errors={validation.errors}
                placeholder={lang.get('account.password', 'signin.register.password')}
                secureTextEntry={showPassword}
                autoCorrect={false}
                onBlur={this.validate}
                onChangeText={this.handleValueChange}
                onSubmitEditing={this.register}
                iconRight={<Icon ios={iconShowPasswordIos} android={iconShowPasswordAndroid} onPress={this.changeIconShowPassword} />}
              />
              {flagerror &&
                <LabelError message={lang.get('password.messageAlert', 'signin.register.message_alert')} />
              }

              <ListItem style={{ backgroundColor: '#fff' }} onPress={this.handleTermsCheck}>
                <CheckBox
                  checked={this.state.terms_accepted}
                  onPress={this.handleTermsCheck}
                />
                <Body>
                  <Text>{lang.get('policy.title', 'signin.register.accept_terms')}</Text>
                </Body>
              </ListItem>

              <Text style={[styles.margin25, styles.textAlignCenter, styles.fontSize10]} onPress={this.promissory}>
                {lang.get('policy.terms', 'signin.register.terms')}
                <Text style={[styles.margin25, styles.textAlignCenter, styles.fontSize10, styles.blue]} onPress={this.promissory}>
                  {lang.get('policy.here', 'signin.register.here')}
                </Text>
              </Text>

              <Text style={[styles.margin25, styles.textAlignCenter, styles.fontSize10]}>
                {lang.get('policy.privacy', 'signin.register.privacy')}
              </Text>
              <Button
                style={styles.margin25}
                block
                onPress={this.registerAccount}
              >
                <Text> {lang.get('button.createAccount', 'signin.button.create_account')}  </Text>
                {httpBusy && <Spinner color='lightblue' />}
              </Button>
            </Form>
            {error.errors &&
              <ListErrors errors={error.errors} />
            }
          </View>
        </Content>
      </KeyboardAwareScrollView>
    );
  }
}

export default InfoProfileComponent;