import http from "../../../shared/Http";
import Auth from "../../../shared/services/Auth";

class AuthLoadingServices {

  sendToken(token) {
    return http.post(`/api/notifications/`, token);
  }

  logout() {
    const companyUuid = {
      uuid: Auth.getUser().uuid
    };

    return http.put(`/api/notifications/block`, companyUuid);
  }

  getProfile(uuid){
    return http.get(`/api/executives/${uuid}?includes=profile,payments_profiles`);
  }

  getCheckList(uuid){
    return http.get(`/api/executives/${uuid}?includes=checklist`);
  }

  getTranslations() {
    return http.get(`/api/translations/json?namespace=executive`);
  }

  getVersion() {
    return http.get(`/api/configurations?key=translations_executive`);
  }
}
export default new AuthLoadingServices;