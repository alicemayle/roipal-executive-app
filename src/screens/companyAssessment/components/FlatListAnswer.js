import React, { PureComponent } from 'react';
import { View, Text, FlatList } from 'react-native';
import autobind from 'class-autobind';
import ItemQuestion from './ItemQuestion';
import ItemAnswer from './ItemAnswer';

class FlatListAnswer extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
    autobind(this);
  }

  _keyExtractor(item, index) {
    return index.toString()
  };


  renderItem(item, index) {
    const { checked } = this.props;

    return(
    <ItemAnswer data={item} onHandleCheckAnswer={this.props.onHandleCheckAnswer} checked={checked}/>
    );
  }

  render() {

    const { data } = this.props;

    return (
      <FlatList
        data={data}
        keyExtractor={this._keyExtractor}
        renderItem={this.renderItem}
      />
    );
  }
}

export default FlatListAnswer;
