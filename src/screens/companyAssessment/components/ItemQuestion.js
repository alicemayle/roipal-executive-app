import React, { Component } from 'react';
import { View, Text } from 'react-native';
import FlatListAnswer from './FlatListAnswer';
import autobind from 'class-autobind';
import { Title, Badge, Button } from 'native-base';
import { Logger } from 'unstated-enhancers';

class ItemQuestion extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    autobind(this);
  }

  convertToArray() {
    const { item } = this.props.data;
    const { options } = item;

    let answers = [];

    for (const prop in options) {
      let newObj = {
        key: prop,
        answer: options[prop]
      }

      answers = [...answers, newObj]
    }

    return answers
  }

  handleCheckAnswer(answer) {
    const { item } = this.props.data;
    const { question, options, number } = item;
    const { onHandleSelectAnswer } = this.props;

    onHandleSelectAnswer(number, answer);
  }

  render() {
    const { item } = this.props.data;
    const { question, options, checked } = item;

    const answers = this.convertToArray();

    return (
      <View style={{flex:1 }}>
        <View style={{flexDirection:'row', justifyContent:'center', marginVertical:20, marginHorizontal:15, paddingBottom:15, borderBottomColor: '#FFFFFF',borderBottomWidth:2}}>
          <View style={{flex:0.08, width:30, height:30, borderRadius:17, backgroundColor:'#FFFFFF'}}>
            <Text style={{textAlign:'center', fontSize: 22, width:'100%', height:'100%', alignItems:'center', textAlignVertical:'center', color:'#5bc1be', fontWeight:'bold'}}>{item.number}</Text>
          </View>
          <View style={{flex:0.8}}>
          <Text style={{ fontWeight: 'bold', fontSize: 22, color: '#FFFFFF' }}> {question} </Text>
          </View>
        </View>
        <FlatListAnswer data={answers} onHandleCheckAnswer={this.handleCheckAnswer} checked={checked} />
      </View>
    );
  }
}

export default ItemQuestion;
