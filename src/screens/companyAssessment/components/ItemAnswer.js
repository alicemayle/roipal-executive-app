import React, { PureComponent } from 'react';
import { ListItem, CheckBox, Body, View, Text, Card, CardItem, Left } from 'native-base';
import autobind from 'class-autobind';

class ItemAnswer extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
    autobind(this)
  }

  handleCheckAnswer() {
    const { item } = this.props.data

    this.props.onHandleCheckAnswer(item.key)
  }

  render() {
    const { checked } = this.props;
    const { item } = this.props.data

    return (
      /*       <ListItem style={{ backgroundColor: 'transparent' }}>
              <CheckBox checked={!!(checked && checked === item.key)} color="green" onPress={this.handleCheckAnswer} />
              <Body>
                <Text>{item.answer}</Text>
              </Body>
            </ListItem> */
        <Card style={{ borderRadius: 10, marginLeft: 15, marginRight: 15, marginBottom: 10, paddingBottom: 15, paddingTop: 15}}>
          <CardItem
            icon
            button
            onPress={this.handleCheckAnswer}
            style={{ backgroundColor: 'trasparent', flexDirection: 'row', alignItems: 'flex-start', padding: 10, borderRadius: 10, }}
          >
            <Left>
              <CheckBox checked={!!(checked && checked === item.key)} color="green" onPress={this.handleCheckAnswer} />
            <Body style={{marginLeft: 20}}>
              <Text>{item.answer}</Text>
            </Body>
            </Left>
          </CardItem>
        </Card>
    );
  }
}

export default ItemAnswer;
