import React, { PureComponent } from 'react';
import { View, Text, FlatList } from 'react-native';
import autobind from 'class-autobind';
import ItemQuestion from './ItemQuestion';

class FlatListQuestion extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
    autobind(this);
  }

  _keyExtractor(item, index) {
    return index.toString()
  };

  renderItem(item, index) {

    const { onHandleSelectAnswer } = this.props;

    return(
    <ItemQuestion data={item} onHandleSelectAnswer={onHandleSelectAnswer}
    />
    );
  }

  render() {
    const { data } = this.props;

    return (
      <FlatList
        data={data}
        keyExtractor={this._keyExtractor}
        renderItem={this.renderItem}
      />
    );
  }
}

export default FlatListQuestion;
