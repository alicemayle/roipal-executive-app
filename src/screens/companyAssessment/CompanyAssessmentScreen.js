import React, { Component } from 'react';
import { ScrollView, InteractionManager } from 'react-native';
import FlatListQuestion from './components/FlatListQuestion';
import Lang from '../../shared/languages/Lang';
import { Container, Spinner, Fab, Icon } from 'native-base';
import { connect } from 'unstated-enhancers';
import companyAssessment from './containers/CompanyAssessment';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import home from '../home/containers/HomeContainer';
import autobind from 'class-autobind';
import acceptance from '../invitations/containers/InvitationAcceptanceContainer';
import styles from '../../theme/globalStyles';
import LinearGradient from '../../components/gradient/IntroGradient';
import auth from '../auth/containers/AuthContainer';
import toast from '../../shared/support/ShowToast';

class CompanyAssessmentScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      counter: 0
    };
    autobind(this);
  }

  setNavigationGoBackConfirmationAlert() {
    this.props.navigation.setParams({
      alert: {
        title: Lang.get('assessment.alerTitle', 'evaluation.assessment.aler_title'),
        message: Lang.get('assessment.alertDirection', 'evaluation.assessment.alert_direction')
      }
    });
  }


  componentDidMount() {
    this.setNavigationGoBackConfirmationAlert();

    InteractionManager.runAfterInteractions(() => {
      this.getCompanyAssessment();
    })
  }

  componentWillUnmount() {
    const { companyAssessment } = this.props.containers;

    companyAssessment.reset();
  }

  async getCompanyAssessment() {
    const { companyAssessment, acceptance } = this.props.containers;
    const { uuid } = acceptance.state.data.mission;

    const params = {
      includes: 'questions,company,mission',
    };

    await companyAssessment.getCompanyAssessment(uuid, params);

    const { error } = this.props.assessment;

    if (error) {
      toast.ShowToast(error.message || Lang.get('message.tryAgain', 'generic.try_again'))
      setTimeout(() => {
        companyAssessment.clearError();
      }, 200)
    }
  }

  handleSelectAnswer(numberQuestion, answer) {
    const { navigation } = this.props;
    const isEditing = navigation.getParam('isEditing');

    if (!isEditing) {
      navigation.setParams({
        isEditing: true
      });
    }

    const { companyAssessment } = this.props.containers;
    const { questions } = companyAssessment.state.data;

    const result = questions.map(item => {
      if (item.number === numberQuestion) {
        item = {
          ...item,
          checked: answer
        }
      }
      return item;
    })

    companyAssessment.setQuestions(result);
  }

  async sendAssessment() {
    const { companyAssessment } = this.props.containers;
    const { data = {}, httpBusy } = companyAssessment.state;

    if (httpBusy) return;

    const { acceptance } = this.props.containers;
    const { uuid } = acceptance.state.data.mission;
    const { questions } = data;

    if (questions.every(item => item.checked)) {
      const result = questions.map(item => {
        const obj = {
          number: item.number,
          answer: item.checked
        }

        return obj;
      })

      const dataAssessment = {
        answers: result
      }

      await companyAssessment.sendCompanyAssessment(uuid, dataAssessment);

      const { error } = this.props.assessment;
      const { data, code, message } = this.props.assessment.result;

      if (error) {
        toast.ShowToast(error.message || Lang.get('message.tryAgain', 'generic.try_again'))

        setTimeout(() => {
          companyAssessment.clearError();
        }, 200)
      } else {
        setTimeout(() => {
          toast.ShowToast(message);
        }, 500);

        await this.resetAssessment();

        await this.setState({
          counter: 0
        })

        if (code === 'NUMBER_OF_ATTEMPTS_ALLOWED') {
          this.props.navigation.navigate('InvitationsListScreen');

        } else if (data.points === '100') {
          const { home, auth } = this.props.containers;
          params = {
            search: this.state.value || '',
            page: 1,
            includes: 'company.profile,room_chat'
          }

          auth.modifyInvitationsReceived();

          await home.getMissions(params);

          this.props.navigation.navigate('HomeScreen');
        }
      }

    } else {
      toast.ShowToast(Lang.get('message.dataRequired', 'generic.data_required'))
    }
  }

  async resetAssessment() {
    const { companyAssessment } = this.props.containers;
    const { data = {} } = companyAssessment.state;
    const { questions } = data;

    const resetAssessment = questions.map(item => {
      delete item.checked;
      return item;
    });

    await companyAssessment.setQuestions(resetAssessment);
  }

  handleNextQuestion() {
    const { data = {} } = this.props.containers.companyAssessment.state;
    const { questions } = data;
    const { counter } = this.state;

    if (counter + 1 < questions.length) {
      if ([questions[counter]].every(item => item.checked)) {
        this.setState({
          counter: counter + 1
        })
      } else {
        setTimeout(() => {
          toast.ShowToast(Lang.get('message.dataRequired', 'generic.data_required'))
        }, 500);
      }
    } else {
      this.sendAssessment();
    }
  }

  render() {
    const { data = {} } = this.props.containers.companyAssessment.state;
    const { questions } = data;
    const { httpBusy } = this.props.assessment;
    const { counter } = this.state;

    return (
      <Container style={styles.container}>
        <NavBarComponent  {...this.props.navigation}
          name={Lang.get('evaluation.evaluation', 'evaluation.assessment.evaluation')} />

        <LinearGradient>
        {
          httpBusy && <Spinner color='lightblue' />
        }
        {
          (data && data !== {} && !httpBusy && data.questions) && <ScrollView style={{width:'100%' }}>
            {/* <Title style={{ marginTop: 25, marginBottom: 25 }}>
              {Lang.get('evaluation.companyDirections')}
            </Title> */}
            <FlatListQuestion
              onHandleSelectAnswer={this.handleSelectAnswer}
              data={[questions[counter]]}
            />
          </ScrollView>
        }
        {
          (data && data !== {} && !httpBusy && data.questions) &&
          <Fab
            active={true}
            direction="up"
            style={{ backgroundColor: '#0285c9' }}
            position="bottomRight"
            onPress={this.handleNextQuestion}
          >
            <Icon name="arrow-forward" />
          </Fab>
        }
        </LinearGradient>
      </Container>
    );
  }
}

const containers = {
  companyAssessment: companyAssessment,
  home: home,
  acceptance: acceptance,
  auth: auth
}

const mapStateToProps = (containers) => {
  return {
    assessment: containers.companyAssessment.state,
    home: containers.home.state,
    auth: containers.auth.state
  };
}

export default connect(containers, mapStateToProps)(CompanyAssessmentScreen);
