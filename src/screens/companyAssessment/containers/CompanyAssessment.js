import { Container } from 'unstated';
import { Logger } from 'unstated-enhancers';
import CompanyAssessmentService from '../services/CompanyAssessmentService';

const initialState = {
  error: undefined,
  message: undefined,
  data: undefined,
  httpBusy: false,
}

class CompanyAssessment extends Container {

  constructor(data = {}) {
    super(data);
    this.state = {
      ...initialState
    }
  }

  setData(data) {
    this.setState({
      ...this.state,
      data,
      __action: 'Company Assessment set data'
    })
  }

  setQuestions(data) {
    this.setState({
      data: {
      ...this.state.data,
      questions: data
      },
      __action: 'Company Assessment set questions'
    })
  }

  async getCompanyAssessment(uuid, params) {

    try {
      this.setState({
        httpBusy: true,
        __action: 'Get Company Assessment starts'
      })

      let response = await CompanyAssessmentService.getAssessment(uuid, params);

      if (response) {
        await this.setState({
          data: response.data[0],
          message: response.message,
          __action: 'Get Company Assessment success'
        })
      }
    } catch (error) {
      await this.setState({
        error: error,
        __action: 'Get Company Assessment error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'Get Company Assessment finally'
        });
      }, 500)
    }

  }

  async sendCompanyAssessment(uuid, data) {

    try {
      this.setState({
        httpBusy: true,
        __action: 'Send Company Assessment starts'
      })

      let response = await CompanyAssessmentService.sendAssessment(uuid, data);

      if(response) {
        await this.setState({
          result: response,
          message: response.message,
          __action: 'Send Company Assessment success'
        })
      }
    } catch (error) {
      await this.setState({
        error: error,
        __action: 'Send Company Assessment error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'Send Company Assessment finally'
        });
      }, 500)
    }

  }

  clearError() {
    this.setState({
      error: undefined,
    })
  }

  reset() {
    this.setState({
      ...initialState,
    })
  }

}

export default CompanyAssessment;
