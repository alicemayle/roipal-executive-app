import Http from '../../../shared/Http';
import Auth from '../../../shared/services/Auth';

class CompanyAssessmentService {

  getAssessment(uuid = 'ea49e84d-beb0-4463-bd06-a03e3fb003f9', params = {}) {

    return Http.get(`/api/missions/${uuid}/assessments`, { params });
  }

  sendAssessment(uuid, data, params = {}) {

    return Http.post(`/api/missions/${uuid}/assessments-evaluation`, data, { params });
  }

}

export default new CompanyAssessmentService;
