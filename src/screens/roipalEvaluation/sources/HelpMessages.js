const titleHelperEvaluation = 'Evalución de Competencias'

const helperEvaluation = `
Hola, si tienes talento en ventas y te gusta vender, sabrás como contestar de manera estratégica frente a las diferentes situaciones que te presentaremos.

Lee cada situación de ventas y pondera las estrategias que llevarías a cabo en cada una de las situaciones.

El número uno será tu mejor elección de acción, el número dos tu segunda elección, y así sucesivamente.

¡Gracias!`

export {
  helperEvaluation, titleHelperEvaluation
}