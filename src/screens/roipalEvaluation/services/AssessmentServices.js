import Auth from "../../../shared/services/Auth";
import http from "../../../shared/Http";
import qs from 'qs';

class AssessmentServices {

   getRoipalAssessment() {
    return http.get('api/assestments/roipal?includes=questions&questions_by_group=2');

}

sendRoipalAssessment(data) {
  return http.post(`api/executives/${Auth.getUser().uuid}/assestments/Roipal?includes=executive.profile`, data);
}
}
export default new AssessmentServices;