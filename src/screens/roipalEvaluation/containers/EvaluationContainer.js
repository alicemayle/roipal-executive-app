import { Container } from 'unstated';
import AssessmentServices from '../services/AssessmentServices';
import lang from '../../../shared/languages/Lang';
import observer from '../../../shared/Observer';

const initialState = {
  httpAssessmentBusy: false,
  httpFinishAssessmentBusy: false,
  finishRequest: undefined,
  finishRequestError: undefined,
  assessmentError: undefined,
  assessment: undefined,
  currentQuestion: undefined,
  newAnswer: undefined,
  answersArray: [],
}

class EvaluationContainer extends Container {
  state = { ...initialState }

  updateState(key, value) {
    const state = { ...this.state };
    state[key] = value;
    this.setState(state);
  }

  setNextQuestion() {
    const currentQuestion = this.state.assessment.shift();
    const remainingQuestions = [...this.state.assessment];
    const answer = Object.keys(currentQuestion.options).join('');
    this.setState({
      assessment: remainingQuestions,
      currentQuestion: currentQuestion,
      newAnswer: {
        "number": currentQuestion.id,
        "answer": answer,
      },
    })
  }

  setAnswer(data) {
    this.setState({
      newAnswer: data,
    })
  }

  concatAnswers() {
    const cloneAnswersArray = [...this.state.answersArray, this.state.newAnswer];
    this.setState({
      answersArray: cloneAnswersArray,
    })
  }

  async getRoipalAssessment() {
    let result = {}
    try {
      this.setState({
        httpAssessmentBusy: true
      })
      const response = await AssessmentServices.getRoipalAssessment();

      if (response) {
        const assessment = response.data.questions;
        const currentQuestion = assessment.shift();
        const remainingQuestions = [...assessment];
        const answer = Object.keys(currentQuestion.options).join('');
        this.setState({
          assessment: remainingQuestions,
          currentQuestion: currentQuestion,
          newAnswer: {
            "number": currentQuestion.id,
            "answer": answer,
          },
        })
  
        result = {
          result: 'Success'
        }
      }
    } catch (error) {
      result = {
        result: 'Error',
        message: lang.get('message.tryAgain')
      }

      this.setState({
        assessmentError: error.message
      })
    } finally {
      this.setState({
        httpAssessmentBusy: false
      })
      observer.notify('seller/resultGetRoipalAssessment', result);
    }
  }

  async sendRoipalAssessment(data) {
    let result = { }
    try {
      await this.concatAnswers();
      discAssessment = {
        answers: [
          ...this.state.answersArray
        ]
      }
      await this.setState({
        httpFinishAssessmentBusy: true
      })
      const response = await AssessmentServices.sendRoipalAssessment(data);

      if (response) {
        this.setState({
          finishRequest: response.message,
        })
  
        result = {
          result: 'Success'
        }
      }
    } catch (error) {
      this.setState({
        finishRequestError: error.message,
      })

      result = {
        result: 'Error',
        message: lang.get('message.tryAgain')
      }
    } finally {
      this.setState({
        httpFinishAssessmentBusy: false
      })
      observer.notify('seller/resultSendRoipalAssessment', result);
    }
  }
}

export default EvaluationContainer;