import { Container } from 'unstated';
import AssessmentServices from '../services/AssessmentServices';
import { Logger } from 'unstated-enhancers';

const initialState = {
  httpBusy: false,
  error: undefined,
  data: undefined,
  answersArray: [],
}

class AnswersRoipalContainer extends Container {
  state = { ...initialState }

  concatAnswers(newAnswer) {
    const index = this.state.answersArray.findIndex(item => item.number === newAnswer.number);
    let cloneAnswersArray = [];
    if (index === -1) {
      cloneAnswersArray = [...this.state.answersArray, newAnswer];
    } else {
      cloneAnswersArray = [...this.state.answersArray];
      cloneAnswersArray[index] = {...newAnswer}
    }
    this.setState({
      answersArray: cloneAnswersArray,
    })
  }

  async sendRoipalAssessment(answers) {
    Logger.dispatch('_sendRoipalAssessment_AnswersRoipalContainer', answers)
    try {

      this.setState({
        httpBusy: true,
        error: undefined,
        __action: 'SAVE_ROIPAL starts'
      });
      const response = await AssessmentServices.sendRoipalAssessment(answers);

      if (response) {
        await this.setState({
          data: response,
          __action: 'SAVE_ROIPAL success'
        });
      }
    } catch (error) {
      await this.setState({
        error,
        __action: 'SAVE_ROIPAL error'
      });
    }
    finally{
      setTimeout( () => {
        this.setState({
          httpBusy: false,
          __action: 'SAVE_ROIPAL finally'
        })
      }, 500 ) 
    } 
  }

  resetState() {
    this.setState({
      ...initialState,
      __action: 'SAVE_ROIPAL reset'
    });
  }
}

export default AnswersRoipalContainer;