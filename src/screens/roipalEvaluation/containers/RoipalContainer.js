import { Container } from 'unstated';
import AssessmentServices from '../services/AssessmentServices';

const initialState = {
  httpBusy: false,
  error: undefined,
  data: undefined,
  assessment: undefined,
  currentQuestion: undefined,
  newAnswer: undefined,
}

class RoipalContainer extends Container {
  state = { ...initialState }

  setAnswer(data) {
    this.setState({
      newAnswer: data,
    })
  }

  setNextQuestion() {
    const currentQuestion = this.state.assessment.shift();
    const remainingQuestions = [...this.state.assessment];
    const answer = Object.keys(currentQuestion.options).join('');
    this.setState({
      assessment: remainingQuestions,
      currentQuestion: currentQuestion,
      newAnswer: {
        "number": currentQuestion.id,
        "answer": answer,
      },
    })
  }

  async getRoipalAssessment() {
    try {
      this.setState({
        httpBusy: true,
        error: undefined,
        __action: 'GET_ROIPAL starts'
      });

      const response = await AssessmentServices.getRoipalAssessment();

      if (response) {
        const assessment = response.data.questions;
        const currentQuestion = assessment.shift();
        const remainingQuestions = [...assessment];
        const answer = Object.keys(currentQuestion.options).join('');
  
        await this.setState({
          data: response,
          assessment: remainingQuestions,
          currentQuestion: currentQuestion,
          newAnswer: {
            "number": currentQuestion.id,
            "answer": answer,
          },
          __action: 'GET_ROIPAL success'
        });
      }
    } catch (error) {
      await this.setState({
        error,
        __action: 'GET_ROIPAL error'
      });
    }
    finally{
      setTimeout( () => {
        this.setState({
          httpBusy: false,
          __action: 'GET_ROIPAL finally'
        });
      }, 500 ) 
    } 
  }

  resetState() {
    this.setState({
      ...initialState,
      __action: 'GET_ROIPAL reset'
    });
  }
}

export default RoipalContainer;