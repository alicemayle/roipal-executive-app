import { Container } from 'unstated';

class ResultsEvaluationContainer extends Container {

  constructor(data = {}) {
    super(data);

  }
}

export default ResultsEvaluationContainer;