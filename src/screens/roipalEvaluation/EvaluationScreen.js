import React, { Component } from 'react';
import { Container, Button, Spinner, Title } from 'native-base';
import { connect } from 'unstated-enhancers';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import lang from '../../shared/languages/Lang';
import checklist from '../../screens/checklist/containers/ChecklistContainer';
import HelperAssessmentComponent from './components/helper/HelperAssessmentComponent';
import { StyleSheet, Text, View, Dimensions, Platform, InteractionManager } from 'react-native';
import SortableList from 'react-native-sortable-list';
import RowQuestion from './RowQuestion';
import resultEvaluation from './containers/ResultsEvaluationContainer';
import roipalAssessment from './containers/RoipalContainer';
import answersAssessment from './containers/AnswersRoipalContainer';
import autobind from 'class-autobind';
import LinearGradient from 'react-native-linear-gradient';
import auth from '../auth/containers/AuthContainer';
import toast from '../../shared/support/ShowToast';

const { width } = Dimensions.get('window');

class EvaluationScreen extends Component {

  constructor(props) {
    super(props);

    this.state = { modalVisible: true }

    autobind(this);
  }


  changeOrder(data) {
    const { navigation } = this.props;
    const isEditing = navigation.getParam('isEditing');

    if (!isEditing) {
      navigation.setParams({
        isEditing: true
      });
    }

    const { roipalAssessment } = this.props.containers;
    const { currentQuestion } = roipalAssessment.state;
    this.newAnswer = {
      "number": currentQuestion.number,
      "answer": data.join(''),
    }
  }

  setNavigationGoBackConfirmationAlert() {
    this.props.navigation.setParams({
      alert: {
        title: lang.get('evaluation.alerTitle', 'evaluation.aler_title'),
        message: lang.get('evaluation.alertDirection', 'evaluation.alert_direction')
      }
    });
  }

  async getRoipalAssessment() {
    const { roipalAssessment } = this.props.containers;
    const disc = await roipalAssessment.getRoipalAssessment();
  }

  setAnswers(data) {
    try {
      const { roipalAssessment } = this.props.containers;
      const { currentQuestion } = roipalAssessment.state;
      const newAnswer = {
        "number": currentQuestion.number,
        "answer": data.join(''),
      }
      roipalAssessment.setAnswer(newAnswer);
    } catch (error) {
      //TODO
    }
  }

  async setNextQuestion() {
    const { httpBusy } = this.props.containers.answersAssessment.state;
    if (httpBusy) return;

    const { roipalAssessment, answersAssessment } = this.props.containers;
    const { assessment, currentQuestion } = roipalAssessment.state;
    this.newAnswer = this.newAnswer === undefined ? {
      "number": currentQuestion.number,
      "answer": currentQuestion ? Object.keys(currentQuestion.options).join('') : undefined,
    } : this.newAnswer;

    await answersAssessment.concatAnswers(this.newAnswer);

    if (assessment.length === 0) {
      const data = {
        answers: answersAssessment.state.answersArray,
      }

      await answersAssessment.sendRoipalAssessment(data);

      const { error } = answersAssessment.state;

      if (error !== undefined) {
        toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'));
        //TODO: Cuando hay un error en la evaluación hay algun comportamiento que agregar en este bloque???
      } else {
        const { checklist, resultEvaluation, auth } = this.props.containers;
        await checklist.setStepAsCompleted('roipal-assessment');
        await auth.setProfile(answersAssessment.state.data.data.executive.profile);
        roipalAssessment.resetState();
        answersAssessment.resetState();
        this.props.navigation.navigate('ChecklistScreen');
      }
    } else {
      roipalAssessment.setNextQuestion();
      this.newAnswer = undefined;
    }
  }

  handleSetModalVisible() {
    const inverse = !this.state.modalVisible;
    this.setState({ modalVisible: inverse });
  }

  componentDidMount() {
    this.setNavigationGoBackConfirmationAlert();

    InteractionManager.runAfterInteractions(() => {
      setTimeout(() => this.getRoipalAssessment(), 0)
    })
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.currentQuestion !== this.props.currentQuestion ||
      nextProps.saveDisc !== this.props.saveDisc ||
      nextProps.requestDisc !== this.props.requestDisc ||
      nextState.modalVisible !== this.state.modalVisible
  }

  render() {
    const { currentQuestion } = this.props.containers.roipalAssessment.state;
    const { modalVisible } = this.state;

    return (
      <Container>
        <HelperAssessmentComponent
          {...this.props.navigation}
          modalVisible={this.state.modalVisible}
          onHandleSetModalVisible={this.handleSetModalVisible}
        />
        <NavBarComponent  {...this.props.navigation}
          name={lang.get('title.assessment', 'evaluation.assessment')}
        />
        {!modalVisible && currentQuestion !== undefined &&
          <View style={styles.container}>
            <LinearGradient
              colors={['#00a7e5', '#5bc1be']}
              start={{ x: 0, y: 0 }}
              end={{ x: 0, y: 1 }}
              style={{
                width: '100%',
                alignItems: 'center',
                justifyContent: 'center',
                paddingHorizontal: 25,
                marginBottom:10
              }}
            >
              <Text
                multiline
                style={{ color: 'white', textAlign: 'justify', marginVertical: 15, fontSize: 17 }}>
                {`${currentQuestion.question}`}
              </Text>
            </LinearGradient>
            <Title>{lang.get('evaluation.competitions', 'evaluation.competitions')}</Title>
            <SortableList
              style={styles.list}
              contentContainerStyle={styles.contentContainer}
              data={currentQuestion.options}
              renderRow={this._renderRow}
              onChangeOrder={this.changeOrder} />
            <Button
              style={{ margin: 25 }}
              block
              onPress={this.setNextQuestion}>
              <Text style={{ color: 'white' }}>{lang.get('button.next', 'generic.button.next')} </Text>
              {this.props.saveDisc && <Spinner color='lightblue' />}
            </Button>
          </View>
        }
        {
          !modalVisible && currentQuestion === undefined && this.props.requestDisc && <Spinner color='lightblue' />
        }
      </Container>
    );
  }

  _renderRow = ({ data, active }) => {
    return <RowQuestion data={data} active={active} />
  }
}

const containers = {
  checklist: checklist,
  resultEvaluation: resultEvaluation,
  roipalAssessment: roipalAssessment,
  answersAssessment: answersAssessment,
  auth: auth
}

const mapStateToProps = (containers) => {
  return {
    requestDisc: containers.roipalAssessment.state.httpBusy,
    saveDisc: containers.answersAssessment.state.httpBusy,
    currentQuestion: containers.roipalAssessment.state.currentQuestion,
  };
}

export default connect(containers, mapStateToProps)(EvaluationScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  title: {
    fontSize: 20,
    paddingVertical: 20,
    color: '#999999',
  },

  list: {
    flex: 1,
  },

  contentContainer: {
    width: width,
    paddingTop: 10,

    ...Platform.select({
      ios: {
        paddingHorizontal: 30,
      },

      android: {
        paddingHorizontal: 0,
      }
    })
  },

  row: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    padding: 16,
    height: 80,
    flex: 1,
    marginTop: 7,
    marginBottom: 12,
    borderRadius: 10,


    ...Platform.select({
      ios: {
        width: width - 30 * 2,
        shadowColor: 'rgba(0,0,0,0.2)',
        shadowOpacity: 1,
        shadowOffset: { height: 2, width: 2 },
        shadowRadius: 2,
      },

      android: {
        width: width - 30 * 2,
        elevation: 0,
        marginHorizontal: 30,
      },
    })
  },

  image: {
    width: 50,
    height: 50,
    marginRight: 30,
    borderRadius: 25,
  },

  text: {
    fontSize: 24,
    color: '#222222',
  },
});