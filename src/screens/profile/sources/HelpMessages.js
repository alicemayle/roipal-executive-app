const helperProfile =`
Brinda información sobre ti, datos de contacto.

 Nota: Puedes proporcionar la información de manera
 escrita y/o verbal mediante un video.`

export {
  helperProfile
}