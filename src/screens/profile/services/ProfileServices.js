import http from '../../../shared/Http';
import auth from '../../../shared/services/Auth';

class ProfileServices {

  updateProfile(data) {
    const uuid = auth.getUser().uuid;
    return http.put(`api/executives/${uuid}?includes=payments_profiles`, data);
  }

  sendFile(data, config) {
    return http.post(`/api/files`, data, config);
  }

  limitations() {
    return http.get('/api/limitations');
  }

  getBanks(country) {
    return http.get(`/api/translations/catalogs?namespace=catalogs&group=${country}`)
  }

  getCountries() {
    return http.get(`/api/translations/catalogs?namespace=catalogs&group=countries`)
  }

  getSkills(){
    return http.get(`/api/translations/catalogs?namespace=catalogs&group=skill`)
  }

  getDocuments(){
    return http.get(`/api/translations/catalogs?namespace=catalogs&group=documents_mx`)
  }

  getDocumentsStatus(uuid){
    return http.get(`/api/executives/${uuid}/documentation`)
  }

  updateDocuments(data, uuid){
    return http.put(`/api/executives/${uuid}/documentation`, data);
  }

  updateLanguage(data) {
    return http.put(`/api/user/lang`, data)
  }
}
export default new ProfileServices;