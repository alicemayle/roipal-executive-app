import React, { Component } from 'react';
import { Content, Text, View, Title, Button, Container } from 'native-base';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import newLang from '../../shared/languages/Lang';
import { Image, SafeAreaView } from 'react-native';
import styles from '../../theme/globalStyles';
import search from '../../images/verification/validate.png'
import autobind from 'class-autobind';
import { connect } from 'unstated-enhancers';
import infoProfile from '../profile/containers/InfoProfileContainer';
import verification from '../profile/containers/VerificationContainer';

class MessageValidationScreen extends Component {
  constructor(props) {
    super(props);
    autobind(this);
  }

  async goToNext() {
    const { infoProfile } = this.props.containers;
    infoProfile.resetState();

    const { navigation } = this.props;
    const goBack = navigation.getParam('goBack');

    if (goBack) {
      this.props.navigation.navigate(goBack);
    } else {
      this.props.navigation.goBack()
    }
  }

  render() {
    return (
      <Container style={[styles.container]}>

        <NavBarComponent  {...this.props.navigation} name={newLang.get('documents.title', 'profile.documents.title')} />

        <SafeAreaView style={{ flex: 1 }}>
          <Content bounces={false} contentContainerStyle={{ padding: 25 }}>
            <View>
              <View style={{ alignItems: 'center', paddingTop: 15 }}>
                <Image style={styles.logo} source={require('../../images/logo_colores/logo_colores.png')} />
              </View>
              <Title style={{ color: '#00a7e5', marginVertical: 30 }}>{newLang.get('documents.titleMessage', 'profile.documents.title_message')}</Title>
              <Text style={{ textAlign: 'justify' }}>{newLang.get('documents.message', 'profile.documents.message')}</Text>
              <Image source={search} style={[styles.logo, { height: 170, marginTop: 25, marginBottom: 25, marginRight: 25, marginLeft: 15 }]} />
            </View>
          </Content>
          <Button
            style={{ margin: 25 }}
            block
            onPress={this.goToNext}
          >
            <Text>{newLang.get('button.understood', 'profile.button.understood')}</Text>
          </Button>
        </SafeAreaView>
      </Container>
    )
  }
}

const containers = {
  infoProfile: infoProfile,
  verification: verification,

}

const mapStateToProps = (containers) => {
  return {
    infoProfile: containers.infoProfile.state,
    verification: containers.verification.state,

  };
}

export default connect(containers, mapStateToProps)(MessageValidationScreen);