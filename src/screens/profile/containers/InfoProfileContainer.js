import { Container } from 'unstated';
import ProfileServices from '../services/ProfileServices';

const initialState = {
  httpBusy: false,
  error: undefined,
  data: undefined,
  video: undefined,
  bio: undefined,
  message: undefined,
  newArea: undefined,
  phone: undefined,
  initialRegion: undefined,
  skillBussy: false,
  clear: false
}

class InfoProfileContainer extends Container {
  state = { ...initialState };

  updateState(key, value) {
    const state = { ...this.state };
    state[key] = value;
    this.setState(state);
  }

  setClearText(field, data) {
    this.setState({
      ...this.state,
      [field]: data,
      clear: true,
      __action: 'Edit Account map set clear text'
    })
  }

  pickPlace(data, address) {
    this.setState({
      ...this.state,
      initialRegion: {
        ...this.state.initialRegion,
        longitude: data.longitude,
        latitude: data.latitude,
      },
      newArea: address,
      clear: false,
      __action: 'Info profile pick place'
    })
  }

  setDataState(field, data) {
    this.setState({
      ...this.state,
      initialRegion: {
        ...this.state.initialRegion,
        [field]: data,
      },
      __action: 'Info profile set data state'
    })
  }

  setSelectedLimitations(data) {
    this.setState({
      ...this.state,
      limitations: data,
      __action: 'Info profile set selected limitations'
    })
  }

  selectLimitation(item) {
    let nolimitation = false;
    const newArray = this.state.limitations.map((element) => {
      if (item.flag === 1) {
        nolimitation = true;
      }
      if (element.uuid === item.uuid) {
        element.selected = !element.selected
      }
      return element
    })

    if (nolimitation) {

      const newArrayLimitations = this.state.limitations.map((element) => {
        if (element.flag === 0) {
          element.selected = false
        }
        return element
      })
      this.setState({
        limitations: newArrayLimitations,
      })
    } else {

      const newArrayLimitations = this.state.limitations.map((element) => {
        if (element.flag === 1) {
          element.selected = false
        }
        return element
      })
      this.setState({
        limitations: newArrayLimitations,
      })
    }
  }

  getSelectedLimitations() {
    const selectedLimitations = this.state.limitations.filter(item => item.selected === true);
    const selectedNameLimitations = selectedLimitations.map(item => item.uuid)
    return selectedNameLimitations;
  }

 async getSkills(){
    try {
      this.setState({
        skillBussy: true,
        __action: 'get skill starts'
      })

      const response = await ProfileServices.getSkills();

      if (response) {
        let skillsList = []

        await response.data.map(item => {
          const data = {
            id: item,
            name: item
          }
          skillsList.push(data)
        })

        await this.setState({
          ...this.state,
          skills: skillsList,
          message: response.message,
          __action: 'get skill setdata'
        })
      }
    } catch (error) {

      await this.setState({
        error: error,
        __action: 'get skill error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          skillBussy: false,
          __action: 'get skill finally'
        });
      }, 500)
    }
  }

  setInfoProfile(data) {
    this.setState({
      ...this.state,
      bio: data.bio,
      phone: data.phone
    })
  }

  async listLimitations() {

    try {
      this.setState({
        httpBusy: true,
        __action: 'get list limitations starts'
      })

      const response = await ProfileServices.limitations();

      if (response) {
        await this.setState({
          limitations: response.data,
          message: response.message,
          __action: 'get list limitations setdata'
        })
      }
    } catch (error) {

      await this.setState({
        error: error,
        __action: 'get list limitations error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'get list limitations finally'
        });
      }, 500)
    }
  }

  async updateProfile(data, formData, config, formDataThumbnail) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'InfoProfileContainer starts'
      })

      let urls = null;
      let urlThumbnail = null;

      if (formData) {
        urls = await ProfileServices.sendFile(formData, config);
      }
      if (formDataThumbnail) {
        urlThumbnail = await ProfileServices.sendFile(formDataThumbnail, config);
      }
      let media = {};
      let dataRequest = data;

      if (urls) {
        if (urls.video) {
          media = {
            ...media,
            video_bio_url: urls.video
          }
        }

        if (urls.image) {
          media = {
            ...media,
            photo_bio_url: urls.image
          }
        }

        if(urlThumbnail && urlThumbnail.image) {
          media = {
            ...media,
            thumbnail_video: urlThumbnail.image
          }
        }

        if (media !== {}) {
          dataRequest = {
            ...data,
            ...media
          }
        }
      }

      const response = await ProfileServices.updateProfile(dataRequest);

      if (response) {
        await this.setState({
          data: response,
          __action: 'InfoProfileContainer setdata'
        })
      }
    } catch (error) {
      if (error.errors) {
        let messages = [];

        for (const [field, errors] of Object.entries(error.errors)) {
          messages = messages.concat(errors)
        }

        error.errors = messages
      }

      await this.setState({
        error: error,
        __action: 'InfoProfileContainer error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'InfoProfileContainer finally'
        });
      }, 500)
    }
  }

  async getCountries() {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get countries starts'
      })

      const response = await ProfileServices.getCountries();

      if (response) {
        let catalogCountries = [];

        await response.data.map(item => {
          const data = {
            label: item.name,
            value: item.name,
          }
          catalogCountries.push(data);
        })

        await this.setState({
          ...this.state,
          httpBusy: false,
          catalogCountries: catalogCountries,
          __action: 'Get countries success'
        })
      }
    } catch (error) {
      await this.setState({
        httpBusy: false,
        error: error,
        __action: 'Get countries error'
      })
    } finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'Get countries finally'
        })
      }, 500)
    }
  }

  clear() {
    this.setState({
      ...this.state,
      error: undefined,
      clear: false,
      __action: 'InfoProfileContainer ends'
    })
  }

  resetState() {
    this.setState({
      ...initialState,
      __action: 'InfoProfileContainer reset'
    })
  }
}

export default InfoProfileContainer;