import { Container } from 'unstated';
import verifiServices from '../services/ProfileServices';

const initialState = {
  httpBusy: false,
  error: undefined,
  data: undefined,
  flag: false,
  documents: [],
  catalogo: [],
  documentStatus: []
}

class VerificationContainer extends Container {
  state = { ...initialState };

  async updateProfile(file, formData, config, name) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Update documents starts'
      })

      let urls = null;

      if (formData) {
        urls = await verifiServices.sendFile(formData, config);
      }

      let documents = this.state.documents

      if (urls) {
        const doc = documents.find(item => item.document_type === file);
        const index = documents.indexOf(doc);

        if (index !== -1) {
          documents[index] =
            {
              "document_type": file,
              "name": name,
              "document_url": urls.image,
              "status": 0
            }
        } else {
          documents.push(
            {
              "document_type": file,
              "document_url": urls.image,
              "name": name,
              "status": 0
            }
          )
        }
      }
      this.setState({
        documents: documents
      })

    } catch (error) {
      if (error.errors) {
        let messages = [];

        for (const [field, errors] of Object.entries(error.errors)) {
          messages = messages.concat(errors)
        }

        error.errors = messages
      }

      await this.setState({
        error: error,
        __action: 'Update documents error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'Update documents finally'
        });
      }, 500)
    }
  }

  async updateDocuments(documents, uuid){
    try {
      this.setState({
        httpBusy: true,
        __action: 'Send update documents starts'
      })

      const response = await verifiServices.updateDocuments(documents, uuid);

      this.setState({
        update: response.data
      })

    } catch (error) {
      if (error.errors) {
        let messages = [];

        for (const [field, errors] of Object.entries(error.errors)) {
          messages = messages.concat(errors)
        }
        error.errors = messages
      }

      await this.setState({
        error: error,
        __action: 'Send update documents error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'Send update documents finally'
        });
      }, 500)
    }
  }

  async getDocuments() {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get catalog documents starts'
      })

      const response = await verifiServices.getDocuments();

      this.setState({
        catalogo: response.data
      })

    } catch (error) {
      if (error.errors) {
        let messages = [];

        for (const [field, errors] of Object.entries(error.errors)) {
          messages = messages.concat(errors)
        }
        error.errors = messages
      }

      await this.setState({
        error: error,
        __action: 'Get catalog documents error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'Get catalog documents finally'
        });
      }, 500)
    }
  }

  async getDocumentsStatus(uuid) {
    try {
      this.setState({
        httpBusy: true,
        __action: 'Get status documents starts'
      })

      const response = await verifiServices.getDocumentsStatus(uuid);

      this.setState({
        documentStatus: response.data
      })

    } catch (error) {
      if (error.errors) {
        let messages = [];

        for (const [field, errors] of Object.entries(error.errors)) {
          messages = messages.concat(errors)
        }
        error.errors = messages
      }

      await this.setState({
        error: error,
        __action: 'Get status documents error'
      });
    }
    finally {
        this.setState({
          httpBusy: false,
          __action: 'Get status documents finally'
        });
    }
  }

  setDocumentsStatus(data) {
    this.setState({
      documentStatus: data,
      __action: 'Set document status'
    })
  }

  async SendDocuments(data) {
    try {
      this.setState({
        flag: true,
        __action: 'Send documents starts'
      })

      if (data) {
        await verifiServices.updateProfile(data);
      }

    } catch (error) {
      if (error.errors) {
        let messages = [];

        for (const [field, errors] of Object.entries(error.errors)) {
          messages = messages.concat(errors)
        }

        error.errors = messages
      }

      await this.setState({
        error: error,
        __action: 'Send documents error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          flag: false,
          __action: 'Send documents finally'
        });
      }, 500)
    }
  }

  clear() {
    this.setState({
      error: undefined,
      __action: 'VerificationContainer clear error'
    })
  }

  resetState() {
    this.setState({
      ...initialState,
      documents: [],
      __action: 'VerificationContainer reset'
    })
  }
}

export default VerificationContainer;