import React, { Component } from 'react';
import { Container, Text, Button, View, Spinner, Thumbnail } from 'native-base';
import lang from '../../shared/languages/Lang';
import autobind from 'class-autobind';
import Validator from 'validatorjs';
import ImagePicker from 'react-native-image-picker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import { TouchableOpacity, Image } from 'react-native';
import LabelError from '../../components/inputs/LabelError';
import styles from '../../theme/globalStyles';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import { connect } from 'unstated-enhancers';
import infoProfile from './containers/InfoProfileContainer';
import language from '../../components/language/containers/LanguagesContainer';
import checklist from '../checklist/containers/ChecklistContainer';
import observer from '../../shared/Observer';
import TextareaInput from '../../components/inputs/TextareaInput';
import TextInput from '../../components/inputs/TextInput';
import ListErrors from '../../components/listErrors/ListErrors';
import ThumbnailLink from '../../components/thumbnailLink/ThumbnailLink';
import avatar from '../../images/avatar/person_executive.png';
import flagMexico from '../../images/flag_mexico/flag_mexico.png';
import flagFrance from '../../images/flag_france/flag_france.png';
import flagEEUU from '../../images/flag_united_states/flag_united_states.png';
import flag from '../../images/flag.png';
import PrepareVideo from '../../shared/processVideo/PrepareVideo';
import MediaMetaData from '../../shared/processVideo/MediaMetaData';
import ModalPhoto from './components/ModalPhoto';
import MimeFile from '../../shared/processVideo/MimeFile';
import MultiSelect from '../../components/multiSelect/MultiSelect';
import toast from '../../shared/support/ShowToast';

class InfoProfileScreen extends Component {
  optionSelected = null;

  constructor(props) {
    super(props);

    this.state = {
      bio: undefined,
      phone: undefined,
      initialRegion: undefined,
      photo: undefined,
      video_bio_url: undefined,
      modalVisible: false,
      modalvideo: false,
      validation: new Validator(data, this.rules),
      country: undefined,
      load: undefined,
      busy: false,
      skillList: [],
      skillValidate: false,
      modalPhoto: false
    }

    autobind(this);
    this.inputs = {};
  }

  validate() {
    const { initialRegion = {} } = this.props.infoProfile;

    const data = {
      ...this.state,
      newArea: initialRegion.address
    };
    const validation = new Validator(data, this.rules);

    validation.setAttributeNames({
      bio: lang.get('profile.description', 'profile.description'),
      newArea: lang.get('complete.address', 'profile.address'),
      phone: lang.get('complete.phone', 'profile.phone')
    });

    this.validation = validation;

    validation.check();

    this.setState({
      validation
    })
  }

  get rules() {
    return {
      'bio': 'required|string',
      'newArea': 'required|string',
      'phone': 'required|numeric', //TODO: falta validacion de maximo y minimo de caracteres, genera error
    }
  }

  handleRefSet(field, component) {
    this.inputs[field] = component;
  }

  async updateProfile(validate = false) {
    const { httpBusy } = this.props.containers.infoProfile.state;
    if (httpBusy) return;

    this.setState({
      busy: true
    })

    if (validate) this.validate()

    const validation = this.validation;

    if (validation.passes()) {
      const { checklist, infoProfile } = this.props.containers;
      const { newArea, initialRegion } = infoProfile.state;
      const { bio, phone, thumbnail, mimeThumbnail, skillList, skillValidate } = this.state;
      let country = this.state.country;

      if (!country) {
        const { selected } = this.props.containers.language.state;

        if (selected === 'es') {
          country = 'México'
        }
        if (selected === 'fr') {
          country = 'Mexique'
        }
        if (selected === 'en') {
          country = 'Mexico'
        }
      }

      if (skillList.length <= 5) {
        const dataProfile = {
          bio: bio,
          address: newArea,
          position: {
            latitude: initialRegion.latitude,
            longitude: initialRegion.longitude
          },
          phone: phone,
          country: country,
          selected_skills: skillList
        }
        let formData = null;
        let formDataThumbnail = null;
        let config = null;

        if (this.state.video_bio_url || this.state.photo) {
          config = {
            headers: {
              'Content-Type': 'multipart/form-data'
            }
          }

          formData = new FormData();
        }

        if (this.state.video_bio_url) {
          formDataThumbnail = new FormData();

          formData.append('video', {
            name: this.state.video_bio_url.mime,
            type: this.state.video_bio_url.mime,
            uri: this.state.video_bio_url.uri
          });

          if (thumbnail) {
            formDataThumbnail.append('image', {
              name: mimeThumbnail,
              type: mimeThumbnail,
              uri: thumbnail
            });
          }
        }

        if (this.state.photo) {
          formData.append('image', {
            name: this.state.photo.mime,
            type: this.state.photo.mime,
            uri: this.state.photo.path
          });
        }
        await infoProfile.updateProfile(dataProfile, formData, config, formDataThumbnail);

        const { error } = infoProfile.state;

        if (error !== undefined) {
          toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))
          setTimeout(() => {
            infoProfile.clear();
          }, 10000)

        } else {
          await checklist.setStepAsCompleted('profile');
          infoProfile.resetState();
          this.resetState();
          this.props.navigation.navigate('ChecklistScreen');
        }
      } else {
        this.setState({
          skillValidate: true
        })
      }
    } else {
      toast.ShowToast(lang.get('message.dataRequired', 'generic.data_required'));

      const fields = Object.keys(this.rules);
      const values = {}

      for (const field of fields) {
        values[field] = this.state[field] || ''
      }

      this.setState({
        ...values
      })
    }

    this.setState({
      busy: false
    })
  }

  pickAddress() {
    this.props.navigation.navigate('MapScreen')
  }

  submitFromKeyboard() {
    this.updateProfile(true)
  }

  handleValueChange(field, value) {
    this.handleIsEditing();

    this.setState({
      [field]: value
    });
  }

  handlePhoneChange(field, value) {
    this.handleIsEditing();

    if (value.length <= 10) {
      this.setState({
        [field]: value
      })
    }
  }

  resetState() {
    this.setState({
      bio: undefined,
      newArea: undefined,
      phone: undefined,
      initialRegion: undefined,
      photo: undefined,
      video_bio_url: undefined,
      modalVisible: false,
      modalvideo: false,
      skillList: undefined
    })
  }

  navigateVideo() {
    this.props.navigation.navigate('VideoProfileScreen', { url: this.state.video_bio_url.uri });
  }

  onSelectedCountry(value) {
    this.setState({
      country: value
    })
  }

  async onConfirmCountry() {
    this.setState({
      countrySelected: true
    })

    const { country } = this.state;

    if (country) {
      await this.setState({
        countryConfirm: country
      })
    }

    setTimeout(() => {
      this.setState({
        countrySelected: false
      })
    }, 200)
  }

  setPreviousCountry() {
    setTimeout(() => {
      const { countryConfirm, countrySelected } = this.state;

      if (!countrySelected && countryConfirm)
        this.onSelectedCountry(countryConfirm)
    }, 100)
  }

  async getCountries() {
    const { infoProfile } = this.props.containers;

    await infoProfile.getCountries();

    const { error } = infoProfile.state;

    if (error) {
      toast.ShowToast(error.message || lang.get('message.tryAgain', 'genric.try_again'))

      setTimeout(() => {
        infoProfile.clear();
      }, 500);
    }
  }

  async getSkills() {
    const { infoProfile } = this.props.containers;

    await infoProfile.getSkills();

    const { error } = infoProfile.state;

    if (error) {
      toast.ShowToast(error.message || lang.get('message.tryAgain', 'genric.try_again'))

      setTimeout(() => {
        infoProfile.clear();
      }, 500);
    }
  }

  setItem(value) {
    this.setState({
      skillValidate: false,
      skillList: value,
      pass: true
    });
  }

  handleIsEditing() {
    const { navigation } = this.props;

    const isEditing = navigation.getParam('isEditing');

    if (!isEditing) {
      navigation.setParams({
        isEditing: true
      });
    }
  }

  setNavigationGoBackConfirmationAlert() {
    this.props.navigation.setParams({
      alert: {
        title: lang.get('info.titleAlert', 'generic.title_alert'),
        message: lang.get('info.messageAlert', 'generic.message_alert')
      }
    });
  }

  componentWillMount() {
    observer.subscribe("seller/resultUpdateProfile", (data) => {
      const { checklist } = this.props.containers;

      if (data.result === 'Success') {
        checklist.setStepAsCompleted('profile');
        this.props.navigation.navigate('ChecklistScreen');
      } else {
        toast.ShowToast(data.message)
      }
    });
  }

  componentWillUnmount() {
    observer.unSubscribe("seller/resultUpdateProfile", (data) => {
      const { checklist } = this.props.containers;

      if (data.result === 'Success') {
        checklist.setStepAsCompleted('profile');
        this.props.navigation.navigate('ChecklistScreen');
      } else {
        toast.ShowToast(data.message)
      }
    });

    const { infoProfile } = this.props.containers;
    infoProfile.setInfoProfile(this.state)
  }

  componentDidMount() {
    const { bio, phone } = this.props.infoProfile;

    this.setState({
      ...this.state,
      bio: bio ? bio : undefined,
      phone: phone ? phone : undefined
    })

    /** DESCOMENTAR PARA INCLUIR CATAGOLO DE PAISES */
    //this.getCountries();

    this.setNavigationGoBackConfirmationAlert();
    this.getSkills();
  }

  componentDidUpdate(prevProps) {
    if (this.props.infoProfile.initialRegion !== prevProps.infoProfile.initialRegion) {
      this.validate()
    }
  }

  selectVideoTapped() {
    const options = {
      title: lang.get('profile.videoPicker', 'payment.video_picker'),
      takePhotoButtonTitle: lang.get('profile.takeVideo', 'payment.take_video'),
      chooseFromLibraryButtonTitle: lang.get('profile.chooseLibrary', 'payment.choose_library'),
      cancelButtonTitle: lang.get('profile.cancelButtonTitle', 'genric.button.cancel'),
      mediaType: 'video',
      videoQuality: 'medium',
      durationLimit: 60,
    };

    ImagePicker.showImagePicker(options, async (response) => {

      if (response.didCancel) {
        return
      } else if (response.error) {
        toast.ShowToast(response.error)
      } else if (response.customButton) {
        return
      } else {

        this.setState({
          load: true,
        })

        const path = response.uri.replace('file://', '')

        const metadata = await MediaMetaData.getMediaMeta(path);

        if (metadata.duration <= 60000) {

          const videoLoaded = await PrepareVideo.prepareVideo(response);

          this.setState({
            modalVisible: false,
            video_bio_url: { ...videoLoaded.video },
            load: false,
            thumbnail: videoLoaded.thumbnail.path,
            mimeThumbnail: videoLoaded.thumbnail.mime
          })

          this.handleIsEditing();
        } else {
          toast.ShowToast(lang.get('profile.exceedsTimeVideo', 'profile.exceeds__video'))
          return
        }
      }
    });
  }

  async selectImageTapped() {
    const options = {
      title: lang.get('profile.imagePicker', 'profile.image_picker'),
      takePhotoButtonTitle: lang.get('profile.take_image', 'profile.exceeds__video'),
      chooseFromLibraryButtonTitle: lang.get('profile.choose_library', 'profile.exceeds__video'),
      cancelButtonTitle: lang.get('profile.cancelButtonTitle', 'generic.button.cancel'),
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, async (response) => {
      if (response.didCancel) {
        return
      } else if (response.error) {
        toast.ShowToast(response.error)
      } else if (response.customButton) {
        return
      } else {

        const extImage = response.uri.split('.').pop();
        const mimeType = await MimeFile.getMime(extImage);

        const _photo = {
          mime: mimeType,
          path: response.uri
        }

        this.setState({
          photo: _photo,
        })

        this.handleIsEditing();
      }
    })
  }

  handleModalPhoto() {
    const inverse = !this.state.modalPhoto;
    this.setState({ modalPhoto: inverse });
  }

  checkPhoto() {
    const { photo } = this.state;

    if(!photo) {
      this.validate()

      const validation = this.validation;

      if (validation.passes()) {
        this.handleModalPhoto()
      } else {
        toast.ShowToast(lang.get('message.dataRequired', 'generic.data_required'));
      }

    } else {
      this.updateProfile()
    }
  }

  render() {
    const { infoProfile } = this.props.containers;
    const { httpBusy, initialRegion = {}, error = {}, catalogCountries = [], skills = [] } = infoProfile.state;
    const { bio, phone, validation, photo, video_bio_url, modalPhoto, thumbnail, load, busy } = this.state;

    return (
      <Container style={styles.container}>
        <NavBarComponent {...this.props.navigation} name={lang.get('title.profile', 'profile.profile')}>
        </NavBarComponent>

        <KeyboardAwareScrollView style={{ padding: 15, }} bounces={false}>
          <View style={{ flexDirection: 'row' }}>
            <View style={{
              flex: 1,
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
              paddingVertical: 10,
            }} >
              <TouchableOpacity onPress={this.selectImageTapped}>
                <View style={{ alignItems: 'flex-end', flexDirection: 'row' }}>
                  <Thumbnail source={photo ? { uri: photo.path } : avatar} style={{
                    height: 100,
                    width: 100,
                    borderRadius: 50,
                  }} />
                </View>
              </TouchableOpacity>
              <Text note style={{ textAlign: 'center', marginTop: 10 }}>{lang.get('profile.logo', 'profile.logo')} </Text>
            </View>

            <ThumbnailLink
              thumbnail={thumbnail}
              videoLabel={lang.get('profile.video', 'profile.video')}
              videoLoaded={lang.get('profile.playVideo', 'profile.play_video')}
              isVideoLoaded={!!!!video_bio_url}
              textColor={'#ABABAB'}
              onVideoSelectionOption={this.selectVideoTapped}
              onNavigateVideo={this.navigateVideo}
              load={load}
            />
          </View>

          <View>
            <Text note style={{ textAlign: 'center' }}>{lang.get('complete.infos', 'profile.infos')}</Text>
          </View>

          <View>

            <TextareaInput
              field='bio'
              value={bio}
              rowSpan={5}
              bordered
              style={{ width: '100%' }}
              placeholder={lang.get('profile.placeholderDescription', 'profile.placeholder_description')}
              errors={validation.errors}
              onChangeText={this.handleValueChange}
              onRefSet={this.handleRefSet}
              onBlur={this.validate}
              autoFocus={true}
            />

            {/**
              * PICKER PARA CATALOGO DE PAISES
              * No se usará para la primera versión
              * descomentar cuando se involucren los otros paises
            **/}
            {/* <View style={{ width:'100%', borderColor: 'lightgray', borderWidth: 1, marginTop: 10, flexDirection:'row', alignItems:'center' }}>
              <Image
                source={this.state.country === 'México' || this.state.country === 'Mexico' || this.state.country === 'Mexique' ? flagMexico
                  : this.state.country === "États-Unis d' Amérique" || this.state.country === 'Estados Unidos de América' || this.state.country === 'United States of America' ? flagEEUU
                  : this.state.country === 'Francia' || this.state.country === 'France' ? flagFrance : flag }
                style={{resizeMode:'contain', marginHorizontal: 10}}
              />
              <RNPickerSelect
                placeholder={{
                  label: lang.get('profile.country', 'profile.country'),
                  value: undefined,
                }}
                items={catalogCountries}
                value={this.state.country}
                onValueChange={this.onSelectedCountry}
                onDonePress={this.onConfirmCountry}
                onClose={this.setPreviousCountry}
                onDownArrow={this.setPreviousCountry}
                style={{
                  inputIOS: {
                    fontSize: 16,
                    padding: 10,
                    color: 'black',
                    width: width * 0.8
                  },
                  modalViewBottom: {
                    backgroundColor: 'white'
                  },
                  modalViewMiddle: {
                    backgroundColor: 'white',
                    borderColor: 'gray',
                    borderWidth: 0.2
                  },
                  iconContainer: {
                    top: 10,
                    right: 12
                  },
                }}
                doneText={lang.get('modal.done', 'generic.butto.done')}
                Icon={() => {
                  return <Icon name="arrow-dropdown" style={{ color: 'gray', fontSize: 24 }} />;
                }}
              />
            </View> */}
            <View style={{ flexDirection: 'row', paddingHorizontal: 10, marginTop: 10, alignItems: 'center' }}>
              <Image source={flagMexico} style={{ resizeMode: 'contain' }} />
              <Text style={{ marginLeft: 10 }}>México</Text>
            </View>

            <TextInput style={{ marginTop: 10 }}
              field="phone"
              next="newArea"
              value={phone}
              refs={this.inputs}
              returnKeyType={"next"}
              placeholder={lang.get('complete.phone', 'profile.phone')}
              keyboardType="phone-pad"
              errors={validation.errors}
              onChangeText={this.handlePhoneChange}
              onRefSet={this.handleRefSet}
              onBlur={this.validate}
            />

            <TextInput
              field="newArea"
              refs={this.inputs}
              style={styles.flexDirectionRow_JustifyBetween}
              placeholder={lang.get('complete.address', 'profile.address')}
              value={initialRegion.address}
              errors={validation.errors}
              onFocus={this.pickAddress}
              onRefSet={this.handleRefSet}
              onBlur={this.validate}
            />

            <View>
              <Text note style={{ textAlign: 'center', marginTop: 15 }}>{lang.get('profile.skillsInfo', 'profile.skills_info')}</Text>
            </View>
            <MultiSelect
              isSelectSingle={false}
              style={{ marginTop: 10 }}
              colorTheme={'#0285c9'}
              colorButtonCancel={'#52bebb'}
              popupTitle={lang.get('profile.skills', 'profile.skills')}
              title={lang.get('profile.selectedSkills', 'profile.selected_skills')}
              data={skills}
              limit={5}
              onSelect={this.setItem}
              onRemoveItem={this.setItem}
              selectButtonText={lang.get('button.save', 'generic.button.save')}
              cancelButtonText={lang.get('button.cancel', 'generic.button.cancel')}
              searchPlaceHolderText={lang.get('button.search', 'generic.button.search')}
              listEmptyTitle={lang.get('profile.coincidences', 'profile.coincidences')}
            />
            {this.state.skillValidate === true && <LabelError message={lang.get('profile.skillsError', 'profile.skills_error')} />}
          </View>

          <View>
            <Button
              style={[styles.margin25, { backgroundColor: busy === true ? 'gray' : '#0285c9', marginBottom: 35 }]}
              block
              onPress={this.checkPhoto}
              disabled={busy}
            >
              <Text> {lang.get('profile.next', 'generic.button.next')} </Text>
              {httpBusy && <Spinner color='lightblue' />}
            </Button>

          </View>

          {error.errors && (
            <ListErrors errors={error.errors} />
          )}
        </KeyboardAwareScrollView>

        <ModalPhoto
          modalPhoto={modalPhoto}
          onCloseModalPhoto={this.handleModalPhoto}
          onUpdateProfile={this.updateProfile}
        />
      </Container>
    );
  }
}

const containers = {
  language: language,
  infoProfile: infoProfile,
  checklist: checklist,
}

const mapStateToProps = (containers) => {
  return {
    infoProfile: containers.infoProfile.state,
  };
}

export default connect(containers, mapStateToProps)(InfoProfileScreen);