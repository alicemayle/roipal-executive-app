import { StyleSheet, } from 'react-native';
import { Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  description: {
    textAlign: 'center',
    margin: 25,
    color: '#6E6E6E'
  },
  cardList: {
    borderRadius: 10,
    width: '95%',
    marginBottom: 10
  },
  viewColum: {
    flexDirection: 'column',
    alignItems: 'center'
  },
  viewSeparator: {
    height: 1,
    width: '100%',
    backgroundColor: 'lightgrey'
  },
  container: {
    flex: 1,
  },
});

export default styles;
