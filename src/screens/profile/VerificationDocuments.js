import React, { Component } from 'react';
import autobind from 'class-autobind';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import { Container, View, Text, Content, Button, Spinner, CardItem, Card } from 'native-base';
import stylesProfile from './styleProfile';
import lang from '../../shared/languages/Lang';
import { Platform, FlatList } from 'react-native';
import PhotographicDocuments from './components/PhotographicDocuments';
import ModalSelected from '../../components/modal/ModalSelected';
import ImageCropPicker from 'react-native-image-crop-picker';
import { connect } from 'unstated-enhancers';
import verification from './containers/VerificationContainer';
import anverso from '../../images/verification/id_anverso.png';
import Permissions from '../../shared/support/Permissions';
import LabelError from '../../components/inputs/LabelError';
import MimeFile from '../../shared/processVideo/MimeFile';
import ImagePicker from 'react-native-image-picker';
import Auth from '../../shared/services/Auth';
import AuthContainer from '../../screens/auth/containers/AuthContainer';
import toast from '../../shared/support/ShowToast';

class VerificationDocuments extends Component {
  optionSelected = null;

  constructor(props) {
    super(props);
    autobind(this);

    this.state = {
      modalVisible: false,
    }
  }

  handleImageSelectionOption(file, photo, index, name) {
    this.optionSelected = photo;
    this.file = file;
    this.index = index;
    this.name = name;

    if (Platform.OS === 'android') {
      this.setModalVisible();
    } else {
      this.selectImageTapped(file, name)
    }
  }

  setModalVisible() {
    const inverse = !this.state.modalVisible;
    this.setState({
      modalVisible: inverse
    });
  }

  handleSelectedOption(type) {
    if (!type) {
      return
    }

    const picker = type === 'gallery' ? 'openPicker' : 'openCamera';

    if (Platform.OS === 'android') {
      this.setState({
        modalVisible: false
      })
    }

    ImageCropPicker[picker]({
      width: 500,
      height: 500,
      cropping: false,
      cropperCircleOverlay: false,
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      compressVideoPreset: 'MediumQuality',
      includeExif: true,
    }).then(image => {

      let docs = [...this.state.catalogo];

      docs[this.index] = {
        ...this.state.catalogo[this.index],
        file: image
      }

      this.setState({
        [this.optionSelected]: image,
        modalVisible: false,
        catalogo: [...docs],
        pass: false
      }
        , _ => this.optionSelected = null)
      this.goToNext(this.file, image, this.name);
      this.handleIsEditing();
    }).catch(_ => { });
  }

  async selectImageTapped(file, name) {
    const options = {
      title: lang.get('profile.imagePicker', 'profile.image_picker'),
      takePhotoButtonTitle: lang.get('profile.takeImage', 'profile.take_image'),
      chooseFromLibraryButtonTitle: lang.get('profile.chooseLibrary', 'profile.choose_library'),
      cancelButtonTitle: lang.get('profile.cancelButtonTitle', 'generic.button.cancel'),
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, async (response) => {
      if (response.didCancel) {
        return
      } else if (response.error) {
        toast.ShowToast(response.error)
      } else if (response.customButton) {
        return
      } else {

        const extImage = response.uri.split('.').pop();
        const mimeType = await MimeFile.getMime(extImage);

        const _photo = {
          mime: mimeType,
          path: response.uri
        }

        let docs = [...this.state.catalogo];

        docs[this.index] = {
          ...this.state.catalogo[this.index],
          file: _photo
        }

        this.setState({
          photo: _photo,
          catalogo: [...docs],
          pass: false
        })

        this.goToNext(file, _photo, name);
        this.handleIsEditing();
      }
    })
  }

  handleCloseModal() {
    this.setState({
      modalVisible: false
    })
  }

  handleIsEditing() {
    const { navigation } = this.props;

    const isEditing = navigation.getParam('isEditing');

    if (!isEditing) {
      navigation.setParams({
        isEditing: true
      });
    }
  }

  async goToNext(file, image, name) {
    const { verification } = this.props.containers;
    const { httpBusy } = verification.state;

    if (httpBusy) return;

    let formData = null;
    let pic = null;

    let config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    }

    if (image.mime === 'image/jpeg') {
      pic = image;

      formData = new FormData();

      formData.append('image', {
        name: pic.mime,
        type: pic.mime,
        uri: pic.path
      });

      await verification.updateProfile(file, formData, config, name);

      this.setState({
        [file]: true
      })
    } else {
      this.setState({
        [file]: false
      })

      toast.ShowToast(lang.get('profile.documents.invalid_file'))
    }

    const { error } = verification.state

    if (error) {
      toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))
      verification.clear()
    }
  }

  async SendDocuments() {
    const { verification } = this.props.containers;
    const { error, documentStatus } = verification.state;
    const docsArray = verification.state.documents;
    const { catalogo_requires } = this.state;

    if (documentStatus.length > 0) {
      const data = docsArray.map(cat => {
        const find = documentStatus.find(item => item.document_type === cat.document_type)

        if (find) {
          const newData = {
            uuid: find.uuid,
            document_type: cat.document_type,
            document_url: cat.document_url,
            status: 0,
            update: 1
          }
          return newData
        } else {
          const newData = {
            ...cat,
            update: 0
          }
          return newData
        }
      }
      )

      const doc_update = data.filter(item => item.update === 1)
      const docs = data.filter(item => item.update === 0)
      const uuid = Auth.getUser().uuid;

      const documentsUpdate = {
        documents: doc_update
      }

      const documents = {
        documents: docs
      }

      if (documentsUpdate.documents.length > 0) {
        await verification.updateDocuments(documentsUpdate, uuid);
      }

      if (documents.documents.length > 0) {
        await verification.SendDocuments(documents);
      }

      const { error } = verification.state;

      if (error) {
        toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))
        setTimeout(() => {
          verification.clear();
        }, 10000)

      } else {
        const { auth } = this.props.containers

        const data = {
          profile: {
            uuid: Auth.getUser().uuid
          }
        }

        auth.getProfile(data)

        const { navigation } = this.props;
        const goBack = navigation.getParam('goBack');

        this.props.navigation.navigate('MessageValidationScreen', { goBack: goBack });
      }
    } else {
      const map_requires = await catalogo_requires.map(item => {
        const finds = docsArray.find(req => req.document_type === item.document_type)
        return finds
      })

      const result = map_requires.includes(undefined);

      if (result) {
        toast.ShowToast(lang.get('profile.documents.data_required'))
        this.setState({
          pass: true
        })
      } else {
        const documents = {
          documents: docsArray
        }

        if (error !== undefined) {
          toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))
          setTimeout(() => {
            verification.clear();
          }, 10000)
        } else {
          await verification.SendDocuments(documents);

          const { error } = verification.state;

          if (error) {
            toast.ShowToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))
            setTimeout(() => {
              verification.clear();
            }, 10000)

          } else {
            const { auth } = this.props.containers

            const data = {
              profile: {
                uuid: Auth.getUser().uuid
              }
            }

            auth.getProfile(data)
            const { navigation } = this.props;
            const goBack = navigation.getParam('goBack');

            this.props.navigation.navigate('MessageValidationScreen', { goBack: goBack });
          }
        }
      }
    }
  }

  setNavigationGoBackConfirmationAlert() {
    this.props.navigation.setParams({
      alert: {
        title: lang.get('info.titleAlert', 'generic.title_alert'),
        message: lang.get('info.messageAlert', 'generic.message_alert')
      }
    });
  }

  async getDocuments() {
    const { verification } = this.props.containers;
    const { error } = verification.state;

    const uuid = Auth.getUser().uuid;

    if (uuid) {
      await verification.getDocumentsStatus(uuid);

      if (error) {
        toast.ShowToast('error')
      } else {
        await verification.getDocuments();
      }
      const { catalogo, documentStatus } = verification.state;

      const data = catalogo.map(cat => {
        const find = documentStatus.find(item => item.document_type === cat.document_type)

        if (find) {
          const newData = {
            ...find,
            required: cat.required
          }
          return newData
        } else {
          const newData = {
            ...cat
          }
          return newData
        }
      }
      )
      const catalogo_requires = data.filter(req => req.required === 1)
      const statusRejected = documentStatus.filter(rej => rej.status === -1)

      this.setState({
        catalogo: data,
        dics: documentStatus,
        catalogo_requires: catalogo_requires,
        docsRejected: statusRejected
      })
    }
  }

  componentWillUnmount() {
    const { verification } = this.props.containers;
    verification.resetState()
  }

  async  componentDidMount() {
    this.setNavigationGoBackConfirmationAlert()

    const permissionCamera = await Permissions.isAllowed('camera');
    const permissionPhoto = await Permissions.isAllowed('photo');

    if (permissionCamera === 'denied') {
      await Permissions.camera()
    }

    if (permissionPhoto === 'denied') {
      await Permissions.photo()
    }

    this.getDocuments()
  }

  renderItem(item) {
    const fotos = 'photo'.concat(item.index)
    const campo = item.item

    return (
      <CardItem style={[{ borderRadius: 10 }]}>
        <PhotographicDocuments
          ImageSelectionOption={() => this.handleImageSelectionOption(item.item.document_type, fotos, item.index, item.item.name, campo)}
          avatar={anverso}
          required={item.item.required === 1 ? true : false}
          name={item.item.name}
          file={item.item.file}
          url={item.item.document_url}
          disable={item.item.status}
          motive={item.item.reason_rejected}
        />
      </CardItem>
    )
  }

  renderSeparator() {
    return (
      <View style={stylesProfile.viewSeparator} />
    )
  }

  render() {
    const { modalVisible, catalogo, dics, docsRejected } = this.state;
    const { flag, documents } = this.props.containers.verification.state;

    const val = docsRejected && docsRejected.length >= 1 ? true
      : dics && dics.length !== catalogo.length ? true
        : documents && documents.length >= 1 ? true
          : false

    const disable = documents && documents.length > 0 ? false : true

    return (
      <Container style={stylesProfile.container}>
        <NavBarComponent {...this.props.navigation} name={lang.get('documents.title', 'profile.documents.title')} />
        <Content bounces={false}>
          <Text style={stylesProfile.description}>
            {lang.get('documents.description', 'profile.documents.description')}
          </Text>
          <View style={stylesProfile.viewColum}>
            <View style={{ flexDirection: 'row', }}>
              <Card style={stylesProfile.cardList}>
                <FlatList
                  data={catalogo}
                  keyExtractor={(item, index) => String(index)}
                  renderItem={this.renderItem}
                  ItemSeparatorComponent={this.renderSeparator}
                />
              </Card>
            </View>
            {
              this.state.pass === true &&
              <LabelError message={lang.get('documents.required', 'profile.documents.required')} />
            }
          </View>
          {
            val &&
            <Button
              style={{ backgroundColor: flag ? '#6E6E6E' : disable ? '#6E6E6E' : '#0285c9', margin: 30 }}
              block
              disabled={disable}
              onPress={this.SendDocuments}
            >
              <Text>{lang.get('profile.next', 'generic.button.next')}</Text>
              {
                flag &&
                <Spinner color='lightblue' />
              }
            </Button>
          }
        </Content>
        <ModalSelected
          modalVisible={modalVisible}
          onSelectedOption={this.handleSelectedOption}
          onHandleCloseModal={this.handleCloseModal}
        />
      </Container>
    )
  }
}

const containers = {
  verification: verification,
  auth: AuthContainer
}

const mapStateToProps = (containers) => {
  return {
    verification: containers.verification.state,
  };
}

export default connect(containers, mapStateToProps)(VerificationDocuments);