import React, { PureComponent } from 'react';
import { Text, View, Button } from 'native-base';
import { Logger } from 'unstated-enhancers';
import autobind from 'class-autobind';

import lang from '../../../shared/languages/Lang';
import styles from '../../../theme/globalStyles';
import Modal from 'react-native-modal';
import { ScrollView } from 'react-native-gesture-handler';

class ModalPhoto extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    }

    autobind(this);
  }

  async update() {
    const { onCloseModalPhoto, onUpdateProfile } = this.props;

    await onCloseModalPhoto();
    onUpdateProfile();
  }

  render() {
    const { onCloseModalPhoto, modalPhoto, onUpdateProfile } = this.props
    Logger.count(this);
    return (
      <Modal
        transparent={true}
        isVisible={modalPhoto}
        onBackdropPress={onCloseModalPhoto}
        hideModalContentWhileAnimating={true}
        presentationStyle={'overFullScreen'}
        useNativeDriver={true}
        style={[{ flex: 1, alignItems: 'center', margin: 0 }]}
      >
        <View style={styles.modal}>
          <Text style={{ textAlign: 'center', fontWeight: 'bold', marginBottom: 20 }}>
            {lang.get('info.titleAlert', 'generic.title_alert')}
          </Text>
          <ScrollView contentContainerStyle={{
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center'
          }}>
            <Text style={{ textAlign: 'justify' }}>
              {lang.get('profile.photoSuggestion', 'profile.photo_suggestion')}{'\n'}
            </Text>
            <Text style={{ textAlign: 'center' }}>
              {lang.get('profile.continue', 'profile.continue')}
            </Text>

            <View
              style={{flexDirection: 'row', marginTop: 25, alignItems:'center'}}>
              <Button
                style={{marginRight: 15}}
                onPress={onCloseModalPhoto}>
                <Text>{lang.get('profile.buttonBack', 'generic.button.back')}</Text>
              </Button>
              <Button
                success
                onPress={this.update}>
                <Text>{lang.get('mission.confirm', 'generic.button.done')}</Text>
              </Button>

            </View>

          </ScrollView>
        </View>
      </Modal>
    )
  }
}

export default (ModalPhoto);