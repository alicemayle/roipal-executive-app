import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { Thumbnail, View, Text } from 'native-base';
import { Logger } from 'unstated-enhancers';

class PhotographicDocuments extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { ImageSelectionOption, name, avatar, required, file, url, disable, motive } = this.props;
    const necessary = required ? '*' : undefined;

    Logger.count(this)
    return (
      <View style={{ flex: 1, flexDirection: 'row', marginBottom: 3 }}>
        <TouchableOpacity onPress={ImageSelectionOption} disabled={disable === 1 ? true : false} style={{flexDirection: 'row'}}>
          <Thumbnail source={file ? { uri: file.path } : url ? { uri: url } : avatar} style={{
            height: 70,
            width: 70,
            marginHorizontal: 10,
            marginVertical: 7,
            borderWidth: disable === -1 ? 3 : 0,
            borderColor: disable === -1 ? '#B40404' : 'transparent'
          }} />
        <View style={{ justifyContent: 'center', marginLeft: 10, flex: 1 }}>
          <Text note style={{ color: disable === -1 ? '#B40404' : undefined }}>{name}</Text>
          {!url &&
            <Text note>{necessary}{required ? 'Requerido' : 'Opcional'}</Text>
          }
          {motive &&
            <Text numberOfLines={2} note style={{ color: '#B40404' }}>{motive}</Text>
          }
        </View>
        </TouchableOpacity>
      </View>
    );
  }
}

export default PhotographicDocuments;