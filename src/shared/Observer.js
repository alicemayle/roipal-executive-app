class Observer {
  constructor() {
    this.listener = {}
  }

  subscribe(eventName, callback) {
    this.listener[eventName] = this.listener[eventName] || [];
    this.listener[eventName].push(callback);
  };
  unSubscribe(eventName, callback) {
    if (this.listener[eventName]) {
      for (var i = 0; i < this.listener[eventName].length; i++) {
        if (this.listener[eventName][i] === callback) {
          this.listener[eventName].splice(i, 1);
          break;
        }
      };
    }
  };
  notify(eventName, data) {
    if (this.listener[eventName]) {
      this.listener[eventName].forEach(function (fn) {
        fn(data);
      });
    }
  };
}

const observer = new Observer();

export default observer;