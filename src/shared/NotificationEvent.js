import Observer from '../shared/Observer';
import NavigationService from '../shared/services/NavigationService';
import { Logger } from 'unstated-enhancers';
import * as events from './events';

const listeners = Object.values(events);

class NotificationEvent {

  subscribe() {
    for (const listener of listeners) {
      listener.subscribe();
    }
  }

  unsubscribe() {
    for (const listener of listeners) {
      listener.unsubscribe();
    }
  }
}

export default new NotificationEvent;