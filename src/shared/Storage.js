import { AsyncStorage } from "react-native";

class Storage {
  put(key, data) {
    return AsyncStorage.setItem(key, data);
  }

  get(key) {
    return AsyncStorage.getItem(key);
  }

  remove(key) {
    return AsyncStorage.removeItem(key);
  }

  clear() {
    return AsyncStorage.clear();
  }
}

export default new Storage