import SocketIOClient from 'socket.io-client';
import Observer from './Observer'
import { Logger } from 'unstated-enhancers';
import Auth from '../shared/services/Auth';
import autobind from 'class-autobind';
import { REACT_APP_API_URL_CHAT } from './support/ApiUrl';

class Socket {

  constructor() {
    autobind(this)
  }

  get client() {
    return this.socket;
  }

  connect() {
    this.socket = SocketIOClient(REACT_APP_API_URL_CHAT, {
      'transports': ['websocket'],
      'pingTimeout': 10800,
      'forceNew': true,
      'jsonp': false,
    });

    this.socket.on('connect', this._authenticate)
    this.socket.on('unauthorized', this._handleUnauthorized)
  }

  connectSocket(callback) {
    this.socket = SocketIOClient(REACT_APP_API_URL_CHAT, {
      'transports': ['websocket'],
      'pingTimeout': 10800,
      'forceNew': true,
      'jsonp': false,
    });

    this.socket.on('connect', _ => {

      this.socket.on('account::activate', callback);

      this.socket.emit('room::subscribe', Auth.getUser().uuid)
    });
  }

  disconnect() {
    this.socket.disconnect()
  }

  on(event, callback) {
    this.socket.on(event, callback)
  }

  off(event, callback) {
    this.socket.off(event, callback)
  }

  join(data, callback) {
    this.socket.emit('room::join', data, callback)
  }

  leave(data, callback) {
    this.socket.emit('room::leave', data, callback)
  }

  writing(data, callback) {
    this.socket.emit('chat::typing', data, callback)
  }

  isconnect(data, callback) {
    this.socket.emit('chat::user-connect', data, callback)
  }

  isdisconnet(data, callback) {
    this.socket.emit('chat::user-disconnect', data, callback)
  }

  _authenticate() {
    const token = Auth.getAccessToken();

    Logger.dispatch('Auth.getAccessToken()', token)

    this.socket.emit('authenticate', { token: token });
  }

  _handleUnauthorized(data) {
    Logger.dispatch('Auth._handleUnauthorized()', data)

    Observer.notify('SOCKET_UNAUTHORIZED', data)
  }

}

export default new Socket;

