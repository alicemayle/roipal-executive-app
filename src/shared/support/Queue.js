'use strict';

let defer

if (typeof process === 'object' && typeof process.nextTick === 'function') {
  defer = process.nextTick;
} else if (typeof Promise === 'function') {
  const resolve = Promise.resolve();
  defer = resolve.then.bind(resolve);
} else if (typeof setImmediate === 'function') {
  defer = setImmediate;
} else {
  defer = setTimeout;
}

class Queue {

   next(cb) {
    defer(cb);
  }

}


export default new Queue();