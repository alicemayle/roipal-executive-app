import { status } from './InvitationStatusConstants';
import lang from '../languages/Lang'

class InvitationStatusManager {

  getStatusColor(value) {
    const color = status.ACCEPTED === value ? 'rgba(58,186,14,1)' : status.REJECTED === value ? '#CD5C5C' : 'gray';
    return color;
  }

  getStatusColorMission(value) {
    const color = status.ACCEPTED === value ? '#ffbb2f'
    : status.REJECTED === value ? '#CD5C5C' : status.COMPLETED === value ? 'rgba(58,186,14,1)'
    : status.CANCELEDBYBUSINESS === value ? '#394a4d' : 'gray';
    return color;
  }

  getStatusTag(value) {
    const tag = status.ACCEPTED === value ? lang.get('invitation.statusAccept', 'invitation_status.status_accept')
          : status.REJECTED === value ? lang.get('invitation.statusReject', 'invitation_status.status_reject')
          :lang.get('activity.statusOnHold', 'invitation_status.status_hold');
    return tag;
  }

  getStatusActivity(value) {
    const tag = status.ACCEPTED === value ? lang.get('activity.statusStart', 'invitation_status.status_start')
          : status.COMPLETED === value ? lang.get('activity.statusFinish', 'invitation_status.status_finish')
          : lang.get('activity.statusOnHold', 'invitation_status.status_hold');
    return tag;
  }

  getStatusMission(value) {
    const tag = status.ACCEPTED === value ? lang.get('mission.statusStart', 'invitation_status.status_start')
          : status.COMPLETED === value ? lang.get('mission.statusFinish', 'invitation_status.status_finish')
          : status.REJECTED === value ? lang.get('mission.statusCancel', 'invitation_status.status_cancel')
          : status.CANCELEDBYBUSINESS === value ? lang.get('mission.statusCanceled', 'invitation_status.status_canceled')
          : lang.get('mission.statusOnHold', 'invitation_status.status_hold');
    return tag;
  }

  isStarted(value) {
    return status.ACCEPTED === value
  }

  isRejected(value) {
    return status.REJECTED === value
  }

  isUninitiated(value) {
    return status.UNINITIATED === value
  }

}

export default new InvitationStatusManager;