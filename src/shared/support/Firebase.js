import firebase from 'react-native-firebase';
import { Manager } from 'unstated-enhancers'

class Firebase {

  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    }
  }

  async getToken() {
    try {
      let fcmToken = await Manager.get('AuthContainer').state.fcm_token;

      if (!fcmToken) {
        fcmToken = await firebase.messaging().getToken();
        if (fcmToken) {
          await Manager.get('AuthContainer').setFcmToken(fcmToken);
        } else {
        }
      }
    } catch (error) {
      alert('error')
    }

  }

  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
  }
}

export default new Firebase;