class StringFunction {
  capitalize(string) {
    return string.toLowerCase().charAt(0).toUpperCase() + string.slice(1);
  }

  toUpperCase(string) {
    return string.toUpperCase();
  }

  toLowerCase(string) {
    return string.toLowerCase();
  }
}

export default new StringFunction;