import { Toast } from "native-base";

class ShowToast {

  ShowToast(text) {
    return (
      Toast.show({
        text: text,
        textStyle: { color: "white", fontSize: 12 },
        duration: 3000,
        position: 'top'
      })
    )
  }

}

export default new ShowToast;