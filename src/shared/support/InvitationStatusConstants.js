const status = {
  UNINITIATED: 0,
  ACCEPTED: 1,
  REJECTED: -1,
  COMPLETED: 10,
  CANCELEDBYBUSINESS: -10
}

export {
  status
}