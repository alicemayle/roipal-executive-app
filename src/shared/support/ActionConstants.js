const actions = {
  MISSION_CANCELED: 'MISSION_CANCELED',
  MISSION_FINISHED: 'MISSION_FINISHED',
  MISSION_STARTED: 'MISSION_STARTED',
  SEND_INVITATION: 'SEND_INVITATION',
}

export {
  actions
}