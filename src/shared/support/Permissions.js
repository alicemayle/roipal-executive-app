import { Platform, Alert } from 'react-native';
import RNPermissions from 'react-native-permissions'
import Lang from '../languages/Lang'

class Permisisons {

  async location() {
    const confirmation = {
      title: Lang.get('getPermissions.location.title', 'permissions.title'),
      message: Lang.get('getPermissions.location.message', 'permissions.location.message')
    }

    let response = await this.request('location', confirmation);

    return response;
  }

  async notification() {
    const confirmation = {
      title: Lang.get('getPermissions.notification.title', 'permissions.title'),
      message: Lang.get('getPermissions.notification.message', 'permissions.notification.message')
    }

    let response = await this.request('notification', confirmation);

    return response;
  }

  async camera() {
    const confirmation = {
      title: Lang.get('getPermissions.camera.title', 'permissions.title'),
      message: Lang.get('getPermissions.camera.message', 'permissions.camera.message')
    }

    let response = await this.request('camera', confirmation);

    return response;
  }

  async photo() {
    const confirmation = {
      title: Lang.get('getPermissions.photo.title', 'permissions.title'),
      message: Lang.get('getPermissions.photo.message', 'permissions.photo.message')
    }

    let response = await this.request('photo', confirmation);

    return response;
  }

  async request(permission, confirmation = null) {
    let response = await this.isAllowed(permission);

    if (response === 'undetermined' || (Platform.OS === 'android' && response === 'denied')) {
      response = await RNPermissions.request(permission);
    } else if (response === 'denied') {
      if (confirmation && Platform.OS === 'ios') {
        Alert.alert(
          confirmation.title,
          confirmation.message,
          [
            { text: 'No', onPress: () => { } },
            { text: 'Ok', onPress: RNPermissions.openSettings },
          ]
        );
      } else {
        RNPermissions.openSettings();
      }
    }

    return response;
  }

  async isAllowed(permission) {
    return await RNPermissions.check(permission);
  }

  openSettings() {
    RNPermissions.openSettings();
  }

}

export default new Permisisons;