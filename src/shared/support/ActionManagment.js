import { actions } from './ActionConstants';

class ActionManagement {

  getMissionCanceled() {
    return actions.MISSION_CANCELED;
  }

  getMissionFinished() {
    return actions.MISSION_FINISHED;
  }

  getMissionStarted() {
    return actions.MISSION_STARTED;
  }

  getSendInvitation() {
    return actions.SEND_INVITATION;
  }

  isMissionCanceled(action) {
    return action === actions.MISSION_CANCELED;
  }

  isMissionFinished(action) {
    return action === actions.MISSION_FINISHED;
  }

  isMissionStarted(action) {
    return action === actions.MISSION_STARTED;
  }

  isSendInvitation(action) {
    return action === actions.SEND_INVITATION;
  }

}

export default new ActionManagement;