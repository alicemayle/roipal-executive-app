import { typeCard } from './TypeCardConstants';

class TypeCardManagement {

  isVisaCard(type) {
    return typeCard.VISA === type;
  }

  isMasterCard(type) {
    return typeCard.MASTERCARD === type;
  }

  isAmericanExpressCard(type) {
    return typeCard.AMERICAN_EXPRESS === type;
  }

  isDiscoverCard(type) {
    return typeCard.DISCOVER === type;
  }
}

export default new TypeCardManagement;