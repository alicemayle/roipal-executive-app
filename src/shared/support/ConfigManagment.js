import { config } from './ConfigConstants';

class ConfigManagement {

  getGrantType() {
    return config.GRANT_TYPE;
  }

  getClientID() {
    return config.CLIENT_ID;
  }

  getClientSecret() {
    return config.CLIENT_SECRET;
  }

  getScope() {
    return config.SCOPE;
  }
}

export default new ConfigManagement;