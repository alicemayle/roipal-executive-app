import MediaMeta from 'react-native-media-meta';

class MediaMetaData {

  static async getMediaMeta(path) {
    try {
      const result = await MediaMeta.get(path)

      return result;

    } catch (error) {
        throw new Error ("No se pudo resolver la promesa de getMediaMeta. Este mensaje está hardcode")
    }
  }
}

export default MediaMetaData;