import MimeFile from "./MimeFile";
import ThumbnailsMedia from "./ThumbnailsMedia";
import { Platform } from 'react-native';

class PrepareVideo {

  static async prepareVideo(source) {
    try {
      let value = null;

        const path = Platform.OS === 'ios' ? source.uri: source.path;

        const result = await ThumbnailsMedia.getThumbnail(path);

        const extVideo = path.split('.').pop();
        const mimeType = await MimeFile.getMime(extVideo);
        
        const extThumbnail = result.path.split('.').pop();
        const mimeThumbnail =  await MimeFile.getMime(extThumbnail);

        value = {
          video : {
            ...source, 
            mime: mimeType
          },
          thumbnail: {
            path: result.path,
            mime: mimeThumbnail
          }
        }

      return value;

    } catch (error) {
      throw new Error("No se pudo resolver la promesa de PrepareVideo. Este mensaje está hardcode")
    }
  }
}

export default PrepareVideo