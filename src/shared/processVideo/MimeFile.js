import * as mime from 'react-native-mime-types';

class MimeFile {
    static async getMime(source) {
        try {
          const result = await mime.lookup(source)
    
          return result;
    
        } catch (error) {
            throw new Error ("No se pudo resolver la promesa de MimeFile. Este mensaje está hardcode")
        }
      }
}

export default MimeFile;