import RNThumbnail from 'react-native-thumbnail';

class ThumbnailsMedia {

  static async getThumbnail(path) {
    try {
      const result = await RNThumbnail.get(path);
      return result;

    } catch (error) {
      throw new Error("No se pudo resolver la promesa de ThumbnailsMedia. Este mensaje está hardcode")
    }
  }

}

export default ThumbnailsMedia;