import axios from 'axios';
import Auth from './services/Auth';
import { Logger } from 'unstated-enhancers';
import Language from './languages/Lang';
import Observer from './Observer';
import { REACT_APP_API_URL } from './support/ApiUrl';

class Http {

  constructor() {
    this.axios = axios.create({
      timeout: 60000
    });

    this.registerResponseInterceptor();
    this.registerRequestInterceptor();

    this.baseURL = 'http://localhost/';
  }

  request(config) {
    return this.axios.request(config);
  }

  get(url, config = {}) {
    return this.axios.get(url, this.getConfig(config));
  }

  delete(url, config = {}) {
    return this.axios.delete(url, this.getConfig(config));
  }

  head(url, config = {}) {
    return this.axios.head(url, this.getConfig(config));
  }

  options(url, config = {}) {
    return this.axios.options(url, this.getConfig(config));
  }

  post(url, data = {}, config = {}) {
    return this.axios.post(url, data, this.getConfig(config))
  }

  put(url, data = {}, config = {}) {
    return this.axios.put(url, data, this.getConfig(config))
  }

  patch(url, data = {}, config = {}) {
    return this.axios.patch(url, data, this.getConfig(config))
  }

  getConfig(config) {
    config = config ? { ...config } : {};

    config.baseURL = REACT_APP_API_URL

    return config;
  }

  registerRequestInterceptor() {
    this.axios.interceptors.request.use(function (config) {

      config.headers['X-Preferred-Language'] = Language.getLocale();
      config.headers['Accept-Language'] = Language.getLocale();

      const token = Auth.getAccessToken();

      if (token !== undefined & token !== null && config.url.indexOf(REACT_APP_API_URL) === -1) {
        config.headers.Authorization = 'Bearer ' + token;
      }

      return config;
    }, function (error) {
      return Promise.reject(error);
    });
  }

  registerResponseInterceptor() {
    this.axios.interceptors.response.use(function (response) {
      return Promise.resolve(response.data);
    }, function (error) {
      let response = error.response || {};

      if (response.status === 401) {
        Observer.notify('invalid_token')

        if (!response.data) {
          let message = error.status ? (error.status + ' ' + error.statusText) : null;
          message = message || error.message;

          response.data = {
            message: message || 'No se tiene información del error'
          }
        }
      }

      return Promise.reject(response.data);
    });
  }
}

const http = new Http();

export default http;