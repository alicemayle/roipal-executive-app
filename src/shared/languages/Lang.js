import source from '../languages/messages';
import Lang from 'lang.js';
import autobind from 'class-autobind';

const newLang = new Lang();
newLang.setMessages(source);
newLang.setLocale('es');
newLang.setFallback('en');

autobind(newLang);

const _api = new Lang();
_api.setLocale('es');
_api.setFallback('en');

export const api = _api;

const get = newLang.get

newLang.get = (index, alt = null) => {
    translation = get(index)
    transAlt = _api.get(alt)

    if (!alt || transAlt === alt || !transAlt) {
        return translation
    }

    return transAlt
}

export default newLang;