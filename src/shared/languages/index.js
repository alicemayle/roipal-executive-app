import signin from './modules/signin'
import generic from './modules/generic'
import introduction from './modules/introduction'
import activity from './modules/activities'
import chat from './modules/chat'
import checklist from './modules/checklist'
import invitations from './modules/invitations'
import payment from './modules/payment'
import evaluation from './modules/evaluation'
import map from './modules/map'
import drawer from './modules/drawer'
import invitation_status from './modules/invitation-manager'
import permissions from './modules/permissions'
import account from './modules/account'
import profile from './modules/profile'
import thumnail from './modules/thumnail'
import modal from './modules/modal'
import mission from './modules/mission'

const dictionary = {
    ...signin,
    ...generic,
    ...introduction,
    ...activity,
    ...chat,
    ...checklist,
    ...invitations,
    ...payment,
    ...evaluation,
    ...map,
    ...drawer,
    ...invitation_status,
    ...permissions,
    ...account,
    ...profile,
    ...thumnail,
    ...modal,
    ...mission
}

export default dictionary
