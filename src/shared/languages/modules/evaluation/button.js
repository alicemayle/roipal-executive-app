const button = {
	'en': {
		"understood": "Understood",
	},
	'es': {
		"understood": "Entendido",
	},
	'fr': {
		"understood": "Compris",
	}
}

export default button
