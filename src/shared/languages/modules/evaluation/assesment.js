const assesment = {
	'en': {
    "aler_title": "Exit assessment",
    "alert_direction": "Are you sure you want to exit the assessment?",
    "evaluation": "Assessment",
	},
	'es': {
    "aler_title": "Abandonar cuestionario",
    "alert_direction": "¿Está seguro que deseas abandonar el cuestionario?",
    "evaluation": "Evaluación",
	},
	'fr': {
    "aler_title": "Quitter le questionnaire",
    "alert_direction": "Êtes-vous sûr de vouloir quitter le questionnaire?",
    "evaluation": "Test",
	}
}

export default assesment
