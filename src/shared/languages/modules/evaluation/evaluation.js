const evaluation = {
	'en': {
    "aler_title": "Exit assessment",
    "alert_direction": "Are you sure you want to exit the assessment? You will lose all data provided",
    "assessment": "Assessment",
    "behavior": "Behavior Evaluation",
    "instructions": "Drag the options and arrange them so the one at the top represents what describes you the most, and the one at the bottom, what describes you the least.",
    "helper_evaluation": "Hello, we want to get to know more about you, such as your communication skills and how you put your experience and knowledge into practice. There are no right or wrong answers, they simply help us to offer you missions that best match your profile.\n\nThis assessment will take around 10 minutes, make sure you can concentrate.\n\nIn each section of the assessment, there are four statements, each one representing a specific behavior. Click and drag each statement and arrange them in descending order, from the one that describes it best at the top, to the one that describes it the least at the bottom.\n\nEnjoy the exercise!",
    "competitions": "Sales skills assessment",
    "helper_roipal_evaluation": "Hello, if you have sales talent and you like to sell, we want to know how you would approach the following sales situations. \n\nRead each situation and rank the strategies you would carry out in order of preference.\n\nNumber one being your top choice, number two your second choice, and so on.\n\nThank you!",
    "star": "ROIPAL Star",
    "complete_first": "You have completed the first stage",
    "disc_profile": "Your behavior profile is"
	},
	'es': {
    "aler_title": "Abandonar evaluación",
    "alert_direction": "¿Estás seguro que quieres abandonar la evaluación? Perderás los datos proporcionados?",
    "assessment": "Evaluación",
    "behavior": "Evaluación de Comportamiento",
    "instructions": "Arrastra las opciones y ordena de forma que la que esté arriba represente lo que más te describa y abajo la que menos te describa.",
    "helper_evaluation": "Hola, nos interesa conocer tu estilo de comportamiento del que deriva tu estilo de comunicación y cómo pones en práctica tu experiencia y conocimientos. No hay resultados buenos ni malos y nos ayudas a ofrecerte misiones relacionadas con tu perfil.\n\nTe pedimos 10 minutos de concentración. En cada sección hay 4 enunciados, cada uno representa un comportamiento específico. Haciendo clic y arrastrando cada enunciado, muévelos verticalmente y ordénalos de tal forma que lo que más te describa, quede hasta arriba a lo que menos te describa, quede hasta abajo.\n\n¡Gracias y disfruta el ejercicio!",
    "competitions": "Evaluación de competencias en ventas",
    "helper_roipal_evaluation": "Hola, si tienes talento en ventas y te gusta vender, sabrás como contestar de manera estratégica frente a las diferentes situaciones que te presentaremos.\n\nLee cada situación de ventas y pondera las estrategias que llevarías a cabo en cada una de las situaciones.\n\nEl número uno será tu mejor elección de acción, el número dos tu segunda elección y así sucesivamente.\n\n¡Gracias!",
    "star": "Estrella ROIPAL",
    "complete_first": "Has completado la primera etapa",
    "disc_profile": "Tu perfil de comportamiento es"
	},
	'fr': {
    "aler_title": "Quitter le test",
    "alert_direction": "Tu es sûr de vouloir abandonner l'évaluation ? Vous perdrez les données fournies",
    "assessment": "Évaluation",
    "behavior": "Évaluation du Comportement",
    "instructions": "Faites glisser les options et triez-les de façon à ce que celle ci-dessus représente ce qui vous décrit le mieux et celle qui vous décrit le moins..",
    "helper_evaluation": "Bonjour, nous nous intéressons à votre style de comportement dont découle votre style de communication et à la façon dont vous mettez en pratique votre expérience et vos connaissances. Il n'y a pas de bons ou mauvais résultats et vous nous aidez à vous proposer des missions liées à votre profil.\n\n10 minutes de concentration sont nécessaires. Dans chaque section, il y a 4 énoncés, chacun représentant un comportement spécifique. En cliquant et en faisant glisser chaque énoncé, déplacez-les verticalement et arrangez-les de manière à ce que ce qui vous décrit le mieux, soit ce qui vous décrit le moins, soit vers le bas.\n\nMerci et bon exercice!",
    "competitions": "Évaluation des compétences en ventes",
    "helper_roipal_evaluation": "Bonjour, si vous avez du talent dans la vente et que vous aimez vendre, vous saurez répondre stratégiquement face aux différentes situations que nous vous présenterons.\n\nLisez chaque situation de vente et pesez les stratégies que vous mettriez en œuvre dans chacune des situations.\n\nLe numéro un sera votre meilleur choix d'action, le numéro deux votre deuxième choix et ainsi de suite.\n\nMerci!",
    "star": "Étoile ROIPAL",
    "complete_first": "Vous avez terminé la première étape",
    "disc_profile": "Votre profil de comportement est"
	}
}

export default evaluation
