import button from './button'
import evaluation from './evaluation'
import assesment from './assesment'

const evaluation = {
	'en.evaluation': {
    ...evaluation.en,
    'button': button.en,
    'assesment': assesment.en
	},
	'es.evaluation': {
    ...evaluation.es,
    'button': button.es,
    'assesment': assesment.es
	},
	'fr.evaluation': {
    ...evaluation.fr,
    'button': button.fr,
    'assesment': assesment.fr
	}
}

export default evaluation
