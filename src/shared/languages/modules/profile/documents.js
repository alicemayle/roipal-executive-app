const documents = {
	'en': {
    "invalid_file": "Invalid file",
    "title": "Documents",
    "description": "We need to validate your data. Please take a photograph or upload a clear image of your documents.",
    "title_message": "Document validation",
    "message": "Your documents are being reviewed. They will be reviewed within 48 hours, once validated, you will be notified. Continue with your registration.",
    "required": "Required images are missing.",
	},
	'es': {
    "invalid_file": "El archivo de no es válido",
    "title": "Documentos",
    "description": "Necesitamos validar tus datos, por favor toma una fotografía de tus documentos o sube una imagen en donde su información sea legible.",
    "title_message": "Validación de Documentos",
    "message": "Tus documentos están en proceso de revisión. Serán revisados en un lapso de 48 hrs, una vez validados, se te notificará. Continúa con tu registro.",
    "required": "Faltan imágenes requeridas.",
	},
	'fr': {
    "invalid_file": "Fichier non valide.",
    "title": "Documents",
    "description": "ous devons valider vos données. Veuillez prendre une photo de vos documents ou télécharger une image où vos informations sont bien lisibles.",
    "title_message": "Validation des documents",
    "message": "Vos documents sont en cours de révision. Ils seront examinés dans les 48 heures. Une fois validé, vous en serez informé. Continuez votre inscription.",
    "required": "Les images requises sont manquantes.",
	}
}

export default documents
