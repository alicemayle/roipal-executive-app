import button from './button'
import profile from './profile'
import documents from './documents'

const profile = {
	'en.profile': {
    ...profile.en,
    'button': button.en,
    'documents': documents.en
	},
	'es.profile': {
    ...profile.es,
    'button': button.es,
    'documents': documents.es
	},
	'fr.profile': {
    ...profile.fr,
    'button': button.fr,
    'documents': documents.fr
	}
}

export default profile
