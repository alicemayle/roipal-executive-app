import status from './status'

const status = {
	'en.status': {
    ...status.en,
	},
	'es.status': {
    ...status.es,
	},
	'fr.status': {
    ...status.fr,
	}
}

export default status
