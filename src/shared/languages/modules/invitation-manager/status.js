const status = {
	'en': {
    "status_accept": "Accepted",
    "status_reject": "Rejected",
    "status_hold": "On hold",
    "status_start": "Initiated",
    "status_finish": "Completed",
    "status_cancel": "Canceled",
    "status_canceled": "Rejected",
	},
	'es': {
    "status_accept": "Aceptada",
    "status_reject": "Rechazada",
    "status_hold": "En espera",
    "status_start": "Iniciada",
    "status_finish": "Completada",
    "status_cancel": "Cancelada",
    "status_canceled": "Rechazado",
	},
	'fr': {
    "status_accept": "Accepté",
    "status_reject": "Refusé",
    "status_hold": "En attente",
    "status_start": "Initié",
    "status_finish": "Complété",
    "status_cancel": "Annulée",
    "status_canceled": "Rejetée",
	}
}

export default status
