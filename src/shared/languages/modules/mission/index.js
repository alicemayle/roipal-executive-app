import mission from './mission'
import home from './home'

const mission = {
	'en.mission': {
    ...mission.en,
    'home': home.en,
	},
	'es.mission': {
    ...mission.es,
    'home': home.es,
	},
	'fr.mission': {
    ...mission.fr,
    'home': home.fr,
	}
}

export default mission
