const mission = {
	'en': {
    "welcome": "Welcome",
    "question": "Are you ready to find your ideal activity?",
    "cancel": "Cancel mission",
    "cancel_question": "Are you sure you want to cancel the mission?",
    "finish_mission": "End mission",
    "finish_action": "Are you sure you want to end the mission?",
    "skip": "Skip",
    "confirm": "Confirm",
    "mission_details": "Mission details",
    "activities": "Activities",
	},
	'es': {
    "welcome": "Bienvenido",
    "question": "No hay misiones que mostrar",
    "cancel": "Cancelar misión",
    "cancel_question": "¿Estás seguro que deseas cancelar la misión?",
    "finish_mission": "Finalizar misión",
    "finish_action": "¿Estás seguro que deseas terminar la misión?",
    "skip": "Omitir",
    "confirm": "Confirmar",
    "mission_details": "Detalles de la misión",
    "activities": "Actividades",
	},
	'fr': {
    "welcome": "Bienvenue",
    "question": "Aucune mission à afficher",
    "cancel": "Annuler la mission",
    "cancel_question": "Êtes-vous sûr de vouloir annuler la mission?",
    "finish_mission": "Fin de mission",
    "finish_action": "Êtes-vous sûr de vouloir terminer la mission?",
    "skip": "Omettre",
    "confirm": "Confirmer",
    "mission_details": "Détails de la mission",
    "activities": "Activités",
  }
}
export default mission
