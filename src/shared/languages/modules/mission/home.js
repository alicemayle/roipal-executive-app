const home = {
	'en': {
    "alert_payment": "Notification",
    "message_alert": "You must register a payment method to access your invitations. Do you want to register now?",
    "cancel": "Cancel",
    "confirm": "Confirm",
    "missions": "Mission",
    "new_invitation": "Pending invitations",
    "search_missions": "Search missions",
    "without_payment": "We do not have your payment method registered",
    "register_payment": "Do you want to register now?",
	},
	'es': {
    "alert_payment": "Notificación",
    "message_alert": "Debes registrar un método de pago para acceder a tus invitaciones ¿Deseas registrarlo ahora?",
    "cancel": "Cancelar",
    "confirm": "Confirmar",
    "missions": "Misión",
    "new_invitation": "Invitaciones pendientes",
    "search_missions": "Buscar misiones",
    "without_payment": "No tenemos registrado tu método de pago",
    "register_payment": "¿Quieres registrarlo ahora?",
	},
	'fr': {
    "alert_payment": "Notification",
    "message_alert": "Vous devez enregistrer un mode de paiement pour avoir accès à vos invitations. Voulez-vous vous inscrire maintenant?",
    "cancel": "Annuler",
    "confirm": "Confirmer",
    "missions": "Mission",
    "new_invitation": "Invitations en attente",
    "search_missions": "Rechercher des missions",
    "without_payment": "Nous n'avons pas enregistré votre mode de paiement",
    "register_payment": "Voulez-vous l'enregistrer maintenant?",
	}
}

export default home
