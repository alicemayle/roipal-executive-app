import thumnail from './thumnail'

const thumnail = {
	'en.thumnail': {
    ...thumnail.en,
	},
	'es.thumnail': {
    ...thumnail.es,
	},
	'fr.thumnail': {
    ...thumnail.fr,
	}
}

export default thumnail
