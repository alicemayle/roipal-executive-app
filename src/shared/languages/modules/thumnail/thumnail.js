const thumnail = {
  'en': {
    "loading": "Loading...",
    "processing": "Processing...",
    "video": "Your video",
    "play_video": "Play video",
  },
  'es': {
    "loading": "Cargando...",
    "processing": "Procesando...",
    "video": "Tu video",
    "play_video": "Reproducir video",
  },
  'fr': {
    "loading": "Chargement...",
    "processing": "Traitement...",
    "video": "Votre vidéo",
    "play_video": "Lire la vidéo",
  }
}

export default thumnail
