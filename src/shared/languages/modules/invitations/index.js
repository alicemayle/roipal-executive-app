import invitations from './edit-account'
import details from './details'

const invitations = {
	'en.invitations': {
    ...invitations.en,
    'details': details.en
	},
	'es.invitations': {
    ...invitations.es,
    'details': details.es
	},
	'fr.invitations': {
    ...invitations.fr,
    'details': details.fr
	}
}

export default invitations
