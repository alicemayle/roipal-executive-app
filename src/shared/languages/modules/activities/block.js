const block = {
	'en': {
    "disabled_gps": "Disabled GPS",
    "required_location": "In order to access all of ROIPAL functionalities, you need to activate your GPS.",
    "intro_settings": "You can access device settings from the Settings button.",
    "settings": "Settings",
	},
	'es': {
    "disabled_gps": "GPS desactivado",
    "required_location": "Necesitas activar tu GPS para que la aplicación te permita acceder a todas sus funcionalidades",
    "intro_settings": "Puede acceder a los ajustes del dispositivo desde el botón Ajustes.",
    "settings": "Ajustes",
	},
	'fr': {
    "disabled_gps": "GPS désactivé",
    "required_location": "Vous devez activer votre GPS pour que l'application vous permette d'accéder à toutes ses fonctionnalités.",
    "intro_settings": "Vous pouvez accéder aux paramètres de l'appareil à l'aide du bouton Paramètres.",
    "settings": "Réglages",
	}
}

export default block
