const record = {
	'en': {
    "comments": "Comments",
    "record_message": "Are you sure you want to leave the activity history record?",
    "evidence": "Add evidence of your activity if you wish.",
    "image": "Provide a photo that documents your activity.",
	},
	'es': {
    "comments": "Comentarios",
    "record_message": "¿Está seguro que desea abandonar el registro de historial de actividad?",
    "evidence": "Agrega evidencias de tu actividad si lo deseas.",
    "image": "Proporciona una fotografía que documente tu actividad",
	},
	'fr': {
    "comments": "Commentaires",
    "record_message": "Êtes-vous sûr de vouloir quitter l'historique des activités?",
    "evidence": "Ajoutez des preuves de votre activité si vous le souhaitez.",
    "image": "Fournir une photo qui documente votre activité",
	}
}

export default record
