import activity from './activities.js'
import record from './record.js'
import block from './block.js'

const activity = {
	'en.activity': {
		'activity': activity.en,
		'record': record.en,
		'block': block.en
	},
	'es.activity': {
	  'activity': activity.es,
		'record': record.es,
		'block': block.es
	},
	'fr.activity': {
		'activity': activity.fr,
		'record': record.fr,
		'block': block.fr
	}
}

export default activity
