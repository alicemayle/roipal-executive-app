import button from './button'
import chat from './edit-account'

const chat = {
	'en.chat': {
    ...chat.en,
	},
	'es.chat': {
    ...chat.es,
	},
	'fr.chat': {
    ...chat.fr,
	}
}

export default chat
