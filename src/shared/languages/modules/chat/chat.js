const chat = {
	'en': {
    "disconnected": "Disconnected",
    "connected": "Connected",
    "writing": "Writing...",
    "send": "Send",
    "message": "Write a message",
	},
	'es': {
    "disconnected": "Desconectado",
    "connected": "Conectado",
    "writing": "Escribiendo...",
    "send": "Enviar",
    "message": "Escribe un mensaje",
	},
	'fr': {
    "disconnected": "Déconnecté",
    "connected": "Connecté",
    "writing": "En train d'écrire...",
    "send": "Envoyer",
    "message": "Écrire un message",
	}
}

export default chat
