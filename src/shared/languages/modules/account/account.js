const account = {
  'en': {
    "image_picker": "Select a image",
    "take_image": "Take a photo",
    "choose_library": "Choose from library",
    "info_account": "ee",
    "update": "Update",
    "my_account": "My account",
    "documents": "Documents",
    "notifications": "Notifications",
    "card": "Payment method",
    "language": "Language",
    "chose_language": "Change the language",
    "disable": "Disable account",
    "enable": "Enable Account",
    "dont_disable.title": "Information",
    "dont_disable.message": "It's not possible to disable your account with initiated missions",
    "option.personal_profile": "Personal profile",
    "option.password": "Password",
    "option.payment": "Bank account",
  },
  'es': {
    "image_picker": "Selector de imagen",
    "take_image": "Tomar una foto",
    "choose_library": "Elegir de la galería",
    "info_account": "Tu cuenta esta activa",
    "update": "Actualizar",
    "my_account": "Mi Cuenta",
    "documents": "Documentos",
    "notifications": "Notificaciones",
    "card": "Método de pago",
    "language": "Idioma",
    "chose_language": "Cambiar el idioma",
    "disable": "Deshabilitar cuenta",
    "enable": "Habilitar Cuenta",
    "dont_disable.title": "Información",
    "dont_disable.message": "No es posible deshabilitar tu cuenta con misiones iniciadas",
    "option.personal_profile": "Perfil personal",
    "option.password": "Contraseña",
    "option.payment": "Cuenta bancaria",
  },
  'fr': {
    "image_picker": "Sélecteur d'images",
    "take_image": "Prenez une photo",
    "choose_library": "Choisissez parmi la bibliothèque",
    "info_account": "Votre compte est actif",
    "update": "Mise à jour",
    "my_account": "Mon compte",
    "documents": "Documents",
    "notifications": "Les notifications",
    "card": "Moyens de paiement",
    "language": "Langage",
    "chose_language": "Changer la langue",
    "disable": "Désactiver le compte",
    "enable": "Activer le compte",
    "dont_disable.title": "Information",
    "dont_disable.message": "Il n'est pas possible de désactiver votre compte avec des missions initiées",
    "option.personal_profile": "Profil personnel",
    "option.password": "Mot de passe",
    "option.payment": "Compte bancaire",
  }
}

export default account
