import account from './account'
import profile from './profile'
import pass from './password'
import payment from './payment'
import disable from './disable'

const account = {
	'en.intro': {
    ...account.en,
    'profile': profile.en,
    'password': pass.en,
    'payment': payment.en,
    'disable': disable.en
	},
	'es.intro': {
    ...account.es,
    'profile': profile.es,
    'password': pass.es,
    'payment': payment.es,
    'disable': disable.es
	},
	'fr.intro': {
    ...account.fr,
    'profile': profile.fr,
    'password': pass.fr,
    'payment': payment.fr,
    'disable': disable.fr
	}
}

export default account
