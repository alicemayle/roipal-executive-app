const password = {
	'en': {
		"current_pass":"Current password",
		"new_pass": "New password",
		"confirm_pass": "Confirm Password",
		"change_password": "Change of password",
		"password": "Password",
		"new_password": "New Password",
		"confirm_password": "Confirm Password",
	},
	'es': {
		"current_pass":"Contraseña actual",
		"new_pass": "Nueva contraseña",
		"confirm_pass": "Confirmar contraseña",
		"change_password": "Cambio de contraseña",
		"password": "Contraseña",
		"new_password": "Nueva Contraseña",
		"confirm_password": "Confirmar Contraseña",
	},
	'fr': {
		"current_pass":"Mot de passe actuel",
		"new_pass": "Nouveau mot de passe",
		"confirm_pass": "Confirmer mot de passe",
		"change_password": "Changement du mot de passe",
		"password": "Mot de passe",
		"new_password": "Nouveau mot de passe",
		"confirm_password": "Confirmer mot de passe",
	}
}

export default password
