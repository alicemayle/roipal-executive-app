const payment = {
	'en': {
    "title": "Bank account",
    "tdc": "Card",
    "paypal": "Paypal",
    "transfer": "Bank account",
    "info": "Remember you can only have one registered payment account. You can add a new payment account to replace the existing one.",
    "update": "Update",
    "wallet": "Wallet",
    "balance": "Positive balance",
    "wallet_info": "If you wish to withdraw your balance in favor you must have a registered bank account",
    "register_account": "Register account",
    "details_balance": "Breakdown of the balance in favor",
    "details": "Details"
	},
	'es': {
    "title": "Cuenta bancaria",
    "tdc": "Tarjeta",
    "paypal": "Paypal",
    "transfer": "Cuenta bancaria",
    "info": "Recuerda que sólo puedes tener un método de pago registrado. Puedes agregar uno nuevo que sustituya al existente.",
    "update": "Actualizar",
    "wallet": "Billetera",
    "balance": "Saldo a favor",
    "wallet_info": "Si deseas retirar tu saldo a favor debes tener una cuenta bancaria registrada",
    "register_account": "Registrar cuenta",
    "details_balance": "Desglose del saldo a favor",
    "details": "Detalles"
	},
	'fr': {
    "title": "Compte bancaire",
    "tdc": "Carte",
    "paypal": "Paypal",
    "transfer": "Compte bancaire",
    "info": "Rappelez-vous que vous ne pouvez avoir qu'un seul mode de paiement enregistré. Vous pouvez en ajouter un nouveau pour remplacer le précédent.",
    "update": "Mise à jour",
    "wallet": "Portefeuille",
    "balance": "Solde créditeur",
    "wallet_info": "Si vous souhaitez retirer votre solde en faveur, vous devez avoir un compte bancaire enregistré",
    "register_account": "Enregistrer un compte",
    "details_balance": "Répartition du solde en faveur",
    "details": "Détails"
	}
},

export default payment
