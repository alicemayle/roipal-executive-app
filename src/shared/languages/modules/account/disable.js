const disable = {
	'en': {
		"disable": "Disable account",
		"option": "Temporarily disable",
		"message_one": "Disabling the account will cancel all previously accepted missions",
    "message_two": "This action may affect your overall ROIPAL rating",
		"message_three": "You will then be able to re-enable your account",
		"erase": "Delete definitively",
		"message": "When you delete your account, all the records you have with ROIPAL will definitely disappear",
		"disable_confirmation": "Are you sure you want to temporarily disable your account?",
		"erase_confirmation": "Are you sure you want to permanently delete your account?",
	},
	'es': {
		"disable": "Deshabilitar cuenta",
		"option": "Deshabilitar temporalmente",
		"message_one": "Al deshabilitar la cuenta se cancelarán todas las misiones que hayan sido aceptadas previamente",
    "message_two": "Esta acción puede afectar tu calificación general en ROIPAL",
		"message_three": "Posteriormente podrás habilitar nuevamente tu cuenta",
		"erase": "Eliminar definitivamente",
		"message":"Al eliminar tu cuenta definitivamente desaparecerán todos los registros que tienes con ROIPAL",
		"disable_confirmation": "¿Estás seguro que deseas deshabilitar temporalmente tu cuenta?",
		"erase_confirmation": "¿Estás seguro que deseas eliminar definitivamente tu cuenta?",
	},
	'fr': {
		"disable": "Désactiver le compte",
		"option": "Désactiver temporairement",
		"message_one": "La désactivation du compte annulera toutes les missions précédemment acceptées",
    "message_two": "Cette mesure peut avoir une incidence sur votre cote globale de ROIPAL",
		"message_three": "Vous pourrez alors réactiver votre compte",
		"erase": "Supprimer définitivement",
		"message": "Lorsque vous supprimez votre compte, tous les enregistrements que vous avez avec ROIPAL disparaîtront définitivement",
		"disable_confirmation": "Êtes-vous sûr de vouloir désactiver temporairement votre compte?",
		"erase_confirmation": "Êtes-vous sûr de vouloir supprimer définitivement votre compte?",
	}
}

export default disable
