const button = {
	'en': {
    "forget_password": "Did you forget your password?",
    "signin": "Sign In",
    "register": "Register",
    "create_account": "Create account",
    "recover_password": "Recover password",
	},
	'es': {
    "forget_password": "¿Olvidaste tu contraseña?",
    "signin": "Entrar",
    "register": "Registrarse",
    "create_account": "Crear cuenta",
    "recover_password": "Recuperar contraseña",
	},
	'fr': {
    "forget_password": "Vous avez oublié votre mot de passe?",
    "signin": "Se connecter",
    "register": "S'inscrire",
    "create_account": "Créer un compte",
    "recover_password": "Récupérez votre mot de passe",
	}
}

export default button
