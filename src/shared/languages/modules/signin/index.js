import signin from './signin'
import button from './button'
import register from './register'

const sig = {
	'en.signin': {
		...signin.en,
		'register': register.en,
		'button': button.en
	},
	'es.signin': {
		...signin.es,
		'register': register.es,
		'button': button.es
	},
	'fr.signin': {
		...signin.fr,
		'register': register.fr,
		'button': button.fr
	}
}

export default sig
