const signin = {
	'en': {
		"email": "Email",
		"password": "Password",
		"reset_password": "Reset password",
		"info_password": "Enter the link that we will send to your email account to recover your password.",
		"send_email": "The email has been sent",
		"verification": "Verify account",
		"verify": "Enter the link we have sent to your email account to verify your account.",
		"resend": "If you have received an email with the confirmation link, click",
		"here": " here",
	},
	'es': {
		"email": "Correo electrónico",
		"password": "Contraseña",
		"reset_password": "Restablecer contraseña",
		"info_password": "Ingrese al link que hemos enviado a su e-mail para recuperar su cuenta.",
		"send_email": "El correo ha sido enviado",
		"verification": "Verificar cuenta",
		"verify": "Ingrese al link que hemos enviado a su e-mail para verificar su cuenta.",
		"resend": "Si no has recibido un correo electrónico con el enlace de confirmación, has clic",
		"here": " aquí",
	},
	'fr': {
		"email": "Courrier",
		"password": "Mot de passe",
		"reset_password": "Réinitialiser le mot de passe",
		"info_password": "Entrez dans le lien que nous enverrons à votre coorriel, pour récupérer votre mot de passe.",
		"send_email": "Le courriel a été envoyé",
		"verification": "Vérifier le compte",
		"verify": "Entrez dans le lien que nous avons envoyé à votre courriel pour vérifier votre compte.",
		"resend": "Si vous n'avez pas reçu de courriel avec le lien de confirmation, cliquez sur",
		"here": " ici",
	}
}

export default signin
