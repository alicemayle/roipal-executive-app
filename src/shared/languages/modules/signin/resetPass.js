const reset = {
	'en': {
    "reset_password": "Reset password",
    "email": "Email",
    "reset_password": "Enter the link that we will send to your email account to recover your password."
	},
	'es': {
    "reset_password": "Restablecer contraseña",
    "email": "Correo electrónico",
    "reset_password": "Ingrese al link que hemos enviado a su e-mail para recuperar su cuenta."
	},
	'fr': {
    "reset_password": "Réinitialiser le mot de passe",
    "email": "Courrier",
    "reset_password": "Entrez dans le lien que nous enverrons à votre coorriel, pour récupérer votre mot de passe."
	}
}

export default reset
