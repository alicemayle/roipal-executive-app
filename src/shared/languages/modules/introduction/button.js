const button = {
	'en': {
    "permit": "Permit",
	},
	'es': {
    "permit": "Permiso",
	},
	'fr': {
    "permit": "Permis",
	}
}

export default button
