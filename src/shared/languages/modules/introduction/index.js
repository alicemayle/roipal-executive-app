import button from './button'
import introduction from './introduction'

const introduction = {
	'en.intro': {
		...introduction.en,
    'button': button.en
	},
	'es.intro': {
		...introduction.es,
    'button': button.es
	},
	'fr.intro': {
		...introduction.fr,
    'button': button.fr
	}
}

export default introduction
