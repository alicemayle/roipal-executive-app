const introduction = {
	'en': {
    "introduction": "Find near you, during the days and hours you want, the best way to express your talent, by promoting or selling hundreds of different services or products! By successfully passing our filter, you will be contacted directly by the companies that need you, wherever you are!",
    "notifications": "Notifications must be enabled for us to send you invitations for available missions",
    "location": "We need to access your location so we can send you invitations nearest to your home. This permission is required to access all the functionalities of the application",
    "picture": "Do you allow ROIPAL to take pictures and record videos with your device? This will allow you to capture and share images, such as your profile picture, and videos documenting your missions",
    "galery": "Do you allow ROIPAL to access your photos and multimedia content? This will allow you to share your images, such as your profile picture, and videos documenting the development of your missions",
	},
	'es': {
    "introduction": "Hola. Encuentra cerca de ti, durante los días y las horas que deseas, la mejor manera de expresar tu talento, al promover o vender cientos de servicios o productos diferentes. Al pasar exitosamente nuestro filtro, serás contactado directamente por las empresas que necesitan tu servicio, en donde estés",
    "notifications": "Necesitamos acceder a las notificaciones para poder enviarte invitaciones de las misiones en las que puede participar",
    "location": "Necesitamos acceder a tu ubicación para hacerte llegar las invitaciones más cercanas a tu domicilio. Este permiso es requerido para tener acceso a todas las funcionalidades de la aplicación",
    "picture": "¿Permites que ROIPAL tome fotografías y grabe videos con tu dispositivo? Esto permitirá que puedas compartirnos tu foto de perfil y videos que documenten el desarrollo de la actividad",
    "galery": "¿Permites que ROIPAL acceda a tus fotos y contenido multimedia? Esto permitirá que desde tus archivos nos puedas compartir tu foto de perfil, entre otras cosas",
	},
	'fr': {
    "introduction": "Trouvez près de chez vous, pendant les jours et les heures que vous voulez, la meilleure façon d'exprimer votre talent, en faisant la promotion ou en vendant des centaines de services ou de produits différents. En passant avec succès notre filtre, vous serez contacté directement par les entreprises qui ont besoin de votre service, où que vous soyez.",
    "notifications": "Nous avons besoin d'avoir accès aux notifications pour pouvoir vous envoyer des invitations aux missions auxquelles vous pouvez participer.",
    "location": "Nous avons besoin d'accéder à votre emplacement, vous recevrez les invitations les plus proches de chez vous. Cette autorisation est nécessaire pour avoir accès à toutes les fonctionnalités de l'application.",
    "picture": "Autorisez-vous ROIPAL à prendre des photos et à enregistrer des vidéos ? Pour que vous puissiez nous fournir votre photo de profil, votre vidéographie, des preuves du développement de vos activités, entre autres fonctionnalités",
    "galery": "Permettez-vous à ROIPAL d'accéder à des photos, du contenu multimédia ? De cette façon, vous pourrez nous fournir votre photo de profil, entre autres, à travers vos fichiers",
	}
}

export default introduction
