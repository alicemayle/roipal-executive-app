const terms = {
	'en': {
    "title": "Accept terms and conditions",
    "promissory_note": "Terms and Conditions",
    "privacy": "By clicking save, you agree to ROIPAL's Terms of Use and Privacy Policy.",
	},
	'es': {
    "title": "Aceptar términos y condiciones",
    "promissory_note": "Términos y Condiciones",
    "privacy": "Al hacer clic en guardar, aceptas las Condiciones de uso y las Políticas de privacidad de ROIPAL.",
	},
	'fr': {
    "title": "Accepter les termes et conditions",
    "promissory_note": "Conditions d'utilisation",
    "privacy": "En cliquant sur Sauvegarder, vous acceptez les Conditions d'utilisation et la Politique de confidentialité de ROIPAL.",
	}
}

export default terms
