import payment from './payment'
import terms from './terms'

const payment = {
	'en.payment': {
    ...payment.en,
    'terms': terms.en
	},
	'es.payment': {
    ...payment.es,
    'terms': terms.es
	},
	'fr.payment': {
    ...payment.fr,
    'terms': terms.fr
	}
}

export default payment
