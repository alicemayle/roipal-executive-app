const map = {
	'en': {
    "title": "Location",
    "location_not_found": "Unable to determine your current location",
    "confirm_location": "Confirm location",
    "placeholder": "Enter an address here...",
	},
	'es': {
    "title": "Ubicación",
    "location_not_found": "No se puede determinar su ubicación actual",
    "confirm_location": "Confirmar ubicación",
    "placeholder": "Escriba una dirección aquí...",
	},
	'fr': {
    "title": "Lieu",
    "location_not_found": "Impossible de déterminer votre position actuelle",
    "confirm_location": "confirmer l'emplacement",
    "placeholder": "Entrez une adresse ici...",
	}
}

export default map
