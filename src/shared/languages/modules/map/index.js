import button from './button'
import map from './map'

const map = {
	'en.map': {
    ...map.en,
	},
	'es.map': {
    ...map.es,
	},
	'fr.map': {
    ...map.fr,
	}
}

export default map
