import generic from './generic.js'
import button from './button.js'
import languaje from './languaje.js'

const gen = {
	'en.generic': {
		...generic.en,
		'button': button.en,
		'languaje': languaje.en
	},
	'es.generic': {
		...generic.es,
		'button': button.es,
		'languaje': languaje.es
	},
	'fr.generic': {
		...generic.fr,
		'button': button.fr,
		'languaje': languaje.fr
	}
}

export default gen
