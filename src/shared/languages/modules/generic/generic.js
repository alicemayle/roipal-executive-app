const generic = {
	'en': {
    "data_required": "Required fields are missing",
    "try_again": "An error has occurred. Please try again",
    "alert_title": "Exit recording",
    "alert_message": "Are you sure you want to exit your recording?",
    "title_alert": "Confirmation",
    "message_alert": "Exit and lose changes?",
    "no_elements": "There are no items to show",
    "accept": "Accept",
    "cancel": "Cancel",
    "confirm": "Confirm",
	},
	'es': {
    "data_required": "Faltan datos requeridos",
    "try_again": "Ha ocurrido un error. Por favor vuelva a intentarlo",
    "alert_title": "Abandonar registro",
    "alert_message": "¿Está seguro que desea abandonar tu registro?",
    "title_alert": "Confirmación",
    "message_alert": "¿Salir y perder cambios?",
    "no_elements": "No hay elementos que mostrar",
    "accept": "Aceptar",
    "cancel": "Cancelar",
    "confirm": "Confirmar",
	},
	'fr': {
    "data_required": "Des données requises manquent",
    "try_again": "Une erreur s'est produite. Veuillez réessayer",
    "alert_title": "Quitter l'enregistrement",
    "alert_message": "Tu es sûr de vouloir abandonner ton enregistrement?",
    "title_alert": "Confirmation",
    "message_alert": "Quitter et perdre les changements?",
    "no_elements": "Il n'y a aucun élément à afficher",
    "accept": "Acceptez",
    "cancel": "Annuler",
    "confirm": "Confirmer",
	}
}

export default generic
