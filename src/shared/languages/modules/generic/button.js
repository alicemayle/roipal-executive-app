const button = {
	'en': {
		"next": "Next",
		"finish": "Finish",
		"cancel": "Cancel",
		"done": "Confirm",
		"update": "Update",
		"back": "Back",
		"search": "Search",
		"accept": "Accept",
		"reject": "Reject",
		"close": "Close",
	},
	'es': {
		"next": "Siguiente",
		"finish": "Terminar",
		"cancel": "Cancelar",
		"done": "Confirmar",
		"update": "Actualizar",
		"back": "Regresar",
		"search": "Buscar",
		"accept": "Aceptar",
		"reject": "Rechazar",
		"close": "Cerrar",
	},
	'fr': {
		"next": "Suivant",
		"finish": "Finir",
		"cancel": "Annuler",
		"done": "Confirmer",
		"update": "Mise à jour",
		"back": "Verso",
		"search": "Rechercher",
		"accept": "Acceptez",
		"reject": "Rejeter",
		"close": "Fermer",
	}
}

export default button
