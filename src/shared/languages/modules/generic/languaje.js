const languaje = {
	'en': {
    "english": "English",
    "spanish": "Spanish",
    "french": "French",
    "language": "Change the language"
	},
	'es': {
    "english": "Inglés",
    "spanish": "Español",
    "french": "Francés",
    "language": "Cambiar el idioma"
	},
	'fr': {
    "english": "Anglais",
    "spanish": "Espagnol",
    "french": "Français",
    "language": "Changer la langue"
	}
}

export default languaje
