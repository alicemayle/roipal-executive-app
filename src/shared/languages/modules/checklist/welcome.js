const welcome = {
	'en': {
    "message_thanks": "Thanks for trusting us. We will work to get missions based on your profile. When we have them, we will send you the invitations.",
    "message_congratulations": "¡CONGRATULATIONS!",
    "message_community": "You are already part of the \nROIPAL community.",
	},
	'es': {
    "message_thanks": "Gracias por confiar en nosotros. Trabajaremos para conseguir misiones de acuerdo a tu perfil. Cuando las tengamos, te enviaremos las invitaciones.",
    "message_congratulations": "¡FELICIDADES!",
    "message_community": "Ya formas parte de la \ncomunidad ROIPAL.",
	},
	'fr': {
    "message_thanks": "Merci de nous faire confiance. Nous nous efforcerons d'obtenir des missions en fonction de votre profil. Quand nous les aurons, nous vous enverrons les invitations.",
    "message_congratulations": "FÉLICITATIONS!",
    "message_community": "Vous faites maintenant partie de la \ncommunauté ROIPAL.",
	}
}

export default welcome
