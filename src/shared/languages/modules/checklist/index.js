import welcome from './welcome.js'
import checklist from './checklist.js'

const checklist = {
	'en.checklist': {
    ...checklist.en,
    'welcome': welcome.en,
	},
	'es.checklist': {
    ...checklist.es,
    'welcome': welcome.es,
	},
	'fr.checklist': {
    ...checklist.fr,
    'welcome': welcome.fr,
	}
}

export default checklist
