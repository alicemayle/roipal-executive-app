const checklist = {
	'en': {
    "checklist": "Registration progress",
    "logout": "Log out",
    "activation": "Activate account",
    "profile": "Complete Profile",
    "disc_assessment": "Behavior assessment",
    "roipal_assessment": "Sales skills assessment",
    "documentation": "Documentation",
    "payment": "Bank account"
	},
	'es': {
    "checklist": "Avance del registro",
    "logout": "Cierre de sesión",
    "activation": "Activar Cuenta",
    "profile": "Completar perfil",
    "disc_assessment": "Evaluación de comportamiento",
    "roipal_assessment": "Evaluación de competencias en ventas",
    "documentation": "Documentación",
    "payment": "Cuenta bancaria"
	},
	'fr': {
    "checklist": "Progression de l'inscription",
    "logout": "Déconnexion",
    "activation": "Activer le compte",
    "profile": "Completez votre profil",
    "disc_assessment": "Test de comportement",
    "roipal_assessment": "Test de compétences en vente",
    "documentation": "Documentation",
    "payment": "Compte bancaire"
	}
}

export default checklist
