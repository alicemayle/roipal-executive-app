import permissions from './permissions'

const permissions = {
	'en.permissions': {
    ...permissions.en,
	},
	'es.permissions': {
    ...permissions.es,
	},
	'fr.permissions': {
    ...permissions.fr,
	}
}

export default permissions
