const permissions = {
  'en': {
    "title": "Authorization",
    "location.message": "You have not authorized the location permissions. Open settings?",
    "notification.message": "You have not authorized the notifications permissions. Open settings?",
    "camera.message": "You have not authorized the access to your camera. Open settings?",
    "photo.message": "You have not authorized the access to your gallery. Open settings?",
  },

  'es': {
    "title": "Autorizar",
    "location.message": "No ha autorizado el acceso a su ubicación. ¿Abrir configuración?",
    "notification.message": "No ha autorizado el envío de notificaciones. ¿Abrir configuración?",
    "camera.message": "Usted no ha autorizado el acceso a su cámara. ¿Abrir ajustes?",
    "photo.message": "Usted no ha autorizado el acceso a su galería. ¿Abrir ajustes?",
  },

  'fr': {
    "title": "Autoriser",
    "location.message": "Vous n'avez pas autorisé l'accès à votre emplacement. Changer les paramêtres?",
    "notification.message": "Vous n'avez pas autorisé l'envoie de notifications. Changer les paramêtres?",
    "camera.message": "Vous n'avez pas autorisé les autorisations de la caméra. Paramètres ouverts?",
    "photo.message": "Vous n'avez pas autorisé l'accès à votre appareil galerie. Changer les paramêtres?"
  }
}

export default permissions
