import button from './button'
import drawer from './drawer'

const drawer = {
	'en.drawer': {
    ...drawer.en,
	},
	'es.drawer': {
    ...drawer.es,
	},
	'fr.drawer': {
    ...drawer.fr,
	}
}

export default drawer
