const drawer = {
  'en': {
    "home": "Missions",
    "setting": "Configuration",
    "logout": "Sign out",
    "invitations": "Invitations",
    "title_alert": "Notification",
    "message_alert": "You must register a payment method to access your invitations. Do you want to register now?",
  },
  'es': {
    "home": "Misiones",
    "setting": "Configuración",
    "logout": "Cerrar Sesión",
    "invitations": "Invitaciones",
    "title_alert": "Notificación",
    "message_alert": "Debes registrar un método de pago para acceder a tus invitaciones ¿Deseas registrarlo ahora?",
  },
  'fr': {
    "home": "Missions",
    "setting": "Paramètres",
    "logout": "Fermer la session",
    "invitations": "Invitations",
    "title_alert": "Notification",
    "message_alert": "Vous devez enregistrer un mode de paiement pour avoir accès à vos invitations. Voulez-vous vous inscrire maintenant?",
  }
}

export default drawer
