import modal from './modal'

const modal = {
	'en.modal': {
    ...modal.en,
	},
	'es.modal': {
    ...modal.es,
	},
	'fr.modal': {
    ...modal.fr,
	}
}

export default modal
