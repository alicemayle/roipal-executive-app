const modal = {
  'en': {
    "title": "select an option",
    "gallery": "Gallery",
    "camera": "Camera",
  },
  'es': {
    "title": "Selecciona una opción",
    "gallery": "Galería",
    "camera": "Cámara",
  },
  'fr': {
    "title": "sélectionnez une option",
    "gallery": "Galerie",
    "camera": "Appareil photo",
  }
}

export default modal
