import moment from 'moment';
import Lang from '../languages/Lang';
import es from 'moment/locale/es';
import fr from 'moment/locale/fr';
import en from 'moment/locale/en-SG';


class Moment {

  setLanguaje(value) {

      let moment_lang_resource = en;

      if(value=== 'es') {
        moment_lang_resource = es;
      }

      if(value=== 'fr') {
        moment_lang_resource = fr;
      }

      moment.locale(value, moment_lang_resource);
  }
}
export default new Moment ;