import qs from 'qs';
import { AsyncStorage } from "react-native";

import Http from '../Http';
import Jwt from './Jwt';

class Auth {

	constructor() {
		data = null;
	}

	attempt(credentials, params) {
		const options = {
			headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      params
    }
		return Http.post('/oauth/token', qs.stringify(credentials), options);
	}

	isAuthenticated() {
    const jwt = this.data ? this.data.token.access_token : null;

    this._isAuthenticated = jwt !== null && jwt !== undefined && !Jwt.isTokenExpired(jwt);

    return this._isAuthenticated;
  }

  getUser() {
    if (this.isAuthenticated()) {
        return this.data.profile;
    }

    return null;
  }

  getToken() {
    if (this.isAuthenticated()) {
      return this.data.token;
    }

    return null;
  }

  getAccessToken() {
    if (this.isAuthenticated()) {
      return this.data.token.access_token;
    }

    return null;
  }

  saveTokenPushNotifications(data){
    return Http.post('/api/notifications',data);
  }

  setData(data) {
    this.data = data;
  }

  getData() {
    return this.data;
  }

}

export default new Auth;