import Observer from '../Observer';
import NavigationService from '../services/NavigationService';
import { Logger, Manager } from 'unstated-enhancers';

class DocumentRejected {

  name = 'DOCUMENTATION_REJECTED'

  async handle(data) {
    const { executive_uuid } = data;

    await Manager.get('VerificationContainer').getDocumentsStatus(executive_uuid);

    const documents = Manager.get('VerificationContainer').state.documentStatus

    await Manager.get('VerificationContainer').setDocumentsStatus(documents);

    NavigationService.navigate('VerificationDocuments');
  }

  subscribe() {
    Observer.subscribe(this.name, this.handle);
  }

  unsubscribe() {
    Observer.unSubscribe(this.name, this.handle);
  }
}

export default DocumentRejected;

