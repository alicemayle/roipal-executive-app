import MissionStarted from './MissionStartedListener';
import MissionCanceled from './MissionCanceledListener';
import MissionFinished from './MissionFinishedListener';
import Invitation from './InvitationListener';
import ValidationEmail from './ValidationEmailListener';
import MessageChat from './MessageChatListener';
import Rejected from './RejectedListener';
import DocsRejected from './DocumentRejectedListener';

const MissionStartedListener = new MissionStarted();
const MissionCanceledListener = new MissionCanceled();
const MissionFinishedListener = new MissionFinished();
const InvitationListener = new Invitation();
const ValidationEmailListener = new ValidationEmail();
const MessageChatListener = new MessageChat();
const RejectedListener = new Rejected();
const DocumentRejectedListener = new DocsRejected();

export {
  MissionStartedListener,
  MissionCanceledListener,
  MissionFinishedListener,
  InvitationListener,
  ValidationEmailListener,
  MessageChatListener,
  RejectedListener,
  DocumentRejectedListener
};