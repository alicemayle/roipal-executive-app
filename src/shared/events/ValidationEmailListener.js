import Observer from '../Observer';
import NavigationService from '../services/NavigationService';
import { Logger, Manager } from 'unstated-enhancers';
import ChecklistStatusService from '../../screens/checklist/services/ChecklistStatusService';

class ValidationEmailListener {

  name = 'ACCOUNT_ACTIVATED'

  async handle(data) {

    const { profile } = data;

    const updatedCheckList = JSON.parse(profile).checklist;

    const currentCheckList = await Manager.get('ChecklistContainer').state.data;

    const mixCheckList = await ChecklistStatusService.getCurrentStatus(updatedCheckList, currentCheckList);

    await Manager.get('ChecklistContainer').setCompletionStatus(mixCheckList);

    NavigationService.navigate('AuthLoading');
  }

  subscribe() {
    Observer.subscribe(this.name, this.handle);
  }

  unsubscribe() {
    Observer.unSubscribe(this.name, this.handle);
  }
}

export default ValidationEmailListener;