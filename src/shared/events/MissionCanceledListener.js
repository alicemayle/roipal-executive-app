import Observer from '../Observer';
import NavigationService from '../services/NavigationService';
import { Logger, Manager } from 'unstated-enhancers';

class MissionCanceledListener {

  name = 'MISSION_CANCELED'

  async handle(data) {
    const { mission_uuid } = data;

    params = {
      search: '',
      limit: 25,
      page: 1,
      includes: 'company.profile,room_chat'
    }

    await Manager.get('HomeContainer').getMissions(params);

    const missions = Manager.get('HomeContainer').state.data

    const missionSelected = missions.find(item => item.uuid === mission_uuid)

    await Manager.get('HomeContainer').updateStatusMissionList(missionSelected);
    await Manager.get('HomeContainer').setMissionSelected(missionSelected);

    NavigationService.navigate('MissionDetailsScreen');
  }

  subscribe() {
    Observer.subscribe(this.name, this.handle);
  }

  unsubscribe() {
    Observer.unSubscribe(this.name, this.handle);
  }
}

export default MissionCanceledListener;

