import Observer from '../Observer';
import NavigationService from '../services/NavigationService';
import { Logger, Manager } from 'unstated-enhancers';

class MessageChat {

  name = 'CHAT_MESSAGE'

  async handle(data) {

    const { mission } = data;
    const mission_info = JSON.parse(mission);
    const { company } = data;
    const company_info = JSON.parse(company);

    const missionSelected = {
      ...mission_info,
      company: {
        ...company_info
      }
    }

    await Manager.get('HomeContainer').setMissionSelected(missionSelected);

    NavigationService.navigate('ChatScreen');
  }

  subscribe() {
    Observer.subscribe(this.name, this.handle);
  }

  unsubscribe() {
    Observer.unSubscribe(this.name, this.handle);
  }
}

export default MessageChat;

