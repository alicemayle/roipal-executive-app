import Observer from '../Observer';
import NavigationService from '../services/NavigationService';
import { Logger, Manager } from 'unstated-enhancers';
import { Alert } from 'react-native';
import lang from '../languages/Lang';

class InvitationListener {

  name = 'SEND_INVITATION'

  async handle(data) {
    const { invitation_uuid, executive_profile } = data;

    const executive_info = JSON.parse(executive_profile)

    const params = {
      'includes': 'company.profile,location,mission'
    }

    await Manager.get('AuthContainer').setProfile(executive_info);
    await Manager.get('InvitationAcceptanceContainer').get(invitation_uuid, params);

    if (executive_info.registered_payment === 0) {

      Alert.alert(
        lang.get('payment.titleAlert', 'invitations.title_alert'),
        lang.get('payment.messageAlert', 'invitations.message_alert'),
        [
          {
            text: lang.get('activity.cancel', 'generic.cancel'),
          },
          {
            text: lang.get('activity.confirm', 'generic.confirm'),
            onPress: () => {
              NavigationService.navigate('RegistrationPaymentScreen'/* , {goBack: 'HomeScreen'} */)
            }
          },
        ],
        {
          cancelable: false
        }
      )
    } else {
      NavigationService.navigate('InvitationDetailScreen');
    }
  }

  subscribe() {
    Observer.subscribe(this.name, this.handle);
  }

  unsubscribe() {
    Observer.unSubscribe(this.name, this.handle);
  }
}

export default InvitationListener;

