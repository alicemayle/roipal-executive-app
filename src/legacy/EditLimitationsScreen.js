import React, { Component } from 'react';
import { Text, Container, Button, Content, Body, ListItem, CheckBox, List, Toast, Spinner } from 'native-base';
import { View, Image } from 'react-native';
import Validator from 'validatorjs';

import styles from '../theme/globalStyles';
import NavBarComponent from '../components/navBar/NavBarComponent';
import lang from '../shared/languages/Lang';
import { connect, Logger } from 'unstated-enhancers';
import language from '../components/language/containers/LanguagesContainer';
import infoProfileContainer from '../screens/account/containers/EditAccountContainer';
import autobind from 'class-autobind';

class EditLimitationsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      validation: new Validator(data, this.rules)
    }
    this.inputs = {};
    autobind(this);
  }

  validate() {
    const data = { someLimitation: this.someLimitation };

    const validation = new Validator(data, this.rules);

    validation.setAttributeNames({
      limitations: lang.get('title.limitations'),
    });

    this.validation = validation;

    validation.check();

    this.setState({
      validation
    })
  }

  get rules() {
    return {
      someLimitation: 'accepted',
    }
  }

  async updateProfile() {

    const { infoProfileContainer } = this.props.containers;
    const { httpBusy } = this.props.containers.infoProfileContainer.state;

    if (httpBusy) return;

    if (this.validation.passes()) {
      const dataProfile = {
        limitations: infoProfileContainer.getSelectedLimitations(),
      }

      params = {
        includes: 'limitations'
      }

      await infoProfileContainer.updateProfile(dataProfile, null, params);

      const { error } = infoProfileContainer.state;

      if (error !== undefined) {
        this.showToast(error.message || lang.get('message.tryAgain'))
        setTimeout(() => {
          infoProfileContainer.clearError();
        }, 3000)

      } else {
        this.props.navigation.navigate('EditAccountScreen');
      }
    } else {
      this.showToast(lang.get('message.dataRequired'));

      const fields = Object.keys(this.rules);
      const values = {}

      for (const field of fields) {
        values[field] = this.state[field] || ''
      }

      this.setState({
        ...values
      })
    }
  }

  renderItem(item, index) {
    return (
      <ListItem key={index} onPress={() => this.selectLimitation(item)}>
        <CheckBox checked={item.selected}
          onPress={() => this.selectLimitation(item)} />
        <Body>
          <Text>{item.limitation}</Text>
        </Body>
      </ListItem>
    )
  }

  async selectLimitation(item) {
    const { navigation } = this.props;
    const isEditing = navigation.getParam('isEditing');

    if (!isEditing) {
      navigation.setParams({
        isEditing: true
      });
    }

    const { infoProfileContainer } = this.props.containers;

    await infoProfileContainer.selectLimitation(item);

    const { limitations } = infoProfileContainer.state;

    this.someLimitation = !limitations.every(item => item.selected === false);
    this.validate();
  }

  componentDidMount() {
    this.setNavigationGoBackConfirmationAlert();
    this.getLimitations();
  }

  showToast(text) {
    Toast.show({
      text: text,
      textStyle: { color: "white", fontSize: 12 },
      duration: 3000,
      position: 'top'
    })
  }

  async getLimitations() {
    const { infoProfileContainer } = this.props.containers;

    await infoProfileContainer.listLimitations();

    const { error } = infoProfileContainer.state;

    if (error) {
      this.showToast(error.message || lang.get('message.tryAgain'))
    } else {
      const { limitations } = infoProfileContainer.state;

      this.someLimitation = !limitations.every(item => item.selected === false);
      this.validate();
    }
  }

  setNavigationGoBackConfirmationAlert() {
    this.props.navigation.setParams({
      alert: {
        title: lang.get('info.titleAlert'),
        message: lang.get('info.messageAlert')
      }
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.limitations !== this.props.limitations ||
      nextProps.httpBusy !== this.props.httpBusy
  }

  render() {
    const { limitations, httpBusy } = this.props.containers.infoProfileContainer.state;

    Logger.count(this);

    return (
      <Container style={styles.container}>

        <NavBarComponent  {...this.props.navigation}
          name={lang.get('title.limitations')} />

        <View style={{ flexDirection: 'row', justifyContent: 'center', paddingTop: 35 }}>
          <Image style={styles.logoLimita} source={require('../../images/logo_colores/logo_colores.png')} />
        </View>

        <View style={{ paddingTop: 25, paddingBottom: 25 }}>
          <Text note style={{ textAlign: 'center', marginRight: 35, marginLeft: 35, fontSize: 17 }}>{lang.get('limitations.indications')}</Text>
        </View>

        <Content padder>
          {!!limitations && <List>
            {limitations.map(this.renderItem)}
          </List>}

          {httpBusy && !limitations && <Spinner color='lightblue' />}

          {!!limitations && <Button
            style={styles.margin25}
            block
            onPress={this.updateProfile}
          >
            <Text>{lang.get('button.update')}</Text>
          </Button>
          }

        </Content>
      </Container>
    );
  }
}

const containers = {
  language: language,
  infoProfileContainer: infoProfileContainer,
}

const mapStateToProps = (containers) => {
  return {
    limitations: containers.infoProfileContainer.state.data,
    httpBusy: containers.infoProfileContainer.state
  };
}

export default connect(containers, mapStateToProps)(EditLimitationsScreen);