import React, { Component } from 'react';
import { Text, Container, Button, Content, ListItem, Title, Thumbnail, Left, Body, Switch } from 'native-base';
import { View, Image } from 'react-native';

import styles from '../theme/globalStyles';
import NavBarComponent from '../components/navBar/NavBarComponent';
import lang from '../shared/languages/Lang';
import { connect } from 'unstated-enhancers';
import resultsEvaluation from '../screens/evaluation/containers/ResultsEvaluationContainer';
import language from '../components/language/containers/LanguagesContainer';

class ResultsEvaluationScreen extends Component {
  constructor(props) {
    super(props);
    this._bind('navigate');
  }

  _bind(...methods) {
    methods.forEach((method) => {
      this[method] = this[method].bind(this);
    });
  }

  navigate() {
    this.props.navigation.navigate('SigninScreen');
  }

  render() {
    const uri = "https://png2.kisspng.com/20180302/low/kisspng-prostatitis-prostate-euclidean-vector-icon-vector-painted-pigeon-5a99087d9fe503.2594122915199786216549.png";
    const avatar = "https://facebook.github.io/react-native/docs/assets/favicon.png";

    return (
      <Container style={styles.container}>

        <NavBarComponent
          {...this.props.navigation}
          name={lang.get('title.assessmentResult')} />

        <Content padder>
          <View style={{ alignItems: 'center', margin: 15 }} >
            <Title style={{ color: 'green', }}>Evaluación ROIPAL completada</Title>
            <Image source={{ uri: uri }} style={{ paddingLeft: 15, width: 150, height: 150, resizeMode: 'contain', margin: 15 }} />
          </View>
          <View style={{ borderWidth: 0.3, marginBottom: 15 }}></View>

          <ListItem thumbnail style={{ marginBottom: 15 }}>
            <Left>
              <Thumbnail square source={{ uri: avatar }} />
            </Left>
            <Body>
              <Text>HUGO FLORES BRAVO</Text>
            </Body>
          </ListItem>
          <View style={{ flexDirection: 'row', margin: 15, justifyContent: 'space-between', }} >
            <Text style={{ marginRight: 15 }} >Estudio socio económico</Text>
            <Switch
            />
          </View>
          <View style={{ borderWidth: 0.3, marginBottom: 15 }}></View>
          <Text style={[styles.margin25, styles.textAlignCenter]}>La empresa que realizará el estudio socioeconómico se pondrá en contacto con usted en un plazo de 24 horas.</Text>
          <Button
            style={styles.margin25}
            block
            info
            onPress={this.navigate}
          >
            <Text>{lang.get('results.results')}  </Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

export default connect({
  language, resultsEvaluation
})(ResultsEvaluationScreen);