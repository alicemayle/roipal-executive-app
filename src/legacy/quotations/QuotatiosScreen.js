import React, { Component } from 'react';
import { Container, Content } from 'native-base';
import { connect } from 'unstated-enhancers';

import styles from '../../theme/globalStyles';
import NavBarComponent from '../../components/navBar/NavBarComponent';
import Quotations from './containers/QuotationsContainer';
import lang from '../../shared/languages/Lang';
import EmptyList from '../../components/emptyList/EmptyList';

class QuotatiosScreen extends Component {
  constructor(props) {
    super(props);
    this._bind('navigate');
  }

  _bind(...methods) {
    methods.forEach((method) => {
      this[method] = this[method].bind(this);
    });
  }

  navigate() {
    this.props.navigation.navigate('HomeScreen');
  }

  render() {

    return (
      <Container style={styles.container}>

        <NavBarComponent
          {...this.props.navigation}
          name={lang.get('title.quotations')} />

        <Content padder >
          <EmptyList message={lang.get('list.noElements')}/>
        </Content>
      </Container >

    );
  }
}

export default connect({
  Quotations
})(QuotatiosScreen);