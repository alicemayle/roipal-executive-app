import React, { Component } from 'react';
import { Content, Text, ListItem, CheckBox, Body, Button } from 'native-base';

import lang from '../../shared/languages/Lang';
import styles from '../../theme/globalStyles';

class DisabilityDataComponent extends Component {
  constructor(props) {
    super(props);
    this._bind('changeCheckDisabilityMobility', 'changeCheckVisualDisability');
  }

  _bind(...methods) {
    methods.forEach((method) => {
      this[method] = this[method].bind(this);
    });
  }

  changeCheckDisabilityMobility() {
    const { registrationContainer } = this.props;

    const isChecked = !registrationContainer.state.disability.mobility;
    registrationContainer.updateStateNested('disability', 'mobility', isChecked)
  }

  changeCheckVisualDisability() {
    const { registrationContainer } = this.props;

    const isChecked = !registrationContainer.state.disability.visual;
    registrationContainer.updateStateNested('disability', 'visual', isChecked)
  }

  render() {
    const { registrationContainer } = this.props;
    const { showDisabilytyComponent, disability } = registrationContainer.state;

    return (
      showDisabilytyComponent &&
      <Content>
        <ListItem>
          <CheckBox
            checked={disability.mobility}
            color="gray"
            onPress={this.changeCheckDisabilityMobility} />
          <Body>
            <Text>{lang.get('disability.mobility')}</Text>
          </Body>
        </ListItem>
        <ListItem>
          <CheckBox
            checked={disability.visual}
            color="gray"
            onPress={this.changeCheckVisualDisability} />
          <Body>
            <Text>{lang.get('disability.visual')}</Text>
          </Body>
        </ListItem>
        <Button style={styles.margin25} block>
          <Text> {lang.get('button.next')} </Text>
        </Button>
      </Content>
    );
  }
}

export default DisabilityDataComponent;