import React, { Component } from 'react';
import { Text, Container, Button, Content, Body, ListItem, CheckBox, List, Toast, Spinner } from 'native-base';
import { View, Image } from 'react-native';

import styles from '../theme/globalStyles';
import NavBarComponent from '../components/navBar/NavBarComponent';
import lang from '../shared/languages/Lang';
import { connect, Logger } from 'unstated-enhancers';
import language from '../components/language/containers/LanguagesContainer';
import infoProfileContainer from '../screens/profile/containers/InfoProfileContainer';
import autobind from 'class-autobind';
import LabelError from '../components/inputs/LabelError';

class LimitationsScreen extends Component {
  constructor(props) {
    super(props);

    autobind(this);
    this.state = {
      error: undefined
    }
  }

  validate() {
    const { infoProfileContainer } = this.props.containers;

    const limitations = infoProfileContainer.getSelectedLimitations();

    if (limitations.length === 0) {
      this.setState({
        error: lang.get('limitations.error')
      })
      return;
    } else {
      this.setState({
        error: undefined
      })
    }
  }
  async navigate() {
    await this.validate();

    const { error } = this.state;

    if (error) {
      return;
    } else {
      this.props.navigation.navigate('InfoProfileScreen');
    }
  }

  renderItem(item, index) {
    return (
      <ListItem key={index} onPress={() => this.selectLimitation(item)}>
        <CheckBox checked={item.selected}
          onPress={() => this.selectLimitation(item)} />
        <Body>
          <Text>{item.limitation}</Text>
        </Body>
      </ListItem>
    )
  }

  async selectLimitation(item) {
    const { navigation } = this.props;
    const isEditing = navigation.getParam('isEditing');

    if (!isEditing) {
      navigation.setParams({
        isEditing: true
      });
    }

    const { infoProfileContainer } = this.props.containers;
    await infoProfileContainer.selectLimitation(item);

    this.validate()
  }

  componentDidMount() {
    this.setNavigationGoBackConfirmationAlert();
    this.getLimitations();
  }

  showToast(text) {
    Toast.show({
      text: text,
      textStyle: { color: "white", fontSize: 12 },
      duration: 3000,
      position: 'top'
    })
  }

  async getLimitations() {
    const { infoProfileContainer } = this.props.containers;

    await infoProfileContainer.listLimitations();

    const { error } = infoProfileContainer.state;

    if (error) {
      this.showToast(error.message || lang.get('message.tryAgain'))
    } else {
      const { limitations } = infoProfileContainer.state;
      const newData = limitations.map(item => {
        item = {
          ...item,
          selected: false
        }
        return item
      })
      await infoProfileContainer.setSelectedLimitations(newData);
    }
  }

  setNavigationGoBackConfirmationAlert() {
    this.props.navigation.setParams({
      alert: {
        title: lang.get('limitations.alerTitle'),
        message: lang.get('limitations.alertDirection')
      }
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.limitations !== this.props.limitations ||
      nextProps.httpBusy !== this.props.httpBusy ||
      nextState.error !== this.state.error
  }

  render() {
    const { limitations, httpBusy } = this.props.containers.infoProfileContainer.state;
    Logger.count(this);

    return (
      <Container style={styles.container}>

        <NavBarComponent  {...this.props.navigation}
          name={lang.get('title.limitations')} />

        <View style={{ flexDirection: 'row', justifyContent: 'center', paddingTop: 35 }}>
          <Image style={styles.logoLimita} source={require('../../images/logo_colores/logo_colores.png')} />
        </View>

        <View style={{ paddingTop: 25, paddingBottom: 25 }}>
          <Text note style={{ textAlign: 'center', marginRight: 35, marginLeft: 35, fontSize: 17 }}>{lang.get('limitations.indications')}</Text>
        </View>

        <Content padder>
          {!!limitations && <List>
            {limitations.map(this.renderItem)}
          </List>}

          { this.state.error && <View style={{width:'100%', marginTop:10, paddingHorizontal:20}}>
              <LabelError message={this.state.error}/>
            </View>
          }
          {httpBusy && !limitations && <Spinner color='lightblue' />}

          {!!limitations && <Button
            style={styles.margin25}
            block
            onPress={this.navigate}
          >
            <Text>{lang.get('limitations.next')}</Text>
          </Button>
          }
        </Content>
      </Container>
    );
  }
}

const containers = {
  language: language,
  infoProfileContainer: infoProfileContainer,
}

const mapStateToProps = (containers) => {
  return {
    limitations: containers.infoProfileContainer.state.data,
    httpBusy: containers.infoProfileContainer.state
  };
}

export default connect(containers, mapStateToProps)(LimitationsScreen);