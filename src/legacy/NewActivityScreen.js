import React, { Component } from 'react';
import { Container, Icon, Button, View, Text, Content, Form, Toast, Spinner } from 'native-base';
import { connect, Logger } from 'unstated-enhancers';
import autobind from 'class-autobind';
import Validator from 'validatorjs';

import NavBarComponent from '../components/navBar/NavBarComponent';
import styles from '../theme/globalStyles';
import lang from '../shared/languages/Lang';
import TextInput from '../components/inputs/TextInput';
import TextareaInput from '../components/inputs/TextareaInput';
import newActivity from '../screens/activities/containers/NewActivityContainer';
import activities from '../screens/activities/containers/ActivityListContainer';
import ListErrors from '../components/listErrors/ListErrors';
var UUID = require('uuid-js');

class NewActivityScreen extends Component {
  constructor(props) {
    super(props);
    autobind(this);

    this.inputs = {};

    this.state = {
      validation: new Validator(data, this.rules)
    }
  }

  showToast(text) {
    Toast.show({
      text: text,
      textStyle: { color: "white", fontSize: 12 },
      duration: 3000,
      position: 'top'
    })
  }

  setNavigationGoBackConfirmationAlert() {
    this.props.navigation.setParams({
      alert: {
        title: lang.get('activity.alertTitle'),
        message: lang.get('activity.alertMessage')
      }
    });
  }

  validate() {
    const data = { ...this.state };
    const validation = new Validator(data, this.rules);

    validation.setAttributeNames({
      title: lang.get('activity.title'),
      description: lang.get('activity.description')
    });

    this.validation = validation;
    validation.check();

    this.setState({
      validation
    })
  }

  get rules() {
    return {
      'title': 'required|string',
      'description': 'required|string',
    }
  }

  handleValueChange(field, value) {
    const { navigation } = this.props;
    const isEditing = navigation.getParam('isEditing');

    if (!isEditing) {
      navigation.setParams({
        isEditing: true
      });
    }

    this.setState({
      [field]: value
    })
  }

  handleRefSet(field, component) {
    this.inputs[field] = component;
  }

  async addActivity() {
    const { httpBusy } = this.props.containers.newActivity.state;
    if (httpBusy) return;

    this.validate();

    if (this.validation.passes()) {
      const { newActivity, activities } = this.props.containers;
      const { mission, data } = activities.state;

      await newActivity.saveActivity(mission, this.state);

      const { error } = newActivity.state;

      if (error !== undefined) {
        this.showToast(error.message || lang.get('message.tryAgain'))

        setTimeout(() => {
          newActivity.clearError();
        }, 10000)

      } else {
        const act = newActivity.state.data;

        const activity = {
          ...this.state,
          status: 0,
          uuid: act.uuid,
        }
        const newActivities = [activity, ...data];

        await activities.setData('data', newActivities);
        this.props.navigation.navigate('MissionDetailsScreen');
      }
    } else {
      this.showToast(lang.get('message.dataRequired'));

      const fields = Object.keys(this.rules);
      const values = {}

      for (const field of fields) {
        values[field] = this.state[field] || ''
      }

      this.setState({
        ...values
      })
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.title !== this.state.title ||
      nextState.description !== this.state.description ||
      nextState.validation !== this.state.validation ||
      nextProps.newActivity.httpBusy !== this.props.newActivity.httpBusy ||
      nextProps.newActivity.error !== this.props.newActivity.error
  }

  componentWillUnmount() {
    const { newActivity } = this.props.containers;
    newActivity.reset();
  }

  componentDidMount() {
    this.setNavigationGoBackConfirmationAlert();
  }

  render() {
    const { validation, title, description } = this.state;
    const { httpBusy, error = {} } = this.props.newActivity;

    return (
      <Container style={[styles.container]}>
        <NavBarComponent {...this.props.navigation} name={lang.get('activity.newActivity')} />

        <Content padder>
          <Form>
            <TextInput
              field="title"
              next="description"
              returnKeyType={"next"}
              autoFocus={true}
              value={title}
              placeholder={lang.get('activity.title')}
              onChangeText={this.handleValueChange}
              onRefSet={this.handleRefSet}
              refs={this.inputs}
              errors={validation.errors}
              onBlur={this.validate}
            />

            <TextareaInput
              field="description"
              rowSpan={5}
              bordered
              value={description}
              placeholder={lang.get('activity.description')}
              onChangeText={this.handleValueChange}
              errors={validation.errors}
              onRefSet={this.handleRefSet}
              onBlur={this.validate}
            />
            <Button
              style={styles.margin25}
              block
              onPress={this.addActivity}
            >
              <Text> {lang.get('activity.add')} </Text>
              {
                httpBusy && <Spinner color="lightblue" />
              }
            </Button>
          </Form>
          {error.errors &&
            <ListErrors errors={error.errors} />
          }
        </Content>
      </Container>
    )
  }
}

const containers = {
  newActivity: newActivity,
  activities: activities
}

const mapStateToProps = (containers) => {
  return {
    newActivity: containers.newActivity.state,
  };
}

export default connect(containers, mapStateToProps)(NewActivityScreen);