import { StyleSheet, } from 'react-native';
import { Dimensions, Platform } from 'react-native';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    /* justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',*/
  },
  absoluteFillObject: {
    ...StyleSheet.absoluteFillObject,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  splashScreen: {
    backgroundColor: '#14141b',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  centeringContent: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  uploadIcon: {
    color: 'white',
    fontSize: 40
  },
  editText: {
    paddingBottom: 7,
    paddingTop: 7,
    paddingLeft: 5,
    color: '#0285c9',
    fontWeight: 'bold'
  },
  drawer: {
    borderBottomEndRadius: 12,
    borderTopEndRadius: 12,
    backgroundColor: '#394a4d',
    width: '15%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  uploadIconVideo: {
    color: '#D8D8D8',
    fontSize: 110
  },
  IconWarning: {
    color: '#E6E6E6',
    fontSize: 90
  },
  IconUser: {
    color: '#d1d1d1',
    fontSize: 90
  },
  margin25: {
    marginTop: 25
  },
  margin15: {
    marginTop: 15
  },
  padding10: {
    padding: 10
  },
  padding20: {
    padding: 20
  },
  flex1_DirectionColumn_JustifyCenter: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center'
  },
  flex1_AlignItemsCenter_JustifyCenter: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  flexDirectionRow_JustifyContent: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  flexDirectionRow_JustifyBetween: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  flex1_AlignItemsCenter_JustifyBetween: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  backgroundColorRGBABlack05: {
    backgroundColor: 'rgba(0,0,0,0.5)'
  },
  backgroundColorRGBABlack01: {
    backgroundColor: 'rgba(0,0,0,0.1)'
  },
  backgroundColorWhite: {
    backgroundColor: 'white'
  },
  borderWidth1: {
    borderWidth: 1
  },
  lightGray: {
    borderColor: '#d1d1d1'
  },
  blue: {
    color: '#0285c9'
  },
  textAlignJustify: {
    textAlign: 'justify'
  },
  textAlignCenter: {
    textAlign: 'center'
  },
  fontSize10: {
    fontSize: 10
  },
  stepsCheckList: {
    width: '100%',
    height: height * 0.4,
    padding: 20,
    alignItems: 'center',
    marginBottom: 20
  },
  listEmpty: {
    fontSize: 12,
    fontWeight: 'normal'
  },
  boxWelcomeMission: {
    justifyContent:'space-between',
    alignContent: 'center',
    alignItems:'center',
    marginTop: 25
  },
  modal: {
    padding: 20,
    backgroundColor: '#f7f7f7',
    borderWidth: 2,
    borderColor: '#0285c9',
    borderRadius: 10,
    width: '90%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  modal2: {
    paddingTop: 25,
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
    paddingBottom: 25,
    justifyContent: 'space-around',
    width: '100%'
  },
  buttonModal: {
    borderRadius: 60,
    width: 60,
    height: 60,
    backgroundColor: '#0285c9',
    justifyContent: 'center'
  },
  barTwoButtonsList: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  logoChecklist: {
    resizeMode: 'contain',
    width: width * 0.6,
    height: 60
  },
  logoLimita: {
    resizeMode: 'contain',
    width: width * 0.4,
    height: 40
  },
  logo: {
    resizeMode: 'contain',
    width: width * 0.8,
    height: 80
  },
  congratulation: {
    resizeMode: 'contain',
    width: width * 0.9,
    height: 140
  },
  icondeck: {
    fontSize: 60,
    color: 'white'
  },
  icondeck2: {
    fontSize: 100,
    color: 'white'
  },
  icondeck3: {
    fontSize: 40,
    color: 'white'
  },
  logoDrawer: {
    resizeMode: 'contain',
    width: width * 0.4,
    height: 70,
  },
  logoWell: {
    resizeMode: 'contain',
    width: width * 0.5,
    height: 60,
  },
  iconCircle2: {
    fontSize: 20,
    color: '#0285c9',
    paddingTop: 3
  },
  thumnails: {
    backgroundColor: 'rgba(91,193,190,0.7)',
    width: 37,
    height: 37,
    borderRadius: 37,
    marginRight: 10,
    alignItems: 'center',
    alignContent: 'center'
  },
  Picker: {
    fontSize: 16,
    paddingTop: 13,
    paddingHorizontal: 25,
    paddingBottom: 12,
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 4,
    backgroundColor: 'white',
    color: 'black',
  },
  marginHorizontal25: {
    marginHorizontal: 25
  },
  ViewBorder: {
    borderColor: 'white',
    textAlign: 'center',
    marginTop: 25,
    borderWidth: 1,
    width: 250,
    alignSelf: 'center'
  },
  TextWelcome: {
    color: 'white',
    fontSize: 20,
  },
  TextWelcomeTitle: {
    marginTop: 30,
    textAlign: 'center',
    marginLeft: 50,
    marginRight: 50
  },
  RenderNotifi: {
    width: '25%',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: 'lightgrey',
    paddingBottom: 10,
    paddingTop: 10
  },
  RenderNotifiText: {
    width: '50%',
    borderBottomWidth: 1,
    borderColor: 'lightgrey',
    paddingBottom: 10,
    paddingTop: 10
  },
  NotifiText: {
    width: '25%',
    paddingBottom: 10,
    paddingTop: 10,
    borderBottomWidth: 1,
    borderColor: 'lightgrey'
  },
  NotifiText1: {
    width: '50%',
    paddingBottom: 10,
    paddingTop: 10,
    borderBottomWidth: 1,
    borderColor: 'lightgrey'
  },
});

export default styles;
