import React from 'react';
import { TouchableOpacity } from 'react-native';
import { View, Text, Thumbnail, Icon, Spinner } from 'native-base';

import image from '../../images/video/video.png';
import lang from '../../shared/languages/Lang';


const ThumbnailLink = ({thumbnail, videoLabel, videoLoaded, isVideoLoaded, textColor, onVideoSelectionOption, onNavigateVideo, progress, load}) => {
  return (
    <View style={{
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      paddingVertical: 10
    }} >
      <TouchableOpacity onPress={onVideoSelectionOption}>
        <View style={{ alignItems: 'flex-end', flexDirection: 'row' }}>
        {load &&
            <View style={{ flexDirection: 'column', width: 100, height: 100 }}>
              <Spinner color='#ABABAB' />
              <Text style={{ paddingBottom: 10, color: '#ABABAB' }}>{lang.get('profile.loading', 'thumnail.loading')}</Text>
            </View>
          }
          {
            !load &&
          <Thumbnail square source={thumbnail ? {uri: thumbnail} : image} style={{
            height: 100,
            width: 100,
          }} />
          }
          {progress && <Spinner style={{ position: 'absolute', marginLeft: 30, paddingBottom: 30 }} color='#fff' /> }
          {progress && <Text style={{ position: 'absolute', paddingBottom: 20, color: '#fff' }}>{lang.get('profile.processing', 'thumnail.processing')}</Text> }
        </View>
      </TouchableOpacity>
      { !isVideoLoaded && <Text style={{ textAlign: 'center', marginTop: 10, color: textColor }}> {videoLabel} </Text>}
      { isVideoLoaded && <Text onPress={onNavigateVideo} style={{ textAlign: 'center', marginTop: 10, textDecorationLine: 'underline', color: textColor }}> {videoLoaded} </Text>}
    </View>
  );
}

ThumbnailLink.defaultProps = {
  videoLabel: lang.get('profile.video', 'thumnail.processing'),
  videoLoaded: lang.get('profile.playVideo', 'thumnail.play_video'),
  isVideoLoaded: false,
  textColor: 'black',
  progress: false
}

export default ThumbnailLink;
