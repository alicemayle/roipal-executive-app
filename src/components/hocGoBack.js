import React from 'react';
import BlankScreen from '../screens/BlankScreen';
import { View } from 'native-base';
import { Alert } from 'react-native'

import NavigationService from '../shared/services/NavigationService';

const routeNeedsEditingCheck = (state) => {
  return state
    && state.params
    && state.params.isEditing
}

const getAlertConfig = (state) => {
  const config = state.params.alert || {};

  return {
    title: config.title,
    message: config.message || 'All changes will be lost, continue anyway?'
  }
}

const showConfirmationAlert = (state) => {
  const alert = getAlertConfig(state);

  Alert.alert(
    alert.title,
    alert.message,
    [
      {
        text: 'Cancel',
        onPress: noop,
        style: 'cancel',
      },
      {
        text: 'OK', onPress: cancel = () => {
          state.params.isEditing = false;

          NavigationService.goBack();
        }
      },
    ],
    {
      cancelable: false
    },
  );
}

const onBack = (state) => {
  if(state) {
    if (routeNeedsEditingCheck(state)) {
      showConfirmationAlert(state)
      return true
    } else {
      return false
    }
  } else {
    NavigationService.goBack();
  }
}

const hocGoBack = (component) => (props) => {
  const Decorated = component;

  const newProps = {
    ...props,
    navigation: {
      ...props.navigation,
      goBack: onBack
    }
  }

  return (
    <View style={{ flex: 1 }}>
      <Decorated {...newProps} />
      <BlankScreen {...newProps} />
    </View>
  );

}

export default hocGoBack;