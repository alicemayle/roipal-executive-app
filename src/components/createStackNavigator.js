import { createStackNavigator as  createStack} from 'react-navigation';
import withGoBackConfirmationAction from './withGoBackConfirmationAction';

const createStackNavigator = (options, config) => {
    return withGoBackConfirmationAction(
        createStack(options, config)
    )
}

export default createStackNavigator;