import React, { PureComponent } from 'react';
import { View, Text, Icon, Button } from 'native-base';
import { Image, Dimensions } from 'react-native';
import logo from '../../images/logo_colores/logo_colores.png';
import styles from '../../theme/globalStyles';

const { height } = Dimensions.get('window');

class EmptyList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { welcome, message } = this.props;

    return (
      <View style={{ alignItems: 'center', flexDirection: 'column', height: height * 0.5, paddingTop: 50 }}>
        <View >
          <Text style={{ fontSize: 20, textAlign: 'center' }} >{message}</Text>
        </View>
        <Icon name="warning" style={[styles.IconWarning]}></Icon>
        <View style={{ paddingTop: '7%' }}>
          {this.props.children}
        </View>
      </View>
    );
  }
}

export default EmptyList;