import React, { Component } from 'react';
import { Text } from 'native-base';

class ValidationErrorMessage extends Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps) {
    return nextProps.errors != this.props.errors
      || nextProps.username != this.props.username
      || nextProps.property != this.props.property
  }

  render() {
    const { errors, value, property } = this.props;
    return (
      errors && value !== undefined && errors.first(property)
        ? <Text style={{ color: 'red', marginLeft: 10 }}>
          {errors.first(property)}
        </Text>
        : null
    );
  }
}

export default ValidationErrorMessage;