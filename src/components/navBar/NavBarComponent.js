import React, { Component } from 'react';
import { Header, Left, Button, Icon, Body, View, Title, Badge, Text, FooterTab } from 'native-base';
import autobind from 'class-autobind';
import { TouchableOpacity, Alert } from 'react-native'
import LinearGradient from 'react-native-linear-gradient';

class NavBarComponent extends Component {
  constructor(props) {
    super(props);
    autobind(this);
  }

  onPressLeftButton() {
    const { onPressLeftButton } = this.props;

    if (onPressLeftButton) {
      onPressLeftButton()
    } else {
      const {state } = this.props;
      if(state.params && state.params.isEditing) {
        this.props.goBack(state);
      } else {
        this.props.goBack();
      }
    }
  }

  render() {
    const { menu = false, name, openDrawer = {}, invitations = 0 } = this.props;

    return (
      <LinearGradient
        colors={['#135481', '#016eb8', '#0285c9', '#00a7e5']}
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
        style={{
          width: '100%',
        }}
      >
        <Header noShadow style={{ flexDirection: 'row', backgroundColor: 'transparent' }}>

          <Button
            transparent
            onPress={this.onPressLeftButton}
          >
            <Icon name={menu === null ? null : menu === true ? null : 'arrow-back'} />
          </Button>
          <Body style={{ alignItems: 'center' }}>
            <Title style={{ color: 'white' }}>{name}</Title>
          </Body>
          {menu === true &&
            <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'flex-start' }} onPress={openDrawer}>
              <Button
                transparent
                onPress={openDrawer}>
                <Icon name={'menu'} style={{ color: 'white' }} />
              </Button>
              { invitations > 0 &&
                <View style={{ backgroundColor: 'red', width: 17.5, height: 17.5, borderRadius: 17.5, position: 'absolute', marginLeft: 30, marginTop: 8, justifyContent: 'center', alignItems: 'center' }}>
                  <Text style={{ color: 'white', fontSize: 10, fontWeight: 'bold' }}>{invitations}</Text>
                </View>
              }
            </TouchableOpacity>

          }
          {menu !== true &&
            <View style={{ width: 70, justifyContent: 'center', alignItems: 'center' }}>
              {this.props.children}
            </View>
          }

        </Header>
      </LinearGradient>
    );
  }
}

export default NavBarComponent;
