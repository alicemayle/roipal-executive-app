import { Container } from 'unstated';
import AuthLoadingServices from '../../../screens/auth/services/AuthLoadingServices';

import { api } from '../../../shared/languages/Lang';

const initialStated = {
  selected: undefined,
  translations: undefined
};

class LanguagesContainer extends Container {
  name = 'LanguagesContainer';

  persist = {
    key: 'LanguagesContainer'
  }

  state = { ...initialStated };

  setData(selected) {
    this.setState({
      selected
    })
  }


  updateState(key, value){
    this.setState({
      [key]: value
    });
  }

  async getTranslationsVersion() {
    try {
      this.setState({
        httpBusy: true,
        __action: 'get translations version starts'
      })

      const response = await AuthLoadingServices.getVersion();

      if (response) {
        await this.setState({
          translations_version: response.data.value,
          message: response.message,
          __action: 'get translations version succes'
        })

        if (!this.state.translations
            || (this.state.translations.version != response.data.value)) { 
            this.getTranslations();
        }
      }
    } catch (error) {
      await this.setState({
        error: error,
        __action: 'get translations version error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'get translations version finally'
        });
      }, 500)
    }
  }

  async getTranslations(){
    try {
      this.setState({
        httpBusy: true,
        __action: 'get translations starts'
      })      

      const response = await AuthLoadingServices.getTranslations();

      if (response) {
        await this.setState({
          translations: response.data,
          message: response.message,
          __action: 'get translations success'
        })
        api.setMessages(response.data)
      }
    } catch (error) {  
      await this.setState({
        error: error,
        __action: 'get translations error'
      });
    }
    finally {
      setTimeout(() => {
        this.setState({
          httpBusy: false,
          __action: 'get translations finally'
        });
      }, 500)
    }
  }
}

export default LanguagesContainer;