import React, { PureComponent } from 'react';
import { Picker } from 'react-native';
import autobind from "class-autobind";
import { Logger } from 'unstated-enhancers';

import lang from '../../shared/languages/Lang';
import { Icon, View } from 'native-base';

class LanguagesComponent extends PureComponent {
  constructor(props) {
    super(props);

    autobind(this);
  }

  valueChange(value, index) {
    const { onValueChange } = this.props;

    if (onValueChange) {
      onValueChange(value);
    }
  }

  render() {
    const { selected, wid } = this.props;
    Logger.count(this);

    return (
      <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', width:'100%'}}>
        <Icon name='language' type='FontAwesome' size={24} style={{ color: '#52bebb' }} />
        <Picker
          mode="dropdown"
          style={{ width: wid ? wid : '60%', marginLeft: wid ? 10 : 0 }}
          selectedValue={selected}
          onValueChange={this.valueChange}
        >
          <Picker.Item label={lang.get('language.spanish', 'generic.languaje.spanish')} value="es" />
          <Picker.Item label={lang.get('language.english', 'generic.languaje.english')} value="en" />
          <Picker.Item label={lang.get('language.french', 'generic.languaje.french')} value="fr" />
        </Picker>
      </View>
    );
  }

}

export default LanguagesComponent;