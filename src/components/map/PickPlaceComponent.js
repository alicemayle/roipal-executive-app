import React, { PureComponent } from 'react';
import { View, Icon, Button } from 'native-base';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { Logger } from 'unstated-enhancers';
import autobind from 'class-autobind';
import lang from '../../shared/languages/Lang';

class PickPlaceComponent extends PureComponent {
  constructor(props) {
    super(props);

    autobind(this);
  }

  async clearText() {
    const { onChangeTextUndefined } = this.props;

    onChangeTextUndefined(undefined);
  }

  onChangeText(text) {
    if (text !== '') {
      this.props.onSetFlagWrite(text)
    }
  }

  renderRightButton() {
    if (this.props.address) {
      return (
        <Button transparent onPress={this.clearText}>
          <Icon style={{color: 'rgba(57, 74, 77, 0.4)'}} name='close-circle'/>
        </Button>
      )
    }
  }

  render() {
    const { address = '', pickPlace } = this.props;
    Logger.count(this)

    return (
      <View style={{ width: '100%', flexDirection:'row', justifyContent: 'flex-end' }}>

        <GooglePlacesAutocomplete
          textInputProps={{
             onChangeText: this.onChangeText,
             clearButtonMode: 'never'
          }}
          placeholder={lang.get('map.placeholder', 'map.placeholder')}
          minLength={2}
          autoFocus={false}
          returnKeyType={"search"}
          listViewDisplayed={false}
          fetchDetails={true}
          onPress={pickPlace}
          currentLocation={false}
          text={address}
          query={{
            key: "AIzaSyBuzXOCfj1kiX3E8OGzkoVmqfbWjTKLYA4",
            language: "es",
            components: 'country:mx',
          }}
          styles={{
            textInputContainer: {
              backgroundColor: "rgba(0,0,0,0.2)",
              width: "100%",
            },
            listView: {
              backgroundColor: "rgba(255,255,255,0.6)",
              position: "relative",
              marginTop: 0,
            },
            description: {
              fontWeight: "bold"
            },
            predefinedPlacesDescription: {
              color: "red"
            },
            powered: {
              height: 0
            },
            poweredContainer: {
              position: "absolute",
              height: 0,
              backgroundColor: "rgba(0,0,0,0)"
            },
            container: {
              paddingTop: 0,
              position: "relative",
              width: "100%",
              alignSelf: "center"
            }
          }}
          currentLocationLabel="Current location"
          nearbyPlacesAPI="GooglePlacesSearch"
          GoogleReverseGeocodingQuery={{}}
          GooglePlacesSearchQuery={{
            rankby: "distance",
            types: "food"
          }}
          filterReverseGeocodingByTypes={[
            "locality",
            "administrative_area_level_3"
          ]}
          debounce={200}
          renderRightButton={this.renderRightButton}
        />
      </View>
    );
  }
}

export default (PickPlaceComponent);
