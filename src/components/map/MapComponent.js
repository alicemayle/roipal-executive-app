import React, { PureComponent } from 'react';
import { Content, Button, Text, View, Toast, Icon } from 'native-base';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { Dimensions, InteractionManager, Platform } from 'react-native';
import Geocoder from "react-native-geocoding";
import autobind from 'class-autobind';
import styles from '../../theme/globalStyles';
import PickPlaceComponent from './PickPlaceComponent';
import { Logger } from 'unstated-enhancers';
import lang from '../../shared/languages/Lang';
import Permissions from '../../shared/support/Permissions';

Geocoder.init("AIzaSyAAjZGbgXrMZDS3T9YqdgDO-NnQC-eDZLA");
const screen = Dimensions.get('window');
const LATITUDE_DELTA = 0.01;
const ASPECT_RATIO = screen.width / screen.height;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class MapComponent extends PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      flagWrite: false,
      clear: false
    }
    autobind(this);
  }

  setMap(map) {
    this.map = map;
  }

  async setCurrentPosition() {
    const { setAddress, initialRegion } = this.props;

    if ((initialRegion.latitude === 0 && initialRegion.longitude === 0) || !initialRegion.address) {
      try {
        await Permissions.location();

        const position = await new Promise((resolve, reject) => {
          navigator.geolocation.getCurrentPosition(resolve, reject, { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 });
        });
        const json = await Geocoder.from(position.coords);
        const addressComponent = json.results[0].formatted_address;

        Logger.dispatch('position initial', { position, addressComponent, ...this.state })

        setTimeout(() => {
          if (!this.state.flagWrite) {
            if (position) {
              setAddress({
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
              }, addressComponent);

              if (this.map) {
                this.map.animateToRegion({
                  latitude: position.coords.latitude,
                  longitude: position.coords.longitude,
                  latitudeDelta: LATITUDE_DELTA,
                  longitudeDelta: LONGITUDE_DELTA
                });
              }
            }
          }
        }, 400)

      } catch (error) {
        InteractionManager.runAfterInteractions(() => {
          Toast.show({
            text: error.message || lang.get('map.locationNotFound', 'map.location_not_found'),
            textStyle: { color: "white", fontSize: 12 },
            duration: 10000,
            position: 'top'
          });
        });
      }
    } else {
      setAddress({
        latitude: initialRegion.latitude,
        longitude: initialRegion.longitude,
        address: initialRegion.address,
      }, initialRegion.address);

      setTimeout(() => {
        if (this.map) {
          this.map.animateToRegion({
            latitude: initialRegion.latitude,
            longitude: initialRegion.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
          });
        }
      }, 200);
    }
  }

  async pickPlace(data, details) {
    const { pickPlace } = this.props;

    await pickPlace(data, details);

    setTimeout(() => {
      if (this.map) {
        this.map.animateToRegion({
          latitude: details.geometry.location.lat,
          longitude: details.geometry.location.lng,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA
        });
      }
    }, 200);
  }

  async onRegionChange(data) {
    const { onChangePlace } = this.props;
    const { clear } = this.state

    setTimeout(() => {
      if (this.debounce !== null) {
        clearTimeout(this.debounce);
      }

      this.debounce = setTimeout(async () => {
        if (data && !clear) {

          try {
            const json = await Geocoder.from(data);

            onChangePlace({
              latitude: data.latitude,
              longitude: data.longitude,
              address: json.results[0].formatted_address,
            });
          } catch (error) {
          }
        }
      }, 500);
    }, 1000)
  }

  async handleSetFlagWrite(value) {
    const { flagWrite } = this.state;

    if (!flagWrite) {
      await this.setState({
        text: value,
        flagWrite: true
      })
    }
  }

  componentDidMount() {
    this.setCurrentPosition();
  }

  componentWillUnmount() {
    this.setState({
      flagWrite: false,
      clear: false
    })
  }

  render() {
    const {
      onPickAddress,
      address,
      onChangeTextUndefined,
      initialRegion
    } = this.props;
    Logger.count(this)

    return (
      <Content contentContainerStyle={[styles.container, styles.absoluteFillObject, { justifyContent: 'space-between' }]}>
        <MapView
          provider={PROVIDER_GOOGLE}
          ref={this.setMap}
          style={[styles.absoluteFillObject, styles.flex1]}
          initialRegion={{
            ...initialRegion,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LATITUDE_DELTA * ASPECT_RATIO,
          }}
          onRegionChange={this.onRegionChange}
        />
        <PickPlaceComponent
          address={address}
          pickPlace={this.pickPlace}
          onChangeTextUndefined={onChangeTextUndefined}
          onSetFlagWrite={this.handleSetFlagWrite}
        />
        {(address !== '' && address !== undefined) &&
          <Button block onPress={onPickAddress} style={{ marginBottom: Platform.OS === 'ios' ? 35 : 10, marginLeft: 30, marginRight: 30 }}>
            <Text>{lang.get('map.confirmLocation', 'map.confirm_location')}</Text>
          </Button>
        }
        {
          address && <View style={{
            left: '50%',
            marginLeft: -24,
            marginTop: -48,
            position: 'absolute',
            top: '50%'
          }}
            pointerEvents="none">
            <Icon name="pin" style={{ color: 'red', fontSize: 35 }} />
          </View>
        }
      </Content>
    )
  }
}

export default MapComponent;