import hocGoBack from '../hocGoBack';
import EditAccountScreen from "../../screens/account/EditAccountScreen";
import EditInfoProfileScreen from '../../screens/account/EditInfoProfileScreen';
import MapScreenEditProfileScreen from '../../screens/account/MapScreen';
import RegistrationPaymentScreen from '../../screens/payment/RegistrationPaymentScreen';
import StarDiscScreen from '../../screens/evaluation/StarDiscScreen';
import EvaluationScreen from '../../screens/evaluation/EvaluationScreen';
import EvaluationCompanyScreen from '../../screens/roipalEvaluation/EvaluationScreen';
import CompanyAssessmentScreen from '../../screens/companyAssessment/CompanyAssessmentScreen';
import ActivityRecordScreen from '../../screens/activities/ActivityRecordScreen';
import ResetPasswordScreen from '../../screens/auth/password-reset/ResetPasswordScreen';
import InfoProfileScreen from '../../screens/profile/InfoProfileScreen';
import VerificationDocuments from '../../screens/profile/VerificationDocuments';
import MapScreen from '../../screens/profile/MapScreen';
import RegistrationScreen from '../../screens/auth/registration/RegistrationScreen';
import MissionDetailsScreen from '../../screens/home/MissionDetailsScreen';

const EditAccount = hocGoBack(EditAccountScreen)
const EditInfoProfile = hocGoBack(EditInfoProfileScreen)
const MapScreenEditProfile = hocGoBack(MapScreenEditProfileScreen)
const RegistrationPayment = hocGoBack(RegistrationPaymentScreen)
const StarDisc = hocGoBack(StarDiscScreen)
const Evaluation = hocGoBack(EvaluationScreen)
const EvaluationCompany = hocGoBack(EvaluationCompanyScreen)
const CompanyAssessment = hocGoBack(CompanyAssessmentScreen)
const ActivityRecord = hocGoBack(ActivityRecordScreen)
const ResetPassword = hocGoBack(ResetPasswordScreen)
const InfoProfile = hocGoBack(InfoProfileScreen)
const VerificationDocumentsHoc = hocGoBack(VerificationDocuments)
const MapHoc = hocGoBack(MapScreen)
const Registration = hocGoBack(RegistrationScreen)
const MissionDetails = hocGoBack(MissionDetailsScreen)

export {
    EditAccount,
    EditInfoProfile,
    MapScreenEditProfile,
    RegistrationPayment,
    StarDisc,
    Evaluation,
    EvaluationCompany,
    CompanyAssessment,
    ActivityRecord,
    ResetPassword,
    InfoProfile,
    VerificationDocumentsHoc,
    MapHoc,
    Registration,
    MissionDetails
}