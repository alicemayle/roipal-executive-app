import React, { Component } from 'react';
import { Text, View } from 'native-base';
import { TouchableOpacity } from 'react-native';

class NewInvitationNotice extends Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.countInvitations !== this.props.countInvitations ||
    nextProps.selected !== this.props.selected

  }

  render() {
      const { message, countInvitations } = this.props;

      return(
        <TouchableOpacity 
        {...this.props}
        >
            <Text style={{color:'#FFF'}}>{`${message} (${countInvitations})`}</Text>
        </TouchableOpacity>
      )
    
  }
}

export default NewInvitationNotice;