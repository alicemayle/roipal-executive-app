import React, { Component } from "react";
import { View, PanResponder, Dimensions, Animated, Platform } from 'react-native'
import { Logger } from "unstated-enhancers";
import { Icon } from "native-base";

const { width, height } = Dimensions.get('window');
const maxHeight = height * 0.40;
const minHeight = height * 0.035;

class ExtendibleView extends Component {
  constructor(props) {

    super(props);
    this.state = {
      customHeight: new Animated.Value(minHeight),
      customWidth: width - 20,
      arrow: 'arrow-dropup'
    }

    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: (evt, gestureState) => true,
      onStartShouldSetPanResponderCapture: (evt, gestureState) => false,
      onMoveShouldSetPanResponder: (evt, { dx, dy }) => dx !== 0 || dy !== 0,
      onMoveShouldSetPanResponderCapture: (evt, gestureState) => false,

      onPanResponderGrant: (evt, gestureState) => {       
        Logger.dispatch('entro al onPanResponderGrant', gestureState)
      },
      onPanResponderMove: (evt, gestureState) => {      
        Logger.dispatch('entro al onPanResponderMove', gestureState)
      },
      onPanResponderTerminationRequest: (evt, gestureState) => true,
      onPanResponderRelease: (evt, gestureState) => {
       
        Logger.dispatch('entro al onPanResponderRelease', gestureState.dy)

        let newHeight = this.state.customHeight._value - gestureState.dy;

        if ( newHeight >= maxHeight ) {
          newHeight = maxHeight;
          this.setState({
            arrow: 'arrow-dropdown'
          })
        } else if (newHeight <= minHeight) {
          newHeight = minHeight;
          this.setState({
            arrow: 'arrow-dropup'
          })
        }
        this.animateView(newHeight, gestureState.vy) 

      },
      onPanResponderTerminate: (evt, gestureState) => {
        Logger.dispatch('entro al onPanResponderTerminate', gestureState)

      },
      onShouldBlockNativeResponder: (evt, gestureState) => {
        Logger.dispatch('entro al onShouldBlockNativeResponder', gestureState)

        return true;
      },
    });
  }

  animateView(value, velocity) {
    Animated.timing(                 
      this.state.customHeight,            
      {
        toValue: value,                  
        duration: 350,              
      }
    ).start();
  }

  render() {
    Logger.dispatch('1.state', this.state)
    return (
      <View
        style={{
          position: 'absolute',
          width: '100%',
          zIndex: 9,
          bottom: Platform.OS === 'ios' ? 25: 0,
          alignItems: 'center',
          paddingHorizontal: 15,
          shadowColor: 'black',
          shadowOpacity: 0.2,
          shadowRadius: 1,
          shadowOffset: {
            height: 5,
            width: 2,
          }
        }}>
          <Animated.View
            style={{
              backgroundColor: '#f3f3f3',
              width: this.state.customWidth,
              height: this.state.customHeight,
              flexDirection: 'column',
              justifyContent: 'flex-start',
              alignItems: 'center',
              paddingHorizontal: 15
            }}
            {...this._panResponder.panHandlers}
          >
            <View>
              <Icon style={{color:"#747474"}} name={this.state.arrow}/>
            </View>
            {this.props.children}

          </Animated.View>
      </View>

    )
  }
}

export default ExtendibleView;