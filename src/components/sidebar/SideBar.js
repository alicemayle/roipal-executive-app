
import React, { Component } from "react";
import { Container, Text, Thumbnail, View, Icon, Button } from 'native-base';
import { Image } from 'react-native';

import SideBarMenuItem from './SideBarMenuItem';
import Lang from "../../shared/languages/Lang";
import styles from "../../theme/globalStyles";
import autobind from 'class-autobind';
import genericLogo from '../../images/drawer/perfil.png';
import { Logger, connect } from "unstated-enhancers";
import authContainer from '../../screens/auth/containers/AuthContainer';
import Observer from '../../shared/Observer';
import LinearGradient from "react-native-linear-gradient";
import invitations from '../../images/drawer/invitations.png';
import settings from '../../images/drawer/settings.png';
import mission from '../../images/drawer/mission.png';
import language from '../language/containers/LanguagesContainer';
import invitationsContainer from '../../screens/invitations/containers/InvitationsListContainer';

class SideBar extends Component {

  constructor(props) {
    super(props);
    autobind(this);
    this.state = {
      invitations_expired: 0
    }
  }

  close() {
    this.props.navigation.closeDrawer();
  }

  closeSession() {
    Observer.notify('logout')
  }

  _renderItem(item, index) {
    const { profile = {} } = this.props.authContainer;
    const { invitations_expired } = this.state;

    return (
      <SideBarMenuItem
        {...this.props}
        key={index}
        item={item}
        invitations={profile.invitations_received - invitations_expired}
        payment={profile.registered_payment}
      />
    )
  }

    async getInvitationsExpired() {
    const { invitations } = this.props.containers;
    const params = {
      includes: 'mission',
    };

    await invitations.fetch(params);

    const { error, data } = invitations.state;

    if (error) {
      this.showToast(error.message || lang.get('message.tryAgain', 'generic.try_again'))

      setTimeout(() => {
        invitations.clearError();
      }, 200)
    } else {
      let expired = 0;

      data.map((item) => {
        if(item.status === 0) {
          if(item.mission.status === 10
            || item.mission.status === -1
            || item.mission.accepted === item.mission.executives_requested) {
              expired = expired + 1
            }
        }
      })

      this.setState({
        invitations_expired: expired
      })
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.authContainer.profile !== this.props.authContainer.profile ||
      nextProps.authContainer.data !== this.props.authContainer.data ||
      nextProps.language.selected !== this.props.language.selected ||
      nextState.invitations_expired !== this.state.invitations_expired
  }

  componentDidMount() {
    this.getInvitationsExpired()
  }

  render() {
    const { profile = {}, data = {} } = this.props.authContainer;
    const logo = profile.photo_bio_url;
    const routes = [
      {
        icon: mission,
        route: 'HomeScreen',
        text: Lang.get('drawer.home', 'drawer.home'),
      },
      {
        icon: invitations,
        route: 'InvitationsListScreen',
        text: Lang.get('drawer.invitations', 'drawer.invitations'),
        highlight: true
      },
      {
        icon: settings,
        route: 'EditAccountScreen',
        text: Lang.get('drawer.setting', 'drawer.setting')
      },
    ]

    return (
      <Container >
        <LinearGradient
              colors={['#135481', '#016eb8', '#0285c9', '#00a7e5', '#1dbed6', '#5bc1be']}
              start={{ x: 0, y:  0 }}
              end={{ x: 0, y: 1 }}
              style={{alignItems: 'flex-end', flex: 1, flexDirection: 'row'}}
            >
            <View style={[styles.drawer]}>
              <Icon name={'keyboard-arrow-right'} type={'MaterialIcons'} onPress={this.close} style={{color: 'gray', fontSize: 45}}></Icon>
            </View>

            <View style={{ height: '100%', alignItems: 'center', justifyContent: 'space-between', width:'85%', paddingBottom: 20 }}>
              <View style={{ paddingVertical: 10, width: '100%', alignItems: "flex-end" }}>
                <Thumbnail
                  circle
                  large
                  source={logo ? { uri: logo } : genericLogo}
                  style={{
                    marginBottom: 15,
                    marginTop: 20,
                    marginHorizontal: 15,
                    borderColor: 'white',
                    borderWidth: logo ? 2 : 0
                  }} />
                <Text numberOfLines={2} style={{ color: '#fff', marginBottom: 5, marginHorizontal: 15 }}>{data.name}</Text>
                <Text style={{ color: '#fff', marginHorizontal: 15 }}>{data.email}</Text>
              </View>

              <View style={{ alignItems: "flex-end", width: '100%', paddingVertical: 10 }}>
                  {routes.map(this._renderItem)}
              </View>

              <Button
                style={{ width: '100%', justifyContent: 'flex-end', paddingVertical: 10 }}
                transparent
                onPress={this.closeSession}
              >
                <Text style={{ textAlign: 'right', color: 'white' }}>
                  {Lang.get('drawer.logout', 'drawer.logout')}
                </Text>
              </Button>

              <Image style={styles.logoDrawer} source={require('../../images/logo_blanco/logo_blanco.png')} />
            </View>
            </LinearGradient>
      </Container>
    );
  }
}

const containers = {
  authContainer: authContainer,
  language: language,
  invitations: invitationsContainer
}

const mapStateToProps = (containers) => {
  return {
    authContainer: containers.authContainer.state,
    language: containers.language.state
  };
}

export default connect(containers, mapStateToProps)(SideBar);