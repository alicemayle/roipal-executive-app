import React, { Component } from 'react';
import { Button, Text, Icon, Item, ListItem, View } from 'native-base';
import lang from '../../shared/languages/Lang';
import { Persist, Manager } from 'unstated-enhancers';
import autobind from 'class-autobind';
import Observer from '../../shared/Observer';
import { Dimensions, Platform, Alert, Image } from 'react-native';
import invi_black from '../../images/drawer/invitation_black.png';

const { height, width } = Dimensions.get('window');

class SideBarMenuItem extends Component {

  constructor(props) {
    super(props);

    autobind(this);

  }

  _navigateToPage() {
    const { item, payment } = this.props;
    const page = item.route;

    if(item.highlight && payment === 0) {
      Alert.alert(
        lang.get('payment.titleAlert', 'drawer.title_alert'),
        lang.get('payment.messageAlert', 'drawer.message_alert'),
        [
          {
            text: lang.get('activity.cancel', 'generic.button.cancel'),
            onPress: () => this.props.navigation.closeDrawer()
          },
          {
            text: lang.get('activity.confirm', 'generic.button.confirm'),
            onPress: () => {
              this.props.navigation.closeDrawer();
              this.props.navigation.navigate('RegistrationPaymentScreen', {goBack: page})
            }
          },
        ],
        {
          cancelable: false
        }
      )
      return;
    }

    if (page === 'NoAuthNavigator') {
      Observer.notify('logout')
    } else {
      this.props.navigation.closeDrawer();
      this.props.navigation.navigate(page)
    }

  }

  render() {
    const { item, invitations } = this.props;

    let margin = 0
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'flex-start',
          backgroundColor: invitations && item.highlight ? 'rgba(218,218,218,0.5)' : 'transparent',
          borderBottomWidth: 1,
          borderBottomColor: 'white'
        }}>
        <Button
          style={{ width: '100%', justifyContent: 'flex-end' }}
          transparent
          onPress={this._navigateToPage}
        >
          <Text style={{ color: invitations && item.highlight ? 'black' : 'white', textAlign: 'right' }}>
            {item.text}
          </Text>
            <Image style={{height: 23, width: 23, marginRight: 18}}
            source={ invitations && item.highlight ? invi_black : item.icon}
            />
        </Button>
        { invitations > 0 && item.highlight &&
          <View style={{position: 'absolute', marginTop: 8, width:'100%', alignItems:'flex-end', paddingRight: 10}}>
            <View style={{ backgroundColor: 'red', width: 17.5, height: 17.5, borderRadius: 17.5, justifyContent: 'center', alignItems: 'center' }}>
              <Text style={{ color: 'white', fontSize: 10, fontWeight: 'bold' }}>{invitations}</Text>
            </View>
          </View>
        }
      </View>
    );
  }
}

export default SideBarMenuItem;