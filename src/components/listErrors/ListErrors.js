import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';
import autobind from 'class-autobind';
import LabelError from '../inputs/LabelError';
import { Logger } from 'unstated-enhancers';

class ListErrors extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    autobind(this);
  }

  _keyExtractor(item, index) {
    Logger.dispatch('keyStractor', index)
    return index.toString()
  };

  renderError({ item, index }) {

    Logger.dispatch('ItemError', item)

    return (
      <View key={index} style={{ flexDirection: 'row' }}>
        <LabelError message='*' />
        <LabelError message={item} />
      </View>
    )
  }

  render() {
    const { errors } = this.props;
    Logger.dispatch('List Errors', errors)

    return (
      <FlatList
        data={errors}
        keyExtractor={this._keyExtractor}
        renderItem={this.renderError}
      />
    );
  }
}

export default ListErrors;
