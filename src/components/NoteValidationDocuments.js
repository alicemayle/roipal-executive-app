import React, { Component } from 'react';
import { Text, View } from 'native-base';
import { TouchableOpacity } from 'react-native';

class NoteValidationDocuments extends Component {
  constructor(props) {
    super(props);
  }

  render() {
      const { message, onUploadDocuments } = this.props;

      return(
        <TouchableOpacity 
        {...this.props}
        style={{
          backgroundColor: '#FF8000',
          alignItems:'center',
          justifyContent:'center',
          padding: 5
        }}
        onPress={onUploadDocuments}
        >
            <Text style={{color:'#FFF'}}>{`${message}`}</Text>
        </TouchableOpacity>
      )
    
  }
}

export default NoteValidationDocuments;