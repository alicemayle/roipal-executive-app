import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { CheckBox, ListItem, Left, Body, View, Text } from 'native-base';
import autobind from 'class-autobind';

class CheckButtonComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    autobind(this);
  }

  changeOption() {
    const { onChangeOption, field } = this.props;

    onChangeOption(field);
  }

  render() {
    return (
      <TouchableOpacity style={{ backgroundColor: 'trasparent', flexDirection:'row', width:'100%', marginBottom:10, alignItems:'center' }}
        onPress={this.changeOption}>
          <CheckBox
            field={this.props.field}
            checked={this.props.checked}
            onPress={this.changeOption}
          />
          <Text style={{marginLeft: 20}}>{this.props.textButton}</Text>
      </TouchableOpacity>
    );
  }
}
export default CheckButtonComponent;