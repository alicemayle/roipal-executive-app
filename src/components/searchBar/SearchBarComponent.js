import React, { Component } from 'react';
import { Icon, Input, View } from 'native-base';
import autobind from 'class-autobind';
import { Logger } from 'unstated-enhancers';
import { Platform } from 'react-native';

class SearchBarComponent extends Component {

  constructor(props) {
    super(props);
    autobind(this);
  }

  componentDidUpdate(prevProps) {
    const { value } = this.props;

    if (prevProps.value !== value) {
      this.handleRequestSearch(value);
    }
  }

  handleTextChange(value) {
    const { onChangeText } = this.props;

    if (onChangeText) {
      onChangeText(value);
    }
  }

  handleRequestSearch(value) {
    const { onSearchRequest, delay = 700, requiredChars = 3 } = this.props;

    if (this.debounce) {
      clearTimeout(this.debounce);

      this.debounce = null;
    }

    if (onSearchRequest && ((value && value.length >= requiredChars) || value==='')) {
      this.debounce = setTimeout(() => {
        onSearchRequest();

        this.debounce = null;
      }, delay);
    }
  }

  //TODO: anexar estilos a archivos de estilos globales, agregar placeholder al lang
  render() {
    const { placeholder, value, clear } = this.props;

    return (
      <View style={{ width: '100%', padding: 16, backgroundColor: '#f7f7f7' }}>
        <View style={{
          height: 37,
          backgroundColor: 'rgba(142, 142, 147, 0.12)',
          borderRadius: 10,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          paddingHorizontal: 10
        }}>
          
          <Input
            value={value}
            placeholder={placeholder}
            style={{ fontSize: 17 }}
            onChangeText={this.handleTextChange}
            clearButtonMode={'while-editing'}
          />
          { Platform.OS === 'android' && clear && value !== '' && value !== undefined && 
            <Icon name='close-circle' style={{ color: '#8e8e93', fontSize: 17, marginHorizontal:5 }} onPress={clear} />
          }
          <Icon style={{ color: '#8e8e93', fontSize: 17 }} name="ios-search" />
        </View>
      </View>
    )
  }
}

export default SearchBarComponent;
