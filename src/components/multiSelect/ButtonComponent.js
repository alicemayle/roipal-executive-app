import React, { Component } from 'react';
import { Text, Button } from 'native-base';
import autobind from 'class-autobind';
import { StyleSheet } from 'react-native'

class ButtonComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    autobind(this);
  }

  render() {
    const { title, backgroundColor, textColor, style, textStyle, onPress, disabled, defaultFont } = this.props;

    return (
      <Button
        activeOpacity={0.7}
        disabled={disabled}
        onPress={onPress}
        style={[styles.button, defaultFont, {
          backgroundColor: disabled ? 'gray' : backgroundColor,
          alignSelf: 'center',
        }, style]}>
        <Text style={[styles.buttonText, defaultFont, { color: textColor || '#fff' }, textStyle]}>
          {title}
        </Text>
      </Button>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    height: 45, paddingHorizontal: 16, flexDirection: 'row', alignItems: 'center', justifyContent: 'center',
  },
  buttonText: { fontSize: 16, fontWeight: 'bold' },
});


export default ButtonComponent;

