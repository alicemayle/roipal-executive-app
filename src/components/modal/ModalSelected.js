import React, { PureComponent } from 'react';
import { View, Text, Button, H3, Content, Icon, Left } from 'native-base';
import { Dimensions, Platform } from 'react-native';
import autobind from 'class-autobind';
import Modal from 'react-native-modal';

import styles from '../../theme/globalStyles';
import lang from '../../shared/languages/Lang';

const { width, height } = Dimensions.get('window');

class ModalSelected extends PureComponent {
  selected = null;

  constructor(props) {
    super(props)
    autobind(this);
  }

  closed() {}

  handleModalHide() {
    const { onModalHide } = this.props;
    if (onModalHide) {

      onModalHide(this.selected)
    }
    this.selected = null;
  }

  handleGallerySelectedOption() {
    this.selected = 'gallery';

    this.handleSelectedOption(this.selected);
  }

  handleVideoSelectedOption() {
    this.selected = 'video';

    this.handleSelectedOption(this.selected);
  }

  handleSelectedOption(option = null) {
    const { onSelectedOption } = this.props

    if (onSelectedOption) {
      onSelectedOption(option)
    }
  }

  handleCloseModal() {
    const { onHandleCloseModal } = this.props;

    if (onHandleCloseModal) {
      onHandleCloseModal();
    }
  }

  render() {
    const marginVertical05 = height * 0.05;

    const { modalVisible, onChange} = this.props;

    return (
      <Modal
        transparent={true}
        isVisible={modalVisible}
        presentationStyle={'overFullScreen'}
        onBackdropPress={this.handleCloseModal}
        useNativeDriver={ true }
        onModalHide={ this.handleModalHide }
        hideModalContentWhileAnimating={ true }
        style={[{ flex: 1, justifyContent: 'flex-end', alignItems: 'center', margin: 0 }]}
      >
          <View style={[styles.modal, { marginBottom: marginVertical05 }]}>
            <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>{lang.get('modal.title', 'modal.title')}</Text>
            <View style={[styles.modal2]}>
              <View>
                <Button style={[styles.buttonModal]} onPress={this.handleGallerySelectedOption}>
                  <Icon name='image' type='FontAwesome' />
                </Button>
                <Text>{lang.get('modal.gallery', 'modal.gallery')}</Text>
              </View>
              <View>
                <Button style={[styles.buttonModal]} onPress={this.handleSelectedOption}>
                  <Icon name='camera-retro' type='FontAwesome' />
                </Button>
                <Text>{lang.get('modal.camera', 'modal.camera')}</Text>
              </View>
            </View>
            <Button
              full
              success
              onPress={this.handleCloseModal}>
              <Text>{lang.get('modal.close', 'generic.button.close')}</Text>
            </Button>
          </View>
      </Modal>
    );
  }
}

export default ModalSelected;
