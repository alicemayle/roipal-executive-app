import React, { Component } from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { StyleSheet } from 'react-native';


export default class IntroGradient extends Component {
  render() {
    const { title = 'titleButton', onPress } = this.props;

    return (
      <LinearGradient
        colors={['#00a7e5', '#5bc1be']}
        start={{ x: 0, y: 0 }}
        end={{ x: 0, y: 1 }}
        style={styles.slide}
      >
        { this.props.children  }
      </LinearGradient>
    )
  }
}

const styles = StyleSheet.create({
  slide: {
    flex: 1, // Take up all screen
    justifyContent: "center", // Center vertically
    alignItems: "center", // Center horizontally
  }
});