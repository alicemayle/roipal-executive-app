import { Alert } from 'react-native';
import { NavigationActions } from 'react-navigation';
import NavigationService from '../shared/services/NavigationService';

(window || global).noop = () => {}

const routeNeedsEditingCheck = (action, state) => {
  return state
    && state.routes[state.index].params
    && action.type === NavigationActions.BACK
    && state.routes[state.index].params.isEditing
}

const getAlertConfig = (state) => {
  const config = state.routes[state.index].params.alert || {};

  return {
    title: config.title,
    message: config.message || 'All changes will be lost, continue anyway?'
  }
}

const showConfirmationAlert = (state) => {
  const alert = getAlertConfig(state);

  Alert.alert(
    alert.title,
    alert.message,
    [
      {
        text: 'Cancel',
        onPress: noop,
        style: 'cancel',
      },
      {
        text: 'OK', onPress: cancel = () => {
          state.routes[state.index].params.isEditing = false;

          NavigationService.goBack();
        }
      },
    ],
    {
      cancelable: false
    },
  );
}

const withGoBackConfirmationAction = (Navigator) => {
  const defaultGetStateForAction = Navigator.router.getStateForAction;

  Navigator.router.getStateForAction = (action, state) => {
    if (routeNeedsEditingCheck(action, state)) {
      // Returning null from getStateForAction means that the action
      // has been handled/blocked, but there is not a new state
      showConfirmationAlert(state)

      return null;
    } else {
      if (state
        && state.routes[state.index].params 
        && state.routes[state.index].params.isEditing === false 
        && action.type === NavigationActions.BACK) {
        action.key = null;
    }
    }

    return defaultGetStateForAction(action, state);
  };

  return Navigator;
}

export default withGoBackConfirmationAction;