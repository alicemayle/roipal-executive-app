import React, { Component } from 'react';
import Video from 'react-native-video';
import { StyleSheet, Text, View, TouchableWithoutFeedback, Platform, SafeAreaView } from "react-native";
import ProgressBar from "react-native-progress/Bar";
import autobind from 'class-autobind';
import { Icon } from 'native-base';

function secondsToTime(time) {
  return ~~(time / 60) + ":" + (time % 60 < 10 ? "0" : "") + time % 60;
}

class PreviewVideo extends Component {
  constructor(props) {
    super(props);
    autobind(this);
    this.state = {
      paused: false,
      progress: 0,
      duration: 0,
    };
  }

  handleMainButtonTouch() {
    if (this.state.progress >= 1) {
      this.player.seek(0);
    }

    this.setState(state => {
      return {
        paused: !state.paused,
      };
    });
  };

  handleProgressPress(e) {
    const position = e.nativeEvent.locationX;
    const progress = (position / 250) * this.state.duration;

    this.player.seek(progress);
  };

  handleProgress(progress) {
    this.setState({
      progress: progress.currentTime / this.state.duration,
    });
  };

  handleEnd() {
    this.setState({
      paused: true
    });
  };

  handleLoad(meta) {
    this.setState({
      duration: meta.duration,
    });
  };

  render() {
    const { path } = this.props;

    return (
      <SafeAreaView style={{ backgroundColor: '#394a4d', flex: 1 }}>
        <View style={[styles.container, { backgroundColor: Platform.OS === 'android' ? 'black' : 'white' }]}>
          <View>
            <Video
              paused={this.state.paused}
              source={{ uri: path }}
              style={styles.backgroundVideo}
              resizeMode="contain"
              onLoad={this.handleLoad}
              onProgress={this.handleProgress}
              onEnd={this.handleEnd}
              ref={ref => {
                this.player = ref;
              }}
            />
            <View style={styles.controls}>
              <TouchableWithoutFeedback onPress={this.handleMainButtonTouch}>
                <Icon name={!this.state.paused ? "pause" : "play"} style={styles.icon} />
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={this.handleProgressPress}>
                <View>
                  <ProgressBar
                    progress={this.state.progress}
                    color="#FFF"
                    unfilledColor="rgba(255,255,255,.5)"
                    borderColor="#FFF"
                    width={250}
                    height={20}
                  />
                </View>
              </TouchableWithoutFeedback>
              <Text style={styles.duration}>
                {secondsToTime(Math.floor(this.state.progress * this.state.duration))}
              </Text>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  controls: {
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    height: 48,
    left: 0,
    bottom: 0,
    right: 0,
    position: "absolute",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    paddingHorizontal: 10,
  },
  mainButton: {
    marginRight: 15,
  },
  backgroundVideo: {
    height: '100%',
    width: '100%',
  },
  duration: {
    color: "#FFF",
    marginLeft: 15,
  },
  icon: {
    color: 'white',
  },
});

export default PreviewVideo;
