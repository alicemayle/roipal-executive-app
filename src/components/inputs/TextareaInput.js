import React, { PureComponent } from 'react';
import { Textarea, View } from 'native-base';
import autobind from 'class-autobind';
import LabelError from './LabelError';
import {Logger} from 'unstated-enhancers';

type Props = {
  errors: Object,
  field: string,
  value?: string | number,
  onChangeText: (field: string, value: string) => void,
  onSubmitEditing?: () => void,
  onBlur?: () => void,
  onRefSet?: (alias: string, input: Object) => void
}

class TextareaInput extends PureComponent {
  props: Props;

  dirty = false;

  constructor(props: Object) {
    super(props)
    autobind(this);
  }

  handleOnSubmitEditing() {
    const { onSubmitEditing } = this.props;

    if (onSubmitEditing) {
      onSubmitEditing();
    }
  }

  handleChangeText(value: string) {
    const { onChangeText, field } = this.props;

    onChangeText(field, value);
  }

  handleOnBlur() {
    if (!this.dirty) {
      this.dirty = true;
    }

    const { onBlur } = this.props;

    if (onBlur) {
      onBlur();
    }
  }

  setRef(component: Object) {
    const { field, onRefSet } = this.props;

    if (onRefSet) {
      onRefSet(field, component);
    }
  }

  render() {
    const { errors, field } = this.props;
    const error = errors ? errors.first(field) : null;

    return (
      <View>
        <Textarea
          {...this.props}
          ref={this.setRef}
          onBlur={this.handleOnBlur}
          onChangeText={this.handleChangeText}
          onSubmitEditing={this.handleOnSubmitEditing}
        />
        { this.dirty && !!error && <LabelError message={error} />}
      </View>

    )
  }
}

export default TextareaInput;