// @flow
import React, { PureComponent } from 'react';
import {  Animated, Easing } from 'react-native';
import { Input, View, Item } from 'native-base';
import autobind from 'class-autobind';
import LabelError from './LabelError';

type Props = {
  errors: Object,
  field: string,
  next?: string,
  refs?: Object,
  value?: string|number,
  iconLeft?: React.Component<Object>,
  iconRight?: React.Component<Object>,
  onChangeText: (field: string, value: string) => void,
  onSubmitEditing?: () => void,
  onFocus?: () => void,
  onBlur?: () => void,
  onRefSet?: (alias: string, input: Object) => void
}

class TextInput extends PureComponent<Props> {
  props: Props
  shakeAnimationValue = new Animated.Value(0);
  dirty = false;
  shakable = true;
  ref = null;
  debounce = null;

  constructor(props: Object) {
    super(props)

    autobind(this);
  }

  componentWillReceiveProps(nextProps: Object) {
    if (this.debounce) {
      clearTimeout(this.debounce);
    }

    if(!this.dirty && !this.debounce) {
      if((this.props.value === undefined || this.props.value === null) && nextProps.value === '') {
          this.dirty = true
      }
    }

    if(this.dirty && this.shakable) {
      const { errors, field } = nextProps;
      const error = errors ? errors.first(field) : null;

      if (error) {
        this.debounce = setTimeout(() => {
          this.shake();

          this.debounce = null;
          this.shakable = false;
        }, 250);
      }
    }
  }

  handleOnSubmitEditing() {
    const { next, onSubmitEditing, refs = {} } = this.props;

    if (next) {
      refs[next] && refs[next]._root.focus();
    }

    if (onSubmitEditing) {
      onSubmitEditing();
    }
  }

  handleOnFocus() {
    this.dirty = false;
    this.shakable = false;

    const { onFocus } = this.props;

    if (onFocus) {
      onFocus();
    }
  }

  handleOnBlur() {
    if (!this.dirty) {
      this.dirty = true;
      this.shakable = true;
    }

    const { onBlur } = this.props;

    if (onBlur) {
      onBlur();
    }
  }

  changeText(value: string) {
    const { onChangeText, field } = this.props;

    onChangeText(field, value);
  }

  setRef(component: Object) {
    const { field, onRefSet } = this.props;

    if (onRefSet) {
      onRefSet(field, component);

      this.ref = component;
    }
  }

  shake() {
    const { shakeAnimationValue } = this;

    shakeAnimationValue.setValue(0);

    Animated.timing(shakeAnimationValue, {
      duration: 375,
      toValue: 3,
      ease: Easing.bounce,
    }).start();
  }

  render() {
    const { errors, field, iconRight, iconLeft } = this.props;
    const error = errors ? errors.first(field) : null;

    const translateX = this.shakeAnimationValue.interpolate({
      inputRange: [0, 0.5, 1, 1.5, 2, 2.5, 3],
      outputRange: [0, -15, 0, 15, 0, -15, 0],
    });

    return (
      <View>
        <Animated.View
          style={
            { transform: [{ translateX }] }
          }
        >
          <Item
            error={ this.dirty && !!error }
            icon={ !!iconLeft || !!iconRight }
            style={{ backgroundColor:'transparent' }}
          >
            { !!iconLeft && iconLeft }

            <Input
              { ...this.props }
              ref={ this.setRef }
              onChangeText={ this.changeText }
              onFocus={ this.handleOnFocus }
              onBlur={ this.handleOnBlur }
              onSubmitEditing={ this.handleOnSubmitEditing }
            />

            { !!iconRight && iconRight }
          </Item>
        </Animated.View>

        { this.dirty && !!error && <LabelError message={ error } /> }

      </View>
    )
  }
}


export default TextInput;