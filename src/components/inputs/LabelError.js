// @flow
import React, { PureComponent } from 'react'
import { Text, View, StyleSheet } from 'react-native'

type Props = {
  message: string
}

const styles = StyleSheet.create({
  error: {
    color: 'red'
  }
})

export class LabelError extends PureComponent<Props> {
  props: Props

  render() {
    const { message } = this.props;

    return (
      <Text style={ styles.error }> { message } </Text>
    )
  }
}

export default LabelError