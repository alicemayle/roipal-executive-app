import color from "color";

import { Platform, Dimensions, PixelRatio } from "react-native";
import defaultStyles from './platform'

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const platform = Platform.OS;
const platformStyle = "material";

const isIphoneX =
  platform === "ios" && deviceHeight === 812 && deviceWidth === 375;

export default {

}