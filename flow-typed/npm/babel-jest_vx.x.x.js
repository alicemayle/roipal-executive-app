// flow-typed signature: 352243812bf9ac7b8f095d42392c5541
// flow-typed version: <<STUB>>/babel-jest_v24.7.0/flow_v0.96.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'babel-jest'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'babel-jest' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'babel-jest/build/index' {
  declare module.exports: any;
}

// Filename aliases
declare module 'babel-jest/build/index.js' {
  declare module.exports: $Exports<'babel-jest/build/index'>;
}
