// flow-typed signature: 05411a9f96d3206ed1047ea741337536
// flow-typed version: <<STUB>>/credit-card-type_v^8.2.0/flow_v0.96.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'credit-card-type'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the
 * community by sending a pull request to:
 * https://github.com/flowtype/flow-typed
 */

declare module 'credit-card-type' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */
declare module 'credit-card-type/dist/js/app.built' {
  declare module.exports: any;
}

declare module 'credit-card-type/dist/js/app.built.min' {
  declare module.exports: any;
}

declare module 'credit-card-type/lib/add-matching-cards-to-results' {
  declare module.exports: any;
}

declare module 'credit-card-type/lib/card-types' {
  declare module.exports: any;
}

declare module 'credit-card-type/lib/clone' {
  declare module.exports: any;
}

declare module 'credit-card-type/lib/find-best-match' {
  declare module.exports: any;
}

declare module 'credit-card-type/lib/is-valid-input-type' {
  declare module.exports: any;
}

declare module 'credit-card-type/lib/matches' {
  declare module.exports: any;
}

// Filename aliases
declare module 'credit-card-type/dist/js/app.built.js' {
  declare module.exports: $Exports<'credit-card-type/dist/js/app.built'>;
}
declare module 'credit-card-type/dist/js/app.built.min.js' {
  declare module.exports: $Exports<'credit-card-type/dist/js/app.built.min'>;
}
declare module 'credit-card-type/index' {
  declare module.exports: $Exports<'credit-card-type'>;
}
declare module 'credit-card-type/index.js' {
  declare module.exports: $Exports<'credit-card-type'>;
}
declare module 'credit-card-type/lib/add-matching-cards-to-results.js' {
  declare module.exports: $Exports<'credit-card-type/lib/add-matching-cards-to-results'>;
}
declare module 'credit-card-type/lib/card-types.js' {
  declare module.exports: $Exports<'credit-card-type/lib/card-types'>;
}
declare module 'credit-card-type/lib/clone.js' {
  declare module.exports: $Exports<'credit-card-type/lib/clone'>;
}
declare module 'credit-card-type/lib/find-best-match.js' {
  declare module.exports: $Exports<'credit-card-type/lib/find-best-match'>;
}
declare module 'credit-card-type/lib/is-valid-input-type.js' {
  declare module.exports: $Exports<'credit-card-type/lib/is-valid-input-type'>;
}
declare module 'credit-card-type/lib/matches.js' {
  declare module.exports: $Exports<'credit-card-type/lib/matches'>;
}
